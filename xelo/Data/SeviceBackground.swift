//
//  SeviceBackground.swift
//  NaviGo
//
//  Created by HoangManh on 9/28/17.
//  Copyright © 2017 DUMV. All rights reserved.
//

import Foundation
import UIKit
public class SeviceBackground{
    
    var backgroundUpdatetask:UIBackgroundTaskIdentifier!=nil
    
    func doBackgroundTask()  {
        let queue = DispatchQueue.global(qos: .background)
        queue.async {
            while true{
                if(DataNavigo.booking.bookingId != nil && DataNavigo.booking.bookingId != ""){
                    DataNavigo.handingScreen?.startRequest()
                }else{
                    
                }                
                DispatchQueue.main.async {
                    DataNavigo.handingScreen?.errorConnect()
                }
                sleep(5)
            }
        }
    }
    
    func beginBackgroundDownLoad(){
        backgroundUpdatetask=UIApplication.shared.beginBackgroundTask(withName: "runbackground"){
            self.endBackgoundDownload()
        }
    }
    
    func endBackgoundDownload() {
        UIApplication.shared.endBackgroundTask(backgroundUpdatetask)
        backgroundUpdatetask=UIBackgroundTaskInvalid
        
    }
}

