//
//  Language.swift
//  xelo
//
//  Created by Nguyen Thieu on 7/2/18.
//  Copyright © 2018 DUMV. All rights reserved.
//

import UIKit


class Language: NSObject {
    
    static var bundle: Bundle? = nil
    
//     @objc override class func initialize() {
//        let current = self.getCurrentLanguage()
//        self.setLanguage(current)
//    }
    
    class func setLanguage(_ language: String) {
        UserDefaults.standard.set(language, forKey: "curren_language")
    }
    
    class func getCurrentLanguage() -> String {
        let defs = UserDefaults.standard
        var lang = defs.object(forKey: "curren_language") as! String?
        if lang?.characters.count == 0 {
            return lang!
        }
        else {
            return "VN"
        }
    }
    
    class func get(_ key: String) -> String {
        var result = bundle!.localizedString(forKey: key, value: nil, table: nil)
        if result == "" {
            result = key
        }
        return result
    }
    
    class func appDocumentDirectory() -> String {
        return NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
    }
    
    class func getLanguageCode(_ language: String) -> String {
        switch language {
        case "English":
            return "EN"
        default:
            return "VN"
        }
    }
    
}

