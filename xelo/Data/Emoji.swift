//
//  Emoji.swift
//  xelo
//
//  Created by Nguyen Thieu on 5/5/19.
//  Copyright © 2019 DUMV. All rights reserved.
//

import Foundation

internal let emoji: [String:Int] = [
    ":taxi:"                : 0x1f695,
    "::):"                  : 0x1f603,
    "::D:"                  : 0x1f600,
    ":gift:"                : 0x1f381,
    ":crown:"               : 0x1f451,
    "::(:"                  : 0x1f61e,
    ":rose:"                : 0x1f339,
    ":christmas_tree:"      : 0x1f384,
    ":sob:"                 : 0x1f62d,
    ":clap:"                : 0x1f44f,
    ":four_leaf_clover:"    : 0x1f340,
    ":confounded:"          : 0x1f616,
    ":bell:"                : 0x1f514,
    ":birthday:"            : 0x1f382,
    
    ":medal:"               : 0x1f3c5,
    ":flag:"                : 0x1f1fb,
    ":handshake:"           : 0x1f91d,
    ":raised_hands:"        : 0x1f64c,
    ":pray:"                : 0x1f64f,
    ":ear:"                 : 0x1f442,
    ":rain:"                : 0x26c8,
    ":sunny:"               : 0x1f324,
    ":sun_with_face:"       : 0x1f31e,
    ":beers:"               : 0x1f37b,
    ":grimacing:"           : 0x1f62c,
    ":rage:"                : 0x1f621,
    ":heart_eyes:"          : 0x1f60d,
]
