//
//  Constant.swift
//  testNavigo
//
//  Created by Linh  on 9/26/17.
//  Copyright © 2017 DUMV. All rights reserved.
//

import UIKit

public class BookingState{
    public static let STATE_NONE = 99
    public static let STATE_NEW = 0; /// ?
    public static let STATE_ACCEPT_BY_DRIVER = 1; // da dc nhan boi tai xe
    public static let STATE_PICKED_UP = 2; // da don dc khach
    public static let STATE_FINISH_TRIP = 4; // chuyen di da hoan thanh
    public static let STATE_CANCEL_BY_PASSENGER = 8; // bi huy boi khach hang aka user
    public static let STATE_CANCEL_BY_DRIVER = 9; // da bi huy boi tai xe
}

public class BookingTypes{
    public static let BOOK_FROM_PASSENGER = 0; /// ?
    public static let BOOK_FROM_DRIVER = 1; // da dc nhan boi tai xe
    public static let BOOK_FROM_CENTER = 2; // da don dc khach
}

public class Errorcode{
    public static let ERROR_NO_INTERNET = "NOT";
    public static let ERROR_KXD = "KXD";
    public static let ERROR_MESSAGE = "errorMessage";
    public static let NAVIGO_ACCOUNT_DUPLICATE = "NVG-001"; //trùng tài khoản
    public static let NAVIGO_ACCOUNT_NOT_FOUND = "NVG-002"; //account not found
    public static let NAVIGO_INVALID_ACTIVE_KEY = "NVG-003"; //ma active khong dung
    public static let NAVIGO_ACCOUNT_ACTIVATED = "NVG-004"; //account da active roi
    public static let NAVIGO_ACCOUNT_BLOCKED = "NVG-005"; //account da active roi
    public static let NAVIGO_ACCOUNT_INVALID = "NVG-006"; //account invalid, must be re-active
    public static let NAVIGO_INVALID_EMAIL = "NVG-007"; //invalid email address
    public static let NAVIGO_SKYMAP_ID_EXISTS = "NVG-008"; //skymap id da ton tai
    public static let NAVIGO_SESSION_EXPIRED = "NVG-009"; //phien lam viec da het han
    public static let NAVIGO_SKYMAP_ID_NOT_FOUND = "NVG-010"; //skymap id not found in system
    public static let NAVIGO_INPUT_REQUIRED = "NVG-011"; //bat buoc phai nhap lieu
    
    public static let NAVIGO_CHANGE_PHOTO_DENIED = "NVG-012"; //Khong cho phep thay anh khi da phe duyet tai khoan
    public static let NAVIGO_INVALID_PROMOTION_CODE = "NVG-013"; //Sai ma khuyen mai -> hoi khach hang xem co book ma khong su dung khuyen mai
    public static let NAVIGO_ACCOUNT_NOT_ACTIVATED = "NVG-014"; //account not activated
    public static let NAVIGO_BOOKING_FAILED = "NVG-015"; //dat cho khong thanh cong
    public static let NAVIGO_NO_CAR_AVAILABLE = "NVG-016"; //khong co xe nao trong vung
    public static let NAVIGO_CANCEL_BY_DRIVER = "NVG-017"; //huy book boi lai xe
    public static let NAVIGO_CANCEL_BY_PASSENGER = "NVG-018"; //huy book boi hanh khach
    public static let NAVIGO_DUPLICATE_BOOK = "NVG-019"; //da ton tai book truoc, can phai show thong tin book do len
    public static let NAVIGO_ACCEPT_BY_OTHER = "NVG-020"; //lenh da duoc nguoi khac xac nhan
    public static let NAVIGO_FINISH_ALREADY = "NVG-021"; //da ket thuc chuyen
    public static let NAVIGO_FINISH_FAILED = "NVG-022"; //da ket thuc chuyen
    public static let NAVIGO_NO_SERVICE_FOUND = "NVG-023"; //da ket thuc chuyen
    public static let NAVIGO_NO_PROMOTION_FOUND = "NVG-024"; //khong dung ma khuyen mai
    public static let NAVIGO_REFER_TO_ANOTHER_SERVICE = "NVG-042"; //khong co xe nao trong vung
    
}
public class URL_APP{
    #if DEBUG
    //public static let URL_ROOT:String = "http://sys.xelo.vn/"
    public static let URL_ROOT:String = "https://dev.xelo.vn/" //sys.xelo.vn
    #else
    public static let URL_ROOT:String = "https://sys.xelo.vn/" //
    #endif
    public static let URL_CHECK_PRICE_HTX = URL_ROOT + "rest/app/passenger/htx/checkPrice";
    public static let URL_HTX_BOOKING_NOW = URL_ROOT + "rest/app/passenger/htx/bookingNow";
    public static let URL_HTX_CHECK_BOOKING = URL_ROOT + "rest/app/passenger/htx/checkBooking";
    public static let URL_HTX_GET_LIVE_BOOKINGS = URL_ROOT + "rest/app/passenger/htx/getLiveBookings";
    public static let URL_BOOKING_NOW = URL_ROOT + "rest/app/passenger/bookingNow";
    public static let URL_LOGIN = URL_ROOT + "rest/app/passenger/login";
    public static let URL_CHAT_BY_CODE = URL_ROOT + "rest/app/passenger/chatByCode";
    public static let URL_INPUT_FOR_BONUS = URL_ROOT + "rest/app/passenger/inputForBonus";
    public static let URL_CHECK_PROMOTION_CODE = URL_ROOT + "rest/app/passenger/checkPromotionCode";
    public static let URL_CREATE_PASSENGER = URL_ROOT + "rest/app/passenger/createPassenger";
    public static let URL_GET_DRIVER = URL_ROOT + "rest/app/passenger/getDriver";
    public static let URL_REACTIVE_PASSENGER = URL_ROOT + "rest/app/passenger/reactivePassenger";
    public static let URL_ACTIVE_PASSENGER = URL_ROOT + "rest/app/passenger/activatePassenger";
    public static let URL_CHECK_BOOKING = URL_ROOT + "rest/app/passenger/checkBooking";
    public static let URL_CANCEL_BOOKING = URL_ROOT + "rest/app/passenger/cancelBooking";
    public static let URL_BOOKING_BUS_TICKET = URL_ROOT + "rest/app/passenger/bookingBusTicket";
    public static let URL_STAR_MARK = URL_ROOT + "rest/app/passenger/starMark";
    public static let URL_LIST_VEHICLES = URL_ROOT + "rest/app/passenger/listVehicles";
    public static let URL_LIST_PUBLIC_VEHICLES = URL_ROOT + "rest/app/passenger/listPublicVehicles";
    public static let URL_LIST_SERVICES = URL_ROOT + "rest/app/passenger/listServices";
    public static let URL_PING = URL_ROOT + "rest/app/passenger/ping";
    public static let URL_GET_PASSENGER = URL_ROOT + "rest/app/passenger/getPassenger";
    public static let URL_UPDATE_PASSENGER = URL_ROOT + "rest/app/passenger/updatePassenger";
    public static let URL_LIST_TRIPS = URL_ROOT + "rest/app/passenger/listTrips";
    public static let URL_CREATE_ADDRESS = URL_ROOT + "rest/app/passenger/createAddress";
    public static let URL_MODIFY_ADDRESS = URL_ROOT + "rest/app/passenger/modifyAddress";
    public static let URL_DELETE_ADDRESS = URL_ROOT + "rest/app/passenger/deleteAddress";
    public static let URL_GET_HISTORIES = URL_ROOT + "rest/app/passenger/getHistories";
    public static let URL_LIST_MESSAGE = URL_ROOT + "rest/app/passenger/listMessage";
    public static let URL_CHECK_PRICE = URL_ROOT + "rest/app/passenger/checkPrice";
    public static let URL_ROUTING = URL_ROOT + "rest/app/passenger/routing";
    public static let URL_SEARCH_BUS_TRIPS = URL_ROOT + "rest/app/passenger/searchBusTrips";
    public static let URL_GET_BUS_TRIP = URL_ROOT + "rest/app/passenger/getBusTrip";
    public static let URL_LIST_HISTORY_TICKETS = URL_ROOT + "rest/app/passenger/listHistoryTickets";
    public static let URL_GET_NOTIFICATIION = URL_ROOT + "rest/app/passenger/getNotification";
    public static let URL_LIST_NOTIFICATIION = URL_ROOT + "rest/app/passenger/listNotication";
    public static let URL_MARK_READ_NOTIFICATIION = URL_ROOT + "rest/app/passenger/markReadNotification";
    public static let URL_LIST_ADDRESES = URL_ROOT + "rest/app/passenger/listAddresses";
    public static let URL_SEARCH_PLACES = URL_ROOT + "rest/app/passenger/searchPlaces";
    public static let URL_GET_PLACE_INFO = URL_ROOT + "rest/app/passenger/getPlaceInfo";
    public static let URL_GET_ADDRESS = URL_ROOT + "rest/app/passenger/getAddress";
    public static let URL_LIST_PROMO_TRANS = URL_ROOT + "rest/app/passenger/listPromoTrans";
    public static let URL_STORE_AVATAR = URL_ROOT + "rest/app/passenger/storeAvatar";
    public static let URL_STORE_CONTACTS = URL_ROOT + "rest/app/passenger/storeContacts";
    public static let URL_GET_LUCKY_GAME = URL_ROOT + "rest/app/passenger/getLuckyGame";
    public static let URL_INPUT_GAME_ANSWER = URL_ROOT + "rest/app/passenger/inputGameAnswer";
    public static let URL_LIST_LUCKY_GAMES = URL_ROOT + "rest/app/passenger/listLuckyGames";
    public static let URL_LUCKY_GAME_ANSWER = URL_ROOT + "rest/app/passenger/luckyGameAnswer";
}
public class Constant{
    public static let FINE_NAME="navigosksy"
    //app.navigo.vn
    #if DEBUG
    //public static let URL_ROOT:String = "http://sys.xelo.vn/"
    public static let URL_ROOT:String = "https://dev.xelo.vn/" //sys.xelo.vn
    #else
    public static let URL_ROOT:String = "https://sys.xelo.vn/" //
    #endif
    //public static let URL_ROOT:String = "http://192.168.1.149:8080/navigo/"
    public static let URL_CHAT:String = URL_ROOT+"chat/"
    public static let URL_ACCOUNT:String = URL_ROOT+"JsonPassenger"
    public static let URL_ADDRESS:String=URL_ROOT+"JsonAddress"
    public static let URL_CONTACT:String = URL_ROOT+"contact.html"
    public static let URL_PARTNER:String=URL_ROOT+"become_driver.html"
    public static let URL_GAME_XELO:String=URL_ROOT+"game.html"
    public static let URL_PROMOTION:String=URL_ROOT+"promotion.html"
    public static let URL_AGREEMENT:String=URL_ROOT+"driver_agreement.html"
    public static let URL_SHARE_AND_BONUS:String=URL_ROOT+"passenger_bonus.html"
    public static let URL_PRIVACY:String=URL_ROOT+"privacy.html"
    public static let URL_EXTRA_CHARGE:String=URL_ROOT+"extra_charge.html"
    public static var driverArrived:String = "Lái xe đã tới"
    public static var driverInWay:String = "Lái xe đang đến"
    public static var appName:String = ""
    public static var inWay:String = "Đang trong hành trình"
    public static var PLATFORM_IOS:String = "iOS"
    //public static var driverArrived:String = "Lái xe đã tới"
    //public static var driverInWay:String = "Lái xe đang đến"
    public static var company:String = "Sản phẩm phát triển bởi\n Công ty cổ phần công nghệ trực tuyến Skysoft"
    public static let deviceModelXSM = 2688
    public static let deviceModelXR = 1792
    public static let deviceModelX = 2436
    public static let deviceModelPlus = 2208
    public static let deviceModelNormal = 1334
    public static let deviceModelSmall = 1136
    public static let cancelByDriver:String = "Rất tiếc lái xe vừa huỷ chuyến. Bạn có muốn đặt lại chuyến khác không?"
}
public class keyJson{
    public static let auThenKey:String="authenKey"
    public static let time:String="time"
    
    public static let action_book="action=bookingNow"
    public static let checkPrice="action=checkPrice"
    
    public static let actionResult:String="actionResult"
    public static let action:String="action"
    public static let services:String="services"
    public static let errorCode:String="errorCode"
    public static let F_GET_ADDRESS = "getAddress"
    public static let F_KEY = "key";
    public static let F_LANGUAGE = "language";
    public static let F_RECONNECT = "reconnect";
    public static let F_TOKEN_FIREBASE = "fireBaseToken";
    public static let F_ID = "_id";
    public static let F_LIST_ADDRESS = "listAddresses"
    public static let F_SEARCH_PLACES = "searchPlaces"
    public static let F_INPUT = "input"
    public static let F_ADDRESS = "addresses"
    public static let F_ADDRESSID = "addressID"
    public static let F_NAME_ADDRESS = "fullName"
    public static let F_ADDRESS_ADDRESS = "fullAddress"
    public static let F_CREATE_ADDRESS = "createAddress"
    public static let F_MODIFY_ADDRESS = "modifyAddress"
    public static let F_DELETES_ADDRESS = "deleteAddress"
    public static let F_OK = "OK"
    public static let F_POINTS = "points"
    public static let F_ROUTING = "routing"
    public static let F_X = "x";
    public static let F_Y = "y";
    public static let F_CURRENT_X = "currentX";
    public static let F_CURRENT_Y = "currentY";
    public static let F_ACTIVE_ACCOUNT = "activatePassenger";
    public static let F_REACTIVE_ACCOUNT = "reactivePassenger";
    public static let F_CREATE_ACCOUNT = "createPassenger";
    public static let F_UPDATE_ACCOUNT = "updatePassenger";
    public static let F_GET_PASSENGER = "getPassenger";
    public static let F_CHECK_BOOKING = "checkBooking";
    public static let F_ACCOUNT_ID = "accountID";
    public static let F_PHOTO_TYPE = "photoType";
    public static let F_PHOTO_DATA = "photoData";
    public static let F_STORE_AVATAR = "storeAvatar";
    public static let F_DRIVER_ID = "driverID";
    public static let F_CUSTOMER_ID = "customerID";
    public static let F_BOOK_TYPE = "bookType";
    public static let F_PASSENGER_ID = "passengerID";
    public static let F_EMAIL = "email";
    public static let F_REFER_MOBILE = "referMobileNo";
    public static let F_TIME = "time";
    public static let F_AUTHEN_KEY = "authenKey";
    public static let F_FULL_NAME = "fullName";
    public static let F_COMPANY_NAME = "companyName";
    public static let F_MOBILE_NO = "mobileNo";
    public static let F_CREATE_DATE = "createDate";
    public static let F_REVIEW_DATE = "reviewDate";
    public static let F_LAST_LOGIN_DATE = "lastLoginDate";
    public static let F_SIGNUP_TYPE = "signupType";
    public static let F_PASSWORD = "password";
    public static let F_ACTIVES_KEY = "activeKey";
    public static let F_ACTIVE_KEY1 = "activeKey";
    public static let F_SECURE_KEY = "secureKey";
    public static let F_DEVICES_SERIAL = "deviceSerial";
    public static let F_ID_CARD_NO = "idCardNo";
    public static let F_LICENSE_NO = "licenseNo"; //so hieu bang lai
    public static let F_LICENSE_TYPE = "licenseType"; //loai bang
    public static let F_DATE_OF_BIRTH = "dateOfBirth"; //ngay thang nam sinh
    public static let F_PLATE_NUMBER = "plateNumber"; //bien so xe
    public static let F_STATUS = "status";
    public static let F_POSITION = "position";
    public static let F_STATE = "state";
    public static let F_STATES = "states";
    public static let F_START_DATE = "startDate";
    public static let F_SPEED = "speed";
    public static let F_DIRECTION = "direction";
    public static let F_VALID = "valid";
    public static let F_DISTANCE = "distance";
    public static let F_SERVICE_KM = "serviceKm";
    public static let F_SERVICE_CHARGE = "serviceCharge";
    public static let F_GPS_TIME = "gpsTime";
    public static let F_GPS_FIXED = "gpsFixed";
    
    public static let F_SERVICE_CODE = "serviceCode";
    public static let F_SERVICE_NAME = "serviceName";
    public static let F_PRICE = "price";
    public static let F_PRICE_FIRST = "priceFirst";
    public static let F_FROM_PRICE = "fromPrice";
    public static let F_TO_PRICE = "toPrice";
    public static let F_MAP_ICON = "mapIcon";
    public static let F_SERVICE_ICON = "serviceIcon";
    public static let F_REFER_SERVICE_CODE = "referServiceCode";
    public static let F_FROM_NORMAL_PRICE = "fromNormalPrice";
    public static let F_TO_NORMAL_PRICE = "toNormalPrice";
    
    public static let F_WAIT_DURATION = "waitDuration";
    public static let F_TWO_WAY = "twoWays";
    public static let F_RETURN_RATE = "returnRate";
    public static let F_WAIT_FEE = "waitFee";
    
    public static let F_BOOKING_ID = "bookingID";
    public static let F_CANCEL_BOOKING = "cancelBooking"
    public static let F_PASSENGER_CONTACT_NO = "passengerContactNo";
    public static let F_PASSENGER_NAME = "passengerName";
    public static let F_DRIVER_CONTACT_NO = "driverContactNo";
    public static let F_DRIVER_NAME = "driverName";
    public static let F_WARNING_MESSAGE = "warningMessage";
    
    public static let F_FROM_X = "fromX";
    public static let F_FROM_Y = "fromY";
    
    public static let F_TO_X = "toX";
    public static let F_TO_Y = "toY";
    
    public static let F_DATE = "date"
    
    public static let F_FROM_ADDRESS = "fromAddress";
    public static let F_TO_ADDRESS = "toAddress";
    
    public static let F_EXTRA_CHARGE = "extraCharge";
    public static let F_CHARGE = "charge";
    public static let F_FROM_CHARGE = "fromCharge";
    public static let F_TO_CHARGE = "toCharge";
    public static let F_FIX_CHARGE = "fixCharge";
    public static let F_ACT_DURATION = "actDuration";
    public static let F_TARIFF_PRICE = "tariffPrice";
    public static let F_READY = "ready";
    public static let F_PEAK_HOUR = "peakHour";
    public static let F_PEAK_RATE = "peakRate";
    public static let F_ROUTING_ID = "routingID";
    
    public static let F_BOOK_DATE = "bookDate";
    public static let F_PICKUP_DATE = "pickupDate";
    public static let F_FINISH_DATE = "finishDate";
    
    public static let F_PROMOTION_VALUE = "promotionValue";
    public static let F_PROMOTION_TYPE = "promotionType";
    public static let F_PROMOTION_CODE = "promotionCode";
    
    public static let F_BLOCK_ROAD = "blockRoad";
    public static let F_NOTE = "note";
    public static let F_SESSION_ID = "sessionID";
    
    public static let F_ECO_CHOICE = "ecoChoice";
    public static let F_PERSONS = "persons";
    public static let F_ECO_PRICE = "ecoPrice";
    public static let F_EXTRA_FEE = "extraFee";
    public static let F_XELO_FEE = "xeloFee";
    public static let F_FEE_RATE = "feeRate";
    public static let F_PICKUP_DISTANCE = "pickupDistance";
    public static let F_WAIT_CHARGE = "waitCharge";
    public static let F_XELO_BONUS = "xeloBonus";
    
    //for OS platform and version
    public static let F_APP_OS = "appOs";
    public static let F_APP_NAME = "appName";
    public static let F_APP_VERSION = "appVersion";
    //for OS platform and version
    public static let F_OS_VERSION = "osVersion";
    public static let F_DEVICE_NAME = "deviceName";
    public static let F_SHOW_SHARE_AND_BONUS = "showShareAndBonus";
    public static let F_RELEASE_NOTE = "releaseNote";
    
    //for photos
    public static let F_AVATAR_FACE = "avatarFace";
    public static let F_AVATAR_FULL = "avatarFull";
    public static let F_ID_CARD_FRONT = "idCardFront";
    public static let F_ID_CARD_BACK = "idCardBack";
    public static let F_LICENSE_FRONT = "licenseFront";
    public static let F_LICENSE_BACK = "licenseBack";
    public static let F_VEHICLE_FRONT = "vehicleFront";
    public static let F_VEHICLE_BACK = "vehicleBack";
    public static let F_REGISTER_FRONT = "registerFront";
    public static let F_REGISTER_BACK = "registerBack";
    
    //for statistic
    public static let F_MONTH = "month";
    public static let F_DAY = "day_";
    public static let F_TOTAL_AMOUNT = "totalAmount";
    public static let F_COUNT = "count";
    public static let F_STAR_MARK = "starMark";
    
    //for param
    public static let F_PARAM_NAME = "paramName";
    public static let F_PARAM_VALUE = "paramValue";
    
    //for administrator
    public static let F_USER_NAME = "userName";
    
    //for cancelReasons
    public static let F_CANCEL_REASONS = "cancelReasons";
    public static let F_CANCEL_NOTE = "cancelNote";
    public static let F_CANCEL_CODE = "cancelCode";
    //for SMS
    public static let F_LIST_MESSAGE = "listMessage";
    public static let F_CONTENT = "content";
    public static let F_SMS_TYPE = "smsType";
    public static let F_SMS_ID = "smsID";
    public static let F_FROM_DRIVER = "fromDriver";
    
    //for Chat
    public static let F_CHAT_TEMPLATE = "chatTemplates";
    public static let F_CHAT_BY_CODE = "chatByCode";
    public static let F_CHAT_CODE = "chatCode";
    public static let F_CHAT_DESCRIPTION = "description";
    
    //for object
    public static let OBJ_BOOKING = "booking";
    public static let OBJ_DRIVER = "driver";
    public static let OBJ_POSITION = "position";
    public static let OBJ_SUMMARY = "summary";
    public static let OBJ_PASSENGER = "passenger";
    
    //for array
    public static let ARRAY_SERVICES = "services";
    public static let ARRAY_VEHICLES = "vehicles";
    public static let ARRAY_BOOKINGS = "bookings";
    
    public static let F_GET_NOTI = "getNotification";
    public static let F_READ_NOTI = "markReadNotification";
    public static let F_NOTI_ID = "notifyID";
    public static let F_NOTI_LIST = "listNotification";
    public static let F_READ = "read";
    public static let F_NOTI_TYPE = "notifyType";
    public static let F_URL = "url";
    public static let F_TITLE = "title";
    
    public static let F_AUTHEN_BY_FIREBASE = "authenByFirebase";
    
    public static let F_BLOCK_ROADS = "blockRoads";
    public static let F_WAY_POINT = "wayPoints";
    
    //GooglePlace
    public static let F_PLACE_INFO = "getPlaceInfo"
    public static let F_PLACES = "places"
    public static let F_PLACE_ID = "placeID";
    public static let F_MAIN_TEXT = "mainText";
    public static let F_DESCRIPTION = "description";
    public static let F_SECONDARY_TEXT = "secondaryText";
    
    //update 14/11/2018
    public static let F_GET_USAGE_FEES = "listUsageFees";
    public static let F_LIST_FEE_TRANS = "listFeeTrans";
    public static let F_RECHARGE_FROM_WALLET = "rechargeFromWallet";
    public static let F_AMOUNT = "amount";
    public static let F_TRANS_TYPE = "transType";
    public static let F_TRANS_DATE = "transDate";
    public static let F_ACCOUNT_TYPE = "accountType";
    public static let F_LIST_PROMO_TRANS = "listPromoTrans";
    
    public static let F_PROMO_WALLET = "promoWallet";
    public static let F_PROMO_EXPIRE_DATE = "promoExpireDate";
    public static let F_DEPOSIT_WALLET = "depositWallet";
    
    //update 22/03/2019 Bustrip----
    
    public static let F_BUS_TRIPS = "busTrips";
    public static let F_BUS_TRIP = "busTrip";
    public static let F_BUS_TICKET = "busTicket";
    public static let F_TICKETS = "tickets";
    
    public static let F_PICKUP_X = "pickupX";
    public static let F_PICKUP_Y = "pickupY";
    
    public static let F_DROP_OFF_X = "dropOffX";
    public static let F_DROP_OFF_Y = "dropOffY";
    
    public static let F_PICKUP_ADDRESS = "pickupAddress";
    public static let F_DROP_OFF_ADDRESS = "dropOffAddress";
    
    public static let F_BUS_OPERATOR = "busOperator";
    public static let F_DEPARTURE_DATE = "departureDate";
    public static let F_NUMBER_OF_BOOKED = "numOfBooked";
    public static let F_MODEL_NAME = "modelName";
    public static let F_TICKET_FARE = "ticketFare";
    public static let F_TRIP_ID = "tripID";
    public static let F_CAPACITY = "capacity";
    public static let F_GEOM = "geom";
    
    public static let F_TICKET_FARES = "ticketFares";
    public static let F_TICKET_ID = "ticketID";
    public static let F_VEHICLE_ID = "vehicleID";
    
    public static let F_QUANTITY_1 = "quantity1";
    public static let F_QUANTITY_2 = "quantity2";
    public static let F_QUANTITY_3 = "quantity3";
    
    public static let F_TICKET_FARE_1 = "ticketFare1";
    public static let F_TICKET_FARE_2 = "ticketFare2";
    public static let F_TICKET_FARE_3 = "ticketFare3";
    
    public static let F_HOTLINE = "hotLine";
    public static let F_CONTACT_NAME = "contactName";
    
    public static let F_PAID = "paid";
    public static let F_STATE_NOTE = "stateNote";
    
    //update 2019/04/05 ----yyyy/MM/dd --- TÌM XE 24H
    public static let F_TRAVEL_LINE = "travelLine";
    public static let F_UPDATE_DATE = "updateDate";
    
    //update 13/06/2019
    public static let F_HOME_ADS = "homeAds";
    public static let F_IMAGE_URL = "imageUrl";
    public static let F_LINK_URL = "linkUrl";
    public static let F_TOKEN_ID = "tokenID";
    //update 24/06/2019
    public static let F_APP_ITEMS = "appItems";
    public static let F_APP_CODE = "appCode";
    
    public static let F_LIST_SERVICES = "listServices";
    public static let F_TYPE = "type";
    
    //05/07/2019
    public static let F_GAME = "game";
    public static let F_AVAILABLE = "available";
    public static let F_GAME_ID = "gameID";
    public static let F_LOTTERY_DATE  = "lotteryDate";
    public static let F_FROM_DATE = "fromDate";
    public static let F_TO_DATE = "toDate";
    public static let F_GAMES = "games";
    public static let F_ANSWER = "answer";
    public static let F_LUCKY_NO = "luckyNo";
    public static let F_ANSWERS = "answers";
    public static let F_WIN = "win";
}

public class Methods{
    public static let F_FOR_BONUS = "inputForBonus";
    public static let F_SEARCH_BUS_TRIP = "searchBusTrips";
    public static let F_GET_BUS_TRIP = "getBusTrip";
    public static let F_BOOKING_BUS_TICKET = "bookingBusTicket";
    public static let F_LIST_HISTORY_TICKET = "listHistoryTickets";
    public static let F_LIST_PUBLIC_VEHICLE = "listPublicVehicles";
    public static let F_GET_LUCKY_GAME = "getLuckyGame";
    public static let F_LIST_LUCKY_GAME = "listLuckyGames";
    public static let F_LUCKY_GAME_ANSWER = "luckyGameAnswer";
    public static let F_INPUT_GAME_ANSWER = "inputGameAnswer";
}

public class NaviColor{
    static var navigation:UIColor = UIColor(red: 29/255, green: 150/255, blue: 224/255, alpha: 1)
    static var naviButton:UIColor = UIColor.gray
    static var naviBtnOrange:UIColor = UIColor.red
    static var naviBtnBlue:UIColor = UIColor(red: 29/255, green: 150/255, blue: 224/255, alpha: 1)
    static var naviLabel:UIColor = UIColor(red: 29/255, green: 150/255, blue: 224/255, alpha: 1)
    static var naviText:UIColor = UIColor(red: 29/255, green: 150/255, blue: 224/255, alpha: 1)
    static var navigationGray:UIColor = UIColor(red: 41/255, green: 48/255, blue: 63/255, alpha: 1)
    static var navigationOrange:UIColor = UIColor(red: 244/255, green: 116/255, blue: 26/255, alpha: 1)
}
//static var naviBtnOrange:UIColor = UIColor(colorLiteralRed: 244/255, green: 125/255, blue: 61/255, alpha: 1)
//(colorLiteralRed: 255/255, green: 131/255, blue: 44/255, alpha: 1)
public class getLocation{
    public static var choice:Int?=0
}

