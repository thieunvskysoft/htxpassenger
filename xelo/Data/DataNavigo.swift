//
//  DataNavigo.swift
//  NaviGo
//
//  Created by HoangManh on 9/22/17.
//  Copyright © 2017 DUMV. All rights reserved.
//

import Foundation
import CoreLocation
import Starscream

public class DataNavigo{
    static var runCheck:Bool? = true
    static var update:Bool? = true
    static var arrService: Array<Service> = Array()
    static var servicePrice: Array<Service> = Array()
    static var passenger = Passenger()
    static var convert = Convert()
    static var booking:Book = Book()
    static var servicesCode:String? = nil
    static var handingScreen:HandingScreen?
    static var activeKey:String? = nil
    static var serviceBackground = SeviceBackground()
    static var loginResult = LoginResult()
    static var serviceCar:Int = 0
    static var duration:Int? = 0
    static var diverID:String? = nil
    static var driverInfo = DriverInfo()
    static var arrBookHistory = [Book]()
    static var arrHistories = [Histories]()
    static var bookingInfo:Book = Book()
    static var idUIController:Int? = 0
    static var checkDrawPath:Bool = true
    static var arrAddress: Array<Address> = Array()
    static var showDialog:Bool = true
    static var deviceModel:Int = 0
    static var arrNotify: Array<Notifications> = Array()
    static var arrMessager = [ChatMessage]()
    static var socket: WebSocket? = nil
    static var ipaDelegate:IAPDelegate? = nil
    static var animationDelegate:AnimationDelegate? = nil
    static var popupDelegate:PopupDelegate? = nil
    static var foreground:Bool = true
    static var notiFB:Notifications? = nil
    static var arrTransWallet = [Transactions]()
    static var arrStates: Array<States> = Array()
    static var serviceAccept:Int = 0
}
