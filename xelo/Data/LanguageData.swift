//
//  LanguageData.swift
//  xelo
//
//  Created by Nguyen Thieu on 7/10/18.
//  Copyright © 2018 DUMV. All rights reserved.
//

import UIKit

public class LanguageData{
    func getstring(fullkey:String) -> String {
        
        return ""
    }
    func localizedString(forKey language:String,forKey key: String) -> String {
        var keyLanuage:String = language
        var fullKey:String = ""
        if language == "" {
            keyLanuage = ".VN"
        }
        fullKey = key + "."+keyLanuage
        var result = Bundle.main.localizedString(forKey: key, value: nil, table: nil)
        
        if result == key {
            result = Bundle.main.localizedString(forKey: fullKey, value: nil, table: "Default")
        }

        return result
    }
}

