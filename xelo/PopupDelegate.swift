//
//  PopupDelegate.swift
//  xelo
//
//  Created by Nguyen Thieu on 8/28/18.
//  Copyright © 2018 DUMV. All rights reserved.
//

import Foundation

protocol PopupDelegate {
    func PopupAccept(code:String, value:String)
    func PopupCancel(code:String, value:String)
    func Timeout()
}
