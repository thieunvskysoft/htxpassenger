//
//  BusListController.swift
//  xelo
//
//  Created by Nguyen Thieu on 2/21/19.
//  Copyright © 2019 DUMV. All rights reserved.
//

import UIKit

class BusListController: UIViewController {

    var arrBus = [BusTrip]()
    var bookDate = Date()
    
    
    @IBOutlet weak var tblBusList: UITableView!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var lblMessage: UILabel!
    
    var EMPTY_SEAT:String = ""
    var CAR_SCHEDULE:String = ""
    
    func loadViewLanguage() {
        let curren_language = UserDefaults.standard.string(forKey: "curren_language")
        let language = LanguageData()
        EMPTY_SEAT = language.localizedString(forKey:curren_language!,forKey: "EMPTY_SEAT")
        CAR_SCHEDULE = language.localizedString(forKey:curren_language!,forKey: "CAR_SCHEDULE")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadViewLanguage()
        tblBusList.dataSource = self
        tblBusList.delegate = self
        tblBusList.separatorStyle = UITableViewCellSeparatorStyle.none
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        let result = formatter.string(from: bookDate)
        lblDate.text = result
        viewBottom.backgroundColor = NaviColor.navigationOrange
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let backButton = UIBarButtonItem()
        backButton.title = " "
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        navigationController?.navigationBar.tintColor = .white
        navigationItem.title = CAR_SCHEDULE
        //navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor :UIColor.white]
        tblBusList.reloadData()
//        let search = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(searchDetail))
//        navigationItem.rightBarButtonItems = [search]
    }
    
    func searchDetail() {
        
    }
    
    func showBusDetail(busDetail:BusTrip)  {
        getBusTrips(tripID: busDetail.tripID)

        
    }
    
    
    
    @IBAction func btnPreDate(_ sender: Any) {
        bookDate = Calendar.current.date(byAdding: .day, value: -1, to: bookDate)!
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        let result = formatter.string(from: bookDate)
        lblDate.text = result
        
        let timeFormat = "yyyy-MM-dd\'T\'"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = timeFormat
        let date = dateFormatter.string(from: bookDate)
        let datefull : Date = Date()
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: datefull)
        let minutes = calendar.component(.minute, from: datefull)
        let second = calendar.component(.second, from: datefull)
        let datetime = date + hour.description + ":" + minutes.description + ":" + second.description + "+0700"
        searchBusTrips(from: BusBookingController.fromLocation, to: BusBookingController.toLocation, date: datetime)
    }
    
    @IBAction func btnNextDate(_ sender: Any) {
        bookDate = Calendar.current.date(byAdding: .day, value: 1, to: bookDate)!
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        let result = formatter.string(from: bookDate)
        lblDate.text = result
        
        let timeFormat = "yyyy-MM-dd\'T\'"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = timeFormat
        let date = dateFormatter.string(from: bookDate)
        let datefull : Date = Date()
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: datefull)
        let minutes = calendar.component(.minute, from: datefull)
        let second = calendar.component(.second, from: datefull)
        let datetime = date + hour.description + ":" + minutes.description + ":" + second.description + "+0700"
        searchBusTrips(from: BusBookingController.fromLocation, to: BusBookingController.toLocation, date: datetime)
    }
    
    func searchBusTrips(from:Point,to:Point,date:String) {
        let CompletionHandler: ((Array<BusTrip>,NavigoError?) -> Void) = {
            arrBusTrips,navigoError in
            DispatchQueue.main.async {
                if(navigoError == nil){
                    if arrBusTrips.count > 0 {
                        self.lblMessage.alpha = 0
                    }else{
                        self.lblMessage.alpha = 1
                    }
                    self.arrBus = arrBusTrips
                    self.tblBusList.reloadData()
                }else{
                    if(navigoError?.errorCode == Errorcode.ERROR_NO_INTERNET){
                        //self.errorNoInternet()
                    }
                    
                }
            }        }
        
        let connector = APIBusTrip();
        connector.SearchBusTripResult(fromPosition: from, toPosition: to, datetime: date, CompletionHandler: CompletionHandler)
        
    }
    
    func getBusTrips(tripID:String) {
        let CompletionHandler: ((BusTripResult,NavigoError?) -> Void) = {
            busTripResult,navigoError in
            DispatchQueue.main.async {
                if(navigoError == nil){
                    let sb = UIStoryboard(name: "BusCar", bundle: nil)
                    let bus = sb.instantiateViewController(withIdentifier: "BusDetailController") as! BusDetailController
                    bus.busDetail = busTripResult
                    self.navigationController?.pushViewController(bus, animated: true)
                }else{
                    if(navigoError?.errorCode == Errorcode.ERROR_NO_INTERNET){
                        //self.errorNoInternet()
                    }
                    
                }
            }        }
        
        let connector = APIBusTrip();
        connector.getBusTrip(tripID: tripID, CompletionHandler: CompletionHandler)
        
    }

}

extension BusListController: UITableViewDelegate, UITableViewDataSource {
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return  arrBus.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BusCell", for: indexPath) as! BusCell
        let bus:BusTrip = arrBus[indexPath.row]
        cell.titleSeatEmpty.text = EMPTY_SEAT
        cell.lblFrom.text = bus.fromAddress
        cell.lblTo.text = bus.toAddress
        let timefull = bus.departureDate.substring(to: 5)
        let datefull = bus.departureDate.substring(from: 6)
        cell.lblDate.text = datefull.substring(to: 5)
        cell.lblTime.text = timefull
        let ticket = bus.capacity - bus.numOfBooked
        cell.lblCompany.text = bus.busOperator
        cell.lblSeat.text = ticket.description
        cell.lblServiceCode.text = bus.modelName
        cell.lblPrice.text = (bus.ticketFare?.description)! + ".000 đ"
        cell.selectionStyle = .none //remove animation click
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // print("chon cai nay ",indexPath.row)
        let bus:BusTrip = arrBus[indexPath.row]
        showBusDetail(busDetail: bus)
        
        
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        let lastElement = DataNavigo.arrBookHistory.count - 1
//        let book:Book = DataNavigo.arrBookHistory[indexPath.row]
//        if  indexPath.row == lastElement && checkLoadData {
//            checkLoadData = !checkLoadData
//            loadHistory(time: self.convertTime12h(value: book.bookDate!))
//        }
        
    }
}

