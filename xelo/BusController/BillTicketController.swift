//
//  BillTicketController.swift
//  xelo
//
//  Created by Nguyen Thieu on 3/29/19.
//  Copyright © 2019 DUMV. All rights reserved.
//

import UIKit

class BillTicketController: UIViewController {
    @IBOutlet weak var lblFromAddress: UILabel!
    @IBOutlet weak var lblToAddress: UILabel!
    @IBOutlet weak var lblBusOperator: UILabel!
    @IBOutlet weak var lblCreateDate: UILabel!
    @IBOutlet weak var lblDepartureDate: UILabel!
    
    @IBOutlet weak var tblBillTicket: UITableView!
    @IBOutlet weak var tbHeightConst: NSLayoutConstraint!
    
    @IBOutlet weak var btnLeftBar: UIBarButtonItem!
    @IBOutlet weak var lblTicketTotal: UILabel!
    @IBOutlet weak var lblPriceTotal: UILabel!
    @IBOutlet weak var lblStateNote: UILabel!
    @IBOutlet weak var lblPaidStatus: UILabel!
    @IBOutlet weak var btnCallBusOperator: UIButton!
    
    @IBOutlet weak var titleBusOperator: UILabel!
    @IBOutlet weak var titleCreateDate: UILabel!
    @IBOutlet weak var titleDeparture: UILabel!
    @IBOutlet weak var titleDescribe: UILabel!
    @IBOutlet weak var titleQuantity: UILabel!
    @IBOutlet weak var titlePrice: UILabel!
    @IBOutlet weak var titleMoney: UILabel!
    @IBOutlet weak var titleTotal: UILabel!
    @IBOutlet weak var titleState: UILabel!
    @IBOutlet weak var titlePay: UILabel!
    @IBOutlet weak var btnContact: UIButton!
    
    @IBOutlet weak var lblHotline: TapabbleLabel!
    
    var BUS_OPERATOR:String = ""
    var CREATE_DATE:String = ""
    var DEPARTURE_DATE:String = ""
    var DESCRIBE:String = ""
    var QUANTITY:String = ""
    var PRICE:String = ""
    var MONEY:String = ""
    var TOTAL:String = ""
    var STATE:String = ""
    var PAY:String = ""
    var CONTACT_TITLE:String = ""
    var BILL_DETAIL:String = ""
    
    func loadViewLanguage() {
        let curren_language = UserDefaults.standard.string(forKey: "curren_language")
        let language = LanguageData()
        BUS_OPERATOR = language.localizedString(forKey:curren_language!,forKey: "BUS_OPERATOR")
        CREATE_DATE = language.localizedString(forKey:curren_language!,forKey: "CREATE_DATE")
        DEPARTURE_DATE = language.localizedString(forKey:curren_language!,forKey: "DEPARTURE_DATE")
        DESCRIBE = language.localizedString(forKey:curren_language!,forKey: "DESCRIBE")
        QUANTITY = language.localizedString(forKey:curren_language!,forKey: "QUANTITY")
        PRICE = language.localizedString(forKey:curren_language!,forKey: "PRICE")
        MONEY = language.localizedString(forKey:curren_language!,forKey: "MONEY")
        TOTAL = language.localizedString(forKey:curren_language!,forKey: "TOTAL")
        STATE = language.localizedString(forKey:curren_language!,forKey: "STATE")
        PAY = language.localizedString(forKey:curren_language!,forKey: "PAY")
        BILL_DETAIL = language.localizedString(forKey:curren_language!,forKey: "BILL_DETAIL")
        CONTACT_TITLE = language.localizedString(forKey:curren_language!,forKey: "CONTACT_TITLE")
        btnContact.setTitle(CONTACT_TITLE, for: .normal)
        
        titleBusOperator.text = BUS_OPERATOR
        titleCreateDate.text = CREATE_DATE
        titleDeparture.text = DEPARTURE_DATE
        titleDescribe.text = DESCRIBE
        titleQuantity.text = QUANTITY
        titlePrice.text = PRICE
        titleMoney.text = MONEY
        titleTotal.text = TOTAL
        titleState.text = STATE
        titlePay.text = PAY
    }
    
    var busTrip = BusTrip()
    var actionValue:Int = 0
    
    var ticketFares: Array<TicketFares> = Array()
    var convert = Convert()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //sbtnLeftBar.customView!.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        loadViewLanguage()
        setTicket(bus: busTrip)
        lblFromAddress.text = busTrip.fromAddress
        lblToAddress.text = busTrip.toAddress
        lblBusOperator.text = busTrip.busOperator
        lblCreateDate.text = convert.convertDatetimeByDateformat(datetime: busTrip.createDate, dateFormat: "dd/MM/yyyy", value: 3)
        lblDepartureDate.text = busTrip.departureDate
        lblHotline.text = busTrip.hotLine
        tblBillTicket.delegate = self
        tblBillTicket.dataSource = self
        tblBillTicket.separatorStyle = UITableViewCellSeparatorStyle.none
        tblBillTicket.isScrollEnabled = false
        let ticketTotal = busTrip.quantity1 + busTrip.quantity2 + busTrip.quantity3
        let price = busTrip.quantity1 * busTrip.ticketFare1 + busTrip.quantity2 * busTrip.ticketFare2 + busTrip.quantity3 * busTrip.ticketFare3
        lblTicketTotal.text = ticketTotal.description
        lblPriceTotal.text = price.description + " K"
        lblStateNote.text = busTrip.stateNote
        if busTrip.paid {
            lblPaidStatus.text = "Thanh toán"
        }else{
            lblPaidStatus.text = "Chưa thanh toán"
        }
        if busTrip.hotLine == "" {
            btnCallBusOperator.alpha = 0
            btnCallBusOperator.isEnabled = false
        }
        btnCallBusOperator.layer.cornerRadius = 5
        btnCallBusOperator.addTarget(self, action: #selector(callPhone) , for: .touchUpInside)
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.tapDetected(sender:)))
        self.lblHotline.addGestureRecognizer(tapGestureRecognizer)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        navigationController?.navigationBar.tintColor = .white
        navigationItem.title = BILL_DETAIL
        //navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor :UIColor.white]
        let height = ticketFares.count * 43
        tbHeightConst.constant = CGFloat(height)
        tblBillTicket.layoutIfNeeded()
        tblBillTicket.reloadData()
    }
    

    
    
    @objc func tapDetected(sender: UITapGestureRecognizer) {
        callPhone()
    }
    
    @objc func callPhone() {
        let phone = busTrip.hotLine
        let phoneNumber:String = "tel:" + phone
        if (phone.characters.count) > 0 {
            if #available(iOS 10, *) {
                UIApplication.shared.open(NSURL(string: phoneNumber)! as URL)
            } else {
                UIApplication.shared.openURL(NSURL(string: phoneNumber)! as URL)
            }
        }
    }
    @IBAction func btnBackController(_ sender: Any) {
        if actionValue == 0 {
            let viewControllers:[UIViewController] = (self.navigationController?.viewControllers)!;
            for controller in viewControllers {
                if controller is TabBusTripController {
                    self.navigationController!.popToViewController(controller, animated: true)
                }
            }
        }else{
            navigationController?.popViewController(animated: true)
        }
    }
    func backViewController() {
        
    }
    
    func setTicket(bus:BusTrip) {
        if bus.quantity1 > 0 {
            ticketFares.append(TicketFares(description: "Vé VIP", note: "", ticketFare: bus.ticketFare1, ticketID: 1, capacity: bus.quantity1))
        }
        if bus.quantity2 > 0 {
            ticketFares.append(TicketFares(description: "Vé phổ thông", note: "", ticketFare: bus.ticketFare2, ticketID: 3, capacity: bus.quantity2))
        }
        if bus.quantity3 > 0 {
            ticketFares.append(TicketFares(description: "Vé ECO", note: "", ticketFare: bus.ticketFare3, ticketID: 3, capacity: bus.quantity3))
        }
    }
}


extension BillTicketController: UITableViewDelegate, UITableViewDataSource {
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return  ticketFares.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BillTicketCell", for: indexPath) as! BillTicketCell
        let ticket:TicketFares = ticketFares[indexPath.row]
        cell.lblNameTicket.text = ticket.description
        cell.lblQuantity.text = ticket.capacity.description
        cell.lblTicketFare.text = ticket.ticketFare.description + " K"
        cell.lblPrice.text = (ticket.capacity * ticket.ticketFare).description + " K"
        cell.selectionStyle = .none //remove animation click
        return cell
    }
    
}


