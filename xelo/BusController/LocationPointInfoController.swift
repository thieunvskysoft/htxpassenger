//
//  LocationPointInfoController.swift
//  xelo
//
//  Created by Nguyen Thieu on 3/12/19.
//  Copyright © 2019 DUMV. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import SwiftyJSON
import Alamofire

class LocationPointInfoController: UIViewController, CLLocationManagerDelegate, GMSMapViewDelegate {
    
    @IBOutlet weak var viewAddress: UIView!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var viewStep: UIView!
    var googleMap:GMSMapView?
    var locationManager = CLLocationManager()
    var locationPoint = CLLocation()
    let marker = GMSMarker()
    var lastMoveMap:Int64 = 0
    var addressLocation:String = ""
    var arrLocation = [Place]()
    var geom:String = ""
    
    var choice = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        setupGoogleMap()
        lblAddress.text = arrLocation[choice].description
    }

    
    override func viewWillAppear(_ animated: Bool) {
        UIApplication.shared.statusBarStyle = .lightContent
        self.navigationController?.isNavigationBarHidden = false
        let backButton = UIBarButtonItem()
        backButton.title = " "
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        navigationController?.navigationBar.tintColor = .white
        navigationItem.title = "Lộ trình"
        //navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor :UIColor.white]
        
        let path = GMSPath.init(fromEncodedPath: geom)
        let polyline = GMSPolyline.init(path: path)
        polyline.strokeWidth = 3
        polyline.strokeColor = UIColor.init(red: 1, green: 38/255, blue: 1/255, alpha: 0.8)
        polyline.map = self.googleMap
        
    }
    
    func createMarker(titleMarker: String, iconMarker: UIImage, latitude: CLLocationDegrees, longitude: CLLocationDegrees) {
        
        marker.position = CLLocationCoordinate2DMake(latitude, longitude)
        marker.title = titleMarker
        marker.icon = self.imageWithImage(image: iconMarker, scaledToSize: CGSize(width: 32.0, height: 32.0))
        marker.map = googleMap
    }
    func imageWithImage(image:UIImage, scaledToSize newSize:CGSize) -> UIImage{
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0);
        //image.draw(in: CGRectMake(0, 0, newSize.width, newSize.height))
        image.draw(in: CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: newSize.width, height: newSize.height))  )
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    
    //setup map
    func setupGoogleMap() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        locationManager.startMonitoringSignificantLocationChanges()
        let camera = GMSCameraPosition.camera(withLatitude: arrLocation[choice].y!, longitude: arrLocation[choice].x!, zoom: 17)
        googleMap = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        googleMap?.camera = camera
        createMarker(titleMarker: arrLocation[choice].description, iconMarker: UIImage(named: "map-flags")!, latitude: arrLocation[choice].y!, longitude: arrLocation[choice].x!)
        
        googleMap?.delegate = self
        googleMap?.isMyLocationEnabled = true
        googleMap?.settings.myLocationButton = true
        //button location
        _ = (self.navigationController?.navigationBar.intrinsicContentSize.height)! + UIApplication.shared.statusBarFrame.height
        do {
            // Set the map style by passing the URL of the local file.
            if let styleURL = Bundle.main.url(forResource: "style", withExtension: "json") {
                googleMap?.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
            } else {
                NSLog("Unable to find style.json")
            }
        } catch {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
        view.addSubview(googleMap!)
        googleMap?.addSubview(viewStep)
        googleMap?.addSubview(viewAddress)
        if(DataNavigo.deviceModel == Constant.deviceModelX){
            googleMap?.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height - 35)
        }else{
            googleMap?.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        }
    }
    @IBAction func btnPre(_ sender: Any) {
        print("Error while get location btnNext",choice)
        if choice > 0 {
            choice = choice - 1
            lblAddress.text = arrLocation[choice].description
            let camera = GMSCameraPosition.camera(withLatitude: arrLocation[choice].y!, longitude: arrLocation[choice].x!, zoom: 17)

            self.googleMap?.animate(to: camera)
            marker.position = CLLocationCoordinate2DMake(arrLocation[choice].y!, arrLocation[choice].x!)
        }
        
    }
    
    @IBAction func btnNext(_ sender: Any) {
        print("Error while get location btnNext",choice,arrLocation.count)
        if choice < arrLocation.count - 1 {
            choice = choice + 1
            lblAddress.text = arrLocation[choice].description
            let camera = GMSCameraPosition.camera(withLatitude: arrLocation[choice].y!, longitude: arrLocation[choice].x!, zoom: 17)
            self.googleMap?.animate(to: camera)
            
            marker.position = CLLocationCoordinate2DMake(arrLocation[choice].y!, arrLocation[choice].x!)
        }
    }
    
    
}
//Map
extension LocationPointInfoController {    
    // MARK: GMSMapview Delegate
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        self.googleMap?.isMyLocationEnabled = true
    }
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        self.googleMap?.isMyLocationEnabled = true
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        locationPoint = CLLocation(latitude: mapView.camera.target.latitude, longitude: mapView.camera.target.longitude)
    }
    
}
