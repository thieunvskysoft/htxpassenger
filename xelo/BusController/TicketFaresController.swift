//
//  TicketFaresController.swift
//  xelo
//
//  Created by Nguyen Thieu on 3/26/19.
//  Copyright © 2019 DUMV. All rights reserved.
//

protocol TicketDelegate {
    
    func didChangeTicket(ticketID: Int, ticketValue: Int)
}

import UIKit

class TicketFaresController: UIViewController {

    @IBOutlet weak var ticketFareTbl: UITableView!
    @IBOutlet weak var heightTbl: NSLayoutConstraint!
    @IBOutlet weak var btnSlide: MMSlidingButton!
    @IBOutlet weak var lblCountTicket: UILabel!
    @IBOutlet weak var lblPriceTotal: UILabel!
    var countTicket:Int = 0
    
    var busDetail = BusTripResult()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnSlide.delegate = self as SlideButtonDelegate
        ticketFareTbl.delegate = self
        ticketFareTbl.dataSource = self
        ticketFareTbl.separatorStyle = UITableViewCellSeparatorStyle.none
        ticketFareTbl.isScrollEnabled = false
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let backButton = UIBarButtonItem()
        backButton.title = " "
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        navigationController?.navigationBar.tintColor = .white
        navigationItem.title = "Chọn vé"
        //navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor :UIColor.white]
        ticketFareTbl.reloadData()
        let height = busDetail.ticketFares.count * 110 + 5
        heightTbl.constant = CGFloat(height)
        ticketFareTbl.layoutIfNeeded()
        btnSlide.dragPointColor = NaviColor.naviBtnOrange
        btnSlide.imageName = #imageLiteral(resourceName: "Arrorw")
        btnSlide.buttonColor = UIColor.gray
        btnSlide.buttonDragPoint = true
        btnSlide.buttonText = "Trượt để đặt vé"
    }
    
    func setPrice() {
        var price = 0
        
        for ticket in busDetail.ticketFares  {
            print("vé----",ticket.ticketFare, ticket.choice)
            countTicket = countTicket + ticket.choice
            price = price + ticket.ticketFare * ticket.choice
        }
        
        lblCountTicket.text = countTicket.description
        lblPriceTotal.text = price.description + " K"
    }
    
    func bookingBusTicket() {
        let CompletionHandler: ((BusTrip,NavigoError?) -> Void) = {
            busDetail,navigoError in
            DispatchQueue.main.async {
                if(navigoError == nil){
                    let sb = UIStoryboard(name: "BusCar", bundle: nil)
                    let bus = sb.instantiateViewController(withIdentifier: "BillTicketController") as! BillTicketController
                    bus.busTrip = busDetail
                    self.navigationController?.pushViewController(bus, animated: true)
                }else{
                    if(navigoError?.errorCode == Errorcode.ERROR_NO_INTERNET){
                        //self.errorNoInternet()
                        print("Lỗi ")
                    }
                    self.btnSlide.reset(btnCheck: true, btnText: "Trượt để đặt vé")
                }
            }
        }
        
        let connector = APIBusTrip();
        connector.bookingBusTicket(busTrip: busDetail.busTrip, currentPosition: BusBookingController.myLocation, arrQuantityTicket: busDetail.ticketFares, CompletionHandler: CompletionHandler)
    }
    

}

extension TicketFaresController:TicketDelegate {
    func didChangeTicket(ticketID: Int, ticketValue: Int) {
        countTicket = 0
        for ticket in busDetail.ticketFares  {
            if ticketID == ticket.ticketID {
                ticket.choice = ticketValue
            }
        }
        setPrice()
    }
}

extension TicketFaresController: UITableViewDelegate, UITableViewDataSource {
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return  busDetail.ticketFares.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TicketFareCell", for: indexPath) as! TicketFareCell
        let ticket:TicketFares = busDetail.ticketFares[indexPath.row]
        cell.lblCapacity.text = ticket.capacity.description
        cell.lblDescription.text = ticket.description
        cell.lblNote.text = "* " + ticket.note
        cell.lblTicketFare.text = ticket.ticketFare.description + " K"
        cell.ticketID = ticket.ticketID
        cell.price = ticket.ticketFare
        cell.capacity = ticket.capacity
        cell.selectionStyle = .none //remove animation click
        cell.delegate = self
        return cell
    }
    
}
extension TicketFaresController:SlideButtonDelegate{
    func buttonStatus(status: String, sender: MMSlidingButton) {
        if (status == "Success") {
            print("Đặt vé------------")
            bookingBusTicket()
            
        }
    }
}
