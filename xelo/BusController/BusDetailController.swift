//
//  BusDetailController.swift
//  xelo
//
//  Created by Nguyen Thieu on 2/21/19.
//  Copyright © 2019 DUMV. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class BusDetailController: UIViewController,GMSMapViewDelegate {

    var busDetail = BusTripResult()
    var arrAddressLocation = [Address]()
    var ticket:Int = 1
    @IBOutlet weak var lblFrom: UILabel!
    @IBOutlet weak var lblTo: UILabel!
    @IBOutlet weak var lblDatetime: UILabel!
    @IBOutlet weak var lblCompanyName: UILabel!
    @IBOutlet weak var lblplateNumber: UILabel!
    @IBOutlet weak var lblTimeStart: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblTicketValue: UILabel!
    
    @IBOutlet weak var btnAccept: UIButton!
    @IBOutlet weak var btnDetail: UIButton!
    @IBOutlet weak var titlePrice: UILabel!
    
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var tblListLocationPoint: UITableView!
    @IBOutlet weak var viewBusDetail: UIView!
    
    var googleMap:GMSMapView?
    let marker = GMSMarker()
    
    var DETAIL:String = ""
    var PRICE:String = ""
    var OPTIONS_TICKET:String = ""
    var const = 0
    func loadViewLanguage() {
        let curren_language = UserDefaults.standard.string(forKey: "curren_language")
        let language = LanguageData()
        PRICE = language.localizedString(forKey:curren_language!,forKey: "PRICE")
        DETAIL = language.localizedString(forKey:curren_language!,forKey: "DETAIL")
        OPTIONS_TICKET = language.localizedString(forKey:curren_language!,forKey: "OPTIONS_TICKET")
        titlePrice.text = PRICE
        btnAccept.setTitle(OPTIONS_TICKET, for: .normal)
        btnDetail.setTitle(DETAIL, for: .normal)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadViewLanguage()
        const = checkConst()
        lblFrom.text = busDetail.busTrip.fromAddress
        lblTo.text = busDetail.busTrip.toAddress
        lblDatetime.text = busDetail.busTrip.departureDate
        lblCompanyName.text = busDetail.busTrip.busOperator
        lblplateNumber.text = busDetail.busTrip.modelName
        lblPrice.text = (busDetail.busTrip.ticketFare?.description)! + " K"
        _ = (self.navigationController?.navigationBar.intrinsicContentSize.height)! + UIApplication.shared.statusBarFrame.height + 40
        topConstraint.constant = CGFloat(const)
        bottomConstraint.constant = CGFloat(-1*const)
        tblListLocationPoint.dataSource = self
        tblListLocationPoint.delegate = self
        setupGoogleMap()
        tblListLocationPoint.reloadData()
    }

    override func viewWillAppear(_ animated: Bool) {
        let backButton = UIBarButtonItem()
        backButton.title = " "
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        navigationController?.navigationBar.tintColor = .white
        navigationItem.title = "Lộ trình chi tiết"
        //navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor :UIColor.white]
        
        //print("busDetail.busTrip.geom",busDetail.busTrip.geom)
        let path = GMSPath.init(fromEncodedPath: busDetail.busTrip.geom)
        let polyline = GMSPolyline.init(path: path)
        polyline.strokeWidth = 3
        polyline.strokeColor = UIColor.init(red: 1, green: 38/255, blue: 1/255, alpha: 0.8)
        polyline.map = self.googleMap
        
//        for places in busDetail.places {
//            if places != nil {
//
//                createMarker(titleMarker: places.description, iconMarker: UIImage(named: "map-flags")!, latitude: places.y!, longitude: places.x!)
//            }
//        }
    }
    
    func checkConst() -> Int {
        var const = 400
        switch UIScreen.main.nativeBounds.height {
        case 1136:
            print("===iPhone 5 or 5S or 5C")
        case 1334:
            print("===iPhone 6/6S/7/8--667")
        case 2208:
            print("===iPhone 6+/6S+/7+/8+---736")
            const = const + 70
        case 2436:
            print("===iPhone X---812")
            const = const + 105
        case 2688:
            print("===iPhone XS Max---896")
            const = const + 190
        case 1792:
            print("===iPhone XR ---896")
            const = const + 190
        default:
            print("===unknown")
        }
        return const
    }
    
    func setupGoogleMap() {
        let camera = GMSCameraPosition.camera(withLatitude: 21.0277501, longitude: 105.7641194, zoom: 12)
        googleMap = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        googleMap?.camera = camera
        //createMarker(titleMarker: arrAddressLocation[choice].fullAddress!, iconMarker: #imageLiteral(resourceName: "marker-end"), latitude: arrAddressLocation[choice].y!, longitude: arrAddressLocation[choice].x!)
        
        googleMap?.delegate = self
        googleMap?.isMyLocationEnabled = true
        googleMap?.settings.myLocationButton = true
        //button location
        _ = (self.navigationController?.navigationBar.intrinsicContentSize.height)! + UIApplication.shared.statusBarFrame.height
        do {
            // Set the map style by passing the URL of the local file.
            if let styleURL = Bundle.main.url(forResource: "style", withExtension: "json") {
                googleMap?.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
            } else {
                NSLog("Unable to find style.json")
            }
        } catch {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
        view.addSubview(googleMap!)
        googleMap?.addSubview(viewBusDetail)
        self.googleMap?.padding = UIEdgeInsets(top: 0, left: 0, bottom: 270, right: 0)
        if(DataNavigo.deviceModel == Constant.deviceModelX){
            googleMap?.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height - 35)
        }else{
            googleMap?.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        }
        googleMap?.addSubview(viewBottom)
        let start = CLLocationCoordinate2DMake(busDetail.places[0].y!, busDetail.places[0].x!)
        let end = CLLocationCoordinate2DMake(busDetail.places[busDetail.places.count - 1].y!, busDetail.places[busDetail.places.count - 1].x!)
        createMarker(titleMarker: busDetail.places[0].description, iconMarker: UIImage(named: "star")!, latitude: busDetail.places[0].y!, longitude: busDetail.places[0].x!)
        createMarker(titleMarker: busDetail.places[busDetail.places.count - 1].description, iconMarker: UIImage(named: "map-flags")!, latitude: busDetail.places[busDetail.places.count - 1].y!, longitude: busDetail.places[busDetail.places.count - 1].x!)
        let bounds = GMSCoordinateBounds(coordinate: start, coordinate: end)
        let boundUpdate = GMSCameraUpdate.fit(bounds, withPadding: 40)
        googleMap!.animate(with: boundUpdate)
    }
    
    func createMarker(titleMarker: String, iconMarker: UIImage, latitude: CLLocationDegrees, longitude: CLLocationDegrees) {
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2DMake(latitude, longitude)
        marker.title = titleMarker
        marker.icon = self.imageWithImage(image: iconMarker, scaledToSize: CGSize(width: 32.0, height: 32.0))
        marker.map = googleMap
    }
    
    func imageWithImage(image:UIImage, scaledToSize newSize:CGSize) -> UIImage{
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0);
        //image.draw(in: CGRectMake(0, 0, newSize.width, newSize.height))
        image.draw(in: CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: newSize.width, height: newSize.height))  )
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    
    @IBAction func btnViewDetail(_ sender: Any) {

        self.view.layoutIfNeeded() // Force lays of all subviews on root view
        UIView.animate(withDuration: 1.0) { [weak self] in // allowing to ARC to deallocate it properly
            if self?.topConstraint.constant != 0  {
                self?.topConstraint.constant = 0
                self?.bottomConstraint.constant = 0
            }else{
                self?.topConstraint.constant = CGFloat(self!.const)
                self?.bottomConstraint.constant = CGFloat(-1*self!.const)
            }
            self?.view.layoutIfNeeded() // Force lays of all subviews on root view again.
        }

    }
    @IBAction func btnBooking(_ sender: Any) {
        let sb = UIStoryboard(name: "BusCar", bundle: nil)
        let skymap = sb.instantiateViewController(withIdentifier: "TicketFaresController") as! TicketFaresController
        skymap.busDetail = busDetail
        self.navigationController?.pushViewController(skymap, animated: true)
    }
    
//    @IBAction func btnUp(_ sender: Any) {
//        ticket += 1
//        //let price = ticket * busDetail.price!
//        //lblPrice.text = price.description
//        lblTicketValue.text = ticket.description
//    }
//
//    @IBAction func btnDown(_ sender: Any) {
//        if ticket > 1 {
//            ticket -= 1
//            //let price = ticket * busDetail.price!
//            //lblPrice.text = price.description
//            lblTicketValue.text = ticket.description
//        }
//
//    }
    func getBusTrip(tripID:String) {
        
    }
}

extension BusDetailController: UITableViewDelegate, UITableViewDataSource {
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return  busDetail.places.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LocationPointCell", for: indexPath) as! LocationPointCell
        let bus = busDetail.places[indexPath.row]
        let timefull = bus.time.substring(from: 11)
        cell.lblLocationAddress.text = bus.description
        cell.lblTime.text = timefull
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("chon cai nay ",indexPath.row)
        let sb = UIStoryboard(name: "BusCar", bundle: nil)
        let skymap = sb.instantiateViewController(withIdentifier: "LocationPointInfoController") as! LocationPointInfoController
        skymap.arrLocation = busDetail.places
        skymap.geom = busDetail.busTrip.geom
        skymap.choice = indexPath.row
        self.navigationController?.pushViewController(skymap, animated: true)
        
        
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {

        
    }
}

//Map
extension BusDetailController {
    // MARK: GMSMapview Delegate
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        self.googleMap?.isMyLocationEnabled = true
    }
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        self.googleMap?.isMyLocationEnabled = true
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        //locationPoint = CLLocation(latitude: mapView.camera.target.latitude, longitude: mapView.camera.target.longitude)
    }
    
}
