//
//  BusBookingController.swift
//  xelo
//
//  Created by Nguyen Thieu on 2/21/19.
//  Copyright © 2019 DUMV. All rights reserved.
//

import UIKit
import CoreLocation
import JTAppleCalendar


class BusBookingController: UIViewController {
    @IBOutlet weak var btnDate: UIButton!
    @IBOutlet weak var lblDatetime: UILabel!
    @IBOutlet weak var btnToday: UIButton!
    @IBOutlet weak var btnTomorow: UIButton!
    @IBOutlet weak var viewDate: UIView!
    @IBOutlet weak var btnSearchBus: UIButton!
    @IBOutlet weak var viewDetail: UIView!
    @IBOutlet weak var viewBottom: UIView!
    
    @IBOutlet weak var viewWeek: UIView!
    
    @IBOutlet weak var calendarHeaderLabel: UILabel!
    @IBOutlet weak var calendarView: JTAppleCalendarView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var viewAddress: UIView!
    @IBOutlet weak var viewCalendar: UIView!
    @IBOutlet weak var lblStart: UILabel!
    @IBOutlet weak var lblEnd: UILabel!
    @IBOutlet weak var starting: UILabel!
    @IBOutlet weak var destination: UILabel!
    
    let locationManager = CLLocationManager()
    static var fromLocation = Point()
    static var fromAddress:String = ""
    static var toLocation = Point()
    static var toAddress:String = ""
    static var myLocation = Point()
    weak var calendarDelegate: CalendarPopUpDelegate?
    
    let calLanguage: CalendarLanguage = .vietnam
    var endDate: Date!
    var startDate: Date = Date().getStart()
    var testCalendar = Calendar(identifier: .gregorian)
    var selectedDate: Date! = Date() {
        didSet {
            setDate()
        }
    }
    var selected:Date = Date() {
        didSet {
            
            calendarView.scrollToDate(selected)
            calendarView.selectDates([selected])
        }
    }

    var STARTING_POINT:String = ""
    var DESTINATION_TITLE:String = ""
    var CHOOSE_DROP_OFF:String = ""
    var FIND_THE_CAR:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        //navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor :UIColor.white]
        self.navigationController?.navigationBar.barTintColor = NaviColor.naviBtnOrange
        self.navigationController?.navigationBar.isTranslucent = false
        loadViewLanguage()
        viewWeek.backgroundColor = NaviColor.navigationOrange
        dateLabel.textColor = NaviColor.navigationOrange
        viewAddress.layer.cornerRadius = 5
        btnSearchBus.layer.cornerRadius = 5
        viewCalendar.layer.cornerRadius = 5
        currentDate = Date()
        endDate = Calendar.current.date(byAdding: .month, value: 2, to: startDate)!
        setCalendar()
        setDate()
        self.calendarView.visibleDates {[unowned self] (visibleDates: DateSegmentInfo) in
            self.setupViewsOfCalendar(from: visibleDates)
        }
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        locationManager.startMonitoringSignificantLocationChanges()
        locationManager.distanceFilter = 10
        locationManager.requestLocation()
        viewBottom.backgroundColor = UIColor.lightGray
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let backButton = UIBarButtonItem()
        backButton.title = " "
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        navigationController?.navigationBar.tintColor = .white
        
        selected = currentDate
        if BusBookingController.fromAddress != "" {
            lblStart.text = BusBookingController.fromAddress
        }
        if BusBookingController.toAddress != "" {
            lblEnd.text = BusBookingController.toAddress
        }
    }
    
    func loadViewLanguage() {
        let curren_language = UserDefaults.standard.string(forKey: "curren_language")
        let language = LanguageData()
        STARTING_POINT = language.localizedString(forKey:curren_language!,forKey: "STARTING_POINT")
        DESTINATION_TITLE = language.localizedString(forKey:curren_language!,forKey: "DESTINATION_TITLE")
        CHOOSE_DROP_OFF = language.localizedString(forKey:curren_language!,forKey: "CHOOSE_DROP_OFF")
        FIND_THE_CAR = language.localizedString(forKey:curren_language!,forKey: "FIND_THE_CAR")
        btnSearchBus.setTitle(FIND_THE_CAR, for: .normal)
        starting.text = STARTING_POINT
        destination.text = DESTINATION_TITLE
        lblEnd.text = CHOOSE_DROP_OFF
    }

    @IBAction func btnStartLocation(_ sender: Any) {
        getLocation.choice = 3
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let view = sb.instantiateViewController(withIdentifier: "TabViewController") as! TabViewController
        self.navigationController?.pushViewController(view, animated: true)
    }
    
    @IBAction func btnEndLocation(_ sender: Any) {
        getLocation.choice = 4
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let view = sb.instantiateViewController(withIdentifier: "TabViewController") as! TabViewController
        self.navigationController?.pushViewController(view, animated: true)
    }
    
    @IBAction func btnReverse(_ sender: Any) {
        let start = lblStart.text
        lblStart.text = lblEnd.text
        lblEnd.text = start
    }
    
    var aPopupContainer: PopupContainer?
    //var testCalendar = Calendar(identifier: Calendar.Identifier.iso8601)
    var currentDate: Date! = Date() {
        didSet {
            setDate()
        }
    }
    @IBAction func showCalendar(_ sender: UIButton) {
        let xibView = Bundle.main.loadNibNamed("CalendarPopUp", owner: nil, options: nil)?[0] as! CalendarPopUp
        xibView.calendarDelegate = self
        xibView.selected = currentDate
        xibView.startDate = Calendar.current.date(byAdding: .month, value: -12, to: currentDate)!
        xibView.endDate = Calendar.current.date(byAdding: .year, value: 2, to: currentDate)!
        PopupContainer.generatePopupWithView(xibView).show()
    }
    
    @IBAction func setToday(_ sender: Any) {
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        let result = formatter.string(from: date)
        lblDatetime.text = result
    }
    @IBAction func setTomorow(_ sender: Any) {
        let date = Calendar.current.date(byAdding: .day, value: 1, to: Date())!
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        let result = formatter.string(from: date)
        lblDatetime.text = result
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
    func setDate() {
//        let formatter = DateFormatter()
//        formatter.dateFormat = "dd/MM/yyyy"
//        let result = formatter.string(from: currentDate)
//        lblDatetime.text = result
        let month = testCalendar.dateComponents([.month], from: selectedDate).month!
        let weekday = testCalendar.component(.weekday, from: selectedDate)
        let year = testCalendar.dateComponents([.year], from: selectedDate).year!
        let monthName = GetHumanDate(month: month, language: calLanguage) // DateFormatter().monthSymbols[(month-1) % 12] //
        let week = GetWeekByLang(weekDay: weekday, language: calLanguage) // DateFormatter().shortWeekdaySymbols[weekday-1]
        
        let day = testCalendar.component(.day, from: selectedDate)
        
        dateLabel.text = "\(week), " + String(day) + " " + monthName + " " + String(year)
    }
    
    @IBAction func btnSearchBus(_ sender: Any) {
        searchListBusTrip()
    }
    
    func searchListBusTrip() {
        //datetime = datetime + hour.description + ":" + minutes.description + ":" + second.description + "+0700"
        let timeFormat = "yyyy-MM-dd\'T\'"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = timeFormat
        let date = dateFormatter.string(from: selectedDate)
        let datefull : Date = Date()
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: datefull)
        let minutes = calendar.component(.minute, from: datefull)
        let second = calendar.component(.second, from: datefull)
        let datetime = date + hour.description + ":" + minutes.description + ":" + second.description + "+0700"
        
        if BusBookingController.fromLocation.x != nil && BusBookingController.toLocation.x != nil {
            searchBusTrips(from: BusBookingController.fromLocation, to: BusBookingController.toLocation, date: datetime)
        }else if BusBookingController.fromLocation.x == nil {
            //Chọn điểm đi
            showMessage(message: "Bạn chưa chọn điểm xuất phát")
        }else if BusBookingController.toLocation.x == nil {
            //chọn điểm đến
            showMessage(message: "Bạn chưa chọn điểm đến")
        }
    }
    
    func setupViewsOfCalendar(from visibleDates: DateSegmentInfo) {
        guard let startDate = visibleDates.monthDates.first?.date else {
            return
        }
        let month = testCalendar.dateComponents([.month], from: startDate).month!
        let monthName = GetHumanDate(month: month, language: calLanguage) // DateFormatter().monthSymbols[Int(month.month!)-1]
        let weekday = testCalendar.component(.weekday, from: selectedDate)
        let year = testCalendar.component(.year, from: startDate)
        let week = GetWeekByLang(weekDay: weekday, language: calLanguage) //
        
        let day = testCalendar.component(.day, from: selectedDate)
        
        dateLabel.text = "\(week), " + String(day) + " " + monthName + " " + String(year)
    }
    
    func setCalendar() {
        calendarView.calendarDataSource = self
        calendarView.calendarDelegate = self
        
        let nibName = UINib(nibName: "CellView", bundle:nil)
        calendarView.register(nibName, forCellWithReuseIdentifier: "CellView")
        calendarView.minimumLineSpacing = 0
        calendarView.minimumInteritemSpacing = 0
    }
    
    func searchBusTrips(from:Point,to:Point,date:String) {
        let CompletionHandler: ((Array<BusTrip>,NavigoError?) -> Void) = {
            arrBusTrips,navigoError in
            DispatchQueue.main.async {
                if(navigoError == nil){
                    if arrBusTrips.count > 0 {
                        let sb = UIStoryboard(name: "BusCar", bundle: nil)
                        let busList = sb.instantiateViewController(withIdentifier: "BusListController") as! BusListController
                        busList.arrBus = arrBusTrips
                        busList.bookDate = self.selectedDate
                        self.navigationController?.pushViewController(busList, animated: true)
                    }else{
                        self.showMessage(message: "Xin lỗi! Chưa có chuyến xe phù hợp cho bạn.")
                    }
                }else{
                    if(navigoError?.errorCode == Errorcode.ERROR_NO_INTERNET){
                        //self.errorNoInternet()
                    }else if (navigoError?.errorCode == Errorcode.NAVIGO_SESSION_EXPIRED){
                        self.reconect()
                    } 
                }
            }
        }        
        let connector = APIBusTrip();
        connector.SearchBusTripResult(fromPosition: from, toPosition: to, datetime: date, CompletionHandler: CompletionHandler)
    }
    
    ///login
    func reconect() {
        let CompletionHandler: ((LoginResult,NavigoError?) -> Void) = {
            loginResult,navigoError in
            DispatchQueue.main.async {
                if(navigoError == nil){
                    self.searchListBusTrip()
                }else{
                    if(navigoError?.errorCode == Errorcode.ERROR_NO_INTERNET){
                        //self.errorNoInternet()
                        print("Lỗi ")
                    }else if (navigoError?.errorCode == Errorcode.NAVIGO_SESSION_EXPIRED){
                        self.reconect()
                    }
                }
            }
        }
        let connector = PassengerConnector();
        connector.loginAccount(passenger: DataNavigo.passenger, reconnect: true, CompletionHandler: CompletionHandler)
    }
    
    
    func getAddress(fromX: Double, fromY: Double)  {
        let CompletionHandler: ((GooglePlace,NavigoError?) -> Void) = {
            address, navigoError in            
            DispatchQueue.main.async {
                if(navigoError == nil){
                    if address.description! != "" {
                        BusBookingController.fromAddress = address.description!
                        self.lblStart.text = address.description!
                    }
                }else{
                    if(navigoError?.errorCode == Errorcode.ERROR_NO_INTERNET){
                        
                    }
                }
            }
        }
        let apiAddress = APIAddress();
        apiAddress.getAddress(fromX: fromX, fromY: fromY,CompletionHandler: CompletionHandler)
    }
    
    func showMessage(message:String) {
        let alert:UIAlertController = UIAlertController(title: "Thông báo", message: message, preferredStyle: UIAlertControllerStyle.alert)
        let btnOk:UIAlertAction = UIAlertAction(title: "Xác nhận", style: .destructive) { (btnOk) in
        }
        alert.addAction(btnOk)
        present(alert, animated: true, completion: nil)
    }
    
    
}
extension BusBookingController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error while get location \(error)")
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        BusBookingController.myLocation.y = locations.last?.coordinate.latitude
        BusBookingController.myLocation.x = locations.last?.coordinate.longitude        
        if BusBookingController.fromAddress == "" {
            BusBookingController.fromLocation.y = locations.last?.coordinate.latitude
            BusBookingController.fromLocation.x = locations.last?.coordinate.longitude
            sleep(1)
            getAddress(fromX: (locations.last?.coordinate.longitude)!, fromY: (locations.last?.coordinate.latitude)!)
        }
        
    }
    
}

extension BusBookingController: CalendarPopUpDelegate {
    func dateChaged(date: Date, time: String) {
        currentDate = date
    }
}
extension BusBookingController: JTAppleCalendarViewDelegate, JTAppleCalendarViewDataSource {
    
    func configureCalendar(_ calendar: JTAppleCalendarView) -> ConfigurationParameters {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy MM dd"
        
        let parameters = ConfigurationParameters(startDate: startDate,
                                                 endDate: endDate,
                                                 numberOfRows: 6,
                                                 calendar: testCalendar,
                                                 generateInDates: .forAllMonths,
                                                 generateOutDates: .tillEndOfGrid,
                                                 firstDayOfWeek: DaysOfWeek.monday)
        
        return parameters
    }
    
    func calendar(_ calendar: JTAppleCalendarView, willDisplay cell: JTAppleCell, forItemAt date: Date, cellState: CellState, indexPath: IndexPath) {
        (cell as? CellView)?.handleCellSelection(cellState: cellState, date: date, selectedDate: selectedDate)
    }
    
    
    func calendar(_ calendar: JTAppleCalendarView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTAppleCell {
        let myCustomCell = calendar.dequeueReusableCell(withReuseIdentifier: "CellView", for: indexPath) as! CellView
        myCustomCell.handleCellSelection(cellState: cellState, date: date, selectedDate: selectedDate)
        return myCustomCell
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didSelectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        selectedDate = date
        (cell as? CellView)?.cellSelectionChanged(cellState, date: date)
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didDeselectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        (cell as? CellView)?.cellSelectionChanged(cellState, date: date)
    }
    
    func calendar(_ calendar: JTAppleCalendarView, willResetCell cell: JTAppleCell) {
        (cell as? CellView)?.selectedView.isHidden = true
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didScrollToDateSegmentWith visibleDates: DateSegmentInfo) {
        setupViewsOfCalendar(from: visibleDates)
    }
}


