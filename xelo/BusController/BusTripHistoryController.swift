//
//  BusTripHistoryController.swift
//  xelo
//
//  Created by Nguyen Thieu on 3/27/19.
//  Copyright © 2019 DUMV. All rights reserved.
//

import UIKit

class BusTripHistoryController: UIViewController {

    @IBOutlet weak var lblMessage: UILabel!
    var arrBus = [BusTrip]()
    @IBOutlet weak var tblTripHistory: UITableView!
    
    var TOTAL_MONEY:String = ""
    var TICKET:String = ""
    var DEPARTURE_DATE:String = ""
    
    func loadViewLanguage() {
        let curren_language = UserDefaults.standard.string(forKey: "curren_language")
        let language = LanguageData()
        TICKET = language.localizedString(forKey:curren_language!,forKey: "TICKET")
        TOTAL_MONEY = language.localizedString(forKey:curren_language!,forKey: "TOTAL_MONEY")
        DEPARTURE_DATE = language.localizedString(forKey:curren_language!,forKey: "DEPARTURE_DATE")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        loadViewLanguage()
        tblTripHistory.dataSource = self
        tblTripHistory.delegate = self
        tblTripHistory.separatorStyle = UITableViewCellSeparatorStyle.none
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("getListHistoryTickets ")
        getListHistoryTickets()
        
    }

    func searchDetail() {
        let bus = BusTrip(pickupAddress: "123", dropOffAddress: "aaa", fromAddress: "HN", toAddress: "TH", pickupX: 10.0, pickupY: 11.0, dropOffX: 11.0, dropOffY: 13.0, busOperator: "Skysoft", tripID: "1231232", plateNumber: "6689", ticketFare: 12, capacity: 8, numOfBooked: 10, modelName: "Limousin", createDate: "", departureDate: "", paid: false, stateNote: "false", ticketFare1: 11, ticketFare2: 11, ticketFare3: 111, quantity1: 11, quantity2: 11, quantity3: 11, vehicleID: 1111, hotLine: "09082047328", contactName: "9382462", geom: "vìev")
        arrBus.append(bus)
        tblTripHistory.reloadData()
    }
    
    func getListHistoryTickets() {
        let CompletionHandler: ((Array<BusTrip>,NavigoError?) -> Void) = {
            busTripArray,navigoError in
            DispatchQueue.main.async {
                if(navigoError == nil){
                    if busTripArray.count > 0 {
                        self.lblMessage.alpha = 0
                    }else{
                        self.lblMessage.alpha = 1
                    }
                    self.arrBus = busTripArray
                    self.tblTripHistory.reloadData()
                }else{
                    if(navigoError?.errorCode == Errorcode.ERROR_NO_INTERNET){
                        //self.errorNoInternet()
                        print("Lỗi ")
                    }else if (navigoError?.errorCode == Errorcode.NAVIGO_SESSION_EXPIRED){
                        self.reconect()
                    }
                    
                }
            }
        }
        
        let connector = APIBusTrip();
        connector.listHistoryTickets(CompletionHandler: CompletionHandler)
    }
    
    ///login
    func reconect() {
        let CompletionHandler: ((LoginResult,NavigoError?) -> Void) = {
            loginResult,navigoError in
            DispatchQueue.main.async {
                if(navigoError == nil){
                    self.getListHistoryTickets()
                }else{
                    if(navigoError?.errorCode == Errorcode.ERROR_NO_INTERNET){
                        //self.errorNoInternet()
                        print("Lỗi ")
                    }else if (navigoError?.errorCode == Errorcode.NAVIGO_SESSION_EXPIRED){
                        self.reconect()
                    }                    
                }
            }
        }
        let connector = PassengerConnector();
        connector.loginAccount(passenger: DataNavigo.passenger, reconnect: true, CompletionHandler: CompletionHandler)
    }
    
    func showBusDetail(busDetail:BusTrip){
        let sb = UIStoryboard(name: "BusCar", bundle: nil)
        let bus = sb.instantiateViewController(withIdentifier: "BillTicketController") as! BillTicketController
        bus.busTrip = busDetail
        bus.actionValue = 1
        self.navigationController?.pushViewController(bus, animated: true)
    }

}
extension BusTripHistoryController: UITableViewDelegate, UITableViewDataSource {
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return  arrBus.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TripHistoryCell", for: indexPath) as! TripHistoryCell
        let bus:BusTrip = arrBus[indexPath.row]
//        cell.titleDeparture.text = DEPARTURE_DATE + ":"
//        cell.titleMoney.text = TOTAL_MONEY + ":"
//        cell.titleTicket.text = TICKET + ":"
        cell.lblFrom.text = bus.fromAddress
        cell.lblTo.text = bus.toAddress
        let timefull = bus.departureDate.substring(to: 5)
        let datefull = bus.departureDate.substring(from: 6)
        cell.lblDateStart.text = datefull.substring(to: 5)
        cell.lblTimeStart.text = timefull
        cell.lblDate.text = "Đặt: " + bus.createDate
        let ticket = bus.quantity1 + bus.quantity2 + bus.quantity3
        let price = bus.quantity1 * bus.ticketFare1 + bus.quantity2 * bus.ticketFare2 + bus.quantity3 * bus.ticketFare3
        cell.lblnumOfBooked.text = ticket.description + " Vé"
        //cell.lblServiceCode.text = bus.modelName
        cell.lblPrice.text = price.description + " K"
        cell.lblbusOperator.text = bus.busOperator
        cell.lblPlateNumber.text = bus.modelName
        cell.lblHotline.text = bus.hotLine
        cell.selectionStyle = .none //remove animation click
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // print("chon cai nay ",indexPath.row)
        let bus:BusTrip = arrBus[indexPath.row]
        showBusDetail(busDetail: bus)
        
        
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        //        let lastElement = DataNavigo.arrBookHistory.count - 1
        //        let book:Book = DataNavigo.arrBookHistory[indexPath.row]
        //        if  indexPath.row == lastElement && checkLoadData {
        //            checkLoadData = !checkLoadData
        //            loadHistory(time: self.convertTime12h(value: book.bookDate!))
        //        }
        
    }
}


