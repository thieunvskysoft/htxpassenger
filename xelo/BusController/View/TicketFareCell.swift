//
//  TicketFareCell.swift
//  xelo
//
//  Created by Nguyen Thieu on 3/26/19.
//  Copyright © 2019 DUMV. All rights reserved.
//

import UIKit

class TicketFareCell: UITableViewCell {
    
    
    @IBOutlet weak var viewSpace: UIView!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblTicketFare: UILabel!
    @IBOutlet weak var lblNote: UILabel!
    @IBOutlet weak var lblCapacity: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblCount: UILabel!
    var price:Int = 0
    var capacity:Int = 0
    var ticketID:Int = 0
    var countTicket:Int = 0
    var delegate: TicketDelegate?
    
    func setTicket(count:Int) {
        countTicket = count
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        //viewSpace.layer.cornerRadius = 10
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    @IBAction func btnDown(_ sender: Any) {
        if countTicket > 0 {
            countTicket -= 1
            lblPrice.text = (countTicket * price).description + " K"
            lblCount.text = countTicket.description
            delegate?.didChangeTicket(ticketID: ticketID, ticketValue: countTicket)
        }

    }
    
    @IBAction func btnUp(_ sender: Any) {
        if countTicket < capacity {
            countTicket += 1
            lblPrice.text = (countTicket * price).description + " K"
            lblCount.text = countTicket.description
            delegate?.didChangeTicket(ticketID: ticketID, ticketValue: countTicket)
        }
    }
    
    
}

