//
//  LocationPointCell.swift
//  xelo
//
//  Created by Nguyen Thieu on 3/13/19.
//  Copyright © 2019 DUMV. All rights reserved.
//

import UIKit

class LocationPointCell: UITableViewCell {
    
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lblLocationAddress: UILabel!
    
    @IBOutlet weak var lblTime: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

