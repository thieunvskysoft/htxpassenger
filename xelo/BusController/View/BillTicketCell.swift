//
//  BillTicketCell.swift
//  xelo
//
//  Created by Nguyen Thieu on 3/29/19.
//  Copyright © 2019 DUMV. All rights reserved.
//

import UIKit

class BillTicketCell: UITableViewCell {
    
    @IBOutlet weak var lblNameTicket: UILabel!
    @IBOutlet weak var lblTicketFare: UILabel!
    @IBOutlet weak var lblQuantity: UILabel!
    @IBOutlet weak var lblPrice: UILabel!

    
    
    @IBOutlet weak var viewSpace: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

