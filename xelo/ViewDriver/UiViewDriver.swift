//
//  UiViewDriver.swift
//  NaviGo
//
//  Created by HoangManh on 9/20/17.
//  Copyright © 2017 DUMV. All rights reserved.
//

import UIKit

class UiViewDriver: UIView {
    @IBOutlet weak var lblMessage: UILabel!
    //@IBOutlet weak var imgDriver: UIImageView!
    @IBOutlet weak var lblDriverName: UILabel!
    @IBOutlet weak var rateView: FloatRatingView!

    @IBOutlet weak var lblInfoCar: UILabel!
    @IBOutlet weak var lblService: UILabel!
    
//    override init(frame: CGRect) {
//        super.init(frame: frame)
//        setupView()
//        
//    }
//    
//    required init?(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
//        setupView()
//    }
//
//    fileprivate func setupView() {
//        //imgDriver.layer.cornerRadius = 50
//        rateView.emptyImage = #imageLiteral(resourceName: "StarEmpty")
//        rateView.fullImage = #imageLiteral(resourceName: "StarFull")
//        rateView.editable = false
//        lblMessage.text = "Chúng tôi đã tìm thấy tài xế cho bạn"
//    }
}
