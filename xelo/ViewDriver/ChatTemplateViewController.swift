//
//  ChatTemplateViewController.swift
//  xelo
//
//  Created by Nguyen Thieu on 8/28/18.
//  Copyright © 2018 DUMV. All rights reserved.
//

import UIKit

class ChatTemplateViewController: UIViewController, ReasonsChoiceDelegate {

    
    let transitioner = CAVTransitioner()
    
    @IBOutlet weak var reasonsChoice: ReasonsChoice!
    var data: [Selectable] = []
    var codeValue:String = ""
    
    override init(nibName: String?, bundle: Bundle?) {
        super.init(nibName: nibName, bundle: bundle)
        
        self.modalPresentationStyle = .custom
        self.transitioningDelegate = self.transitioner
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    convenience init() {
        self.init(nibName:nil, bundle:nil)
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        for reason in DataNavigo.loginResult.arrReasons  {
            let points = VoteModel(title: reason.CancelNote!, code: reason.CancelCode!, isSelected: false, isUserSelectEnable: true)
            data.append(points)
        }
//        reasonsChoice.isRightToLeft = false
//        reasonsChoice.delegate = self
//        reasonsChoice.data = data
//        reasonsChoice.selectionType = .single
//        reasonsChoice.cellHeight = 50
//        reasonsChoice.arrowImage = nil
//        reasonsChoice.selectedImage = UIImage(named: "checked")
//        reasonsChoice.unselectedImage = UIImage(named: "unchecked")
    }
    
    func didSelectRowAt(indexPath: IndexPath) {
        
    }

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnAccept: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    @IBAction func accept(_ sender: Any) {
        self.presentingViewController?.dismiss(animated: true)
    }
    @IBAction func cancel(_ sender: Any) {
        self.presentingViewController?.dismiss(animated: true)
    }
    
}
