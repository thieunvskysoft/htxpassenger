//
//  VoteModel.swift
//  xelo
//
//  Created by Nguyen Thieu on 8/22/18.
//  Copyright © 2018 DUMV. All rights reserved.
//

import UIKit

class VoteModel: NSObject,Selectable {
    var title: String
    var code: String  
    var isSelected: Bool = false
    var isUserSelectEnable: Bool = true
    
    init(title:String,code:String, isSelected:Bool,isUserSelectEnable:Bool) {
        self.title = title
        self.code = code
        self.isSelected = isSelected
        self.isUserSelectEnable = isUserSelectEnable
    }
}
