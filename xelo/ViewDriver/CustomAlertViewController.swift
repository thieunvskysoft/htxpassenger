
import UIKit

class CustomAlertViewController : UIViewController {
    //var iapDelegate : IAPDelegate?
    var wayCheck:Bool = false
    let transitioner = CAVTransitioner()
    
    @IBOutlet weak var imgOnewayCheck: UIImageView!
    @IBOutlet weak var imgTwowayCheck: UIImageView!
    
    @IBOutlet weak var btnOneWay: UIButton!
    @IBOutlet weak var btnTwoWay: UIButton!
    @IBOutlet weak var lblTimeWaiting: UILabel!
    @IBOutlet weak var lblMinute: UILabel!
    @IBOutlet weak var btnConfirm: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    var ONE_WAY:String = "Một chiều"
    var TWO_WAY:String = "Hai chiều"
    var CONFIRM:String = "Xác nhận"
    var CANCEL:String = "Huỷ"
    var TIME_WAITING:String = "Thời gian chờ"
    var MINUTE:String = "Phút"
    override init(nibName: String?, bundle: Bundle?) {
        super.init(nibName: nibName, bundle: bundle)
        
        self.modalPresentationStyle = .custom
        self.transitioningDelegate = self.transitioner
        
    }
    override func viewDidLoad() {
        loadViewLanguage()
        if DataNavigo.booking.twoWays {
            wayCheck = true
            imgTwowayCheck.image = #imageLiteral(resourceName: "checkways")
            imgOnewayCheck.image = nil
            txtDuration.text = DataNavigo.booking.waitDuration?.description
        }else{
            txtDuration.isEnabled = false
        }
    }
    
    func loadViewLanguage() {
        let curren_language = UserDefaults.standard.string(forKey: "curren_language")
        let language = LanguageData()
        CONFIRM = language.localizedString(forKey:curren_language!,forKey: "CONFIRM")
        CANCEL = language.localizedString(forKey:curren_language!,forKey: "CANCEL")
        ONE_WAY = language.localizedString(forKey:curren_language!,forKey: "ONE_WAY")
        TWO_WAY = language.localizedString(forKey:curren_language!,forKey: "TWO_WAY")
        MINUTE = language.localizedString(forKey:curren_language!,forKey: "MINUTE")
        TIME_WAITING = language.localizedString(forKey:curren_language!,forKey: "TIME_WAITING")
        
        self.btnConfirm.setTitle(self.CONFIRM, for: .normal)
        self.btnCancel.setTitle(self.CANCEL, for: .normal)
        self.btnOneWay.setTitle(self.ONE_WAY, for: .normal)
        self.btnTwoWay.setTitle(self.TWO_WAY, for: .normal)
        lblTimeWaiting.text = TIME_WAITING
        lblMinute.text = MINUTE
    }
    
    @IBOutlet weak var txtDuration: UITextField!
    
    convenience init() {
        self.init(nibName:nil, bundle:nil)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("NSCoding not supported")
    }
    @IBAction func btnOneway(_ sender: Any) {
        imgOnewayCheck.image = #imageLiteral(resourceName: "checkways")
        imgTwowayCheck.image = nil
        txtDuration.isEnabled = false
        wayCheck = false
    }
    @IBAction func btnTwoway(_ sender: Any) {
        imgTwowayCheck.image = #imageLiteral(resourceName: "checkways")
        imgOnewayCheck.image = nil
        txtDuration.isEnabled = true
        wayCheck = true
        if txtDuration.text == "" {
            txtDuration.text = "30"
        }
    }
    
    @IBAction func doAccept(_ sender: Any) {
        if(wayCheck == true && txtDuration.text != ""){
            DataNavigo.booking.waitDuration = Int(txtDuration.text!)
        }else{
            
        }
        DataNavigo.booking.twoWays = wayCheck
        DataNavigo.ipaDelegate?.purchaseSuccessful()
        self.presentingViewController?.dismiss(animated: true)
        
    }
    
    
    @IBAction func doDismiss(_ sender:Any) {
        self.presentingViewController?.dismiss(animated: true)
    }
}
