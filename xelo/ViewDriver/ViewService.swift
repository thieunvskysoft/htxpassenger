//
//  ViewService.swift
//  NaviGo
//
//  Created by HoangManh on 9/29/17.
//  Copyright © 2017 DUMV. All rights reserved.
//

import UIKit

class ViewService: UIView {
    
    @IBOutlet var viewAll: UIView!
    @IBOutlet weak var viewService: UIView!
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var viewHidden: UIView!
    @IBOutlet weak var imgService: UIImageView!
    @IBOutlet weak var priceService: UILabel!
    @IBOutlet weak var nameService: UILabel!
    
    @IBOutlet weak var peak_img: UIImageView!
    @IBOutlet weak var normalPrice: UILabel!
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    @IBInspectable var borderColor: UIColor = UIColor.clear {
        didSet{
            setupView()
        }
    }
    // MARK: - Private Helper Methods
    
    // Performs the initial setup.
    fileprivate func setupView() {
        let view = viewFromNibForClass()
        priceService.sizeToFit()
        view.frame = bounds
        view.layer.cornerRadius = 5
        view.autoresizingMask = [
            .flexibleWidth,
            .flexibleHeight
        ]
        normalPrice.textColor = UIColor.darkGray
        nameService.center.y = imgService.center.y
        priceService.center.y = imgService.center.y
        nameService.sizeToFit()
        priceService.sizeToFit()
        viewTop.frame = CGRect(x: 50, y: 8, width: 40, height: 3)
        viewTop.center.x = view.center.x
        viewTop.layer.cornerRadius = 1.5
        viewHidden.layer.borderColor = borderColor.cgColor
        viewHidden.layer.borderWidth = 1
        viewHidden.layer.cornerRadius = 5
        viewService.layer.borderColor = borderColor.cgColor
        viewService.layer.borderWidth = 1
        viewService.layer.cornerRadius = 5
        viewAll.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0)
        //let imageData = try! Data(contentsOf: Bundle.main.url(forResource: "adventure-time", withExtension: "gif")!)
        //peak_img.image = UIImage.loadData(imageData)
        addSubview(view)
    }
    
    // Loads a XIB file into a view and returns this view.
    fileprivate func viewFromNibForClass() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    
}
