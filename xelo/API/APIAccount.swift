//
//  APIAcount.swift
//  testNavigo
//
//  Created by Linh  on 10/9/17.
//  Copyright © 2017 DUMV. All rights reserved.
//

import Foundation
import SwiftyJSON

class PassengerConnector{
    func getAuthenKey() {
        let key:String = "ABQjsaA138aA139091ABkshfoqhfldpqng27191nfd"
        let time:String = Int64(Date().timeIntervalSince1970*1000).description
        
        let anthenkey = DataNavigo.passenger.acountID + "-"+time+"-"+DataNavigo.passenger.deviceSerial+"-"+DataNavigo.passenger.securekey + "-" + key
        print("anthenkey1",anthenkey)
        let ss = MD5(anthenkey).lowercased()
        DataNavigo.passenger.authenkey = ss
        DataNavigo.passenger.time = time
        
    }
    var passenger:Passenger? = nil
    func loginAccount(passenger:Passenger,reconnect:Bool, CompletionHandler: @escaping (LoginResult, NavigoError?) -> Void)  {
        getAuthenKey()
        let curren_language = UserDefaults.standard.string(forKey: "curren_language")
        let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        let sysVersion = UIDevice.current.systemVersion
        let deviceName = UIDevice().type.rawValue
        
        var body:String = ""
        body += keyJson.auThenKey + "=" + passenger.authenkey
        body += "&" + keyJson.time + "=" + passenger.time+"&" + keyJson.F_ACCOUNT_ID + "="+passenger.acountID
        body += "&" + keyJson.F_DEVICES_SERIAL + "=" + passenger.deviceSerial
        body += "&" + keyJson.F_APP_OS + "=" + Constant.PLATFORM_IOS
        body += "&" + keyJson.F_APP_VERSION + "=" + version!
        body += "&" + keyJson.F_OS_VERSION + "=" + sysVersion
        body += "&" + keyJson.F_DEVICE_NAME + "=" + deviceName
        body += "&" + keyJson.F_TOKEN_FIREBASE + "=" + passenger.tokenID
        body += "&" + keyJson.F_LANGUAGE + "=" + curren_language!
        body += "&" + keyJson.F_RECONNECT + "=" + reconnect.description
        print("body",body)
        let myUrl = URL(string: URL_APP.URL_LOGIN)
        var request = URLRequest(url:myUrl!)
        request.httpMethod="POST"
        request.httpBody=body.data(using: String.Encoding.utf8, allowLossyConversion: false)
        
        let convert = Convert()
        var navigoError:NavigoError? = nil
        let loginResult = LoginResult()
        print("URL_ACCOUNT", URL_APP.URL_LOGIN + "?" + body)
        DispatchQueue.global(qos: .userInitiated).async {
            
            let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
                if error != nil{
                    navigoError = NavigoError()
                    navigoError?.errorCode = Errorcode.ERROR_KXD
                    navigoError?.errorMessage = error?.localizedDescription
                }else {
                    do {
                        if let data = data,
                            let   html = String(data: data, encoding: String.Encoding.utf8) {
                            let json = JSON(parseJSON: html)
                            print("loginAccount", json)
                            if !reconnect {
                                let seviceCode = json["serviceCode"].stringValue
                                var k:Int = 0
                                if let arrServices = json[keyJson.services].array {
                                    for services in arrServices  {
                                        if  services is JSON{
                                            let serviceN :Service = convert.jsonConvertService(json: services)
                                            if serviceN.serviceCode?.range(of: seviceCode) != nil{
                                                DataNavigo.serviceCar = k
                                            }
                                            k += 1
                                            loginResult.arrService.append(serviceN)
                                        }
                                    }
                                }
                                if let arrReason = json[keyJson.F_CANCEL_REASONS].array {
                                    for reason in arrReason  {
                                        if  reason is JSON{
                                            let points:CancelReasons = convert.jsonConvertReasons(json:reason)
                                            loginResult.arrReasons.append(points)
                                        }
                                    }
                                }
                                if let arrChat = json[keyJson.F_CHAT_TEMPLATE].array {
                                    for item in arrChat  {
                                        if  item is JSON{
                                            let points:ChatTemplates = convert.jsonConvertChatTemplates(json:item)
                                            loginResult.arrChatTemplate.append(points)
                                        }
                                    }
                                }
                                if let arrHomeAds = json[keyJson.F_HOME_ADS].array {
                                    for item in arrHomeAds  {
                                        if  item is JSON{
                                            let homeAds:HomeAds = convert.jsonConvertHomeAds(json:item)
                                            loginResult.arrHomeAds.append(homeAds)
                                        }
                                    }
                                }
                                if let arrAppItems = json[keyJson.F_APP_ITEMS].array {
                                    for item in arrAppItems  {
                                        if  item is JSON{
                                            let value:AppItem = convert.jsonConvertAppItem(json:item)
                                            loginResult.arrAppItem.append(value)
                                        }
                                    }
                                }
                                loginResult.tokenID = json[keyJson.F_TOKEN_ID].stringValue
                                loginResult.appUrl = json["appUrl"].stringValue
                                loginResult.hasNewVersion = json["hasNewVersion"].boolValue
                                loginResult.releaseNote = json[keyJson.F_RELEASE_NOTE].stringValue
                                loginResult.showShareAndBonus = json[keyJson.F_SHOW_SHARE_AND_BONUS].boolValue
                                loginResult.peakHour = json[keyJson.F_PEAK_HOUR].boolValue
                                loginResult.peakRate = json[keyJson.F_PEAK_RATE].doubleValue
                                loginResult.warningMessage = json[keyJson.F_WARNING_MESSAGE].stringValue
                                loginResult.googlePlaceApiKey = json["googlePlaceApiKey"].stringValue
                                let iconString = json[keyJson.F_AVATAR_FACE].stringValue
                                let data: NSData = NSData(base64Encoded: iconString , options: .ignoreUnknownCharacters)!
                                loginResult.avatarFace = UIImage(data: data as Data)
                                let authenByFirebase = json[keyJson.F_AUTHEN_BY_FIREBASE].boolValue
                                if authenByFirebase != nil {
                                    DataNavigo.passenger.authenByFirebase = authenByFirebase
                                }
                                let mobileNo = json["mobileNo"].stringValue
                                if mobileNo != "" {
                                    DataNavigo.passenger.numberphoneUser = mobileNo
                                }
                                DataNavigo.passenger.nameUser = json[keyJson.F_FULL_NAME].stringValue
                            }
                            loginResult.sessionID = json[keyJson.F_SESSION_ID].stringValue
                            var actionResult:String = ""
                            print("sessionID",json[keyJson.F_SESSION_ID].stringValue)
                            actionResult = json[keyJson.actionResult].stringValue
                            if actionResult.range(of: keyJson.F_OK) != nil {
                                self.passenger = passenger
                                if(json[keyJson.OBJ_BOOKING] != nil){
                                    loginResult.booking = convert.jsonConvertBook1(json: json)
                                }
                            } else {
                                navigoError = NavigoError()
                                navigoError?.errorCode = json[keyJson.errorCode].stringValue
                                navigoError?.errorMessage = json[Errorcode.ERROR_MESSAGE].stringValue
                            }
                        }
                    }catch {
                        navigoError = NavigoError()
                        navigoError?.errorCode = Errorcode.ERROR_NO_INTERNET
                        navigoError?.errorMessage = "Unknow error"
                    }
                }
                CompletionHandler(loginResult, navigoError)
            }
            task.resume()
        }
    }
    
    func reconnect(CompletionHandler: @escaping (LoginResult, NavigoError?) -> Void) {
        loginAccount(passenger: DataNavigo.passenger, reconnect: true, CompletionHandler: CompletionHandler)
    }
    
    func createAccount(passenger:Passenger, CompletionHandler: @escaping (Passenger, NavigoError?) -> Void) {
        var navigoError:NavigoError? = nil
        var body:String = ""
        body = keyJson.F_DEVICES_SERIAL + "=" + passenger.deviceSerial
        body = body + "&" + keyJson.F_FULL_NAME + "=" + passenger.nameUser
        body = body + "&" + keyJson.F_EMAIL + "=" + passenger.emailUser
        body = body + "&" + keyJson.F_REFER_MOBILE + "=" + passenger.referMobileNo
        body = body + "&" + keyJson.F_MOBILE_NO + "=" + passenger.numberphoneUser.addingPercentEncodingForQueryParameter()!
        body = body + "&" + keyJson.F_AUTHEN_BY_FIREBASE + "=true"
        body = body + "&" + keyJson.F_APP_OS + "=" + Constant.PLATFORM_IOS
        body = body + "&" + keyJson.F_APP_NAME + "=htx"
        let myUrl = URL(string: URL_APP.URL_CREATE_PASSENGER)
        var request = URLRequest(url:myUrl!)
        print("create account",body)
        request.httpMethod="POST"
        request.httpBody=body.data(using: String.Encoding.utf8, allowLossyConversion: false)
        let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            if error != nil{
                navigoError = NavigoError()
                navigoError?.errorCode = Errorcode.ERROR_NO_INTERNET
                navigoError?.errorMessage = error?.localizedDescription
            }else {
                do {
                    if let data = data,
                        let html = String(data: data, encoding: String.Encoding.utf8) {
                        let json = JSON(parseJSON: html)
                        print("json- create", json)
                        var actionResult:String = ""
                        actionResult = json[keyJson.actionResult].stringValue
                        if actionResult.range(of: keyJson.F_OK) != nil{
                            passenger.acountID = json[keyJson.F_ACCOUNT_ID].stringValue
                            passenger.accountExisting = json["accountExisting"].boolValue
                            passenger.authenByFirebase =  json[keyJson.F_AUTHEN_BY_FIREBASE].boolValue
                        }else{
                            navigoError = NavigoError()
                            navigoError?.errorCode = json[keyJson.errorCode].stringValue
                            navigoError?.errorMessage = json[Errorcode.ERROR_MESSAGE].stringValue
                        }
                    }
                } catch {
                    navigoError = NavigoError()
                    navigoError?.errorCode = Errorcode.ERROR_NO_INTERNET
                    navigoError?.errorMessage = "Unknow error"
                }
            }
            CompletionHandler(passenger,navigoError)
        }
        task.resume()
    }
    
    func activeAccount(passenger:Passenger ,CompletionHandler: @escaping (Passenger, NavigoError?) -> Void,activeKey:String) {
        var navigoError:NavigoError? = nil
        var body:String = ""
        body = keyJson.F_ACCOUNT_ID + "=" + passenger.acountID
        body = body + "&" + keyJson.F_DEVICES_SERIAL + "=" + passenger.deviceSerial
        body = body + "&" + keyJson.F_ACTIVES_KEY + "=" + activeKey
        
        let myUrl = URL(string: URL_APP.URL_ACTIVE_PASSENGER)
        var request = URLRequest(url:myUrl!)
        request.httpMethod="POST"
        request.httpBody=body.data(using: String.Encoding.utf8, allowLossyConversion: false)
        let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            if error != nil{
                navigoError = NavigoError()
                navigoError?.errorCode = Errorcode.ERROR_NO_INTERNET
                navigoError?.errorMessage = error?.localizedDescription
            } else {
                do {
                    if let data = data,
                        let   html = String(data: data, encoding: String.Encoding.utf8) {
                        let json = JSON(parseJSON: html)
                        print("json",json)
                        var actionResult:String = ""
                        actionResult = json[keyJson.actionResult].stringValue
                        if actionResult.range(of: keyJson.F_OK) != nil{
                            passenger.acountID = (json[keyJson.F_ACCOUNT_ID].stringValue)
                            passenger.securekey = (json[keyJson.F_SECURE_KEY].stringValue)
                            print("securekey",json[keyJson.F_SECURE_KEY].stringValue)
                        }else{
                            navigoError = NavigoError()
                            navigoError?.errorCode = json[keyJson.errorCode].stringValue
                            navigoError?.errorMessage = json[Errorcode.ERROR_MESSAGE].stringValue
                        }
                    }
                } catch {
                    navigoError = NavigoError()
                    navigoError?.errorCode = Errorcode.ERROR_NO_INTERNET
                    navigoError?.errorMessage = "Unknow error"
                }
                CompletionHandler(passenger,navigoError)
            }
        }
        task.resume()
    }
    
    func activeAccountByFirebase(passenger:Passenger,uID:String, tokenLogin:String ,CompletionHandler: @escaping (Passenger, NavigoError?) -> Void,activeKey:String) {
        var navigoError:NavigoError? = nil
        var body:String = ""
        body = keyJson.F_ACCOUNT_ID + "=" + passenger.acountID
        body = body + "&" + keyJson.F_DEVICES_SERIAL + "=" + passenger.deviceSerial
        body = body + "&" + keyJson.F_ACTIVES_KEY + "=" + activeKey
        body = body + "&" + keyJson.F_TOKEN_ID + "=" + tokenLogin
        body = body + "&" + "uid" + "=" + uID
        print("body",body)
        let myUrl = URL(string: URL_APP.URL_ACTIVE_PASSENGER)
        var request = URLRequest(url:myUrl!)
        request.httpMethod="POST"
        request.httpBody=body.data(using: String.Encoding.utf8, allowLossyConversion: false)
        let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            if error != nil{
                navigoError = NavigoError()
                navigoError?.errorCode = Errorcode.ERROR_NO_INTERNET
                navigoError?.errorMessage = error?.localizedDescription
            } else {
                do {
                    if let data = data,
                        let   html = String(data: data, encoding: String.Encoding.utf8) {
                        let json = JSON(parseJSON: html)
                        print("json",json)
                        var actionResult:String = ""
                        actionResult = json[keyJson.actionResult].stringValue
                        if actionResult.range(of: keyJson.F_OK) != nil{
                            passenger.acountID = (json[keyJson.F_ACCOUNT_ID].stringValue)
                            passenger.securekey = (json[keyJson.F_SECURE_KEY].stringValue)
                            print("securekey",json[keyJson.F_SECURE_KEY].stringValue)
                        }else{
                            navigoError = NavigoError()
                            navigoError?.errorCode = json[keyJson.errorCode].stringValue
                            navigoError?.errorMessage = json[Errorcode.ERROR_MESSAGE].stringValue
                        }
                    }
                } catch {
                    navigoError = NavigoError()
                    navigoError?.errorCode = Errorcode.ERROR_NO_INTERNET
                    navigoError?.errorMessage = "Unknow error"
                }
                CompletionHandler(passenger,navigoError)
            }
        }
        task.resume()
    }
    
    func reActiveAccount(passenger:Passenger ,CompletionHandler: @escaping (Passenger, NavigoError?) -> Void) {
        var navigoError:NavigoError? = nil
        var body:String = ""
        body = keyJson.F_DEVICES_SERIAL + "=" + passenger.deviceSerial
        body = body + "&" + keyJson.F_FULL_NAME + "=" + passenger.nameUser
        body = body + "&" + keyJson.F_EMAIL + "=" + passenger.emailUser
        body = body + "&" + keyJson.F_MOBILE_NO + "=" + passenger.numberphoneUser.addingPercentEncodingForQueryParameter()!
        body = body + "&" + keyJson.F_APP_OS + "=" + Constant.PLATFORM_IOS
        body = body + "&" + keyJson.F_APP_NAME + "=htx"
        print("body",body)
        let myUrl = URL(string: URL_APP.URL_REACTIVE_PASSENGER)
        var request = URLRequest(url:myUrl!)
        request.httpMethod="POST"
        request.httpBody=body.data(using: String.Encoding.utf8, allowLossyConversion: false)
        let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            if error != nil {
                navigoError = NavigoError()
                navigoError?.errorCode = Errorcode.ERROR_NO_INTERNET
                navigoError?.errorMessage = error?.localizedDescription
            } else {
                do {
                    if let data = data,
                        let   html = String(data: data, encoding: String.Encoding.utf8) {
                        let json = JSON(parseJSON: html)
                        print("reActiveAccount",json)
                        var actionResult:String = ""
                        actionResult = json[keyJson.actionResult].stringValue
                        
                        if actionResult.range(of: keyJson.F_OK) != nil{
                            passenger.acountID = (json[keyJson.F_ACCOUNT_ID].stringValue)
                            passenger.securekey = (json[keyJson.F_SECURE_KEY].stringValue)
                        } else {
                            navigoError = NavigoError()
                            navigoError?.errorCode = json[keyJson.errorCode].stringValue
                            navigoError?.errorMessage = json[Errorcode.ERROR_MESSAGE].stringValue
                        }
                    }
                } catch {
                    navigoError = NavigoError()
                    navigoError?.errorCode = Errorcode.ERROR_NO_INTERNET
                    navigoError?.errorMessage = "Unknow error"
                }
            }
            CompletionHandler(passenger, navigoError)
        }
        task.resume()
    }
    
    func updatePassenger(fullName:String, email:String, CompletionHandler: @escaping (String, NavigoError?) -> Void) {
        var navigoError:NavigoError? = nil
        var body:String = ""
        body = keyJson.F_FULL_NAME + "=" + fullName
        body = body + "&" + keyJson.F_EMAIL + "=" + email
        let myUrl = URL(string: URL_APP.URL_UPDATE_PASSENGER)
        var request = URLRequest(url:myUrl!)
        request.httpMethod="POST"
        request.httpBody=body.data(using: String.Encoding.utf8, allowLossyConversion: false)
        let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            if error != nil{
                navigoError = NavigoError()
                navigoError?.errorCode = Errorcode.ERROR_NO_INTERNET
                navigoError?.errorMessage = error?.localizedDescription
            }else {
                do {
                    if let data = data,
                        let html = String(data: data, encoding: String.Encoding.utf8) {
                        let json = JSON(parseJSON: html)
                        print("json- create", json)
                        var actionResult:String = ""
                        actionResult = json[keyJson.actionResult].stringValue
                        if actionResult.range(of: keyJson.F_OK) != nil{
                            //                            passenger.acountID = json[keyJson.F_ACCOUNT_ID].stringValue
                            //                            passenger.accountExisting = json["accountExisting"].boolValue
                            //                            passenger.authenByFirebase =  json[keyJson.F_AUTHEN_BY_FIREBASE].boolValue
                        }else{
                            navigoError = NavigoError()
                            navigoError?.errorCode = json[keyJson.errorCode].stringValue
                            navigoError?.errorMessage = json[Errorcode.ERROR_MESSAGE].stringValue
                        }
                    }
                } catch {
                    navigoError = NavigoError()
                    navigoError?.errorCode = Errorcode.ERROR_NO_INTERNET
                    navigoError?.errorMessage = "Unknow error"
                }
            }
            CompletionHandler("",navigoError)
        }
        task.resume()
    }
    
    func getPassenger(CompletionHandler: @escaping (Passenger, NavigoError?) -> Void) {
        var navigoError:NavigoError? = nil
        let body:String = ""
        let myUrl = URL(string: URL_APP.URL_GET_PASSENGER)
        let passengerInfo = Passenger()
        _ = Convert()
        var request = URLRequest(url:myUrl!)
        request.httpMethod="GET"
        request.httpBody=body.data(using: String.Encoding.utf8, allowLossyConversion: false)
        let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            if error != nil {
                navigoError = NavigoError()
                navigoError?.errorCode = Errorcode.ERROR_NO_INTERNET
                navigoError?.errorMessage = error?.localizedDescription
            } else {
                do {
                    if let data = data,
                        let   html = String(data: data, encoding: String.Encoding.utf8) {
                        let json = JSON(parseJSON: html)
                        print("passengerInfo",json)
                        var actionResult:String = ""
                        actionResult = json[keyJson.actionResult].stringValue
                        
                        if actionResult.range(of: keyJson.F_OK) != nil{
                            //driverInfo =  convert.jsonConvertDriverInfo(json: json)
                            passengerInfo.nameUser = json[keyJson.OBJ_PASSENGER][keyJson.F_FULL_NAME].stringValue
                            passengerInfo.numberphoneUser = json[keyJson.OBJ_PASSENGER][keyJson.F_MOBILE_NO].stringValue
                            passengerInfo.emailUser = json[keyJson.OBJ_PASSENGER][keyJson.F_EMAIL].stringValue
                            
                        } else {
                            navigoError = NavigoError()
                            navigoError?.errorCode = json[keyJson.errorCode].stringValue
                            navigoError?.errorMessage = json[Errorcode.ERROR_MESSAGE].stringValue
                        }
                    }
                } catch {
                    navigoError = NavigoError()
                    navigoError?.errorCode = Errorcode.ERROR_NO_INTERNET
                    navigoError?.errorMessage = "Unknow error"
                }
            }
            CompletionHandler(passengerInfo,navigoError)
        }
        task.resume()
    }
    
    func getListServices(type:String ,CompletionHandler: @escaping (Array<Service> ,NavigoError?) -> Void)  {
        var body:String = ""
        body += keyJson.F_TYPE + "=" + type
        var arrService:Array<Service> = Array()
        let convert = Convert()
        let myUrl = URL(string: URL_APP.URL_LIST_SERVICES)
        var request = URLRequest(url:myUrl!)
        print("URL_ACCOUNT", URL_APP.URL_LIST_SERVICES + "?" + body)
        request.httpMethod="POST"
        request.httpBody=body.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var navigoError:NavigoError? = nil
        DispatchQueue.global(qos: .userInitiated).async {
            let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
                if error != nil{
                    navigoError = NavigoError()
                    navigoError?.errorCode = Errorcode.ERROR_NO_INTERNET
                    navigoError?.errorMessage = error?.localizedDescription
                } else {
                    do {
                        if let data = data,
                            let   html = String(data: data, encoding: String.Encoding.utf8) {
                            let json = JSON(parseJSON: html)
                            print("json arrService",json)
                            let actionResult = json[keyJson.actionResult].stringValue
                            if actionResult.range(of: keyJson.F_OK) != nil {
                                let seviceCode = json["serviceCode"].stringValue
                                var k:Int = 0
                                if let arrServices = json[keyJson.services].array {
                                    for services in arrServices  {
                                        if  services is JSON{
                                            let serviceN :Service = convert.jsonConvertService(json: services)
                                            if serviceN.serviceCode?.range(of: seviceCode) != nil{
                                                DataNavigo.serviceCar = k
                                            }
                                            k += 1
                                            arrService.append(serviceN)
                                        }
                                    }
                                }
                            } else {
                                navigoError = NavigoError()
                                navigoError?.errorCode = json[keyJson.errorCode].stringValue
                                navigoError?.errorMessage = json[Errorcode.ERROR_MESSAGE].stringValue
                            }
                        }
                    } catch {
                        navigoError = NavigoError()
                        navigoError?.errorCode = Errorcode.ERROR_NO_INTERNET
                        navigoError?.errorMessage = "Unknow error"
                    }
                }
                CompletionHandler(arrService, navigoError)
            }
            task.resume()
        }
    }
    
    func storeAvatar(imgAvata:UIImage,CompletionHandler: @escaping (String, NavigoError?) -> Void) {
        var navigoError:NavigoError? = nil
        let imageData:NSData = UIImageJPEGRepresentation(imgAvata, 1.0)! as NSData
        let strBase64 = imageData.base64EncodedString(options: .endLineWithLineFeed)
        var body:String = ""
        body = keyJson.F_PHOTO_DATA + "=" + strBase64.addingPercentEncodingForQueryParameter()!
        let myUrl = URL(string: URL_APP.URL_STORE_AVATAR)
        var actionResult:String = ""
        var request = URLRequest(url:myUrl!)
        request.httpMethod="POST"
        request.httpBody=body.data(using: String.Encoding.utf8, allowLossyConversion: false)
        let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            if error != nil {
                navigoError = NavigoError()
                navigoError?.errorCode = Errorcode.ERROR_NO_INTERNET
                navigoError?.errorMessage = error?.localizedDescription
            } else {
                do {
                    if let data = data,
                        let   html = String(data: data, encoding: String.Encoding.utf8) {
                        let json = JSON(parseJSON: html)
                        print("passengerInfo",json)
                        
                        actionResult = json[keyJson.actionResult].stringValue
                        
                        if actionResult.range(of: keyJson.F_OK) != nil{
                            
                            
                        } else {
                            navigoError = NavigoError()
                            navigoError?.errorCode = json[keyJson.errorCode].stringValue
                            navigoError?.errorMessage = json[Errorcode.ERROR_MESSAGE].stringValue
                        }
                    }
                } catch {
                    navigoError = NavigoError()
                    navigoError?.errorCode = Errorcode.ERROR_NO_INTERNET
                    navigoError?.errorMessage = "Unknow error"
                }
            }
            CompletionHandler(actionResult,navigoError)
        }
        task.resume()
    }
    
}

public enum Model : String {
    case simulator     = "simulator/sandbox",
    iPod1              = "iPod 1",
    iPod2              = "iPod 2",
    iPod3              = "iPod 3",
    iPod4              = "iPod 4",
    iPod5              = "iPod 5",
    iPad2              = "iPad 2",
    iPad3              = "iPad 3",
    iPad4              = "iPad 4",
    iPad5              = "iPad 5",
    iPhone4            = "iPhone 4",
    iPhone4S           = "iPhone 4S",
    iPhone5            = "iPhone 5",
    iPhone5S           = "iPhone 5S",
    iPhone5C           = "iPhone 5C",
    iPadMini1          = "iPad Mini 1",
    iPadMini2          = "iPad Mini 2",
    iPadMini3          = "iPad Mini 3",
    iPadAir1           = "iPad Air 1",
    iPadAir2           = "iPad Air 2",
    iPadPro9_7         = "iPad Pro 9.7\"",
    iPadPro9_7_cell    = "iPad Pro 9.7\" cellular",
    iPadPro12_9        = "iPad Pro 12.9\"",
    iPadPro12_9_cell   = "iPad Pro 12.9\" cellular",
    iPadPro2_12_9      = "iPad Pro 2 12.9\"",
    iPadPro2_12_9_cell = "iPad Pro 2 12.9\" cellular",
    iPhone6            = "iPhone 6",
    iPhone6plus        = "iPhone 6 Plus",
    iPhone6S           = "iPhone 6S",
    iPhone6Splus       = "iPhone 6S Plus",
    iPhoneSE           = "iPhone SE",
    iPhone7            = "iPhone 7",
    iPhone7plus        = "iPhone 7 Plus",
    iPhone8            = "iPhone 8",
    iPhone8plus        = "iPhone 8 Plus",
    iPhoneX            = "iPhone X",
    unrecognized       = "?unrecognized?"
}

// #-#-#-#-#-#-#-#-#-#-#-#-#-#-#
//MARK: UIDevice extensions
// #-#-#-#-#-#-#-#-#-#-#-#-#-#-#

public extension UIDevice {
    public var type: Model {
        var systemInfo = utsname()
        uname(&systemInfo)
        let modelCode = withUnsafePointer(to: &systemInfo.machine) {
            $0.withMemoryRebound(to: CChar.self, capacity: 1) {
                ptr in String.init(validatingUTF8: ptr)
                
            }
        }
        var modelMap : [ String : Model ] = [
            "i386"      : .simulator,
            "x86_64"    : .simulator,
            "iPod1,1"   : .iPod1,
            "iPod2,1"   : .iPod2,
            "iPod3,1"   : .iPod3,
            "iPod4,1"   : .iPod4,
            "iPod5,1"   : .iPod5,
            "iPad2,1"   : .iPad2,
            "iPad2,2"   : .iPad2,
            "iPad2,3"   : .iPad2,
            "iPad2,4"   : .iPad2,
            "iPad2,5"   : .iPadMini1,
            "iPad2,6"   : .iPadMini1,
            "iPad2,7"   : .iPadMini1,
            "iPhone3,1" : .iPhone4,
            "iPhone3,2" : .iPhone4,
            "iPhone3,3" : .iPhone4,
            "iPhone4,1" : .iPhone4S,
            "iPhone5,1" : .iPhone5,
            "iPhone5,2" : .iPhone5,
            "iPhone5,3" : .iPhone5C,
            "iPhone5,4" : .iPhone5C,
            "iPad3,1"   : .iPad3,
            "iPad3,2"   : .iPad3,
            "iPad3,3"   : .iPad3,
            "iPad3,4"   : .iPad4,
            "iPad3,5"   : .iPad4,
            "iPad3,6"   : .iPad4,
            "iPhone6,1" : .iPhone5S,
            "iPhone6,2" : .iPhone5S,
            "iPad4,2"   : .iPadAir1,
            "iPad5,4"   : .iPadAir2,
            "iPad4,4"   : .iPadMini2,
            "iPad4,5"   : .iPadMini2,
            "iPad4,6"   : .iPadMini2,
            "iPad4,7"   : .iPadMini3,
            "iPad4,8"   : .iPadMini3,
            "iPad4,9"   : .iPadMini3,
            "iPad6,3"   : .iPadPro9_7,
            "iPad6,4"   : .iPadPro9_7_cell,
            "iPad6,12"  : .iPad5,
            "iPad6,7"   : .iPadPro12_9,
            "iPad6,8"   : .iPadPro12_9_cell,
            "iPad7,1"   : .iPadPro2_12_9,
            "iPad7,2"   : .iPadPro2_12_9_cell,
            "iPhone7,1" : .iPhone6plus,
            "iPhone7,2" : .iPhone6,
            "iPhone8,1" : .iPhone6S,
            "iPhone8,2" : .iPhone6Splus,
            "iPhone8,4" : .iPhoneSE,
            "iPhone9,1" : .iPhone7,
            "iPhone9,2" : .iPhone7plus,
            "iPhone9,3" : .iPhone7,
            "iPhone9,4" : .iPhone7plus,
            "iPhone10,1" : .iPhone8,
            "iPhone10,2" : .iPhone8plus,
            "iPhone10,3" : .iPhoneX,
            "iPhone10,4" : .iPhone8,
            "iPhone10,5" : .iPhone8plus,
            "iPhone10,6" : .iPhoneX
        ]
        
        if let model = modelMap[String.init(validatingUTF8: modelCode!)!] {
            if model == .simulator {
                if let simModelCode = ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] {
                    if let simModel = modelMap[String.init(validatingUTF8: simModelCode)!] {
                        return simModel
                    }
                }
            }
            return model
        }
        return Model.unrecognized
    }
}
