//
//  APIBusTrip.swift
//  xelo
//
//  Created by Nguyen Thieu on 3/22/19.
//  Copyright © 2019 DUMV. All rights reserved.
//

import Foundation
import SwiftyJSON

class APIBusTrip{
    
    func SearchBusTripResult(fromPosition:Point, toPosition:Point , datetime:String,CompletionHandler: @escaping (Array<BusTrip>, NavigoError?) ->Void)  {
        var body:String=""
        //body = keyJson.action + "=" + Methods.F_SEARCH_BUS_TRIP
        body += keyJson.F_FROM_Y + "=" + (fromPosition.y?.description)!
        body += "&" + keyJson.F_FROM_X + "=" + (fromPosition.x?.description)!
        body += "&" + keyJson.F_TO_Y + "=" + (toPosition.y?.description)!
        body += "&" + keyJson.F_TO_X + "=" + (toPosition.x?.description)!
        body += "&" + keyJson.F_DATE + "=" + datetime.addingPercentEncodingForQueryParameter()!
        print("body",body)
        let myUrl = URL(string: URL_APP.URL_SEARCH_BUS_TRIPS)
        var request = URLRequest(url:myUrl!)
        var navigoError:NavigoError? = nil
        request.httpMethod="POST"
        request.httpBody=body.data(using: String.Encoding.utf8, allowLossyConversion: false)
        let convert = Convert()
        var arrBusTrips: Array<BusTrip> = Array()
        let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            if error != nil{
                navigoError = NavigoError()
                navigoError?.errorCode = Errorcode.ERROR_NO_INTERNET
                navigoError?.errorMessage = error?.localizedDescription
            } else {
                do {
                    if let data = data,
                        let   html = String(data: data, encoding: String.Encoding.utf8) {
                        let json = JSON(parseJSON: html)
                        print("SearchBusTripResult",json)
                        let actionResult:String = json[keyJson.actionResult].stringValue
                        if(actionResult.contains(keyJson.F_OK)){
                            if let arrBusTrip = json[keyJson.F_BUS_TRIPS].array {
                                for bus in arrBusTrip  {
                                    if  bus is JSON{
                                        let busTrip :BusTrip = convert.jsonConvertBusTrips(jsons: bus)
                                        arrBusTrips.append(busTrip)
                                    }
                                }
                            }
                        }else{
                            navigoError = NavigoError()
                            navigoError?.errorCode = json[keyJson.errorCode].stringValue
                            navigoError?.errorMessage = json[Errorcode.ERROR_MESSAGE].stringValue
                        }
                    }
                } catch {
                    navigoError = NavigoError()
                    navigoError?.errorCode = Errorcode.ERROR_NO_INTERNET
                    navigoError?.errorMessage = "Unknow error"
                }
            }
            CompletionHandler(arrBusTrips,navigoError)
        }
        task.resume()
    }
    
    func getBusTrip(tripID:String,CompletionHandler: @escaping (BusTripResult, NavigoError?) ->Void)  {
        var body:String=""
        //body = keyJson.action + "=" + Methods.F_GET_BUS_TRIP
        body += keyJson.F_TRIP_ID + "=" + tripID
        print("body",body)
        let myUrl = URL(string: URL_APP.URL_GET_BUS_TRIP)
        var request = URLRequest(url:myUrl!)
        var navigoError:NavigoError? = nil
        request.httpMethod="POST"
        request.httpBody=body.data(using: String.Encoding.utf8, allowLossyConversion: false)
        let convert = Convert()
        let busTripResult = BusTripResult()
        let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            if error != nil{
                navigoError = NavigoError()
                navigoError?.errorCode = Errorcode.ERROR_NO_INTERNET
                navigoError?.errorMessage = error?.localizedDescription
            } else {
                do {
                    if let data = data,
                        let   html = String(data: data, encoding: String.Encoding.utf8) {
                        let json = JSON(parseJSON: html)
                        print("SearchBusTripResult",json)
                        let actionResult:String = json[keyJson.actionResult].stringValue
                        if(actionResult.contains(keyJson.F_OK)){
                            let busTrip :BusTrip = convert.jsonConvertBusTrips(jsons: json[keyJson.F_BUS_TRIP])
                            busTripResult.busTrip = busTrip
                            
                            if let arrBusPlace = json[keyJson.F_PLACES].array {
                                for busPlace in arrBusPlace  {
                                    if  busPlace is JSON{
                                        let place :Place = convert.jsonConvertBusPlace(json: busPlace)
                                        busTripResult.places.append(place)
                                    }
                                }
                            }
                            if let arrBusTicket = json[keyJson.F_TICKET_FARES].array {
                                for busTicket in arrBusTicket  {
                                    if  busTicket is JSON{
                                        let ticket :TicketFares = convert.jsonConvertTicketFares(jsons: busTicket)
                                        busTripResult.ticketFares.append(ticket)
                                    }
                                }
                            }
                        }else{
                            navigoError = NavigoError()
                            navigoError?.errorCode = json[keyJson.errorCode].stringValue
                            navigoError?.errorMessage = json[Errorcode.ERROR_MESSAGE].stringValue
                        }
                    }
                } catch {
                    navigoError = NavigoError()
                    navigoError?.errorCode = Errorcode.ERROR_NO_INTERNET
                    navigoError?.errorMessage = "Unknow error"
                }
            }
            CompletionHandler(busTripResult,navigoError)
        }
        task.resume()
    }
    
    func bookingBusTicket(busTrip:BusTrip,currentPosition:Point,arrQuantityTicket:Array<TicketFares>,CompletionHandler: @escaping (BusTrip, NavigoError?) ->Void)  {
        var body:String=""
        //body = keyJson.action + "=" +  Methods.F_BOOKING_BUS_TICKET
        body += keyJson.F_TRIP_ID + "=" + busTrip.tripID
        body += "&" + keyJson.F_PICKUP_Y + "=" + (busTrip.pickupY?.description)!
        body += "&" + keyJson.F_PICKUP_X + "=" + (busTrip.pickupX?.description)!
        body += "&" + keyJson.F_DROP_OFF_Y + "=" + (busTrip.dropOffY?.description)!
        body += "&" + keyJson.F_DROP_OFF_X + "=" + (busTrip.dropOffX?.description)!
        body += "&" + keyJson.F_X + "=" + (currentPosition.x?.description)!
        body += "&" + keyJson.F_Y + "=" + (currentPosition.y?.description)!
        body += "&" + keyJson.F_FROM_ADDRESS + "=" + busTrip.fromAddress
        body += "&" + keyJson.F_TO_ADDRESS + "=" + busTrip.toAddress
        for item in arrQuantityTicket {
            if item.ticketID == 1 {
                body += "&" + keyJson.F_QUANTITY_1 + "=" + item.choice.description
            }
            if item.ticketID == 2 {
                body += "&" + keyJson.F_QUANTITY_2 + "=" + item.choice.description
            }
            if item.ticketID == 3 {
                body += "&" + keyJson.F_QUANTITY_3 + "=" + item.choice.description
            }
        }
        print("body",body)
        let myUrl = URL(string: URL_APP.URL_BOOKING_BUS_TICKET)
        var request = URLRequest(url:myUrl!)
        var navigoError:NavigoError? = nil
        request.httpMethod="POST"
        request.httpBody=body.data(using: String.Encoding.utf8, allowLossyConversion: false)
        let convert = Convert()
        var busTrip = BusTrip()
        let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            if error != nil{
                navigoError = NavigoError()
                navigoError?.errorCode = Errorcode.ERROR_NO_INTERNET
                navigoError?.errorMessage = error?.localizedDescription
            } else {
                do {
                    if let data = data,
                        let   html = String(data: data, encoding: String.Encoding.utf8) {
                        let json = JSON(parseJSON: html)
                        print("bookingBusTicket",json)
                        let actionResult:String = json[keyJson.actionResult].stringValue
                        if(actionResult.contains(keyJson.F_OK)){
                            busTrip = convert.jsonConvertBusTrips(jsons: json[keyJson.F_BUS_TICKET])
                        }else{
                            navigoError = NavigoError()
                            navigoError?.errorCode = json[keyJson.errorCode].stringValue
                            navigoError?.errorMessage = json[Errorcode.ERROR_MESSAGE].stringValue
                        }
                    }
                } catch {
                    navigoError = NavigoError()
                    navigoError?.errorCode = Errorcode.ERROR_NO_INTERNET
                    navigoError?.errorMessage = "Unknow error"
                }
            }
            CompletionHandler(busTrip, navigoError)
        }
        task.resume()
    }
    
    func listHistoryTickets(CompletionHandler: @escaping (Array<BusTrip>, NavigoError?) ->Void)  {
        var body:String=""
        //body = keyJson.action + "=" + Methods.F_LIST_HISTORY_TICKET
        //body += "&" + keyJson.F_TRIP_ID + "=" + tripID
        let myUrl = URL(string: URL_APP.URL_LIST_HISTORY_TICKETS)
        var request = URLRequest(url:myUrl!)
        var navigoError:NavigoError? = nil
        request.httpMethod="GET"
        request.httpBody=body.data(using: String.Encoding.utf8, allowLossyConversion: false)
        let convert = Convert()
        var arrBusTrips: Array<BusTrip> = Array()
        let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            if error != nil{
                navigoError = NavigoError()
                navigoError?.errorCode = Errorcode.ERROR_NO_INTERNET
                navigoError?.errorMessage = error?.localizedDescription
            } else {
                do {
                    if let data = data,
                        let   html = String(data: data, encoding: String.Encoding.utf8) {
                        let json = JSON(parseJSON: html)
                        print("listHistoryTickets",json)
                        let actionResult:String = json[keyJson.actionResult].stringValue
                        if(actionResult.contains(keyJson.F_OK)){
                            if let arrBusTrip = json[keyJson.F_TICKETS].array {
                                for bus in arrBusTrip  {
                                    if  bus is JSON{
                                        let busTrip :BusTrip = convert.jsonConvertBusTrips(jsons: bus)
                                        arrBusTrips.append(busTrip)
                                    }
                                }
                            }
                        }else{
                            navigoError = NavigoError()
                            navigoError?.errorCode = json[keyJson.errorCode].stringValue
                            navigoError?.errorMessage = json[Errorcode.ERROR_MESSAGE].stringValue
                        }
                    }
                } catch {
                    navigoError = NavigoError()
                    navigoError?.errorCode = Errorcode.ERROR_NO_INTERNET
                    navigoError?.errorMessage = "Unknow error"
                }
            }
            CompletionHandler(arrBusTrips, navigoError)
        }
        task.resume()
    }
}

