//
//  APIWallet.swift
//  xelo
//
//  Created by Nguyen Thieu on 11/17/18.
//  Copyright © 2018 DUMV. All rights reserved.
//

import Foundation
import Foundation
import SwiftyJSON

class APIWallet {
    
    func DateTime() -> String {
        let timeFormat = "yyyy-MM-dd\'T\'"
        let date : Date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = timeFormat
        var datetime = dateFormatter.string(from: date)
        
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: date)
        let minutes = calendar.component(.minute, from: date)
        let second = calendar.component(.second, from: date)
        return datetime + hour.description + ":" + minutes.description + ":" + second.description + "+0700"
    }
    
    func listPromoTrans(CompletionHandler: @escaping (Array<Transactions>, WalletInfo, NavigoError?) ->Void) {
        var body:String=""
        //body = keyJson.action + "=" + keyJson.F_LIST_PROMO_TRANS
        body += keyJson.F_START_DATE + "=" + DateTime().addingPercentEncodingForQueryParameter()!
        let myUrl = URL(string: URL_APP.URL_LIST_PROMO_TRANS)
        var navigoError:NavigoError? = nil
        var request = URLRequest(url:myUrl!)
        let convert = Convert()
        let walletInfo = WalletInfo()
        var arrTransactions = [Transactions]()
        request.httpMethod="POST"
        request.httpBody=body.data(using: String.Encoding.utf8, allowLossyConversion: false)
        let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            if error != nil{
                navigoError = NavigoError()
                navigoError?.errorCode = Errorcode.ERROR_NO_INTERNET
                navigoError?.errorMessage = error?.localizedDescription
            } else {
                do {
                    if let data = data,
                        let   html = String(data: data, encoding: String.Encoding.utf8) {
                        let json = JSON(parseJSON: html)
                        print("F_LIST_FEE_TRANS",json)
                        let actionResult:String = json[keyJson.actionResult].stringValue

                        if(actionResult.contains(keyJson.F_OK)){
                            walletInfo.promoWallet = json[keyJson.F_PROMO_WALLET].intValue
                            let promoDate = json[keyJson.F_PROMO_EXPIRE_DATE].stringValue
                            
                            walletInfo.promoExpireDate = convert.convertDatetimeByDateformat(datetime: promoDate, dateFormat: "dd/MM/yyyy HH:mm:ss", value: 1)
                            
                            walletInfo.depositWallet = json[keyJson.F_DEPOSIT_WALLET].intValue
                            if let arrTrans = json["transactions"].array{
                                for transactions in arrTrans  {
                                    if transactions is JSON{
                                        let trans :Transactions = convert.jsonConvertTransactions(jsons: transactions)
                                        arrTransactions.append(trans)
                                    }
                                }
                            }
                            
                        }else{
                            navigoError = NavigoError()
                            navigoError?.errorCode = json[keyJson.errorCode].stringValue
                            navigoError?.errorMessage = json[Errorcode.ERROR_MESSAGE].stringValue
                        }
                    }
                } catch {
                    navigoError = NavigoError()
                    navigoError?.errorCode = Errorcode.ERROR_NO_INTERNET
                    navigoError?.errorMessage = "Unknow error"
                }
            }
            CompletionHandler(arrTransactions,walletInfo, navigoError)
        }
        task.resume()
    }
}
