//
//  XeloGameAPI.swift
//  xelo
//
//  Created by Nguyen Thieu on 7/5/19.
//  Copyright © 2019 DUMV. All rights reserved.
//

import Foundation
import SwiftyJSON

class APIXeloGame{
    
    
    func getLuckyGame(CompletionHandler: @escaping (LuckyGame, NavigoError?) ->Void)  {
        var body:String=""
        //body = keyJson.action + "=" + Methods.F_GET_LUCKY_GAME
        print("body",body)
        let myUrl = URL(string: URL_APP.URL_GET_LUCKY_GAME)
        var request = URLRequest(url:myUrl!)
        var navigoError:NavigoError? = nil
        request.httpMethod="POST"
        request.httpBody=body.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var actionResult:String = ""
        let convert = Convert()
        var gameInfo = LuckyGame()
        let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            if error != nil{
                navigoError = NavigoError()
                navigoError?.errorCode = Errorcode.ERROR_NO_INTERNET
                navigoError?.errorMessage = error?.localizedDescription
            } else {
                do {
                    if let data = data,
                        let   html = String(data: data, encoding: String.Encoding.utf8) {
                        let json = JSON(parseJSON: html)
                        print("getGameInfo",json)
                        actionResult = json[keyJson.actionResult].stringValue
                        if(actionResult.contains(keyJson.F_OK)){
                            gameInfo = convert.jsonConvertGame(json: json[keyJson.F_GAME])
                        }else{
                            navigoError = NavigoError()
                            navigoError?.errorCode = json[keyJson.errorCode].stringValue
                            navigoError?.errorMessage = json[Errorcode.ERROR_MESSAGE].stringValue
                        }
                    }
                } catch {
                    navigoError = NavigoError()
                    navigoError?.errorCode = Errorcode.ERROR_NO_INTERNET
                    navigoError?.errorMessage = "Unknow error"
                }
            }
            CompletionHandler(gameInfo,navigoError)
        }
        task.resume()
    }
    
    func inputGameAnswer(gameID:Int,answerValue:String,luckyNo:String,CompletionHandler: @escaping (String, NavigoError?) ->Void)  {
        var body:String=""
        //body = keyJson.action + "=" +  Methods.F_INPUT_GAME_ANSWER
        body += keyJson.F_GAME_ID + "=" + gameID.description
        body += "&" + keyJson.F_ANSWER + "=" + answerValue
        body += "&" + keyJson.F_LUCKY_NO + "=" + luckyNo
        print("body",body)
        let myUrl = URL(string: URL_APP.URL_INPUT_GAME_ANSWER)
        var request = URLRequest(url:myUrl!)
        var navigoError:NavigoError? = nil
        request.httpMethod="POST"
        request.httpBody=body.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var actionResult:String = ""
        let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            if error != nil{
                navigoError = NavigoError()
                navigoError?.errorCode = Errorcode.ERROR_NO_INTERNET
                navigoError?.errorMessage = error?.localizedDescription
            } else {
                do {
                    if let data = data,
                        let   html = String(data: data, encoding: String.Encoding.utf8) {
                        let json = JSON(parseJSON: html)
                        print("inputGameAnswer",json)
                         actionResult = json[keyJson.actionResult].stringValue
                        if(actionResult.contains(keyJson.F_OK)){
                            
                        }else{
                            navigoError = NavigoError()
                            navigoError?.errorCode = json[keyJson.errorCode].stringValue
                            navigoError?.errorMessage = json[Errorcode.ERROR_MESSAGE].stringValue
                        }
                    }
                } catch {
                    navigoError = NavigoError()
                    navigoError?.errorCode = Errorcode.ERROR_NO_INTERNET
                    navigoError?.errorMessage = "Unknow error"
                }
            }
            CompletionHandler(actionResult, navigoError)
        }
        task.resume()
    }
    
    func listHistoryGame(CompletionHandler: @escaping (Array<LuckyGame>, NavigoError?) ->Void)  {
        var body:String=""
        //body = keyJson.action + "=" + Methods.F_LIST_LUCKY_GAME
        let myUrl = URL(string: URL_APP.URL_LIST_LUCKY_GAMES)
        var request = URLRequest(url:myUrl!)
        var navigoError:NavigoError? = nil
        request.httpMethod="GET"
        request.httpBody=body.data(using: String.Encoding.utf8, allowLossyConversion: false)
        let convert = Convert()
        var arrLuckyGames: Array<LuckyGame> = Array()
        let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            if error != nil{
                navigoError = NavigoError()
                navigoError?.errorCode = Errorcode.ERROR_NO_INTERNET
                navigoError?.errorMessage = error?.localizedDescription
            } else {
                do {
                    if let data = data,
                        let   html = String(data: data, encoding: String.Encoding.utf8) {
                        let json = JSON(parseJSON: html)
                        print("listHistoryarrLuckyGames",json)
                        let actionResult:String = json[keyJson.actionResult].stringValue
                        if(actionResult.contains(keyJson.F_OK)){
                            if let arrGame = json[keyJson.F_GAMES].array {
                                for item in arrGame  {
                                    if  item is JSON{
                                        let game :LuckyGame = convert.jsonConvertGame(json: item)
                                        arrLuckyGames.append(game)
                                    }
                                }
                            }
                        }else{
                            navigoError = NavigoError()
                            navigoError?.errorCode = json[keyJson.errorCode].stringValue
                            navigoError?.errorMessage = json[Errorcode.ERROR_MESSAGE].stringValue
                        }
                    }
                } catch {
                    navigoError = NavigoError()
                    navigoError?.errorCode = Errorcode.ERROR_NO_INTERNET
                    navigoError?.errorMessage = "Unknow error"
                }
            }
            CompletionHandler(arrLuckyGames, navigoError)
        }
        task.resume()
    }
    
    func luckyGameAnswer(gameID:Int,CompletionHandler: @escaping (Array<Answers>, NavigoError?) ->Void)  {
        var body:String=""
        //body = keyJson.action + "=" +  Methods.F_LUCKY_GAME_ANSWER
        body += keyJson.F_GAME_ID + "=" + gameID.description
        print("body",body)
        let myUrl = URL(string: URL_APP.URL_LUCKY_GAME_ANSWER)
        var request = URLRequest(url:myUrl!)
        var navigoError:NavigoError? = nil
        request.httpMethod="POST"
        request.httpBody=body.data(using: String.Encoding.utf8, allowLossyConversion: false)
        let convert = Convert()
        var arrAnswers: Array<Answers> = Array()
        let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            if error != nil{
                navigoError = NavigoError()
                navigoError?.errorCode = Errorcode.ERROR_NO_INTERNET
                navigoError?.errorMessage = error?.localizedDescription
            } else {
                do {
                    if let data = data,
                        let   html = String(data: data, encoding: String.Encoding.utf8) {
                        let json = JSON(parseJSON: html)
                        print("luckyGameAnswer",json)
                        let actionResult:String = json[keyJson.actionResult].stringValue
                        if(actionResult.contains(keyJson.F_OK)){
                            if let arrAsw = json[keyJson.F_ANSWERS].array {
                                for item in arrAsw  {
                                    if  item is JSON{
                                        let asw :Answers = convert.jsonConvertAnswer(json: item)
                                        arrAnswers.append(asw)
                                    }
                                }
                            }
                        }else{
                            navigoError = NavigoError()
                            navigoError?.errorCode = json[keyJson.errorCode].stringValue
                            navigoError?.errorMessage = json[Errorcode.ERROR_MESSAGE].stringValue
                        }
                    }
                } catch {
                    navigoError = NavigoError()
                    navigoError?.errorCode = Errorcode.ERROR_NO_INTERNET
                    navigoError?.errorMessage = "Unknow error"
                }
            }
            CompletionHandler(arrAnswers, navigoError)
        }
        task.resume()
    }
}


