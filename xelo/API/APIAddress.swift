//
//  APIAddress.swift
//  NaviGo
//
//  Created by HoangManh on 11/3/17.
//  Copyright © 2017 DUMV. All rights reserved.
//

import Foundation
import SwiftyJSON

class APIAddress{
    
    func getListAddress( CompletionHandler: @escaping (Array<Address>, NavigoError?) -> Void)  {
        var body:String = ""
        //body += keyJson.action + "="+keyJson.F_LIST_ADDRESS
        let myUrl = URL(string: URL_APP.URL_LIST_ADDRESES)
        var request = URLRequest(url:myUrl!)
        request.httpMethod="GET"
        request.httpBody=body.data(using: String.Encoding.utf8, allowLossyConversion: false)
        let convert = Convert()
        var navigoError:NavigoError? = nil
        var arrAddress: Array<Address> = Array()
        DispatchQueue.global(qos: .userInitiated).async {
            let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
                if error != nil{
                    navigoError = NavigoError()
                    navigoError?.errorCode = Errorcode.ERROR_NO_INTERNET
                    navigoError?.errorMessage = error?.localizedDescription
                } else {
                    do {
                        if let data = data,
                            let   html = String(data: data, encoding: String.Encoding.utf8) {
                            let json = JSON(parseJSON: html)
                            
                            let actionResult = json[keyJson.actionResult].stringValue
                            if actionResult.range(of: keyJson.F_OK) != nil {
                                if let arraddress = json[keyJson.F_ADDRESS].array {
                                    for addressjs in arraddress  {
                                        if  addressjs is JSON{
                                            let address :Address = convert.jsonCovertAddress(json: addressjs)
                                            arrAddress.append(address)
                                          //  print("dulieu ",address.addressID)
                                        }
                                    }
                                }
                            } else {
                                navigoError = NavigoError()
                                navigoError?.errorCode = json[keyJson.errorCode].stringValue
                                navigoError?.errorMessage = json[Errorcode.ERROR_MESSAGE].stringValue
                            }
                        }
                    } catch {
                        navigoError = NavigoError()
                        navigoError?.errorCode = Errorcode.ERROR_NO_INTERNET
                        navigoError?.errorMessage = "Unknow error"
                    }
                }
                CompletionHandler(arrAddress, navigoError)                
            }
            task.resume()
        }
    }
    
    func createAddress(address:Address ,CompletionHandler: @escaping (NavigoError?) -> Void)  {
        
        var body:String = ""
        
        //body += keyJson.action + "="+keyJson.F_CREATE_ADDRESS
        body += keyJson.F_NAME_ADDRESS + "=" + address.fullName!
        body += "&" + keyJson.F_ADDRESS_ADDRESS + "=" + address.fullAddress!
        body += "&" + keyJson.F_X + "=" + (address.x?.description)!
        body += "&" + keyJson.F_Y + "=" + (address.y?.description)!
        
        let myUrl = URL(string: URL_APP.URL_CREATE_ADDRESS)
        var request = URLRequest(url:myUrl!)
        request.httpMethod="POST"
        request.httpBody=body.data(using: String.Encoding.utf8, allowLossyConversion: false)
        
        var navigoError:NavigoError? = nil
        DispatchQueue.global(qos: .userInitiated).async {
            
            let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
                if error != nil{
                    navigoError = NavigoError()
                    navigoError?.errorCode = Errorcode.ERROR_NO_INTERNET
                    navigoError?.errorMessage = error?.localizedDescription
                } else {
                    do {
                        if let data = data,
                            let   html = String(data: data, encoding: String.Encoding.utf8) {
                            let json = JSON(parseJSON: html)
                            
                            let actionResult = json[keyJson.actionResult].stringValue
                            if actionResult.range(of: keyJson.F_OK) != nil {
                                
                            } else {
                                navigoError = NavigoError()
                                navigoError?.errorCode = json[keyJson.errorCode].stringValue
                                navigoError?.errorMessage = json[Errorcode.ERROR_MESSAGE].stringValue
                            }
                        }
                    } catch {
                        navigoError = NavigoError()
                        navigoError?.errorCode = Errorcode.ERROR_NO_INTERNET
                        navigoError?.errorMessage = "Unknow error"
                    }
                }
                CompletionHandler(navigoError)
            }
            task.resume()
        }
    }
 
    
    func modifyAddress(address:Address ,CompletionHandler: @escaping (NavigoError?) -> Void)  {
        var body:String = ""
        //body += keyJson.action + "="+keyJson.F_MODIFY_ADDRESS
        body += keyJson.F_ADDRESSID + "=" + address.addressID!
        body += "&" + keyJson.F_NAME_ADDRESS + "=" + address.fullName!
        body += "&" + keyJson.F_ADDRESS_ADDRESS + "=" + address.fullAddress!
        body += "&" + keyJson.F_X + "=" + (address.x?.description)!
        body += "&" + keyJson.F_Y + "=" + (address.y?.description)!
        let myUrl = URL(string: URL_APP.URL_MODIFY_ADDRESS)
        var request = URLRequest(url:myUrl!)
        request.httpMethod="POST"
        request.httpBody=body.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var navigoError:NavigoError? = nil
        DispatchQueue.global(qos: .userInitiated).async {
            let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
                if error != nil{
                    navigoError = NavigoError()
                    navigoError?.errorCode = Errorcode.ERROR_NO_INTERNET
                    navigoError?.errorMessage = error?.localizedDescription
                } else {
                    do {
                        if let data = data,
                            let   html = String(data: data, encoding: String.Encoding.utf8) {
                            let json = JSON(parseJSON: html)
                            let actionResult = json[keyJson.actionResult].stringValue
                            if actionResult.range(of: keyJson.F_OK) != nil {
                            } else {
                                navigoError = NavigoError()
                                navigoError?.errorCode = json[keyJson.errorCode].stringValue
                                navigoError?.errorMessage = json[Errorcode.ERROR_MESSAGE].stringValue
                            }
                        }
                    } catch {
                        navigoError = NavigoError()
                        navigoError?.errorCode = Errorcode.ERROR_NO_INTERNET
                        navigoError?.errorMessage = "Unknow error"
                    }
                }
                CompletionHandler(navigoError)
            }
            task.resume()
        }
    }
    
    func deleteAddress(address:Address ,CompletionHandler: @escaping (NavigoError?) -> Void)  {
        var body:String = ""
        //body += keyJson.action + "="+keyJson.F_DELETES_ADDRESS
        body += keyJson.F_ADDRESSID + "=" + address.addressID!
        let myUrl = URL(string: URL_APP.URL_DELETE_ADDRESS)
        var request = URLRequest(url:myUrl!)
        request.httpMethod="POST"
        request.httpBody=body.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var navigoError:NavigoError? = nil
        DispatchQueue.global(qos: .userInitiated).async {
            let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
                if error != nil{
                    navigoError = NavigoError()
                    navigoError?.errorCode = Errorcode.ERROR_NO_INTERNET
                    navigoError?.errorMessage = error?.localizedDescription
                } else {
                    do {
                        if let data = data,
                            let   html = String(data: data, encoding: String.Encoding.utf8) {
                            let json = JSON(parseJSON: html)
                            
                            let actionResult = json[keyJson.actionResult].stringValue
                            if actionResult.range(of: keyJson.F_OK) != nil {
                            } else {
                                navigoError = NavigoError()
                                navigoError?.errorCode = json[keyJson.errorCode].stringValue
                                navigoError?.errorMessage = json[Errorcode.ERROR_MESSAGE].stringValue
                            }
                        }
                    } catch {
                        navigoError = NavigoError()
                        navigoError?.errorCode = Errorcode.ERROR_NO_INTERNET
                        navigoError?.errorMessage = "Unknow error"
                    }
                }
                CompletionHandler(navigoError)
            }
            task.resume()
        }
    }
    
    func getListPlace(place:String, CompletionHandler: @escaping (Array<GooglePlace>, NavigoError?) -> Void)  {
        var body:String = ""
        //body += keyJson.action + "="+keyJson.F_SEARCH_PLACES
        body += keyJson.F_INPUT + "=" + place
        let myUrl = URL(string: URL_APP.URL_SEARCH_PLACES)
        var request = URLRequest(url:myUrl!)
        request.httpMethod="POST"
        request.httpBody=body.data(using: String.Encoding.utf8, allowLossyConversion: false)
        let convert = Convert()
        var navigoError:NavigoError? = nil
        var arrAddress: Array<GooglePlace> = Array()
        DispatchQueue.global(qos: .userInitiated).async {
            let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
                if error != nil{
                    navigoError = NavigoError()
                    navigoError?.errorCode = Errorcode.ERROR_NO_INTERNET
                    navigoError?.errorMessage = error?.localizedDescription
                } else {
                    do {
                        if let data = data,
                            let   html = String(data: data, encoding: String.Encoding.utf8) {
                            let json = JSON(parseJSON: html)
                            print("json", json)
                            let actionResult = json[keyJson.actionResult].stringValue
                            if actionResult.range(of: keyJson.F_OK) != nil {
                                if let arrPlace = json[keyJson.F_PLACES].array {
                                    for place in arrPlace  {
                                        if  place is JSON{
                                            let googlePlace :GooglePlace = convert.jsonConvertGooglePlace(json: place)
                                            arrAddress.append(googlePlace)
                                        }
                                    }
                                }
                            } else {
                                navigoError = NavigoError()
                                navigoError?.errorCode = json[keyJson.errorCode].stringValue
                                navigoError?.errorMessage = json[Errorcode.ERROR_MESSAGE].stringValue
                            }
                        }
                    } catch {
                        navigoError = NavigoError()
                        navigoError?.errorCode = Errorcode.ERROR_NO_INTERNET
                        navigoError?.errorMessage = "Unknow error"
                    }
                }
                CompletionHandler(arrAddress, navigoError)
            }
            task.resume()
        }
    }
    
    func getPlaceInfo(place:String ,CompletionHandler: @escaping (Double, Double ,NavigoError?) -> Void)  {
        var body:String = ""
        //body += keyJson.action + "="+keyJson.F_PLACE_INFO
        body += keyJson.F_PLACE_ID + "=" + place
        body += "&" + keyJson.F_KEY + "=" + DataNavigo.loginResult.googlePlaceApiKey
        let myUrl = URL(string: URL_APP.URL_GET_PLACE_INFO)
        var request = URLRequest(url:myUrl!)
        request.httpMethod="POST"
        request.httpBody=body.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var navigoError:NavigoError? = nil
        var lat:Double = 0
        var lon:Double = 0
        DispatchQueue.global(qos: .userInitiated).async {
            let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
                if error != nil{
                    navigoError = NavigoError()
                    navigoError?.errorCode = Errorcode.ERROR_NO_INTERNET
                    navigoError?.errorMessage = error?.localizedDescription
                } else {
                    do {
                        if let data = data,
                            let   html = String(data: data, encoding: String.Encoding.utf8) {
                            let json = JSON(parseJSON: html)
                            print("json",json)
                            let actionResult = json[keyJson.actionResult].stringValue
                            if actionResult.range(of: keyJson.F_OK) != nil {
                                lat = json[keyJson.F_Y].doubleValue
                                lon = json[keyJson.F_X].doubleValue
                            } else {
                                navigoError = NavigoError()
                                navigoError?.errorCode = json[keyJson.errorCode].stringValue
                                navigoError?.errorMessage = json[Errorcode.ERROR_MESSAGE].stringValue
                            }
                        }
                    } catch {
                        navigoError = NavigoError()
                        navigoError?.errorCode = Errorcode.ERROR_NO_INTERNET
                        navigoError?.errorMessage = "Unknow error"
                    }
                }
                CompletionHandler(lat, lon, navigoError)
            }
            task.resume()
        }
    }
    
    func getRouting(fromPositionX:Double,fromPositionY:Double,toPositionX:Double,toPositionY:Double,CompletionHandler: @escaping (Array<Point>, NavigoError?) ->Void) {
        var navigoError:NavigoError? = nil
        var body:String = ""
        //body += keyJson.action + "="+keyJson.F_ROUTING
        body = keyJson.F_FROM_X + "=" + fromPositionX.description
        body = body + "&" + keyJson.F_FROM_Y + "=" + fromPositionY.description
        body = body + "&" + keyJson.F_TO_X + "=" + toPositionX.description
        body = body + "&" + keyJson.F_TO_Y + "=" + toPositionY.description
        let myUrl = URL(string: URL_APP.URL_ROUTING)
        var request = URLRequest(url:myUrl!)
        var arrPoints: Array<Point> = Array()
        
        request.httpMethod="POST"
        request.httpBody=body.data(using: String.Encoding.utf8, allowLossyConversion: false)
        let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            if error != nil{
                navigoError = NavigoError()
                navigoError?.errorCode = Errorcode.ERROR_NO_INTERNET
                navigoError?.errorMessage = error?.localizedDescription
            }else {
                do {
                    if let data = data,
                        let   html = String(data: data, encoding: String.Encoding.utf8) {
                        let json = JSON(parseJSON: html)
                        print("checkPrice",json)
                        let actionResult:String = json[keyJson.actionResult].stringValue
                        if(actionResult.contains(keyJson.F_OK)){
                            let convert = Convert()
                            if let arrPoint = json[keyJson.F_POINTS].array {
                                for point in arrPoint  {
                                    if  point is JSON{
                                        let points :Point = convert.jsonConvertPoint(json:point)
                                        arrPoints.append(points)
                                    }
                                }
                            }
                        }else{
                            navigoError = NavigoError()
                            navigoError?.errorCode = json[keyJson.errorCode].stringValue
                            navigoError?.errorMessage = json[Errorcode.ERROR_MESSAGE].stringValue
                            
                        }
                    }
                } catch {
                    navigoError = NavigoError()
                    navigoError?.errorCode = Errorcode.ERROR_NO_INTERNET
                    navigoError?.errorMessage = "Unknow error"
                }
            }
            CompletionHandler(arrPoints, navigoError)
        }
        task.resume()
    }
    
    func getAddress(fromX:Double,fromY:Double ,CompletionHandler: @escaping (GooglePlace,NavigoError?) -> Void)  {
        var body:String = ""
        //body += keyJson.action + "="+keyJson.F_GET_ADDRESS
        body += keyJson.F_X + "=" + fromX.description
        body += "&" + keyJson.F_Y + "=" + fromY.description
        let myUrl = URL(string: URL_APP.URL_GET_ADDRESS)
        var request = URLRequest(url:myUrl!)
        request.httpMethod="POST"
        request.httpBody=body.data(using: String.Encoding.utf8, allowLossyConversion: false)
        var navigoError:NavigoError? = nil
        var googlePlace  = GooglePlace()
        DispatchQueue.global(qos: .userInitiated).async {
            let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
                if error != nil{
                    navigoError = NavigoError()
                    navigoError?.errorCode = Errorcode.ERROR_NO_INTERNET
                    navigoError?.errorMessage = error?.localizedDescription
                } else {
                    do {
                        if let data = data,
                            let   html = String(data: data, encoding: String.Encoding.utf8) {
                            let json = JSON(parseJSON: html)
                            print("getAddress",json)
                            let convert = Convert()
                            let actionResult = json[keyJson.actionResult].stringValue
                            if actionResult.range(of: keyJson.F_OK) != nil {
                                //address
                                googlePlace = convert.jsonConvertGooglePlace(json: json)
                            } else {
                                navigoError = NavigoError()
                                navigoError?.errorCode = json[keyJson.errorCode].stringValue
                                navigoError?.errorMessage = json[Errorcode.ERROR_MESSAGE].stringValue
                            }
                        }
                    } catch {
                        navigoError = NavigoError()
                        navigoError?.errorCode = Errorcode.ERROR_NO_INTERNET
                        navigoError?.errorMessage = "Unknow error"
                    }
                }
                CompletionHandler(googlePlace, navigoError)
            }
            task.resume()
        }
    }
}
