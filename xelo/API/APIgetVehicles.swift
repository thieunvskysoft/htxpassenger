//
//  Position.swift
//  testNavigo
//
//  Created by Linh  on 10/12/17.
//  Copyright © 2017 DUMV. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire
import GooglePlaces

class APIgetVehicles{
    
    func getVehicles(location:Point,serviceCode:String, currentLocation: CLLocation,CompletionHandler: @escaping (Array<Vehicle>,Array<Service>,Array<BlockRoads>, Bool,NavigoError?) ->Void)  {
        
        var navigoError:NavigoError? = nil
        var arrLV:Array<Vehicle> = Array()
        var body:String = ""
        let locaX = location.x ?? 0.0
        let locaY = location.y ?? 0.0
        body += keyJson.F_Y + "=" + locaY.description
        body += "&" + keyJson.F_X + "=" + locaX.description
        body += "&" + keyJson.F_SERVICE_CODE + "=" + serviceCode
        body += "&" + keyJson.F_CURRENT_X + "=" + currentLocation.coordinate.longitude.description
        body += "&" + keyJson.F_CURRENT_Y + "=" + currentLocation.coordinate.latitude.description
        //print("body",body)
        let myUrl = URL(string: URL_APP.URL_LIST_VEHICLES)
        var request = URLRequest(url:myUrl!)
        request.httpMethod="POST"
        request.httpBody=body.data(using: String.Encoding.utf8, allowLossyConversion: false)
        let convert = Convert()
        var arrService: Array<Service> = Array()
        var arrblockRoads: Array<BlockRoads> = Array()
        var getRoad = true
        
        let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            if error != nil{
                navigoError = NavigoError()
                navigoError?.errorCode = Errorcode.ERROR_NO_INTERNET
                navigoError?.errorMessage = error?.localizedDescription
            } else {
                do {
                    if let data = data,
                        let   html = String(data: data, encoding: String.Encoding.utf8) {
                        //print("html", html)
                        //print("------------------------------------------")
                            let json = JSON(parseJSON: html)
                            let actionResult = json[keyJson.actionResult].stringValue
                            if actionResult.range(of: keyJson.F_OK) != nil {
                                if let arrvehicles = json["vehicles"].array {
                                    for jsonVehicle in arrvehicles  {
                                        if  jsonVehicle is JSON{
                                            let vehicles :Vehicle = convert.jsonConvertVehicle(json: jsonVehicle)
                                            arrLV.append(vehicles)
                                        }
                                    }
                                }
                                
                                if let arrServices = json[keyJson.services].array {
                                    for services in arrServices  {
                                        if  services is JSON{
                                            let servicePrice :Service = convert.jsonConvertServicePrice(json:services)
                                            arrService.append(servicePrice)
                                        }
                                    }
                                }
                                if let arrRoad = json[keyJson.F_BLOCK_ROADS].array {
                                    if arrRoad.count > 0 {
                                        for item in arrRoad  {
                                            if  item is JSON{
                                                let value :BlockRoads = convert.jsonConvertBlockRoads(json:item)
                                                arrblockRoads.append(value)
                                            }
                                        }
                                    }else {
                                        getRoad = false
                                    }
                                }

                            } else {
                                navigoError = NavigoError()
                                navigoError?.errorCode = json[keyJson.errorCode].stringValue
                                navigoError?.errorMessage = json[Errorcode.ERROR_MESSAGE].stringValue
                            }
                        }
                } catch {
                    navigoError?.errorCode = Errorcode.ERROR_NO_INTERNET
                }
            }
            CompletionHandler(arrLV,arrService,arrblockRoads,getRoad,navigoError)
        }
        task.resume()
    }
    func listPublicVehicles(location:Point,CompletionHandler: @escaping (Array<Vehicle>,NavigoError?) ->Void)  {
        
        var navigoError:NavigoError? = nil
        var arrLV:Array<Vehicle> = Array()
        var body:String = ""
        //body = keyJson.action + "=" + Methods.F_LIST_PUBLIC_VEHICLE
        let locaX = location.x ?? 0.0
        let locaY = location.y ?? 0.0
        body += keyJson.F_Y + "=" + locaY.description
        body += "&" + keyJson.F_X + "=" + locaX.description
        print("body",body)
        let myUrl = URL(string: URL_APP.URL_LIST_PUBLIC_VEHICLES)
        var request = URLRequest(url:myUrl!)
        request.httpMethod="POST"
        request.httpBody=body.data(using: String.Encoding.utf8, allowLossyConversion: false)
        let convert = Convert()
        let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            if error != nil{
                navigoError = NavigoError()
                navigoError?.errorCode = Errorcode.ERROR_NO_INTERNET
                navigoError?.errorMessage = error?.localizedDescription
            } else {
                do {
                    if let data = data,
                        let   html = String(data: data, encoding: String.Encoding.utf8) {
                        //print("html", html)
                        //print("------------------------------------------")
                        let json = JSON(parseJSON: html)
                        let actionResult = json[keyJson.actionResult].stringValue
                        if actionResult.range(of: keyJson.F_OK) != nil {
                            if let arrVehicles = json["vehicles"].array {
                                for item in arrVehicles  {
                                    if  item is JSON{
                                        let vehicle :Vehicle = convert.jsonConvertVehicle(json: item)
                                        arrLV.append(vehicle)
                                    }
                                }
                            }
                            
                        } else {
                            navigoError = NavigoError()
                            navigoError?.errorCode = json[keyJson.errorCode].stringValue
                            navigoError?.errorMessage = json[Errorcode.ERROR_MESSAGE].stringValue
                        }
                    }
                } catch {
                    navigoError?.errorCode = Errorcode.ERROR_NO_INTERNET
                }
            }
            CompletionHandler(arrLV,navigoError)
        }
        task.resume()
    }
}

class PolyLine {

    func getLineMap(startLocation: CLLocation, endLocation: CLLocation,CompletionHandler: @escaping (JSON) -> Void) {
        let origin = "\(startLocation.coordinate.latitude),\(startLocation.coordinate.longitude)"
        let destination = "\(endLocation.coordinate.latitude),\(endLocation.coordinate.longitude)"
        let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving"
        Alamofire.request(url).responseJSON { response in
            if let json = try? JSON(data: response.data!) {
                CompletionHandler(json)
            }
        }

    }
    
    
}

