//
//  ExtraChargeController.swift
//  xelo
//
//  Created by Nguyen Thieu on 8/14/18.
//  Copyright © 2018 DUMV. All rights reserved.
//

import UIKit
import WebKit

class ExtraChargeController: UIViewController ,UIWebViewDelegate {
    
    var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        let backButton = UIBarButtonItem()
        backButton.title = " "
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        navigationController?.navigationBar.tintColor = .white
        navigationItem.title = "Thông tin phụ phí"
        
        webView = UIWebView(frame: UIScreen.main.bounds)
        webView.delegate = self
        view.addSubview(webView)
        if let url = URL(string: Constant.URL_EXTRA_CHARGE) {
            //  print(url)
            let request = URLRequest(url: url)
            webView.loadRequest(request)
        }
    }
    
}
