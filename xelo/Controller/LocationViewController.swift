//
//  LocationViewController.swift
//  NaviGo
//
//  Created by HoangManh on 9/20/17.
//  Copyright © 2017 DUMV. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import SwiftyJSON
import Alamofire

class LocationViewController: UIViewController, CLLocationManagerDelegate, GMSMapViewDelegate {

    var googleMap:GMSMapView?
    var locationManager = CLLocationManager()
    var currentZoom:CGFloat = 15.0
    var locationPoint = CLLocation()
    var locationDragged:Bool = false
    var addressLocation:String = ""
    var point:Int?
    var lastMoveMap:Int64 = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        DataNavigo.idUIController = 3
        setupGoogleMap()
    }
    override func viewWillAppear(_ animated: Bool) {
        btnSetPoint.isEnabled = false
        btnSetPoint.backgroundColor = UIColor.gray
        lastMoveMap = getCurrentMillis()
        getLocationAddress()
        //startTimer()
    }
    
    //setup map
    func setupGoogleMap() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        locationManager.startMonitoringSignificantLocationChanges()
        locationManager.distanceFilter = 2
        locationManager.requestLocation()
        locationManager.requestAlwaysAuthorization()
        let camera = GMSCameraPosition.camera(withLatitude: 21.05, longitude: 105.777711, zoom: 15)
        googleMap = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        googleMap?.camera = camera
        googleMap?.delegate = self
        googleMap?.isMyLocationEnabled = true
        googleMap?.settings.myLocationButton = true
        //button location
        self.googleMap?.padding = UIEdgeInsets(top: 0, left: 0, bottom: 50, right: 0)
        
        let navBarHeight = (self.navigationController?.navigationBar.intrinsicContentSize.height)! + UIApplication.shared.statusBarFrame.height + 40
        googleMap?.addSubview(viewLoation)
        viewLoation.frame = CGRect(x: 5, y: 5, width: view.frame.width - 10, height: 45)
        googleMap?.addSubview(likePoint)
        likePoint.frame = CGRect(x: view.frame.width - 35, y: 15, width: 25, height: 25)
        likePoint.addTarget(self, action: #selector(setLikePoint), for: .touchUpInside)
        
        // set text viewlocation
        if getLocation.choice == 1 {
            viewLoation.buttonText = "Điểm đi"
            viewLoation.imageName = #imageLiteral(resourceName: "des")
        }else{
            viewLoation.buttonText = "Điểm đến"
            viewLoation.imageName = #imageLiteral(resourceName: "cur")
        }
        do {
            // Set the map style by passing the URL of the local file.
            if let styleURL = Bundle.main.url(forResource: "style", withExtension: "json") {
                googleMap?.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
            } else {
                NSLog("Unable to find style.json")
            }
        } catch {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
        view.addSubview(googleMap!)
        googleMap?.addSubview(btnGetLocation)
        if(DataNavigo.deviceModel == Constant.deviceModelX){
            googleMap?.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height - navBarHeight - 33)
            btnGetLocation.frame = CGRect(x: ((googleMap?.frame.width)! - 20) / 2, y: ((googleMap?.frame.height)! - navBarHeight - 70) / 2, width: 20, height: 20)

        }else{
            googleMap?.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height - navBarHeight)
            btnGetLocation.frame = CGRect(x: ((googleMap?.frame.width)! - 20) / 2, y: ((googleMap?.frame.height)! - 70) / 2, width: 20, height: 20)

        }   
        googleMap?.addSubview(btnSetPoint)
        
        btnSetPoint.frame = CGRect(x: 5, y: (googleMap?.frame.height)! - 50 , width: (googleMap?.frame.width)! - 10, height: 45)
        btnSetPoint.layer.cornerRadius = 5
        
        btnSetPoint.addTarget(self, action: #selector(popToMap), for: .touchUpInside)
    }
    
    //chuyển về map
    @objc func popToMap() {
        setLocationPoint()
//        let viewControllers:[UIViewController] = (self.navigationController?.viewControllers)!;
//        for controller in viewControllers {
//            if controller is MapController {
//                self.navigationController!.popToViewController(controller, animated: true)
//            }
//        }
    }
    
    func setLocationPoint() {
        DataNavigo.checkDrawPath = true
        DataNavigo.booking.estDistance = 0
        if getLocation.choice == 1 {
            DataNavigo.booking.fromPosition?.y = locationPoint.coordinate.latitude
            DataNavigo.booking.fromPosition?.x = locationPoint.coordinate.longitude
            DataNavigo.booking.fromAddress = addressLocation
        }
        else if getLocation.choice == 2 {
            DataNavigo.booking.toPosition?.y = locationPoint.coordinate.latitude
            DataNavigo.booking.toPosition?.x = locationPoint.coordinate.longitude
            DataNavigo.booking.toAddress = addressLocation
        }
        else if getLocation.choice == 3{
            BusBookingController.fromLocation.y = locationPoint.coordinate.latitude
            BusBookingController.fromLocation.x = locationPoint.coordinate.longitude
            BusBookingController.fromAddress = addressLocation
        }
        else if getLocation.choice == 4{
            BusBookingController.toLocation.y = locationPoint.coordinate.latitude
            BusBookingController.toLocation.x = locationPoint.coordinate.longitude
            BusBookingController.toAddress = addressLocation
        }
        navigationController?.popViewController(animated: true)
        //MapController.drawPath(se)
    }
    
    func getLocationAddress() {
     //   print("getLocationAddress")
        getAddress(fromX: locationPoint.coordinate.longitude, fromY: locationPoint.coordinate.latitude)
    }
    func createPoint(namePoint:String) {
        let address = Address()
        address.y=locationPoint.coordinate.latitude
        address.x=locationPoint.coordinate.longitude
        address.fullAddress = addressLocation
        address.fullName = namePoint
        self.createaddress(address: address)
    }
    
    func getAddress(fromX: Double, fromY: Double)  {
        let CompletionHandler: ((GooglePlace,NavigoError?) -> Void) = {
            address, navigoError in
            
            DispatchQueue.main.async {
                if(navigoError == nil){
                    if address.description! != "" {
                        self.addressLocation = address.description!
                        self.viewLoation.locationText = address.description!
                    }

                    // print(lines)
                    self.btnSetPoint.isEnabled = true
                    self.btnSetPoint.backgroundColor = NaviColor.naviBtnOrange
                }else{
                    if(navigoError?.errorCode == Errorcode.ERROR_NO_INTERNET){
                        
                    }else if (navigoError?.errorCode == Errorcode.NAVIGO_SESSION_EXPIRED){
                        self.reconect()
                    }
                }
            }
        }
        let apiAddress = APIAddress();
        apiAddress.getAddress(fromX: fromX, fromY: fromY,CompletionHandler: CompletionHandler)
    }
    
    ///login
    func reconect() {
        let CompletionHandler: ((LoginResult,NavigoError?) -> Void) = {
            loginResult,navigoError in
            DispatchQueue.main.async {
                
            }
        }
        let connector = PassengerConnector();
        connector.loginAccount(passenger: DataNavigo.passenger, reconnect: true, CompletionHandler: CompletionHandler)
    }
    
    func createaddress(address:Address)  {
        let CompletionHandler: ((NavigoError?) -> Void) = {
            navigoError in
            
            DispatchQueue.main.async {
                if(navigoError == nil){
                    self.likePoint.setImage(#imageLiteral(resourceName: "StarFull"), for: .normal)
                    self.getAll()
                }else{
                    if(navigoError?.errorCode == Errorcode.ERROR_NO_INTERNET){
                        
                    }
                    else{
                    }
                }
            }
        }
        let apiAddress = APIAddress();
        apiAddress.createAddress(address: address,CompletionHandler: CompletionHandler)
    }
    
    func getAll()  {
        let CompletionHandler: ((Array<Address>,NavigoError?) -> Void) = {
            lisAddress,navigoError in
            
            DispatchQueue.main.async {
                if(navigoError == nil){
                    DataNavigo.arrAddress.removeAll()
                    DataNavigo.arrAddress = lisAddress
                    // print("diemyeuthich",DataNavigo.arrAddress.count)
                    //  self.tblListPoint.reloadData()
                    //NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refresh"), object: nil, userInfo: nil)
                }else{
                    if(navigoError?.errorCode == Errorcode.ERROR_NO_INTERNET){
                        
                    }
                    else{
                        
                    }
                }
            }
        }
        let apiAddress = APIAddress();
        apiAddress.getListAddress(CompletionHandler: CompletionHandler)
    }
    
    @objc func setLikePoint() {
        let alert:UIAlertController = UIAlertController(title: "Tạo điểm yêu thích", message: "", preferredStyle: UIAlertControllerStyle.alert)
        alert.addTextField { (txtPoint) in
            txtPoint.placeholder = "Tên điểm "
            txtPoint.keyboardType = .default
        }
        let btnOk:UIAlertAction = UIAlertAction(title: "Xác nhận", style: .destructive) { (btnOk) in
            let namePoint = (alert.textFields![0].text)!
            self.createPoint(namePoint: namePoint)
        }
        let btnCancel:UIAlertAction = UIAlertAction(title: "Huỷ", style: .destructive) { (btnCancel) in
            
        }
        alert.addAction(btnOk)
        alert.addAction(btnCancel)
        present(alert, animated: true, completion: nil)
    }
    
    let btnSetPoint:UIButton = {
        let btn = UIButton()
        btn.setTitle("Xác nhận", for: .normal)
        return btn
    }()
    
    let btnGetLocation:UIImageView = {
        let imgView = UIImageView()
        imgView.contentMode = .center
        imgView.image = #imageLiteral(resourceName: "get_location")
        return imgView
    }()
    
    let viewLoation:LocationViewButton = {
        let btn = LocationViewButton()
        return btn
    }()
    
    let likePoint:UIButton = {
        let btn = UIButton()
        btn.backgroundColor = UIColor.white
        btn.setImage(#imageLiteral(resourceName: "StarEmpty"), for: .normal)
        return btn
    }()
    
    func getCurrentMillis()->Int64 {
        return Int64(Date().timeIntervalSince1970 * 1000)
    }
}

//Map
extension LocationViewController {
    // MARK: CLLocation Manager Delegate
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
      //  print("Error while get location \(error)")
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    //    print("didupdateLocation")
        let location = locations.last
        if (getLocation.choice == 1 && DataNavigo.booking.fromPosition?.x != nil) {
            let camera = GMSCameraPosition.camera(withLatitude: (DataNavigo.booking.fromPosition?.y)!, longitude: (DataNavigo.booking.fromPosition?.x)!, zoom: 17.0)
            self.googleMap?.animate(to: camera)
        }else if(getLocation.choice == 2 && DataNavigo.booking.toPosition?.x != nil){
            let camera = GMSCameraPosition.camera(withLatitude: (DataNavigo.booking.toPosition?.y)!, longitude: (DataNavigo.booking.toPosition?.x)!, zoom: 17.0)
            self.googleMap?.animate(to: camera)
        }else{
            let camera = GMSCameraPosition.camera(withLatitude: (location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!, zoom: 17.0)
            self.googleMap?.animate(to: camera)
        }

        self.locationManager.stopUpdatingLocation()
    }
    
    // MARK: GMSMapview Delegate
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        self.googleMap?.isMyLocationEnabled = true
        getAddress(fromX: position.target.longitude, fromY: position.target.latitude)
        //getAddressByGeocode(coordinate: position.target)
    }
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        self.googleMap?.isMyLocationEnabled = true
        //startTimer()
        locationDragged = true
        if (gesture) {
            lastMoveMap = getCurrentMillis()
            addressLocation = ""
            mapView.selectedMarker = nil
            self.likePoint.setImage(#imageLiteral(resourceName: "StarEmpty"), for: .normal)
            btnSetPoint.isEnabled = false
            btnSetPoint.backgroundColor = UIColor.gray
        }
    }

    
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
     //   print("Click")
        lastMoveMap = getCurrentMillis()
        addressLocation = ""
        btnSetPoint.isEnabled = false
        btnSetPoint.backgroundColor = UIColor.gray
        googleMap?.isMyLocationEnabled = true
        googleMap?.selectedMarker = nil
        return false
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        lastMoveMap = getCurrentMillis()
        addressLocation = ""
        locationPoint = CLLocation(latitude: mapView.camera.target.latitude, longitude: mapView.camera.target.longitude)
    }
    
    func getAddressByGeocode(coordinate:CLLocationCoordinate2D) {
        self.btnSetPoint.backgroundColor = UIColor.gray
        //if (getCurrentMillis() - lastMoveMap >= (1*1000) && addressLocation == ""){
            let geocoder = GMSGeocoder()
            geocoder.reverseGeocodeCoordinate(coordinate) { response , error in
                if let address = response?.firstResult() {
                    let lines = address.lines! as [String]
                    self.addressLocation = lines.joined(separator: "\n")
                    self.viewLoation.locationText = lines.joined(separator: "\n")
                   // print(lines)
                    self.btnSetPoint.isEnabled = true
                    self.btnSetPoint.backgroundColor = NaviColor.naviBtnOrange
                }
            }
        //}
    }
}
