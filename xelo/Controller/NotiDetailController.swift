//
//  NotiDetailController.swift
//  xelo
//
//  Created by Nguyen Thieu on 2/2/18.
//  Copyright © 2018 DUMV. All rights reserved.
//

import UIKit

class NotiDetailController: UIViewController,HandingScreen, UIWebViewDelegate {
    
    var webView: UIWebView!
    
    
    @IBOutlet weak var viewContent: UIView!
    @IBOutlet weak var txtContent: UITextView!
    static var notifyID:String = ""
    var notify:Notifications? = nil
    static var noti:Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.tintColor = .white
        navigationItem.title = "Chi tiết thông báo"
        webView = UIWebView(frame: UIScreen.main.bounds)
        webView.delegate = self
        view.addSubview(webView)
        getNotification()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotiDetailController.noti = false
        DataNavigo.notiFB = nil
        if(DataNavigo.arrNotify.count > 0 && notify?.notifyID == DataNavigo.arrNotify[0].notifyID)
        {
            DataNavigo.arrNotify.remove(at: 0)
        }
    }
    override func viewWillAppear(_ animated: Bool) {

        self.navigationController?.isNavigationBarHidden = false
        let backButton = UIBarButtonItem()
        backButton.title = " "
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        
    }
    func getNotification() {
        let CompletionHandler: ((Notifications,NavigoError?) -> Void) = {noti,navigoError in
            DispatchQueue.main.async {
                if(navigoError == nil){
                    self.notify = noti
                    self.showNotiInfo()
                }else{
                    if navigoError?.errorCode?.range(of: Errorcode.NAVIGO_SESSION_EXPIRED) != nil{
                    }
                    if(navigoError?.errorCode == Errorcode.ERROR_NO_INTERNET){
                        self.errorNoInternet()
                    }
                    else{
                        print("navigoError", navigoError?.errorCode)
                        print("navigoError", Errorcode.ERROR_MESSAGE)
                        self.errorServerSend(errorCode: (navigoError?.errorCode)!, errorToast: Errorcode.ERROR_MESSAGE)
                    }
                }
            }
        }
        let connector = APINotification();
        connector.getNotification(notifyID: NotiDetailController.notifyID, CompletionHandler: CompletionHandler)
    }
    
    func showNotiInfo() {
        let str = StringConvert()
        if NotiDetailController.noti {
            if(DataNavigo.notiFB != nil && DataNavigo.notiFB?.url != nil && DataNavigo.notiFB?.url != ""){
                if (DataNavigo.notiFB?.url?.range(of: "http") != nil){
//                    if let url = URL(string:(DataNavigo.notiFB?.url!)!) {
//                        let request = URLRequest(url: url)
//                        webView.loadRequest(request)
//                        viewContent.alpha = 0
//                        webView.alpha = 1
//                    }
                    if let url = URL(string: (DataNavigo.notiFB?.url!)!), UIApplication.shared.canOpenURL(url) {
                        UIApplication.shared.openURL(url)
                    }
                }else{
                    if let url = URL(string:Constant.URL_ROOT + (DataNavigo.notiFB?.url!)!) {
                        let request = URLRequest(url: url)
                        webView.loadRequest(request)
                        viewContent.alpha = 0
                        webView.alpha = 1
                    }
                }
            }else{
                
                navigationItem.title = DataNavigo.notiFB?.title
                self.txtContent.text = str.emojiConvert(str: (DataNavigo.notiFB?.content!)!)
                viewContent.alpha = 1
                webView.alpha = 0
            }
            markReadNotification(notification: DataNavigo.notiFB!)
            
        }else{
            markReadNotification(notification: notify!)
            if(notify?.url != ""){
                if (notify?.url?.range(of: "http") != nil){
//                    if let url = URL(string: (notify?.url!)!) {
//                        let request = URLRequest(url: url)
//                        webView.loadRequest(request)
//                        viewContent.alpha = 0
//                        webView.alpha = 1
//                    }
                    if let url = URL(string: (notify?.url!)!), UIApplication.shared.canOpenURL(url) {
                        UIApplication.shared.openURL(url)
                    }
                }else{
                    if let url = URL(string:Constant.URL_ROOT + (notify?.url!)!) {
                        let request = URLRequest(url: url)
                        webView.loadRequest(request)
                        viewContent.alpha = 0
                        webView.alpha = 1
                    }
                }
            }else{
                navigationItem.title = notify?.title
                self.txtContent.text = str.emojiConvert(str: notify!.content!)
                viewContent.alpha = 1
                webView.alpha = 0
            }
        }
    }
    func markReadNotification(notification:Notifications) {
        
        let CompletionHandler: ((Notifications,NavigoError?) -> Void) = {noti,navigoError in
            
            DispatchQueue.main.async {
                if(navigoError == nil){
                    
                }else{
                    if(navigoError?.errorCode == Errorcode.ERROR_NO_INTERNET){
                        self.errorNoInternet()
                    }
                    else{
                        self.errorServerSend(errorCode: (navigoError?.errorCode)!, errorToast: (navigoError?.errorMessage)!)
                    }
                    
                }
            }
        }
        
        let connector = APINotification();
        connector.markReadNotification(notify: notification, CompletionHandler: CompletionHandler)
    }
    func errorNoInternet() {
        
    }
    
    func errorConnect() {
        
    }
    
    func errorServerSend(errorCode: String, errorToast: String) {
        
    }
    
    func startRequest() {
        
    }
    
    func endRequest() {
        
    }
    
}

