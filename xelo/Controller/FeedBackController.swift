//
//  FeedBackController.swift
//  NaviGo
//
//  Created by HoangManh on 10/10/17.
//  Copyright © 2017 DUMV. All rights reserved.
//

import UIKit

class FeedBackController: UIViewController, UITextViewDelegate,UITextFieldDelegate {
    
    @IBOutlet weak var imgDriver: UIImageView!
    @IBOutlet weak var lblNameDriver: UILabel!
    @IBOutlet weak var imgStart: UIImageView!
    @IBOutlet weak var imgEnd: UIImageView!
    @IBOutlet weak var lblStart: UILabel!
    @IBOutlet weak var lblEnd: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var vRating: FloatRatingView!
    @IBOutlet weak var btnRating: UIButton!
    @IBOutlet weak var vLocation: UIView!
    @IBOutlet weak var vPrice: UIView!
    @IBOutlet weak var vRate: UIView!
    @IBOutlet weak var vDistance: UIView!
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var promotionValue: UILabel!
    @IBOutlet weak var priceBook: UILabel!
    @IBOutlet weak var plateNumber: UILabel!
    @IBOutlet weak var extraCharge: UILabel!
    
    @IBOutlet weak var ExtraLabel: UILabel!
    @IBOutlet weak var DistanceLabel: UILabel!
    @IBOutlet weak var PriceLabel: UILabel!
    @IBOutlet weak var PromotionLabel: UILabel!
    @IBOutlet weak var lblPriceBook: UILabel!
    @IBOutlet weak var btnRate: UIButton!
    @IBOutlet weak var btnExtraCharge: UIButton!
    @IBOutlet weak var excludingTolls: UILabel!
    
    @IBOutlet weak var scrollview: UIScrollView!
    
    @IBOutlet weak var mutilTextField: UITextView!
    
    @IBOutlet weak var bottomConst: NSLayoutConstraint!
    //@IBOutlet weak var mutilTextField: MultilineTextField!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblContent: UILabel!
    
    
    @IBOutlet weak var viewContent: UIView!
    
    var yFloat:CGFloat = 0
    
    var driverRating:Int = 0
    
    var TOTAL_CHARGE:String = "Cước: "
    var DISTANCE_:String = "Lộ trình: "
    var PROMOTION:String = "Khuyến mại: "
    var NOTIFICATION_TITLE:String = "Thông báo"
    var CONFIRM:String = "Xác nhận"
    var CANCEL:String = "Huỷ"
    var PAYMENT:String = "Thanh toán"
    var RATE_PLS:String = "Vui lòng đánh giá dịch vụ"
    var EXCLUDING_TOLLS:String = "(Chưa gồm phí cầu đường)"
    var THANKYOU:String = "Cảm ơn bạn đã sử dụng dịch vụ của chúng tôi"
    var SURCHARGE:String = "Phụ phí: "
    
    var ONE_POINT_TITLE:String = "Quá tệ"
    var ONE_POINT_CONTENT:String = "Rất tiếc vì những trải nghiệm không tốt bạn gặp phải trong chuyến đi. Hãy để lại phản hồi của bạn để Xế Lô có hướng xử lí, khắc phục."
    var ONE_POINT_RESULT:String = "Chúng tôi xin ghi nhận và có hướng xử lí phù hợp nhất. Hệ thống đã chặn tài xế này để bạn không gặp phải trong các chuyến đi sau."
    
    var TWO_POINT_TITLE:String = "Không hài lòng"
    var TWO_POINT_CONTENT:String = "Điều gì trong chuyến đi đã khiến bạn không hài lòng? Để lại phản hồi để Xế Lô nâng cao chất lượng dịch vụ!"
    var TWO_POINT_RESULT:String = "Cảm ơn những phản hồi và ý kiến đóng góp của bạn."
    
    var THREE_POINT_TITLE:String = "Bình thường"
    var THREE_POINT_CONTENT:String = "Hãy đóng góp những ý kiến để Xế Lô nâng cao chất lượng dịch vụ hơn!"
    var THREE_POINT_RESULT:String = "Cảm ơn những phản hồi và ý kiến đóng góp của bạn."
    
    var FOUR_POINT_TITLE:String = "Dịch vụ tốt"
    var FOUR_POINT_CONTENT:String = "ãy đóng góp những ý kiến của bạn để Xế Lô nâng cao chất lượng dịch vụ hơn nữa!"
    var FOUR_POINT_RESULT:String = "Cảm ơn những phản hồi và ý kiến đóng góp của bạn."
    
    var FIVE_POINT_TITLE:String = "Dịch vụ xuất sắc"
    var FIVE_POINT_CONTENT:String = "Cảm ơn bạn đã sử dụng Xế Lô. Đừng quên để lại lời khen khích lệ các bác tài!"
    var FIVE_POINT_RESULT:String = "Cảm ơn bạn đã tin tưởng sử dụng dịch vụ của Xế Lô"
    
    func loadViewLanguage() {
        let curren_language = UserDefaults.standard.string(forKey: "curren_language")
        let language = LanguageData()
        TOTAL_CHARGE = language.localizedString(forKey:curren_language!,forKey: "TOTAL_CHARGE")
        DISTANCE_ = language.localizedString(forKey:curren_language!,forKey: "DISTANCE_")
        PROMOTION = language.localizedString(forKey:curren_language!,forKey: "PROMOTION")
        NOTIFICATION_TITLE = language.localizedString(forKey:curren_language!,forKey: "NOTIFICATION_TITLE")
        CONFIRM = language.localizedString(forKey:curren_language!,forKey: "CONFIRM")
        CANCEL = language.localizedString(forKey:curren_language!,forKey: "CANCEL")
        PAYMENT = language.localizedString(forKey:curren_language!,forKey: "PAYMENT")
        RATE_PLS = language.localizedString(forKey:curren_language!,forKey: "RATE_PLS")
        EXCLUDING_TOLLS = language.localizedString(forKey:curren_language!,forKey: "EXCLUDING_TOLLS")
        SURCHARGE = language.localizedString(forKey:curren_language!,forKey: "SURCHARGE")
        
        ONE_POINT_TITLE = language.localizedString(forKey:curren_language!,forKey: "ONE_POINT_TITLE")
        ONE_POINT_CONTENT = language.localizedString(forKey:curren_language!,forKey: "ONE_POINT_CONTENT")
        ONE_POINT_RESULT = language.localizedString(forKey:curren_language!,forKey: "ONE_POINT_RESULT")
        
        TWO_POINT_TITLE = language.localizedString(forKey:curren_language!,forKey: "TWO_POINT_TITLE")
        TWO_POINT_CONTENT = language.localizedString(forKey:curren_language!,forKey: "TWO_POINT_CONTENT")
        TWO_POINT_RESULT = language.localizedString(forKey:curren_language!,forKey: "TWO_POINT_RESULT")
        
        THREE_POINT_TITLE = language.localizedString(forKey:curren_language!,forKey: "THREE_POINT_TITLE")
        THREE_POINT_CONTENT = language.localizedString(forKey:curren_language!,forKey: "THREE_POINT_CONTENT")
        THREE_POINT_RESULT = language.localizedString(forKey:curren_language!,forKey: "THREE_POINT_RESULT")
        
        FOUR_POINT_TITLE = language.localizedString(forKey:curren_language!,forKey: "FOUR_POINT_TITLE")
        FOUR_POINT_CONTENT = language.localizedString(forKey:curren_language!,forKey: "FOUR_POINT_CONTENT")
        FOUR_POINT_RESULT = language.localizedString(forKey:curren_language!,forKey: "FOUR_POINT_RESULT")
        
        FIVE_POINT_TITLE = language.localizedString(forKey:curren_language!,forKey: "FIVE_POINT_TITLE")
        FIVE_POINT_CONTENT = language.localizedString(forKey:curren_language!,forKey: "FIVE_POINT_CONTENT")
        FIVE_POINT_RESULT = language.localizedString(forKey:curren_language!,forKey: "FIVE_POINT_RESULT")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        vRating.delegate = self
        if(DataNavigo.idUIController == 0){
            //DataNavigo.bookingInfo = DataNavigo.booking
            DataNavigo.idUIController = 1
        }else{
            DataNavigo.idUIController = 8
        }
        
        let backButton = UIBarButtonItem()
        backButton.title = " "
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        //navigationItem.title = "Kết thúc hành trình"
        navigationItem.title = DataNavigo.bookingInfo.bookDate
        
        navigationController?.navigationBar.tintColor = .white
        btnExtraCharge.addTarget(self, action: #selector(viewExtraCharge) , for: .touchUpInside)
        mutilTextField.delegate = self
        mutilTextField.returnKeyType = .done
        mutilTextField.layer.cornerRadius = 5
        mutilTextField.text = "Nhận xét của bạn...."
        mutilTextField.textColor = UIColor.lightGray
        //mutilTextField.autocapitalizationType = .words
        lblContent.numberOfLines = 0
        NotificationCenter.default.addObserver(self, selector: #selector(FeedBackController.keyboardDidShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(FeedBackController.keyboardWillBeHidden(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("viewWillAppear")
        loadViewLanguage()
        DistanceLabel.text = DISTANCE_
        PriceLabel.text = TOTAL_CHARGE
        PromotionLabel.text = PROMOTION
        lblPriceBook.text = PAYMENT
        excludingTolls.text = EXCLUDING_TOLLS
        ExtraLabel.text = SURCHARGE
//        btnRate
        //viewContent.addSubview(mutilTextField)
        setupView()
    }

    func textViewDidChange(_ textView: UITextView) {
        print("textViewDidChange", textView)
    }
    override func viewWillDisappear(_ animated: Bool) {
        print("ve man map roi")
//        DataNavigo.bookingInfo = Book()
//        if(DataNavigo.idUIController == 1){
//            DataNavigo.driverInfo.fullName = nil
//        }
    }
    
    @objc func viewExtraCharge() {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let signin = sb.instantiateViewController(withIdentifier: "ExtraChargeController") as! ExtraChargeController
        self.navigationController?.pushViewController(signin, animated: true)
    }
    
    
    @objc func keyboardDidShow(_ notification: NSNotification) {
        let keyBoard = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as! NSValue
        let sizeKey = keyBoard.cgRectValue
        let heightKey = sizeKey.height
        let kc = view.frame.height - (mutilTextField.frame.origin.y + heightKey + 30)

        if kc < 50 {
            let a = kc * -1 + 150
            bottomConst.constant = a
            let point:CGPoint = CGPoint(x: 0, y: a)
            scrollview.setContentOffset(point, animated: true)
        }
        
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if (text == "\n") {
            mutilTextField.resignFirstResponder()
        }
        return true
    }

    func textViewDidBeginEditing(_ textView: UITextView) {
        if mutilTextField.textColor == UIColor.lightGray {
            mutilTextField.text = ""
            mutilTextField.textColor = UIColor.black
        }
    }
    
    @objc func keyboardWillBeHidden(_ notification: NSNotification) {
        self.bottomConst.constant = 0
        UIView.animate(withDuration: 1) {
            self.scrollview.layoutSubviews()
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
            NotificationCenter.default.addObserver(self, selector: #selector(SignInController.keyboardDidShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        } else {
            textField.resignFirstResponder()
            return true;
        }
        return false
    }
    
    func setupView() {
        imgStart.image = #imageLiteral(resourceName: "des")
        imgEnd.image = #imageLiteral(resourceName: "cur")
        imgDriver.image = DataNavigo.driverInfo.avatarFace ?? nil
        imgDriver.layer.cornerRadius = 30
        imgDriver.clipsToBounds = true
        lblNameDriver.text = DataNavigo.driverInfo.fullName
        plateNumber.text = DataNavigo.bookingInfo.plateNumber ?? ""
        lblStart.textColor = UIColor.black
        lblStart.text = DataNavigo.bookingInfo.fromAddress
        lblEnd.text = DataNavigo.bookingInfo.toAddress
        btnRating.addTarget(self, action: #selector(feedBackDriver), for: .touchUpInside)
        btnRating.backgroundColor = NaviColor.naviBtnOrange
        btnRating.layer.cornerRadius = 5
        vLocation.layer.cornerRadius = 5
        //vPrice.layer.cornerRadius = 5
        vRate.layer.cornerRadius = 5
        vDistance.layer.cornerRadius = 5
        
        lblDistance.text = String(format: "%.1f", ((DataNavigo.bookingInfo.estDistance)!/1000)) + " Km"
        vRating.type = .wholeRatings
        vRating.editable = true
        let price = Int(ceilf(Float(DataNavigo.bookingInfo.estCharge!/1000)))*1000
        let extra = Int(ceilf(Float(DataNavigo.bookingInfo.extraCharge!/1000)))*1000
        let promotionPrice = Int(DataNavigo.bookingInfo.promotionValue!)
        let priBook = price - extra
        var priceValue = price - promotionPrice
        if (priceValue < 0){
            priceValue = 0
        }
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.groupingSeparator = "."
        let formattedString = formatter.string(for: DataNavigo.bookingInfo.estCharge)
        promotionValue.text = "-" + formatter.string(for: promotionPrice)! + " VNĐ"
        if DataNavigo.bookingInfo.state == BookingState.STATE_FINISH_TRIP {
            lblPrice.text = formatter.string(for: priBook)! + " VNĐ"
            extraCharge.text = formatter.string(for: extra)! + " VNĐ"
        }else{
            lblPrice.text =  "0 VNĐ"
            extraCharge.text = "0 VNĐ"
        }
        priceBook.text = formatter.string(for: priceValue)! + " VNĐ"
        if DataNavigo.bookingInfo.starPoint != 0 {
            lblMessage.text = "Bạn đã đánh giá dịch vụ"
            btnRating.alpha = 0
            vRating.rating = Double(DataNavigo.bookingInfo.starPoint)
            vRating.editable = false
            mutilTextField.isEditable = false
        }else{
            lblMessage.text = RATE_PLS
            btnRating.alpha = 1
            vRating.rating = 0
            vRating.editable = true
        }
    }
    
    @objc func feedBackDriver()  {
        //print("Start", driverRating)
        if driverRating == 0 {
            let alertController = UIAlertController(title: NOTIFICATION_TITLE, message: "Vui lòng chọn sao để đánh giá", preferredStyle: .alert)
            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in

            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion:nil)
        }else{
            starMark(start: driverRating)
        }
        
    }
    
    func detailDriver() -> UIView {
        let viewDetail = UIView()
        viewDetail.frame = CGRect(x: 0, y: 0, width: view.frame.height, height: 250)
        let navBarHeight = (self.navigationController?.navigationBar.intrinsicContentSize.height)! + UIApplication.shared.statusBarFrame.height
        let imageDiver = UIImageView()
        imageDiver.frame = CGRect(x: 100, y: navBarHeight + 20, width: 80, height: 80)
        imageDiver.image = DataNavigo.driverInfo.avatarFace
        imageDiver.layer.cornerRadius = 40
        imageDiver.clipsToBounds = true
        viewDetail.addSubview(imageDiver)
        imageDiver.center.x = self.view.center.x
        view.addSubview(lblDriverName)
        lblDriverName.frame = CGRect(x: 20, y: navBarHeight + 110, width: view.frame.width - 50, height: 25)
        lblDriverName.text = DataNavigo.booking.driver?.driverName
        lblDriverName.sizeToFit()
        lblDriverName.center.x = self.view.center.x
        
        return viewDetail
    }
    
    let lblDriverName:UILabel = {
        let lbl = UILabel()
        lbl.textColor = #colorLiteral(red: 0.9411764741, green: 0.4980392158, blue: 0.3529411852, alpha: 1)
        lbl.font = UIFont.systemFont(ofSize: 18)
        return lbl
    }()
    
    let viewRateDriver:FloatRatingView = {
        let rate = FloatRatingView()
        rate.contentMode = UIViewContentMode.scaleAspectFit
        rate.type = .floatRatings
        //rate.rating = 3.6
        rate.minRating = 0
        rate.maxRating = 5
        rate.editable = true
        rate.emptyImage = #imageLiteral(resourceName: "StarEmpty")
        rate.fullImage = #imageLiteral(resourceName: "StarFull")
        return rate
    }()
//    
//    func getPrice(service:Int) -> String {
//        let point = DataNavigo.arrService[service]
//        var price:String = ""
//        if MapController.locationSucsess {
//            let min = Float((point.fromPrice?.description)!)! * Float(DataNavigo.booking.estDistance!)
//            let max = Float((point.toPrice?.description)!)! * Float(DataNavigo.booking.estDistance!)
//            let priceMin = String(Int(ceilf(min/1000)))
//            let priceMax = String(Int(ceilf(max/1000)))
//            price = priceMin + " - " + priceMax + "K"
//        }else{
//            let priceMin = String(Int(ceilf(Float(point.fromPrice!))/1000))
//            let priceMax = String(Int(ceilf(Float(point.toPrice!))/1000))
//            price = priceMin + " - " + priceMax + "K/Km"
//        }
//        let formatter = NumberFormatter()
//        formatter.numberStyle = .decimal
//        formatter.groupingSeparator = "."
//        let formattedString = formatter.string(for: price)
//        //print(String(describing: formattedString))
//        return formattedString!
//    }
    
    let btnAccess:DesignButton = {
        let btn = DesignButton()
        btn.backgroundColor = NaviColor.naviBtnOrange
        btn.setTitle("Đánh giá",for: .normal)
        btn.rounded = true
        btn.buttonRadius = 5
        btn.borderWidth = 0.6
        return btn
    }()
    
    func starMark(start:Int)  {
        var messageNoti = ""
        
        switch Int(start) {
        case 1:
            messageNoti = ONE_POINT_RESULT
        case 2:
            messageNoti = TWO_POINT_RESULT
        case 3:
            messageNoti = THREE_POINT_RESULT
        case 4:
            messageNoti = FOUR_POINT_RESULT
        case 5:
            messageNoti = FIVE_POINT_RESULT
        default:
            messageNoti = ""
        }
        
        let CompletionHandler: ((NavigoError?) -> Void) = {
            navigoError in
            DispatchQueue.main.async {
                if(navigoError == nil){
                    let alertController = UIAlertController(title: self.NOTIFICATION_TITLE, message: messageNoti, preferredStyle: .alert)
                    let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                        let viewControllers:[UIViewController] = (self.navigationController?.viewControllers)!;
                        for controller in viewControllers {
                            if controller is MapController {
                                self.navigationController!.popToViewController(controller, animated: true)
                            }
                        }
                    }
                    alertController.addAction(OKAction)
                    self.present(alertController, animated: true, completion:nil)
                }else{
                    if(navigoError?.errorCode == Errorcode.ERROR_NO_INTERNET){
                    }
                    else{
                        if(navigoError?.errorCode == Errorcode.ERROR_NO_INTERNET){
                            if navigoError?.errorCode?.range(of: Errorcode.NAVIGO_SESSION_EXPIRED) != nil{
                                self.login()}
                        }
                    }
                }
            }
        }
        let starMark = APIstarMark();
        starMark.starMark(book: DataNavigo.bookingInfo, starMark: start, comment: mutilTextField.text, CompletionHandler: CompletionHandler)
    }
    
    /// login lai
    func login() {
        let CompletionHandler: ((LoginResult,NavigoError?) -> Void) = {
            loginResult,navigoError in
            DispatchQueue.main.async {
                if(navigoError == nil){
                    
                }else{
                    if(navigoError?.errorCode == Errorcode.ERROR_NO_INTERNET){
                        if navigoError?.errorCode?.range(of: Errorcode.NAVIGO_SESSION_EXPIRED) != nil{
                            self.login()
                        }
                    }
                }
            }
        }
        let connector = PassengerConnector();
        connector.reconnect(CompletionHandler: CompletionHandler)
    }
    
    
    
    
}
extension FeedBackController : FloatRatingViewDelegate {
    func floatRatingView(_ ratingView: FloatRatingView, isUpdating rating: Double) {
        print("---------------")
        print(rating)
        driverRating = Int(rating)
        
    }
    
    func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Double) {
        print("+++++++++++")
        print(rating)
        switch Int(rating) {
        case 1:
            lblTitle.text = ONE_POINT_TITLE
            lblContent.text = ONE_POINT_CONTENT
        case 2:
            lblTitle.text = TWO_POINT_TITLE
            lblContent.text = TWO_POINT_CONTENT
        case 3:
            lblTitle.text = THREE_POINT_TITLE
            lblContent.text = THREE_POINT_CONTENT
        case 4:
            lblTitle.text = FOUR_POINT_TITLE
            lblContent.text = FOUR_POINT_CONTENT
        case 5:
            lblTitle.text = FIVE_POINT_TITLE
            lblContent.text = FIVE_POINT_CONTENT
        default:
            lblTitle.text = ""
            lblContent.text = ""
        }
        driverRating = Int(rating)
    }
}

extension FeedBackController: HandingScreen {
    func errorConnect() {
        
    }
    func errorNoInternet() {
        // khong co internet
        
    }
    func errorServerSend(errorCode: String, errorToast: String) {
        if errorCode.range(of: Errorcode.NAVIGO_SESSION_EXPIRED) != nil{
            login()
        }
    }
    func startRequest() {

    }
    func endRequest() {

    }
}

