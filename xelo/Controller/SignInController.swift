//
//  SignInController.swift
//  NaviGo
//
//  Created by HoangManh on 9/20/17.
//  Copyright © 2017 DUMV. All rights reserved.
//

import UIKit
import Firebase

class SignInController: UIViewController,HandingScreen, UITextFieldDelegate,countryPickerProtocol  {
    func didPickCountry(model: Country) {
        localeCountry = model
        txtCountryCode.text = model.flag! + " " + "(+" + model.e164Cc! + ")"
    }
    
    private func addLocaleCountryCode() {
        
            localeCountry = countries.list.filter {($0.iso2Cc == "VN")}.first
            txtCountryCode.text = (localeCountry?.flag!)! + " " + "(+" + (localeCountry?.e164Cc!)! + ")"
    }
    let countries:Countries = {
        return Countries.init(countries: JSONReader.countries())
    }()
    let notificationName = Notification.Name("Did selected language")
    var language = [String]()
    var localeCountry:Country?
    var k:Int? = 1
    
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var contentview: UIView!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    func signAccount() {
           // print("sign account")// "+" + (localeCountry?.e164Cc!)!
        
        var phone = self.txtPhone.text!

        if phone.first == "0" {
            phone = phone.substring(from: 1)
        }
        print("phone",txtCountryCode.text! + phone)
        DataNavigo.passenger.nameUser = self.txtName.text!
        DataNavigo.passenger.emailUser = self.txtEmail.text!
        DataNavigo.passenger.referMobileNo = self.txtReferMobileNo.text!
        DataNavigo.passenger.numberphoneUser = "+" + (localeCountry?.e164Cc!)! + phone
        k = 1
        createAccount()
    }
    
    func createAccount() {
        let CompletionHandler: ((Passenger,NavigoError?) -> Void) = {
            passenger,navigoError in
            DispatchQueue.main.async {
                if(navigoError == nil){
                    //self.endRequest()
                    DataNavigo.passenger.acountID = passenger.acountID
                    DataNavigo.passenger.authenByFirebase = passenger.authenByFirebase
                    DataNavigo.passenger.accountExisting = passenger.accountExisting
                    if passenger.accountExisting && !passenger.authenByFirebase {
                        self.reactive()
                    }else if passenger.accountExisting && passenger.authenByFirebase{
                        self.endRequest()
                    }else{
                        self.endRequest()
                    }
                }else{
                    if(navigoError?.errorCode == Errorcode.ERROR_NO_INTERNET){
                        self.errorNoInternet()
                    }
                    else{
                        self.errorServerSend(errorCode: (navigoError?.errorCode)!, errorToast: (navigoError?.errorMessage)!)
                    }
                }
            }    
        }    
        let connector = PassengerConnector();
        connector.createAccount(passenger: DataNavigo.passenger, CompletionHandler: CompletionHandler)
    }

    func reactive() {
        let CompletionHandler: ((Passenger,NavigoError?) -> Void) = {
            passenger,navigoError in
            
            DispatchQueue.main.async {
                DataNavigo.passenger.acountID = passenger.acountID
                DataNavigo.passenger.securekey = passenger.securekey
                DataNavigo.passenger.authenByFirebase = passenger.authenByFirebase
                if(navigoError == nil){
                    self.endRequest()
                }else{
                    if(navigoError?.errorCode == Errorcode.ERROR_NO_INTERNET){
                        self.errorNoInternet()
                    }
                    else{
                        self.errorServerSend(errorCode: (navigoError?.errorCode)!, errorToast: (navigoError?.errorMessage)!)
                    }
                }
            }        }
        
        let connector = PassengerConnector();
        connector.reActiveAccount(passenger: DataNavigo.passenger, CompletionHandler: CompletionHandler)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        //navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor :UIColor.white]
        //self.navigationController?.navigationBar.barTintColor = NaviColor.navigation
        navigationController?.navigationBar.alpha = 1
    }
    
    @IBAction func changeLanguage(_ sender: Any) {
        
        let actionSheet = UIAlertController(title: "Language", message: "", preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: {(_ action: UIAlertAction) -> Void in
            self.dismiss(animated: true, completion: {() -> Void in })
        }))
        for item in language {
            actionSheet.addAction(UIAlertAction(title: item, style: .default, handler: {(_ action: UIAlertAction) -> Void in
                let langCode = Language.getLanguageCode(item)
                Language.setLanguage(langCode)
                self.loadViewLanguage()
                self.dismiss(animated: true, completion: {() -> Void in })
            }))
        }
        self.present(actionSheet, animated: true, completion: {})
    }
    
    
    @IBOutlet weak var btnLanguage: UIButton!
    @IBOutlet weak var btnSignin: DesignButton!
    
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var btnSignUp: DesignButton!
    @IBOutlet weak var txtCountryCode: UITextField!
    @IBOutlet weak var txtReferMobileNo: FloatLabelTextField!
    
    @IBOutlet weak var txtFirstPrivacy: UILabel!
    @IBOutlet weak var lblAnd: TapabbleLabel!
    @IBOutlet weak var lblPrivacy: TapabbleLabel!
    @IBOutlet weak var lblAgreement: TapabbleLabel!
    
    //DataLanguage---
    var noti_Title:String = "Thông báo"
    var CONFIRM:String = "Xác nhận"
    var CANCEL:String = "Huỷ"
    var EDIT:String = "Sửa"
    var ASK_PHONE_NUMBER :String = "Đây là số điện thoại của bạn?"
    var MISSING_INFORMATION = "Thiếu thông tin"
    var CONFIRM_SMS_NUMBER = "Bạn sẽ sớm nhận được SMS kích hoạt vào số điện thoại này. Bạn có xác nhận số điện thoại này không?"
    var LANGUAGE_VIEW = "Ngôn ngữ"
    //----
    var name:String!
    var email:String!
    var yFloat:CGFloat = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        language = ["English", "Việt Nam"]
        self.loadViewLanguage()
        txtName.text = name
        txtEmail.text = email
        startRequest()
        //contentview.addSubview(imgLogo)
        imgLogo.frame = CGRect(x: 10, y: 50, width: 122, height: 40)
        imgLogo.center.x = self.view.center.x
        contentview.addSubview(lblTitle)
        lblTitle.frame = CGRect(x: 10, y: 120, width: 280, height: 60)
        lblTitle.sizeToFit()
        lblTitle.center.x = self.view.center.x
        txtName.keyboardType = .default
        txtName.returnKeyType = UIReturnKeyType.next
        txtPhone.keyboardType = .numberPad
        txtPhone.returnKeyType = UIReturnKeyType.next
        txtEmail.keyboardType = .emailAddress
        txtEmail.returnKeyType = UIReturnKeyType.next
        txtReferMobileNo.keyboardType = .numberPad
        txtReferMobileNo.returnKeyType = UIReturnKeyType.next
        txtName.delegate = self
        txtReferMobileNo.delegate = self
        txtPhone.delegate = self
        txtEmail.delegate = self
        btnSignUp.buttonRadius = 5
        btnSignUp.rounded = true
        //btnSignUp.backgroundColor = UIColor(colorLiteralRed: 0, green: 150/255, blue: 255/255, alpha: 0.8)
        btnSignUp.addTarget(self, action: #selector(displayMessageNoti), for: .touchUpInside)
        // Do any additional setup after loading the view.
        addLocaleCountryCode()
            self.addDoneButtonOnKeyboard()
        NotificationCenter.default.addObserver(self, selector: #selector(SignInController.keyboardDidShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(SignInController.keyboardWillBeHidden(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.tapDetected(sender:)))
        self.lblPrivacy.addGestureRecognizer(tapGestureRecognizer)
        let tapGestureRecognizerAgreement = UITapGestureRecognizer(target: self, action: #selector(self.tapDetectedAgreement(sender:)))
        self.lblAgreement.addGestureRecognizer(tapGestureRecognizerAgreement)

    }
    @objc func tapDetected(sender: UITapGestureRecognizer) {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let activecode = sb.instantiateViewController(withIdentifier: "PrivacyController") as! PrivacyController
        //present(activecode, animated: true, completion: nil)
        self.navigationController?.pushViewController(activecode, animated: true)
        
    }
    @objc func tapDetectedAgreement(sender: UITapGestureRecognizer) {
        
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let activecode = sb.instantiateViewController(withIdentifier: "AgreementController") as! AgreementController
        //present(activecode, animated: true, completion: nil)
        self.navigationController?.pushViewController(activecode, animated: true)
        
    }

    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 45))
        doneToolbar.barStyle = UIBarStyle.default
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(SignInController.doneButtonAction))
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        
        doneToolbar.items = items as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        
        self.txtPhone.inputAccessoryView = doneToolbar
        self.txtReferMobileNo.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction()
    {
        self.txtPhone.resignFirstResponder()
        self.txtReferMobileNo.resignFirstResponder()
    }
    @objc func keyboardDidShow(_ notification: NSNotification) {
        let keyBoard = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as! NSValue
        let sizeKey = keyBoard.cgRectValue
        let heightKey = sizeKey.height
        let kc = view.frame.height - (yFloat + heightKey)
        if kc < 0 {
            let a = kc * -1 + 50
            bottomConstraint.constant = a
            let point:CGPoint = CGPoint(x: 0, y: a)
            scrollview.setContentOffset(point, animated: true)
        }
        
    }

    @IBAction func textfieldEdit(_ sender: Any) {
        let txt:UITextField = sender as! UITextField
        txt.delegate = self
        self.yFloat = txt.frame.origin.y + 30
    }
    
    @objc func keyboardWillBeHidden(_ notification: NSNotification) {
        self.bottomConstraint.constant = 0
        UIView.animate(withDuration: 1) {
            self.scrollview.layoutSubviews()
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
            NotificationCenter.default.addObserver(self, selector: #selector(SignInController.keyboardDidShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        } else {
            textField.resignFirstResponder()
            return true;
        }
        return false        
    }
    
    
    let buttonActive: DesignButton = {
        let btn = DesignButton()
        btn.backgroundColor = NaviColor.naviButton
        btn.setTitle("Đăng ký",for: .normal)
        btn.rounded = true
        btn.buttonRadius = 5
        btn.borderWidth = 0.6
        return btn
    }()
    
    let imgLogo:UIImageView = {
        let img = UIImageView()
        img.image = #imageLiteral(resourceName: "logo_flash")
        return img
    }()
    
    let lblAppName: UILabel = {
        let label = UILabel()
        label.text = "XELO"
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 20)
        label.textColor = NaviColor.naviLabel
        return label
        
    }()
    let lblTitle: UILabel = {
        let label = UILabel()
        label.text = ""
        label.textAlignment = .center
        label.numberOfLines = 2
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = UIColor.black
        return label
    }()
    
    func loadViewLanguage() {
        let curren_language = UserDefaults.standard.string(forKey: "curren_language")
        let language = LanguageData()
        //language.localizedString(forKey:curren_language!,forKey: "TITLE_NOTI_REGISTER")
        txtPhone.placeholder = language.localizedString(forKey:curren_language!,forKey: "PHONE_INPUT")
        txtName.placeholder = language.localizedString(forKey:curren_language!,forKey: "NAME_INPUT")
        txtEmail.placeholder = language.localizedString(forKey:curren_language!,forKey: "EMAIL_INPUT")
        lblTitle.text = language.localizedString(forKey:curren_language!,forKey: "TITLE_NOTI_REGISTER")
        txtFirstPrivacy.text = language.localizedString(forKey:curren_language!,forKey: "FIRST_TXT_PRIVACY")
        lblAnd.text = language.localizedString(forKey:curren_language!,forKey: "CONJUGATED_WORDS")
        lblPrivacy.text = language.localizedString(forKey:curren_language!,forKey: "PRIVACY_POLICY")
        lblAgreement.text = language.localizedString(forKey:curren_language!,forKey: "TERM_OF_SERVICE")
        lblPrivacy.text = language.localizedString(forKey:curren_language!,forKey: "PRIVACY_POLICY")
        lblAgreement.text = language.localizedString(forKey:curren_language!,forKey: "TERM_OF_SERVICE")
        btnSignUp.setTitle(language.localizedString(forKey:curren_language!,forKey: "REGISTER_PASSENGER"), for: .normal)
        noti_Title = language.localizedString(forKey:curren_language!,forKey: "NOTIFICATION_TITLE")
        CONFIRM = language.localizedString(forKey:curren_language!,forKey: "CONFIRM")
        EDIT = language.localizedString(forKey:curren_language!,forKey: "EDIT")
        CANCEL = language.localizedString(forKey:curren_language!,forKey: "CANCEL")
        //ASK_PHONE_NUMBER
        ASK_PHONE_NUMBER = language.localizedString(forKey:curren_language!,forKey: "ASK_PHONE_NUMBER")
        CONFIRM_SMS_NUMBER = language.localizedString(forKey:curren_language!,forKey: "CONFIRM_SMS_NUMBER")
        MISSING_INFORMATION = language.localizedString(forKey:curren_language!,forKey: "MISSING_INFORMATION")
        LANGUAGE_VIEW = language.localizedString(forKey:curren_language!,forKey: "LANGUAGE_VIEW")
        if curren_language == "EN" {
            btnLanguage.setImage(#imageLiteral(resourceName: "vnflag"), for: .normal)
        }else{
            btnLanguage.setImage(#imageLiteral(resourceName: "enflag"), for: .normal)
        }
    }
    
    
    @IBAction func showCountryCode(_ sender: Any) {
        let listScene = CountryCodeListController()
        listScene.delegate = self
        listScene.countries = countries
        navigationController?.pushViewController(listScene, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    fileprivate func isCheckValidateform() -> Bool {
        var ischeck = true
        let phone = txtPhone.text
        let providedEmailAddress = txtEmail.text
        _ = isValidEmailAddress(emailAddressString: providedEmailAddress!)
        if phone?.trimmingCharacters(in: .whitespaces).count != 0 {
            
        }else {
            displayAlertMessage(messageToDisplay: MISSING_INFORMATION)
            ischeck = false
        }
//        if !isEmailAddressValid{
//            displayAlertMessage(messageToDisplay: "Địa chỉ email không hợp lệ")
//            ischeck = false
//        }
        return ischeck
        //UserDefaults.standard.isLoggedIn()
    }
    func isValidEmailAddress(emailAddressString: String) -> Bool {
        
        var returnValue = true
        let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
        
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = emailAddressString as NSString
            let results = regex.matches(in: emailAddressString, range: NSRange(location: 0, length: nsString.length))
            
            if results.count == 0
            {
                returnValue = false
            }
            
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        
        return  returnValue
    }
    
    func displayAlertMessage(messageToDisplay: String)
    {
        if messageToDisplay != ""  {
            let alertController = UIAlertController(title: noti_Title, message: messageToDisplay, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: CONFIRM , style: .default) { (action:UIAlertAction!) in
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion:nil)
        }
    }
    
    @objc func displayMessageNoti()
    {
        if isCheckValidateform() {
            self.signAccount()
        }
    }
    func errorConnect() {
        // dang docve thi bi loi
      //  print("dsadsadsadsadasdsadsadda12132132132")
    }
    func errorNoInternet() {
        // khong co internet
      //  print("dsadsadsadsadasdsadsadda")
    }
    func errorServerSend(errorCode: String, errorToast: String) {
        if errorCode.range(of: Errorcode.NAVIGO_ACCOUNT_DUPLICATE) != nil{
            let alert:UIAlertController = UIAlertController(title: "Tài khoản đã được đăng ký", message: "Bạn có muốn kích hoạt lại tài khoản này không?", preferredStyle: UIAlertControllerStyle.alert)
            
            let btnOk:UIAlertAction = UIAlertAction(title: CONFIRM, style: .destructive) { (btnOk) in
                self.reactive()
            }
            let btnCancel:UIAlertAction = UIAlertAction(title: CANCEL, style: .destructive) { (btnCancel) in
                
            }
            alert.addAction(btnOk)
            alert.addAction(btnCancel)
            present(alert, animated: true, completion: nil)
        }else if errorCode.range(of: Errorcode.NAVIGO_ACCOUNT_NOT_FOUND) != nil{
            
        }else if errorCode.range(of: Errorcode.NAVIGO_ACCOUNT_ACTIVATED) != nil{
            
        }else if errorCode.range(of: Errorcode.NAVIGO_ACCOUNT_BLOCKED) != nil{
            
        }else if errorCode.range(of: Errorcode.NAVIGO_ACCOUNT_INVALID) != nil{
            
        }else if errorCode.range(of: Errorcode.NAVIGO_INVALID_EMAIL) != nil{
            
        }else if errorCode.range(of: Errorcode.NAVIGO_SKYMAP_ID_EXISTS) != nil{
            
        }else{
            let alert:UIAlertController = UIAlertController(title: noti_Title, message: errorToast, preferredStyle: UIAlertControllerStyle.alert)
            
            let btnOk:UIAlertAction = UIAlertAction(title: CONFIRM, style: .destructive) { (btnOk) in
            }
            alert.addAction(btnOk)
            present(alert, animated: true, completion: nil)
        }
    }
    func startRequest() {
        DataNavigo.handingScreen = self
    }
    
    func endRequest() {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let activecode = sb.instantiateViewController(withIdentifier: "SimpleActiveCodeController") as! SimpleActiveCodeController
        //present(activecode, animated: true, completion: nil)
        self.navigationController?.pushViewController(activecode, animated: true)
       
    }
}

