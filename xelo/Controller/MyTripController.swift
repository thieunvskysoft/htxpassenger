//
//  MyTripController.swift
//  xelo
//
//  Created by Nguyen Thieu on 2/29/20.
//  Copyright © 2020 DUMV. All rights reserved.
//

import UIKit

class MyTripController: UIViewController {

    var arrBook = [Book]()
    
    @IBOutlet weak var tbtrip: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        let backButton = UIBarButtonItem()
        backButton.title = " "
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        navigationController?.navigationBar.tintColor = .red
        self.navigationController?.navigationBar.barTintColor = .white
        navigationItem.title = "Cuốc xe đã đặt"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.red]
        self.tbtrip.rowHeight = 150
//        tbtrip.dataSource = self
//        tbtrip.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getLiveBooking()
    }
    
    func getLiveBooking()  {
        let CompletionHandler: ((Array<Book>,NavigoError?) -> Void) = {
            booking,navigoError in
            DispatchQueue.main.async {
                if(navigoError == nil){
                    self.arrBook = booking
                    self.tbtrip.reloadData()
                }else{
                    
                }
            }
        }
        let booking = APIBook();
        booking.getLiveBookings(CompletionHandler: CompletionHandler)
    }
    
}
extension MyTripController: UITableViewDelegate, UITableViewDataSource {
    
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrBook.count
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TripCell", for: indexPath) as! TripCell
        let info:Book = arrBook[indexPath.row]
        cell.lblStart.text = info.fromAddress
        cell.lblEnd.text = info.toAddress
        cell.lblTime.text = info.departureDate
        cell.lblPrice.text = "đ " + (info.fromCharge?.formatnumberPrice())! 
        cell.tripNote.text = info.note
        cell.tripId.text = info.bookingId
        if info.state == 0 {
            cell.lblType.text = "Đang tìm"
        }
        if info.state == 1 {
            cell.lblType.text = "Đã nhận"
        }
        if info.state == 2 {
            cell.lblType.text = "Đã đón"
        }
        if info.note == "" {
            cell.lblNoteLabel.text = ""
        }
        return (cell)
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let info:Book = arrBook[indexPath.row]
        if info.state == 0 {
                    let sb = UIStoryboard(name: "Main", bundle: nil)
            let tripInfo = sb.instantiateViewController(withIdentifier: "TripInfoController") as! TripInfoController
            tripInfo.tripInfo = arrBook[indexPath.row]
            self.navigationController?.pushViewController(tripInfo, animated: true)
        }else {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let tripInfo = sb.instantiateViewController(withIdentifier: "TripAcceptController") as! TripAcceptController
        tripInfo.tripInfo = arrBook[indexPath.row]
        self.navigationController?.pushViewController(tripInfo, animated: true)
        }


    }
}
