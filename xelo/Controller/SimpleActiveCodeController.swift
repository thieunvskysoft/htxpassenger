//
//  SimpleActiveCodeController.swift
//  xelo
//
//  Created by Nguyen Thieu on 4/26/18.
//  Copyright © 2018 DUMV. All rights reserved.
//

import UIKit
import Firebase

class SimpleActiveCodeController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var scrollActive: UIScrollView!
    @IBOutlet weak var contentActive: UIView!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var btnReactive: UIButton!
    
    @IBOutlet weak var btnNext: UIButton!
    
    var RESEND_CODE:String = "Gửi lại mã sau "
    var RESEND:String = "Gửi lại mã"
    var RESEND_TIME:String = " giây"
    var PLS_ENTER_ACTICE_CODE = "Vui lòng nhập mã kích hoạt!"
    var NOTIFICATION_TITLE:String = "Thông báo"
    var CONFIRM:String = "Xác nhận"
    var CANCEL:String = "Huỷ"
    
    static var phoneCountry:String?
    
    var verification:String?
    var tokenIDLogin:String?
    var userID:String?
    var statusActive:Int? = 1
    var countFirebase:Int? = 0
    var codeActive = ""
    var codeInputView:CodeActiveView!
    override func viewDidLoad() {
        super.viewDidLoad()
        btnNext.layer.cornerRadius = 30
        DataNavigo.handingScreen = self
        addDoneButtonOnKeyboard()
        let frame = CGRect(x: (view.frame.width-250)/2, y: 120, width: 250, height: 40)
        codeInputView = CodeActiveView(frame: frame)
        codeInputView.delegate = self
        contentActive.addSubview(codeInputView)
        codeInputView.keyboardType = .numberPad
        startTimer()
        codeInputView.becomeFirstResponder()
        codeInputView.center.x = self.view.center.x
        NotificationCenter.default.addObserver(self, selector: #selector(SimpleActiveCodeController.keyboardDidShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(SimpleActiveCodeController.keyboardWillBeHidden(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.tapDetected(sender:)))
        self.view.addGestureRecognizer(tapGestureRecognizer)
        btnNext.addTarget(self, action: #selector(nextToInfo), for: .touchUpInside)
        let tapGestureRecognizerActive = UITapGestureRecognizer(target: self, action: #selector(self.tapDetectedActive(sender:)))
        self.lblMessage.addGestureRecognizer(tapGestureRecognizerActive)
        btnReactive.addTarget(self, action: #selector(reActivecode), for: .touchUpInside)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        loadViewLanguage()
//        DataNavigo.driver.deviceSerial=UIDevice.current.identifierForVendor!.uuidString
//        let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
//        DataNavigo.driver.appVersion = version!
//        DataNavigo.driver.appos = Constant.PLATFORM_IOS
        if DataNavigo.passenger.authenByFirebase && countFirebase! < 3 {
            getCodeByFirebase()
        }
    }
    
    func loadViewLanguage() {
        let curren_language = UserDefaults.standard.string(forKey: "curren_language")
        let language = LanguageData()
        RESEND = language.localizedString(forKey:curren_language!,forKey: "RESEND")
        RESEND_CODE = language.localizedString(forKey:curren_language!,forKey: "RESEND_CODE")
        RESEND_TIME = language.localizedString(forKey:curren_language!,forKey: "RESEND_TIME")
        PLS_ENTER_ACTICE_CODE = language.localizedString(forKey:curren_language!,forKey: "PLS_ENTER_ACTICE_CODE")
        NOTIFICATION_TITLE = language.localizedString(forKey:curren_language!,forKey: "NOTIFICATION_TITLE")
        CONFIRM = language.localizedString(forKey:curren_language!,forKey: "CONFIRM")
        CANCEL = language.localizedString(forKey:curren_language!,forKey: "CANCEL")
    }
    
    func getCodeByFirebase() {
        print("DataNavigo.passenger.numberphoneUser",DataNavigo.passenger.numberphoneUser)
        countFirebase = countFirebase! + 1
        PhoneAuthProvider.provider().verifyPhoneNumber(DataNavigo.passenger.numberphoneUser, uiDelegate: nil) { (verificationID, error) in
            if let error = error {
                debugPrint("12312414",error.localizedDescription)
                return
            }
            print("verificationID",verificationID ?? "")
            self.verification = verificationID
        }
    }
    
    func signFirebase(verificationCode:String) {
        print("verification",verification ?? "signInAndRetrieveData(with:completion:) ")
        let credential = PhoneAuthProvider.provider().credential(withVerificationID: verification!, verificationCode: verificationCode)
        Auth.auth().signIn(with: credential) { (user, error) in
            if let error = error {
                debugPrint(error.localizedDescription)
                self.showMessage(message: "Vui lòng kiểm tra lại mã kích hoạt")
                self.btnNext.isEnabled = true
            }else {
                self.userID = user?.uid
                user?.getIDToken(completion: { (idToken, error) in
                    if let error = error {
                        debugPrint(error.localizedDescription)
                    }else {
                        self.tokenIDLogin = idToken
                        self.activeByFirebase()
                    }
                })
                
            }
        }
    }
    
    func showMessage(message:String) {
        let alert:UIAlertController = UIAlertController(title: NOTIFICATION_TITLE, message: message, preferredStyle: UIAlertControllerStyle.alert)
        let btnOk:UIAlertAction = UIAlertAction(title: CONFIRM, style: .destructive) { (btnOk) in
        }
        alert.addAction(btnOk)
        present(alert, animated: true, completion: nil)
    }
    
    @objc func tapDetected(sender: UITapGestureRecognizer) {
        codeInputView.becomeFirstResponder()
        
    }
    @objc func tapDetectedActive(sender: UITapGestureRecognizer) {
        startTimer()
    }
    func activecode() {
        if codeActive.count < 6 {
            let alert:UIAlertController = UIAlertController(title: NOTIFICATION_TITLE, message: PLS_ENTER_ACTICE_CODE, preferredStyle: UIAlertControllerStyle.alert)
            let btnOk:UIAlertAction = UIAlertAction(title: CONFIRM, style: .destructive) { (btnOk) in
            }
            alert.addAction(btnOk)
            present(alert, animated: true, completion: nil)
        }else{
            btnNext.backgroundColor = UIColor.gray
            btnNext.isEnabled = false
            statusActive = 1
            
            if DataNavigo.passenger.authenByFirebase && countFirebase! < 3 {
                signFirebase(verificationCode: codeActive)
            }else{
                active()
            }
        }
    }
    
    @objc func reActivecode() {
        if DataNavigo.passenger.authenByFirebase && countFirebase! < 2 {
            getCodeByFirebase()
        }else{
            reactiveAccount()
        }
        btnNext.isEnabled = true
        startTimer()
        btnReactive.isEnabled = false
    }
    
    //active
    func active() {
        let CompletionHandler: ((Passenger,NavigoError?) -> Void) = {
            passenger,navigoError in
            DispatchQueue.main.async {
                DataNavigo.passenger.acountID = passenger.acountID
                DataNavigo.passenger.securekey = passenger.securekey
                if(navigoError == nil){
                    self.login()
                }else{
                    if(navigoError?.errorCode == Errorcode.ERROR_NO_INTERNET){
                        self.errorNoInternet()
                    } else{
                        self.errorServerSend(errorCode: (navigoError?.errorCode)!, errorToast: (navigoError?.errorMessage)!)
                    }
                }
            }
        }
        let connector = PassengerConnector();
        connector.activeAccount(passenger: DataNavigo.passenger, CompletionHandler: CompletionHandler, activeKey: codeActive)
    }
    
    //active
    func activeByFirebase() {
        print("111",userID,"------------", tokenIDLogin!)
        let CompletionHandler: ((Passenger,NavigoError?) -> Void) = {
            passenger,navigoError in
            DispatchQueue.main.async {
                DataNavigo.passenger.acountID = passenger.acountID
                DataNavigo.passenger.securekey = passenger.securekey
                if(navigoError == nil){
                    try? Auth.auth().signOut()
                    self.login()
                }else{
                    if(navigoError?.errorCode == Errorcode.ERROR_NO_INTERNET){
                        self.errorNoInternet()
                    } else{
                        self.errorServerSend(errorCode: (navigoError?.errorCode)!, errorToast: (navigoError?.errorMessage)!)
                    }
                }
            }
        }
        let connector = PassengerConnector();
        connector.activeAccountByFirebase(passenger: DataNavigo.passenger, uID: userID!, tokenLogin: tokenIDLogin!, CompletionHandler: CompletionHandler, activeKey: codeActive)
    }
    
    //reactive
    func reactiveAccount() {
        countFirebase = countFirebase! + 1
        let CompletionHandler: ((Passenger,NavigoError?) -> Void) = {
            passenger,navigoError in
            DispatchQueue.main.async {
                DataNavigo.passenger.acountID = passenger.acountID
                DataNavigo.passenger.securekey = passenger.securekey
                if(navigoError == nil){
                    //self.endRequest()
                    //self.login()
                }else{
                    if(navigoError?.errorCode == Errorcode.ERROR_NO_INTERNET){
                        self.errorNoInternet()
                    }
                    else{
                        self.errorServerSend(errorCode: (navigoError?.errorCode)!, errorToast: (navigoError?.errorMessage)!)
                    }
                }
            }
        }
        let connector = PassengerConnector();
        connector.reActiveAccount(passenger: DataNavigo.passenger, CompletionHandler: CompletionHandler)
    }  
    
    func login() {
        let CompletionHandler: ((LoginResult,NavigoError?) -> Void) = {
            loginResult,navigoError in
            DispatchQueue.main.async {
                DataNavigo.passenger.saveInformationUser()
                DataNavigo.arrService = (loginResult.arrService)
                DataNavigo.booking = (loginResult.booking)
                DataNavigo.loginResult = loginResult
                if(navigoError == nil){
                    let sb = UIStoryboard(name: "Main", bundle: nil)
                    let skymap = sb.instantiateViewController(withIdentifier: "HomeController") as! HomeController
                    self.navigationController?.pushViewController(skymap, animated: true)
                }else{
                    if(navigoError?.errorCode == Errorcode.ERROR_NO_INTERNET){
                        self.errorNoInternet()
                    }
                    else{
                        self.errorServerSend(errorCode: (navigoError?.errorCode)!, errorToast: (navigoError?.errorMessage)!)
                    }
                }
            }
        }
        let connector = PassengerConnector();
        connector.loginAccount(passenger: DataNavigo.passenger, reconnect: false, CompletionHandler: CompletionHandler)
    }
    
    var countdownTimer: Timer!
    var totalTime = 30
    func startTimer() {
        totalTime = 30
        countdownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
    }
    
    @objc func updateTime() {
        if totalTime != 0 {
            totalTime -= 1
            lblMessage.text = RESEND_CODE + totalTime.description + RESEND_TIME
            lblMessage.textColor = UIColor.black
        } else {
            endTimer()
            lblMessage.text = RESEND
            lblMessage.textColor = UIColor.orange
            btnReactive.isEnabled = true
        }
    }
    
    func endTimer() {
        countdownTimer.invalidate()
    }
    
    
    @objc func keyboardDidShow(_ notification: NSNotification) {
        let keyBoard = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as! NSValue
        let sizeKey = keyBoard.cgRectValue
        let heightKey = sizeKey.height
        let kc = view.frame.height - (codeInputView.frame.origin.y + heightKey + 150)
        if kc < 0 {
            let a = kc * -1 + 30
            bottomConstraint.constant = a
            let point:CGPoint = CGPoint(x: 0, y: a)
            scrollActive.setContentOffset(point, animated: true)
        }
        
    }
    
    @objc func nextToInfo() {
        activecode()
    }
    @objc func keyboardWillBeHidden(_ notification: NSNotification) {
        self.bottomConstraint.constant = 0
        UIView.animate(withDuration: 1) {
            self.scrollActive.layoutSubviews()
        }
    }
    
    func addDoneButtonOnKeyboard()
    {
        var doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 45))
        doneToolbar.barStyle = UIBarStyle.default
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: Selector("doneButtonAction"))
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        doneToolbar.items = items as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
    }
    
    func doneButtonAction()
    {
        self.codeInputView.resignFirstResponder()
    }
    
    @IBAction func btnback(_ sender: Any) {
        navigationController?.popViewController(animated: true)
        
    }
}
extension SimpleActiveCodeController:HandingScreen{
    func errorNoInternet() {
        
    }
    
    func errorConnect() {
        
    }
    
    func errorServerSend(errorCode: String, errorToast: String) {
        let alert:UIAlertController = UIAlertController(title: NOTIFICATION_TITLE, message: errorToast, preferredStyle: UIAlertControllerStyle.alert)
        let btnOk:UIAlertAction = UIAlertAction(title: CONFIRM, style: .destructive) { (btnOk) in
        }
        alert.addAction(btnOk)
        present(alert, animated: true, completion: nil)
    }
    
    func startRequest() {
        
    }
    
    func endRequest() {
        if(statusActive == 1){
            //DataNavigo.driver.saveInformationUser()
            //login()
        }else if(statusActive == 2){

        }else if(statusActive == 3){
            let sb = UIStoryboard(name: "Main", bundle: nil)
            let skymap = sb.instantiateViewController(withIdentifier: "MapController") as! MapController
            self.navigationController?.pushViewController(skymap, animated: true)
        }
        
        
    }
    
    
}

extension SimpleActiveCodeController: CodeActiveViewDelegate{
    func codeInputView(_ codeInputView: CodeActiveView, didFinishWithCode code: String)
    {
        codeActive = code
        print("code")
    }
}


