//
//  InformationDriver.swift
//  NaviGo
//
//  Created by HoangManh on 10/6/17.
//  Copyright © 2017 DUMV. All rights reserved.
//

import UIKit

class InformationDriverController: UIViewController {
    var timer:Timer? = nil
    
    var PLATE_NUMBER:String = "Biển số: "
    var VEHICLE_TYPE:String = "Loại xe: "
    var VEHICLE_COLOR:String = "Màu xe: "
    var SLIDE_TO_CANCEL:String = "Trượt để huỷ chuyến"
    var DISTANCE_:String = "Quãng đường: "
    var PROMOTION:String = "Khuyến mại: "
    var ONE_WAY_TITLE:String = "Chuyến 1 chiều"
    var TWO_WAY_TITLE:String = "Chuyến 2 chiều"
    var NOTIFICATION_TITLE:String = "Thông báo"
    var CONFIRM:String = "Xác nhận"
    var CANCEL:String = "Huỷ"
    var CHARGE_BOOK:String = "Giá cước: "
    var DRIVER_INFORMATION:String = "Thông tin tài xế"
    
    
    override func viewDidLoad() {
        
        if(DataNavigo.booking.state == BookingState.STATE_NEW){
            //Đã đặt chuyến -- đang tìm tài xế
        }else if(DataNavigo.booking.state == BookingState.STATE_ACCEPT_BY_DRIVER){
            
            if DataNavigo.booking.fixCharge {
                detailBook.lblPrice.text = "N/A"
            }else{
                
            }
        }else if(DataNavigo.booking.state == BookingState.STATE_PICKED_UP){
            //Tài xế xác nhận đón khách
            
            btnSlideCancel.isHidden = true
            if DataNavigo.booking.fixCharge {
            }else{
                
            }
            //self.showToast(message: "Bắt đầu chuyến đi")
        }else if(DataNavigo.booking.state == BookingState.STATE_FINISH_TRIP){
            
           
            
        }else if(DataNavigo.booking.state == BookingState.STATE_CANCEL_BY_DRIVER){
            
            
            
        }else if(DataNavigo.booking.state == BookingState.STATE_CANCEL_BY_PASSENGER){
            
          
        }else {
            
        }
        
    
        
        
        super.viewDidLoad()
        DataNavigo.idUIController = 5
        
        btnSlideCancel.delegate = self
        //btnSlideCancel.disable()
        self.navigationController?.isNavigationBarHidden = false
        let backButton = UIBarButtonItem()
        backButton.title = ""
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        let navBarHeight = (self.navigationController?.navigationBar.intrinsicContentSize.height)! + UIApplication.shared.statusBarFrame.height
        //self.navigationController?.navigationBar.topItem?.backBarButtonItem?.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        navigationController?.navigationBar.tintColor = .white
        //navigationItem.title = "Thông tin tài xế"
        self.view.addSubview(detailDriver())
        self.view.addSubview(detailBook)
        detailBook.frame = CGRect(x: 5, y: navBarHeight + 260, width: view.frame.width - 10 , height: 130)
        
        self.view.addSubview(btnSlideCancel)
        btnSlideCancel.frame = CGRect(x: 20, y: view.frame.size.height - 100, width: view.frame.size.width - 40, height: 50)
        self.view.addSubview(btnLoading)
        btnLoading.frame = CGRect(x: 20, y: view.frame.size.height - 100, width: view.frame.size.width - 40, height: 50)
        btnLoading.alpha = 0

        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(countDownTimer), userInfo: nil, repeats: true)
    }
    
    
    override func viewWillLayoutSubviews() {

    }
    override func loadView() {
        countDownTimer()

    }
    
    override func viewDidLayoutSubviews() {
        
        countDownTimer()

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        //Về map-----
        if timer != nil {
            timer?.invalidate()
            timer = nil
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        countDownTimer()
        loadViewLanguage()
        btnSlideCancel.buttonText = SLIDE_TO_CANCEL
        navigationItem.title = DRIVER_INFORMATION
    }
    
    func loadViewLanguage() {
        let curren_language = UserDefaults.standard.string(forKey: "curren_language")
        let language = LanguageData()
        PLATE_NUMBER = language.localizedString(forKey:curren_language!,forKey: "PLATE_NUMBER")
        VEHICLE_TYPE = language.localizedString(forKey:curren_language!,forKey: "VEHICLE_TYPE")
        VEHICLE_COLOR = language.localizedString(forKey:curren_language!,forKey: "VEHICLE_COLOR")
        SLIDE_TO_CANCEL = language.localizedString(forKey:curren_language!,forKey: "SLIDE_TO_CANCEL")
        DISTANCE_ = language.localizedString(forKey:curren_language!,forKey: "DISTANCE_")
        ONE_WAY_TITLE = language.localizedString(forKey:curren_language!,forKey: "ONE_WAY_TITLE")
        TWO_WAY_TITLE = language.localizedString(forKey:curren_language!,forKey: "TWO_WAY_TITLE")
        PROMOTION = language.localizedString(forKey:curren_language!,forKey: "PROMOTION")
        NOTIFICATION_TITLE = language.localizedString(forKey:curren_language!,forKey: "NOTIFICATION_TITLE")
        CONFIRM = language.localizedString(forKey:curren_language!,forKey: "CONFIRM")
        CANCEL = language.localizedString(forKey:curren_language!,forKey: "CANCEL")
        CHARGE_BOOK = language.localizedString(forKey:curren_language!,forKey: "CHARGE_BOOK")
        DRIVER_INFORMATION = language.localizedString(forKey:curren_language!,forKey: "DRIVER_INFORMATION")
    }
    
    func showToast(message : String) {
        
        let toastLabel = UILabel(frame: CGRect(x: 10, y: self.view.frame.size.height-100, width: self.view.frame.size.width - 20 , height: 35))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "Montserrat-Light", size: 12.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 15;
        toastLabel.clipsToBounds  =  true
        
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 2.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    
    func detailDriver() -> UIView {
        let viewDetail = UIView()
        viewDetail.frame = CGRect(x: 0, y: 0, width: view.frame.height, height: view.frame.height / 2)
        let navBarHeight = (self.navigationController?.navigationBar.intrinsicContentSize.height)! + UIApplication.shared.statusBarFrame.height
        let imageDiver = UIImageView()
        imageDiver.frame = CGRect(x: 100, y: navBarHeight + 20, width: 80, height: 80)
        imageDiver.image = DataNavigo.driverInfo.avatarFace
        imageDiver.layer.cornerRadius = 40
        imageDiver.clipsToBounds = true
        viewDetail.addSubview(imageDiver)
        imageDiver.center.x = self.view.center.x
        
        view.addSubview(lblDriverName)
        lblDriverName.frame = CGRect(x: 20, y: navBarHeight + 110, width: view.frame.width - 50, height: 25)
        lblDriverName.text = DataNavigo.booking.driver?.driverName
        lblDriverName.sizeToFit()
        lblDriverName.center.x = self.view.center.x
        
        viewRateDriver.frame = CGRect(x: view.frame.width / 2 + 50, y: navBarHeight + 140, width: 140, height: 25)
        viewRateDriver.rating = DataNavigo.driverInfo.starPoint!
        viewDetail.addSubview(viewRateDriver)
        viewRateDriver.center.x = self.view.center.x
        
        viewDetail.addSubview(lblCarNumber)
        lblCarNumber.text = PLATE_NUMBER + DataNavigo.booking.plateNumber!
        lblCarNumber.font = UIFont.systemFont(ofSize: 14)
        lblCarNumber.frame = CGRect(x: 20, y: navBarHeight + 180, width: 100, height: 25)
        lblCarNumber.sizeToFit()
        
        viewDetail.addSubview(lblServiceCar)
        lblServiceCar.text = VEHICLE_TYPE + DataNavigo.driverInfo.vehicleType!
        lblServiceCar.font = UIFont.systemFont(ofSize: 14)
        lblServiceCar.frame = CGRect(x: 20, y: navBarHeight + 205, width: 100, height: 25)
        lblServiceCar.sizeToFit()
        
        viewDetail.addSubview(lblColorCar)
        lblColorCar.text = VEHICLE_COLOR + DataNavigo.driverInfo.color!
        lblColorCar.font = UIFont.systemFont(ofSize: 14)
        lblColorCar.frame = CGRect(x: 20, y: navBarHeight + 230, width: 100, height: 25)
        lblColorCar.sizeToFit()
        
        return viewDetail
    }

    let btnSlideCancel: MMSlidingButton = {
        let button = MMSlidingButton()
        button.buttonText = "Trượt để huỷ chuyến"
        button.dragPointWidth = 50
        button.dragPointColor = NaviColor.naviBtnOrange
        button.imageName = #imageLiteral(resourceName: "Arrorw")
        button.buttonColor = NaviColor.naviButton
        button.buttonDragPoint = true
        return button
    }()
    
    let btnLoading: LoadingButton = {
        let button = LoadingButton()
        return button
    }()
    
    let detailBook: ViewSlider = {
        let v = ViewSlider()
        v.lblStart.text = DataNavigo.booking.fromAddress != nil ? DataNavigo.booking.fromAddress! :"Unknow"
        v.lblEnd.text = DataNavigo.booking.toAddress != nil ? DataNavigo.booking.toAddress! :"Unknow"
        v.lblDistance.text = String(format: "%.1f", ((DataNavigo.booking.estDistance)!/1000)) + " Km"
     //   print("")
        //v.lblPrice.text = String(Int(ceilf(Float(DataNavigo.booking.tariffPrice! * DataNavigo.booking.estDistance!/1000000) ))*1000) + " VNĐ"
        v.lblStart.textColor = #colorLiteral(red: 0.9411764741, green: 0.4980392158, blue: 0.3529411852, alpha: 1)
        v.lblEnd.textColor = #colorLiteral(red: 0.9411764741, green: 0.4980392158, blue: 0.3529411852, alpha: 1)
        v.lblDistance.textColor = #colorLiteral(red: 0.9411764741, green: 0.4980392158, blue: 0.3529411852, alpha: 1)
        v.lblPrice.textColor = #colorLiteral(red: 0.9411764741, green: 0.4980392158, blue: 0.3529411852, alpha: 1)
        v.imgStart.image = #imageLiteral(resourceName: "des")
        v.imgStart.contentMode = .center
        v.imgEnd.image = #imageLiteral(resourceName: "cur")
        v.imgEnd.contentMode = .center
        return v
    }()
    
    let lblDriverName:UILabel = {
        let lbl = UILabel()
        lbl.textColor = #colorLiteral(red: 0.9411764741, green: 0.4980392158, blue: 0.3529411852, alpha: 1)
        lbl.font = UIFont.systemFont(ofSize: 18)
        return lbl
    }()
    let lblCarNumber:UILabel = {
        let lbl = UILabel()
        lbl.textColor = #colorLiteral(red: 0.9411764741, green: 0.4980392158, blue: 0.3529411852, alpha: 1)
        return lbl
    }()
    let lblServiceCar:UILabel = {
        let lbl = UILabel()
        lbl.textColor = #colorLiteral(red: 0.9411764741, green: 0.4980392158, blue: 0.3529411852, alpha: 1)
        return lbl
    }()
    let lblColorCar:UILabel = {
        let lbl = UILabel()
        lbl.textColor = #colorLiteral(red: 0.9411764741, green: 0.4980392158, blue: 0.3529411852, alpha: 1)
        return lbl
    }()
    let viewRateDriver:FloatRatingView = {
        let rate = FloatRatingView()
        rate.contentMode = UIViewContentMode.scaleAspectFit
        rate.type = .floatRatings
        rate.rating = 3.6
        rate.minRating = 0
        rate.maxRating = 5
        rate.editable = false
        rate.emptyImage = #imageLiteral(resourceName: "StarEmpty")
        rate.fullImage = #imageLiteral(resourceName: "StarFull")
        return rate
    }()
    
    @objc func countDownTimer() {
        if(DataNavigo.booking.state == BookingState.STATE_NEW){
            //Đã đặt chuyến -- đang tìm tài xế  
        }else if(DataNavigo.booking.state == BookingState.STATE_ACCEPT_BY_DRIVER){
            //Tài xế nhận chuyến --- đang đi đón
            let backButton = UIBarButtonItem()
            backButton.title = " "
            self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
            
            btnSlideCancel.alpha = 1
            detailBook.labelPrice.text = CHARGE_BOOK
            if DataNavigo.booking.fixCharge {
                detailBook.lblPrice.text = "N/A"
            }else{
                let formatter = NumberFormatter()
                formatter.numberStyle = .decimal
                formatter.groupingSeparator = ","
                //Tiền server trả về, k nhân-- sửa lại
                let formattedString = formatter.string(for: Int(ceilf(Float(DataNavigo.booking.estCharge!/1000)))*1000)
                // print(String(describing: formattedString))
                detailBook.lblPrice.text = formattedString! + " VNĐ"
            }
        }else if(DataNavigo.booking.state == BookingState.STATE_PICKED_UP){
            //Tài xế xác nhận đón khách
            
            btnSlideCancel.alpha = 0
            detailBook.labelPrice.text = CHARGE_BOOK
            if DataNavigo.booking.fixCharge {
                detailBook.lblPrice.text = "N/A"
            }else{
                let formatter = NumberFormatter()
                formatter.numberStyle = .decimal
                formatter.groupingSeparator = ","
                let formattedString = formatter.string(for: Int(ceilf(Float(DataNavigo.booking.estCharge!/1000)))*1000)
                //  print(String(describing: formattedString))
                detailBook.lblPrice.text = formattedString! + " VNĐ"
            }
            //self.showToast(message: "Bắt đầu chuyến đi")
        }else if(DataNavigo.booking.state == BookingState.STATE_FINISH_TRIP){
            
            let viewControllers:[UIViewController] = (self.navigationController?.viewControllers)!;
            for controller in viewControllers {
                if controller is MapController {
                    self.navigationController!.popToViewController(controller, animated: true)
                }
            } 
            
        }else if(DataNavigo.booking.state == BookingState.STATE_CANCEL_BY_DRIVER){
            
            DataNavigo.booking.state = BookingState.STATE_NONE
            DataNavigo.booking.clean()
            
        }else if(DataNavigo.booking.state == BookingState.STATE_CANCEL_BY_PASSENGER){
            
            DataNavigo.booking.state = BookingState.STATE_NONE
            DataNavigo.booking.clean()
        }else {
            
        } 
    }
}
extension InformationDriverController: SlideButtonDelegate {
    
    func buttonStatus(status: String, sender: MMSlidingButton) {
        if (status == "Success") {
            self.btnLoading.startAnimating()
            self.btnLoading.alpha = 1
            let sb = UIStoryboard(name: "Main", bundle: nil)
            let skymap = sb.instantiateViewController(withIdentifier: "ReasonsController") as! ReasonsController
            self.navigationController?.pushViewController(skymap, animated: true)

        }
    }
}


extension InformationDriverController: HandingScreen {
    func errorConnect() {
        
        
    }
    func errorNoInternet() {
        // khong co internet
        
    }
    func errorServerSend(errorCode: String, errorToast: String) {
  
    }
    func startRequest() {
 
    }
    func endRequest() {
        if(DataNavigo.booking.bookingId != nil ){
            DataNavigo.runCheck = true
            if(DataNavigo.booking.state == BookingState.STATE_NEW){
                //Đã đặt chuyến -- đang tìm tài xế
                
            }else if(DataNavigo.booking.state == BookingState.STATE_ACCEPT_BY_DRIVER){
         
            }else if(DataNavigo.booking.state == BookingState.STATE_PICKED_UP){

            }else if(DataNavigo.booking.state == BookingState.STATE_FINISH_TRIP){
                let viewControllers:[UIViewController] = (self.navigationController?.viewControllers)!;
                for controller in viewControllers {
                    if controller is MapController {
                        self.navigationController!.popToViewController(controller, animated: true)
                    }
                } 
            }else if(DataNavigo.booking.state == BookingState.STATE_CANCEL_BY_DRIVER){
                DataNavigo.booking.state = BookingState.STATE_NONE
                DataNavigo.booking.clean()                
            }else if(DataNavigo.booking.state == BookingState.STATE_CANCEL_BY_PASSENGER){
                DataNavigo.booking.state = BookingState.STATE_NONE
                DataNavigo.booking.clean()
            }else {
                
            }
        }else {
         //   print("Da huy thanh cong")
            self.btnLoading.stopAnimating()
            self.btnLoading.alpha = 0
            let viewControllers:[UIViewController] = (self.navigationController?.viewControllers)!;
            for controller in viewControllers {
                if controller is MapController {
                    self.navigationController!.popToViewController(controller, animated: true)
                }
            }
        }
    }
    
    
}
