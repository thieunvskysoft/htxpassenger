//
//  TabViewController.swift
//  NaviGo
//
//  Created by HoangManh on 9/20/17.
//  Copyright © 2017 DUMV. All rights reserved.
//

import UIKit

class TabViewController: UIViewController {

    enum TabIndex : Int {
        case firstChildTab = 0
        case secondChildTab = 1
        case threeChildTab = 2
    }
    
    @IBOutlet weak var segmentedControl: TwicketSegmentedControl!
    

    @IBOutlet weak var contentView: UIView!
    
    var currentViewController: UIViewController?
    lazy var firstChildTabVC: UIViewController? = {
        let firstChildTabVC = self.storyboard?.instantiateViewController(withIdentifier: "SearchPlaceController")
        return firstChildTabVC
    }()
    lazy var secondChildTabVC : UIViewController? = {
        let secondChildTabVC = self.storyboard?.instantiateViewController(withIdentifier: "ListPointController")
        return secondChildTabVC
    }()
    lazy var threeChildTabVC : UIViewController? = {
        let threeChildTabVC = self.storyboard?.instantiateViewController(withIdentifier: "LocationViewController")
        return threeChildTabVC
    }()
    
    
    var MAP_TAB_TITLE:String = "Bản đồ"
    var SEARCH_BY_MAP_TITLE:String = "Tìm kiếm"
    var FAVORITE_POINT:String = "Yêu thích"
    var CHOOSE_START_P:String = "Chọn điểm đi"
    var CHOOSE_DROP_OFF:String = "Chọn điểm đến"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarStyle = .lightContent
        self.navigationController?.isNavigationBarHidden = false
    //    print("-------------------------")
        getAll()
        loadViewLanguage()
        let titles = [SEARCH_BY_MAP_TITLE, FAVORITE_POINT, MAP_TAB_TITLE]
        segmentedControl.setSegmentItems(titles)
        segmentedControl.delegate = self
        displayCurrentTab(TabIndex.firstChildTab.rawValue)
    }
    
    func loadViewLanguage() {
        let curren_language = UserDefaults.standard.string(forKey: "curren_language")
        let language = LanguageData()
        MAP_TAB_TITLE = language.localizedString(forKey:curren_language!,forKey: "MAP_TAB_TITLE")
        SEARCH_BY_MAP_TITLE = language.localizedString(forKey:curren_language!,forKey: "SEARCH_BY_MAP_TITLE")
        FAVORITE_POINT = language.localizedString(forKey:curren_language!,forKey: "FAVORITE_POINT")
        CHOOSE_START_P = language.localizedString(forKey:curren_language!,forKey: "CHOOSE_START_P")
        CHOOSE_DROP_OFF = language.localizedString(forKey:curren_language!,forKey: "CHOOSE_DROP_OFF")
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
     //   print("==========================")
        UIApplication.shared.statusBarStyle = .lightContent
        self.navigationController?.isNavigationBarHidden = false
        let backButton = UIBarButtonItem()
        backButton.title = " "
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        //self.navigationController?.navigationBar.topItem?.backBarButtonItem?.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        //navigationController?.navigationBar.tintColor = .white
        if getLocation.choice == 1 {
            navigationItem.title = CHOOSE_START_P
        }else{
            navigationItem.title = CHOOSE_DROP_OFF
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if let currentViewController = currentViewController {
            currentViewController.viewWillDisappear(animated)
        }
    }
    
    func getAll()  {
        let CompletionHandler: ((Array<Address>,NavigoError?) -> Void) = {
            lisAddress,navigoError in
            
            DispatchQueue.main.async {
                if(navigoError == nil){
                    DataNavigo.arrAddress.removeAll()
                    DataNavigo.arrAddress = lisAddress
                   // print("diemyeuthich",DataNavigo.arrAddress.count)
                    //  self.tblListPoint.reloadData()
                    //NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refresh"), object: nil, userInfo: nil)
                }else{
                    if(navigoError?.errorCode == Errorcode.ERROR_NO_INTERNET){
                        
                    }
                    else{
                        
                    }
                }
            }
        }
        let apiAddress = APIAddress();
        apiAddress.getListAddress(CompletionHandler: CompletionHandler)
    }
    
    func displayCurrentTab(_ tabIndex: Int){
        if let vc = viewControllerForSelectedSegmentIndex(tabIndex) {
            
            self.addChildViewController(vc)
            vc.didMove(toParentViewController: self)
            vc.view.frame = self.contentView.bounds
            self.contentView.addSubview(vc.view)
            self.currentViewController = vc
            view.endEditing(true)
        }
    }
    
    func viewControllerForSelectedSegmentIndex(_ index: Int) -> UIViewController? {
        var vc: UIViewController?
        switch index {
        case TabIndex.firstChildTab.rawValue :
            vc = firstChildTabVC
        case TabIndex.secondChildTab.rawValue :
            vc = secondChildTabVC
        case TabIndex.threeChildTab.rawValue :
            vc = threeChildTabVC
            //getAll()
        default:
            return nil
        }
        
        return vc
    }
    
}
extension TabViewController: TwicketSegmentedControlDelegate{
    func didSelect(_ segmentIndex: Int) {
        displayCurrentTab(segmentIndex)
    }
}
