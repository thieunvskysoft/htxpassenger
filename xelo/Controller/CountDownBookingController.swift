//
//  CountDownBookingController.swift
//  xelo
//
//  Created by Nguyen Thieu on 10/29/18.
//  Copyright © 2018 DUMV. All rights reserved.
//

import UIKit

class CountDownBookingController: UIViewController {

    @IBOutlet weak var lblexcludingTolls: UILabel!
    @IBOutlet weak var lblBookPrice: UILabel!
    @IBOutlet weak var lblDriverService: UILabel!
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var lblStart: UILabel!
    @IBOutlet weak var lblEnd: UILabel!
    @IBOutlet weak var lblNote: UILabel!
    @IBOutlet weak var viewDistance: UIView!
    @IBOutlet weak var viewNote: UIView!
    @IBOutlet weak var lblPearRate: UILabel!
    @IBOutlet weak var lblWays: UILabel!
    @IBOutlet weak var lblPromotion: UILabel!
    @IBOutlet weak var btnSliding: MMSlidingButton!
    @IBOutlet weak var viewDetail: UIView!
    @IBOutlet weak var viewLoading: ViewLoader!
    @IBOutlet weak var lblPlateNumber: UILabel!
    @IBOutlet weak var lblBlockRoad: UILabel!
    
    
    var TOTAL_CHARGE:String = "Cước: "
    var EXPECTED_CHARGE:String = "Cước dự kiến: "
    var MINUTE:String = " phút"
    var SLIDE_TO_CANCEL:String = "Trượt để huỷ chuyến"
    var DISTANCE_:String = "Quãng đường: "
    var PROMOTION:String = "Khuyến mại: "
    var ONE_WAY_TITLE:String = "Cuốc 1 chiều"
    var TWO_WAY_TITLE:String = "Cuốc 2 chiều"
    var NOTIFICATION_TITLE:String = "Thông báo"
    var CONFIRM:String = "Xác nhận"
    var CANCEL:String = "Huỷ"
    var FINDING_DRIVER:String = "Đang tìm lái xe..."
    var EXCLUDING_TOLLS:String = "Chưa bao gồm phí cầu đường"
    var PEAK_HOUR:String = ""
    var timer:Timer? = nil
    var check:Int = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DataNavigo.animationDelegate = self
        btnSliding.delegate = self as SlideButtonDelegate
        btnSliding.buttonText = "Trượt để huỷ chuyến"
        btnSliding.dragPointWidth = 50
        btnSliding.buttonDragPoint = true
        btnSliding.dragPointColor = NaviColor.naviBtnOrange
        btnSliding.imageName = #imageLiteral(resourceName: "Arrorw")
        btnSliding.buttonColor = UIColor.gray
        viewLoading.startAnimating()
//        viewDetail.layer.masksToBounds = false
//        viewDetail.layer.shadowOffset = CGSize(width: 1, height: 1)
//        viewDetail.layer.shadowRadius = 5
//        viewDetail.layer.shadowOpacity = 0.6
//        viewDetail.layer.cornerRadius = 5
//        viewDetail.layer.cornerRadius = 5
        viewDetail.layer.borderWidth = 0.3
        

        DataNavigo.idUIController = 4
        check = 0
        DataNavigo.driverInfo = DriverInfo()
        DataNavigo.animationDelegate = self
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(countDownTimer), userInfo: nil, repeats: true)
    }
    
    func loadViewLanguage() {
        let curren_language = UserDefaults.standard.string(forKey: "curren_language")
        let language = LanguageData()
        TOTAL_CHARGE = language.localizedString(forKey:curren_language!,forKey: "TOTAL_CHARGE")
        EXPECTED_CHARGE = language.localizedString(forKey:curren_language!,forKey: "EXPECTED_CHARGE")
        MINUTE = language.localizedString(forKey:curren_language!,forKey: "MINUTE")
        SLIDE_TO_CANCEL = language.localizedString(forKey:curren_language!,forKey: "SLIDE_TO_CANCEL")
        DISTANCE_ = language.localizedString(forKey:curren_language!,forKey: "DISTANCE_")
        ONE_WAY_TITLE = language.localizedString(forKey:curren_language!,forKey: "ONE_WAY_TITLE")
        TWO_WAY_TITLE = language.localizedString(forKey:curren_language!,forKey: "TWO_WAY_TITLE")
        PROMOTION = language.localizedString(forKey:curren_language!,forKey: "PROMOTION")
        NOTIFICATION_TITLE = language.localizedString(forKey:curren_language!,forKey: "NOTIFICATION_TITLE")
        CONFIRM = language.localizedString(forKey:curren_language!,forKey: "CONFIRM")
        CANCEL = language.localizedString(forKey:curren_language!,forKey: "CANCEL")
        FINDING_DRIVER = language.localizedString(forKey:curren_language!,forKey: "FINDING_DRIVER")
        EXCLUDING_TOLLS = language.localizedString(forKey:curren_language!,forKey: "EXCLUDING_TOLLS")
        PEAK_HOUR = language.localizedString(forKey:curren_language!,forKey: "PEAK_HOUR")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("viewWillAppear")
        DataNavigo.handingScreen = self
        viewLoading.startAnimating()
        loadViewLanguage()
        lblStart.text = DataNavigo.booking.fromAddress != nil ? DataNavigo.booking.fromAddress! :"Unknow"
        lblEnd.text = DataNavigo.booking.toAddress != nil ? DataNavigo.booking.toAddress! :"Unknow"
        if(DataNavigo.booking.twoWays)
        {
            lblWays.text = "Cuốc hai chiều(" + (DataNavigo.booking.waitDuration?.description)! + "p)"
            
        }else{
            lblWays.text = ONE_WAY_TITLE
        }
        
        lblDistance.text = String(format: "%.1f", ((DataNavigo.booking.estDistance)!/1000)) + " Km"
        let promotionValue = DataNavigo.booking.promotionCode
        if (promotionValue != "" && promotionValue != nil){
            lblPromotion.text = (promotionValue?.description)! + " Đ"
        }
        if (DataNavigo.booking.plateNumber != "") {
            print("DataNavigo.booking.plateNumber", DataNavigo.booking.plateNumber?.count ?? "")
            //lblPlateNumber.text = (DataNavigo.booking.plateNumber?.substring(to: (DataNavigo.booking.plateNumber?.characters.count)! - 2))! + "xx"
        }
        btnSliding.buttonText = SLIDE_TO_CANCEL
        lblexcludingTolls.text = EXCLUDING_TOLLS
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        //Về map-----
        if timer != nil {
            timer?.invalidate()
            timer = nil
        }
    }

    func reStartAnimating() {
        viewLoading.startAnimating()
    }


    func getPrice(service:Int) -> String {
        let point = DataNavigo.arrService[service]
        var price:String = ""
        if MapController.locationSucsess {
            if (DataNavigo.servicePrice.count != 0){
                let priceMin = Int(ceilf(Float(DataNavigo.servicePrice[service].fromPrice!)/1000))
                let priceMax = Int(ceilf(Float(DataNavigo.servicePrice[service].toPrice!)/1000))
                let priceBook = Int(ceilf(Float(DataNavigo.servicePrice[service].price!)/1000))
                price = priceMin.description + " - " + priceMax.description + "K"
                if(priceBook > 0){
                    price = priceBook.description + "K"
                }
            }else{
                let priceMin = Int(ceilf(Float(DataNavigo.booking.fromCharge!)/1000))
                let priceMax = Int(ceilf(Float(DataNavigo.booking.toCharge!)/1000))
                //let priceBook = Int(ceilf(Float(DataNavigo.servicePrice[service].price!/1000)))
                price = priceMin.description + " - " + priceMax.description + "K"
                //                if(priceBook > 0){
                //                    price = priceBook.description + "K"
                //                }
            }
            
        }else{
            let priceMin = String(Int(ceilf(Float(point.fromPrice!))/1000))
            let priceMax = String(Int(ceilf(Float(point.toPrice!))/1000))
            price = priceMin + " - " + priceMax + "K/Km"
            if(priceMin.elementsEqual(priceMax)){
                price = priceMin + "K/Km"
            }
        }
        return price
    }
    
    func priceTotal(price:Int, service:Int) -> Int {
        var actCharge = (Int(price * Int(DataNavigo.booking.estDistance!))/1000)
        if(DataNavigo.booking.twoWays) {
            let ser:Service = DataNavigo.arrService[service]
            
            if(ser != nil) {
                if(ser.returnRate != 0) {
                    actCharge += ((actCharge*ser.returnRate!)/100);
                }
                //wait charge
                if(ser.waitFee != 0 && DataNavigo.booking.waitDuration != 0) {
                    let waitCharge = (DataNavigo.booking.waitDuration! * ser.waitFee!)/60;
                    actCharge += waitCharge
                }
            }
        }
        return actCharge
    }
    
    @objc func countDownTimer() {
        print("DataNavigo.booking.peakHour",DataNavigo.booking.peakHour ?? "")
        if(DataNavigo.booking.state == BookingState.STATE_ACCEPT_BY_DRIVER){
            if (DataNavigo.driverInfo.fullName == nil && check == 0){
                print("Lấy thông tin lái xe...")
                showAlertPath()
                getdriverInfor()
            }else{
                print("Lấy xong thông tin lái xe")
                popToMap()
            }
            
        }else if(DataNavigo.booking.state == BookingState.STATE_NEW || DataNavigo.booking.state == BookingState.STATE_NONE){
            
        }else if(DataNavigo.booking.state == nil || DataNavigo.booking.state == BookingState.STATE_CANCEL_BY_PASSENGER){
            popToMap()
        }else{
            let viewControllers:[UIViewController] = (self.navigationController?.viewControllers)!;
            for controller in viewControllers {
                if controller is MapController {
                    self.navigationController!.popToViewController(controller, animated: true)
                }
            }
        }
        if DataNavigo.booking.fixCharge {
            let price = Int(ceilf(Float(DataNavigo.booking.estCharge!/1000)))
            lblBookPrice.text = price.description + "K"
        }else{
            lblBookPrice.text = getPrice(service: DataNavigo.serviceCar)
        }
        let promotionValue = DataNavigo.booking.promotionCode
        if (promotionValue != "" && promotionValue != nil){
            lblPromotion.text = promotionValue
        }
        if (DataNavigo.booking.plateNumber != "" && DataNavigo.booking.plateNumber != nil) {
            print("DataNavigo.booking.plateNumber", DataNavigo.booking.plateNumber?.count ?? "")
            let plateCount = DataNavigo.booking.plateNumber?.count
            lblPlateNumber.text = (DataNavigo.booking.plateNumber?.substring(to: plateCount! - 2))! + "xx"
            //lblPlateNumber.text = DataNavigo.booking.plateNumber
        }
        if DataNavigo.booking.blockRoad
        {
            lblBlockRoad.text = "Điểm đầu/Điểm cuối của bạn đang nằm trong khung giờ cấm xe, việc đặt xe có thể khó khăn hơn."
        }else if DataNavigo.booking.peakHour! {
            
            lblBlockRoad.text = "Khung giờ cao điểm, đêm muộn, việc đặt xe có thể khó khăn hơn."
        }else {
            lblBlockRoad.text = ""
        }
    }
    
    //getDriverInfo------------------
    func getdriverInfor()  {
        print("get---driver")
        let CompletionHandler: ((DriverInfo,NavigoError?) -> Void) = {
            driverinfo,navigoError in
            DispatchQueue.main.async {
                if(navigoError == nil){
                    DataNavigo.driverInfo = driverinfo
                    MapController.showAlert = true
                    DataNavigo.checkDrawPath = true
                    self.alertPath.dismiss(animated: true, completion: nil)
                }else{
                    if(navigoError?.errorCode == Errorcode.ERROR_NO_INTERNET){
                        self.errorNoInternet()
                    }
                    else{
                        self.errorServerSend(errorCode: (navigoError?.errorCode)!, errorToast: (navigoError?.errorMessage)!)
                    }
                    DataNavigo.driverInfo = DriverInfo()
                }
            }
        }
        let connector = APIBook();
        connector.getDriverInfo(book: DataNavigo.booking, CompletionHandler: CompletionHandler)
    }
    
    func cancelBooking()  {
        DataNavigo.runCheck = false
        DataNavigo.update = false
        let CompletionHandler: ((Book,NavigoError?) -> Void) = {
            booking,navigoError in
            DispatchQueue.main.async {
                if(navigoError == nil){
                    DataNavigo.booking.state = nil
                    DataNavigo.booking.bookingId = nil
                    self.popToMap()
                }else{
                    if(navigoError?.errorCode == Errorcode.ERROR_NO_INTERNET){
                        self.errorNoInternet()
                    }
                    else{
                        self.errorServerSend(errorCode: (navigoError?.errorCode)!, errorToast: Errorcode.ERROR_MESSAGE)
                    }
                }
            }
        }
        let connector = APIBook();
        connector.cancelBooking(book: DataNavigo.booking, cancelCode: "", CompletionHandler: CompletionHandler)
    }
    
    
    func popToMap() {
        //DataNavigo.checkDrawPath = true
        //alertPath.dismiss(animated: true, completion: nil)
        let viewControllers:[UIViewController] = (self.navigationController?.viewControllers)!
        for controller in viewControllers {
            print("controller",controller)
            if controller is MapController {
                print("MapController---")
                sleep(1)
                self.navigationController!.popToViewController(controller as! MapController, animated: true)
                print("MapController222")
            }
        }
    }
    
    var alertPath = UIAlertController()
    
    func showAlertPath() {
        alertPath = UIAlertController(title: nil, message: "Đang lấy thông tin tài xế...\n\n", preferredStyle: .alert)
        let spinnerIndicator = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        spinnerIndicator.center = CGPoint(x: 135.0, y: 65.5)
        spinnerIndicator.color = UIColor.black
        spinnerIndicator.startAnimating()
        check = 1
        alertPath.view.addSubview(spinnerIndicator)
        self.present(alertPath, animated: false, completion: nil)
    }
    
    func formatterPrice(price: Int) -> String {
        var str:String = ""
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.groupingSeparator = "."
        str = formatter.string(for: price)!
        return str
    }
}

extension CountDownBookingController: HandingScreen {
    func errorConnect() {
        // dang docve thi bi loi
    }
    func errorNoInternet() {
        // khong co internet
        
    }
    func errorServerSend(errorCode: String, errorToast: String) {
        //        print("errorCode",errorCode)
        //        let alert:UIAlertController = UIAlertController(title: "Đã có lỗi trong khi xử lý", message: "Bạn có muốn thử lại?", preferredStyle: UIAlertControllerStyle.alert)
        //
        //        let btnOk:UIAlertAction = UIAlertAction(title: "Xác nhận", style: .destructive) { (btnOk) in
        //
        //        }
        //        let btnCancel:UIAlertAction = UIAlertAction(title: "Huỷ", style: .destructive) { (btnCancel) in
        //
        //        }
        //        alert.addAction(btnOk)
        //        alert.addAction(btnCancel)
        //        present(alert, animated: true, completion: nil)
        
        self.popToMap()
    }
    func startRequest() {
        
    }
    
    func endRequest() {
    }
    
    
    // huy booking
    
    /// login lai
    func login() {
        let CompletionHandler: ((LoginResult,NavigoError?) -> Void) = {
            loginResult,navigoError in
            DispatchQueue.main.async {
                if(navigoError == nil){
                    //self.endRequest()
                }else{
                    if(navigoError?.errorCode == Errorcode.ERROR_NO_INTERNET){
                        self.errorNoInternet()
                    }
                    else{
                        self.errorServerSend(errorCode: (navigoError?.errorCode)!, errorToast: Errorcode.ERROR_MESSAGE)
                    }
                    
                }
            }
        }
        let connector = PassengerConnector();
        connector.reconnect(CompletionHandler: CompletionHandler)
    }
    
}
extension CountDownBookingController: SlideButtonDelegate {
    
    func buttonStatus(status: String, sender: MMSlidingButton) {
        if (status == "Success") {
            self.cancelBooking()
            
        }
    }
}
extension CountDownBookingController: AnimationDelegate{
    func Timeout() {
        self.cancelBooking()
    }
    
    func AnimationSuccess() {
        self.reStartAnimating()
    }
    
    func AnimationFailed() {
        
    }
    
    
    
}
