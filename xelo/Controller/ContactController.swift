//
//  ContactController.swift
//  NaviGo
//
//  Created by HoangManh on 10/26/17.
//  Copyright © 2017 DUMV. All rights reserved.
//

import UIKit

class ContactController: UIViewController, UIWebViewDelegate {
    
    var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        let backButton = UIBarButtonItem()
        backButton.title = " "
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        navigationController?.navigationBar.tintColor = .white
        navigationItem.title = "Liên hệ"
        _ = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        webView = UIWebView(frame: UIScreen.main.bounds)
        webView.delegate = self
        view.addSubview(webView)
        if let url = URL(string: Constant.URL_CONTACT + "?" + keyJson.F_TOKEN_ID + "="+DataNavigo.loginResult.tokenID) {
            let request = URLRequest(url: url)
            webView.loadRequest(request)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
