//
//  ReportDirver.swift
//  NaviGo
//
//  Created by HoangManh on 10/6/17.
//  Copyright © 2017 DUMV. All rights reserved.
//

import UIKit

class ReportDirverController: UIViewController {

    var screenHeight : Double = 0
    var screenWidth : Double = 0
    let ass:Int = 75
    let huy:UIButton = UIButton()
    

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let screenSize = UIScreen.main.bounds
        screenHeight = Double(screenSize.height)
        screenWidth = Double(screenSize.width)
        
        view.addSubview(createView1())
        view.addSubview(createView2())
        view.addSubview(createView3())
        view.addSubview(createView4())
    }
    
    func detailDriver() -> UIView {
        let viewDetail = UIView()
        viewDetail.frame = CGRect(x: 0, y: 0, width: view.frame.height, height: view.frame.height / 2)
        let navBarHeight = (self.navigationController?.navigationBar.intrinsicContentSize.height)! + UIApplication.shared.statusBarFrame.height
        let imageDiver = UIImageView()
        imageDiver.frame = CGRect(x: 100, y: navBarHeight + 20, width: 80, height: 80)
        imageDiver.image = DataNavigo.driverInfo.avatarFace
        imageDiver.layer.cornerRadius = 40
        imageDiver.clipsToBounds = true
        viewDetail.addSubview(imageDiver)
        imageDiver.center.x = self.view.center.x
        
        
        
        
        return viewDetail
        
    }
    
    let lblDriverName:UILabel = {
        let lbl = UILabel()
        lbl.textColor = #colorLiteral(red: 0.9411764741, green: 0.4980392158, blue: 0.3529411852, alpha: 1)
        lbl.font = UIFont.systemFont(ofSize: 18)
        return lbl
    }()
    let lblCarNumber:UILabel = {
        let lbl = UILabel()
        lbl.textColor = #colorLiteral(red: 0.9411764741, green: 0.4980392158, blue: 0.3529411852, alpha: 1)
        lbl.font = UIFont.systemFont(ofSize: 18)
        return lbl
    }()
    let lblServiceCar:UILabel = {
        let lbl = UILabel()
        lbl.textColor = #colorLiteral(red: 0.9411764741, green: 0.4980392158, blue: 0.3529411852, alpha: 1)
        lbl.font = UIFont.systemFont(ofSize: 18)
        return lbl
    }()
    let lblColorCar:UILabel = {
        let lbl = UILabel()
        lbl.textColor = #colorLiteral(red: 0.9411764741, green: 0.4980392158, blue: 0.3529411852, alpha: 1)
        lbl.font = UIFont.systemFont(ofSize: 18)
        return lbl
    }()
    let viewRateDriver:FloatRatingView = {
        let rate = FloatRatingView()
        rate.contentMode = UIViewContentMode.scaleAspectFit
        rate.type = .floatRatings
        rate.rating = 3.6
        rate.minRating = 0
        rate.maxRating = 5
        rate.editable = false
        rate.emptyImage = #imageLiteral(resourceName: "StarEmpty")
        rate.fullImage = #imageLiteral(resourceName: "StarFull")
        return rate
    }()
    
    func createView1() -> UIView {
        let viewC:UIView = UIView()
        viewC.layer.cornerRadius = 5
        viewC.backgroundColor = UIColor.white
        viewC.frame = CGRect(x: 10, y: ass, width: Int(screenWidth)-20, height: Int(165))
        
        let anh = UIImageView()
        anh.frame = CGRect(x: (screenWidth-20)/2 - 50, y: 5, width: 100, height: 100)
        anh.image = #imageLiteral(resourceName: "dog1")
        
        let anh1 = UIImageView()
        anh1.frame = CGRect(x: (screenWidth-20)/2 - 50, y: 5, width: 100, height: 100)
        anh1.image = #imageLiteral(resourceName: "Image")
        
        let text:UILabel = UILabel()
        text.font = text.font.withSize(CGFloat(16))
        text.frame = CGRect(x: 0, y: 100, width: screenWidth-20, height: 30)
        text.text = DataNavigo.driverInfo.fullName
        text.textAlignment = NSTextAlignment.center
        
        let texts:UILabel = UILabel()
        texts.font = texts.font.withSize(CGFloat(18))
        texts.frame = CGRect(x: 0, y: 130, width: screenWidth-20, height: 25)
        texts.text = "★ ★ ★ ★ ★"
        texts.textColor = UIColor.orange
        texts.textAlignment = NSTextAlignment.center
        
        viewC.addSubview(anh)
        viewC.addSubview(anh1)
        viewC.addSubview(text)
        
        viewC.addSubview(texts)
        return viewC
    }
    func createView2() -> UIView {
        
        let vtexA = UIView()
        vtexA.backgroundColor = UIColor.white
        vtexA.frame = CGRect(x: 0, y: 10, width: screenWidth-20, height: 45)
        let anhA = UIImageView()
        anhA.frame = CGRect(x: 5, y: 5, width: 35, height:35)
        anhA.image = #imageLiteral(resourceName: "cur")
        let tex1A = UILabel()
        tex1A.font = tex1A.font.withSize(CGFloat(10))
        tex1A.frame = CGRect(x: 45, y: 3, width: Int(screenWidth-20)-45, height: 10)
        tex1A.text = "Điểm đi"
        let tex2A = UILabel()
        tex2A.font = tex2A.font.withSize(CGFloat(14))
        tex2A.frame = CGRect(x: 45, y: 13, width: Int(screenWidth-20)-45, height: 30)
        tex2A.text = DataNavigo.booking.fromAddress
        vtexA.addSubview(anhA)
        vtexA.addSubview(tex1A)
        vtexA.addSubview(tex2A)
        
        let vtexB = UIView()
        vtexB.backgroundColor = UIColor.white
        vtexB.frame = CGRect(x: 0, y: 65, width: Int(screenWidth-20), height: 45)
        let anhB = UIImageView()
        anhB.frame = CGRect(x: 5, y: 5, width: 35, height:35)
        anhB.image = #imageLiteral(resourceName: "des")
        let tex1B = UILabel()
        tex1B.font = tex1A.font.withSize(CGFloat(10))
        tex1B.frame = CGRect(x: 45, y: 3, width: Int(screenWidth-20)-45, height: 10)
        tex1B.text = "Điểm đến"
        let tex2B = UILabel()
        tex2B.font = tex2A.font.withSize(CGFloat(14))
        tex2B.frame = CGRect(x: 45, y: 13, width: Int(screenWidth-20)-45, height: 30)
        tex2B.text = "Diem Di"
        vtexB.addSubview(anhB)
        vtexB.addSubview(tex1B)
        vtexB.addSubview(tex2B)
        
        let v = UIView()
        v.addSubview(vtexA)
        v.addSubview(vtexB)
        
        v.frame = CGRect(x: 10, y: 165 + ass, width: Int(screenWidth-20), height: 119)
        return v
    }
    func createView3() -> UIView {
        let v = UIView()
        v.layer.cornerRadius = 5
        v.backgroundColor = UIColor.white
        let hei = Int(screenHeight)-ass-10-119-55-165
        
        v.frame = CGRect(x: 10, y: 165 + ass + 119, width: Int(screenWidth-20), height: hei)
        
        let textk:UILabel = UILabel()
        textk.font = textk.font.withSize(CGFloat(16))
        textk.frame = CGRect(x: 8, y: 8, width: Int(screenWidth-20), height: 40)
        textk.textAlignment = NSTextAlignment.left
        textk.text = "Tong Tien Chuyen : "
        
        let textk1:UILabel = UILabel()
        textk1.font = textk1.font.withSize(CGFloat(16))
        textk1.frame = CGRect(x: 8, y: 8, width: Int(screenWidth-20)-18, height: 40)
        textk1.textAlignment = NSTextAlignment.right
        textk1.text = "15.000 vnd"
        
        let textk2:UILabel = UILabel()
        textk2.font = textk2.font.withSize(CGFloat(16))
        textk2.frame = CGRect(x: 8, y: 50, width: Int(screenWidth-20), height: 40)
        textk2.textAlignment = NSTextAlignment.left
        textk2.text = "Quang Duong Di : "
        
        let textk12:UILabel = UILabel()
        textk12.font = textk12.font.withSize(CGFloat(16))
        textk12.frame = CGRect(x: 8, y: 50, width: Int(screenWidth-20)-18, height: 40)
        textk12.textAlignment = NSTextAlignment.right
        textk12.text = "15 km"
        
        let textk3:UILabel = UILabel()
        textk3.font = textk3.font.withSize(CGFloat(16))
        textk3.frame = CGRect(x: 8, y: hei - 90, width: Int(screenWidth-20), height: 40)
        textk3.textAlignment = NSTextAlignment.center
        textk3.text = "Vui Long Danh Gia Dich Vu Lai Xe"
        
        v.addSubview(textk)
        v.addSubview(textk3)
        v.addSubview(textk1)
        v.addSubview(textk2)
        v.addSubview(textk12)
        return v
    }
    func createView4() -> UIView {
        let v = UIView()
        let k = 60
        v.frame = CGRect(x: 10, y: Int(screenHeight)-k, width: Int(screenWidth-20), height: k)
        
        huy.frame = CGRect(x: 0, y: 5, width: Int(screenWidth-20), height: 40)
        huy.backgroundColor =  UIColor(red: 6/255, green: 114/255, blue: 231/255, alpha: 1)
        
        let textk:UILabel = UILabel()
        textk.font = textk.font.withSize(CGFloat(16))
        textk.frame = CGRect(x: 0, y: 5, width: Int(screenWidth-20), height: 40)
        textk.textColor = UIColor.white
        textk.textAlignment = NSTextAlignment.center
        textk.text = "Gui Danh Gia"
        
        v.addSubview(huy)
        v.addSubview(textk)
        
        return v
    }

}
