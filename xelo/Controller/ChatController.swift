//
//  ChatController.swift
//  xelo
//
//  Created by Nguyen Thieu on 2/28/18.
//  Copyright © 2018 DUMV. All rights reserved.
//

import UIKit
import SwiftyJSON
import Starscream

class ChatController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextViewDelegate {
    //var socket: WebSocket!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var btnInvia: UIButton!
    @IBOutlet weak var constraintAltezzaText: NSLayoutConstraint!
    var checkSocket:Bool = true
    
    var tastieraSu = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textView.delegate = self
        textView.returnKeyType = .done
        textView.layer.cornerRadius = 15
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 65
        DataNavigo.handingScreen = self

        textView.autocorrectionType = .no
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.tapDetected(sender:)))
        self.tableView.addGestureRecognizer(tapGestureRecognizer)
//        let panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(self.panDetected(sender:)))
//        self.view.addGestureRecognizer(panGestureRecognizer)
    }
    @objc func tapDetected(sender: UITapGestureRecognizer) {
        if textView.isFirstResponder {
            textView.resignFirstResponder()
        }
    }
    func panDetected(sender: UIPanGestureRecognizer){
        if textView.isFirstResponder {
            textView.resignFirstResponder()
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.tintColor = .white
        DataNavigo.socket?.delegate = self
        DataNavigo.ipaDelegate = self
        DataNavigo.popupDelegate = self
        if !(DataNavigo.socket?.isConnected)! {
            DataNavigo.socket?.connect()
        }
        navigationItem.title = DataNavigo.booking.driver?.driverName
        self.tabBarController?.tabBar.isHidden = true
        tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        NotificationCenter.default.addObserver(self, selector: #selector(ChatController.tastieraFuori(notifica:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ChatController.tastieraDentro(notifica:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)

        self.navigationController?.isNavigationBarHidden = false
        let backButton = UIBarButtonItem()
        backButton.title = " "
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        DataNavigo.arrMessager.removeAll()
        listMessage()
    }
    
    func login() {
        let CompletionHandler: ((LoginResult,NavigoError?) -> Void) = {
            loginResult,navigoError in
            DispatchQueue.main.async {
                if(navigoError == nil){
                    self.loginSocket()
                    self.showToast(message: "Đã kết nối lại")
                }else{
                    if(navigoError?.errorCode == Errorcode.ERROR_NO_INTERNET){
                        self.errorNoInternet()
                    }
                    else{
                        self.errorServerSend(errorCode: (navigoError?.errorCode)!, errorToast: (navigoError?.errorMessage)!)
                    }
                }
            }
        }
        let connector = PassengerConnector();
        self.showToast(message: "Mất Kết nối")
        connector.reconnect(CompletionHandler: CompletionHandler)
    }
    
    func loginSocket() {
        var request = URLRequest(url: URL(string: Constant.URL_CHAT + DataNavigo.loginResult.sessionID)!)
        request.timeoutInterval = 5
        DataNavigo.socket = WebSocket(request: request)
        DataNavigo.socket?.delegate = self
    }
    
    func listMessage()  {
        let CompletionHandler: ((ChatMessage,NavigoError?) -> Void) = {
            messages,navigoError in
            DispatchQueue.main.async {
                if(navigoError == nil){
                    self.tableView.reloadData()
                }else{
                    if(navigoError?.errorCode == Errorcode.ERROR_NO_INTERNET){
                        if navigoError?.errorCode?.range(of: Errorcode.NAVIGO_SESSION_EXPIRED) != nil{
                            self.login()
                        }
                    }
                    else{
                    }
                }
            }
        }     
        let message = APIChatMessage();
        message.listMessage(CompletionHandler: CompletionHandler)
    }
    
    func chatByCode(code:String, value:String)  {
        let CompletionHandler: ((String,NavigoError?) -> Void) = {
            messages,navigoError in
            DispatchQueue.main.async {
                if(navigoError == nil){
                    let date : Date = Date()
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "dd/MM/yyyy HH:mm"
                    let createDate = dateFormatter.string(from: date)
                    let messaggio = ChatMessage(driverID: "", driverName: "", passengerID: "", passengerName: "", fromDriver: false, createDate: createDate, content: value)
                    DataNavigo.arrMessager.append(messaggio)
                    self.tableView.reloadData()
                }else{
                    if(navigoError?.errorCode == Errorcode.ERROR_NO_INTERNET){
                        if navigoError?.errorCode?.range(of: Errorcode.NAVIGO_SESSION_EXPIRED) != nil{
                            self.login()
                        }
                    }
                    else{
                    }
                }
            }
        }
        let message = APIChatMessage();
        message.chatByCode(chatCode: code, bookingID: DataNavigo.booking.bookingId!, CompletionHandler: CompletionHandler)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        //messaggiRef.removeAllObservers()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if DataNavigo.arrMessager.count != 0 {
            let indice = IndexPath.init(row: DataNavigo.arrMessager.count - 1, section: 0)
            tableView.scrollToRow(at: indice, at: .top, animated: false)
        }
    }
    
    @objc func tastieraDentro(notifica: Notification) {
        tastieraInOut(su: false, notifica: notifica)
    }
    
    @objc func tastieraFuori(notifica: Notification) {
        tastieraInOut(su: true, notifica: notifica)
    }
    
    func tastieraInOut(su: Bool, notifica: Notification) {
        guard su != tastieraSu else {
            return
        }
        let info = notifica.userInfo
        let fineTastiera: CGRect = ((info?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue)!
        let durataAnimazione: TimeInterval = info?[UIKeyboardAnimationDurationUserInfoKey] as! Double
        
        UIView.animate(withDuration: durataAnimazione, delay: 0, options: .curveEaseInOut, animations: {
            let dimensioneTastiera = self.view.convert(fineTastiera, to: nil)
            let spostamentoVerticale = dimensioneTastiera.size.height * (su ? -1 : 1)
            self.view.frame = self.view.frame.offsetBy(dx: 0, dy: spostamentoVerticale)
            self.tastieraSu = !self.tastieraSu
        }, completion: nil)
        
    }
    
    @IBAction func openTemplate(_ sender: Any) {
        let vc = TemplateChat()
        self.present(vc, animated: true)
    }
    
    @IBAction func btnCall(_ sender: Any) {
        let phone = DataNavigo.driverInfo.mobileNo
        let phoneNumber:String = "tel:" + phone!
        if (phone?.characters.count)! > 0 {
            if #available(iOS 10, *) {
                UIApplication.shared.open(NSURL(string: phoneNumber)! as URL)
            } else {
                UIApplication.shared.openURL(NSURL(string: phoneNumber)! as URL)
            }
        }
    }
    
    @IBAction func inviaPremuto(_ sender: UIButton) {
        if textView.text != "" {
            let date : Date = Date()
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd/MM/yyyy HH:mm"
            let createDate = dateFormatter.string(from: date)
            let messaggio = ChatMessage(driverID: "", driverName: "", passengerID: "", passengerName: "", fromDriver: false, createDate: createDate, content: textView.text)
            DataNavigo.arrMessager.append(messaggio)
            let content = convertJson(content: textView.text)
            DataNavigo.socket?.write(string: content)
            textView.text = ""
            textViewDidChange(textView)
            self.tableView.reloadData()
        }
//        if textView.isFirstResponder {
//            textView.resignFirstResponder()
//        }
        let indice = IndexPath.init(row: DataNavigo.arrMessager.count - 1, section: 0)
        tableView.scrollToRow(at: indice, at: .top, animated: false)
    }
    
    func textViewDidChange(_ textView: UITextView) {
        textView.isScrollEnabled = false
        let larghezzaFissa = textView.frame.size.width
        let nuovaDimensione = textView.sizeThatFits(CGSize(width: larghezzaFissa, height: CGFloat.greatestFiniteMagnitude))
        let nuovaAltezza = nuovaDimensione.height
        if nuovaAltezza > 245.5 {
            textView.isScrollEnabled = true
        } else {
            var nuovoFrame = textView.frame
            nuovoFrame.size = CGSize(width: max(larghezzaFissa, nuovaDimensione.width), height: nuovaAltezza)
            constraintAltezzaText.constant = nuovaAltezza
            textView.frame = nuovoFrame
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataNavigo.arrMessager.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let messaggio = DataNavigo.arrMessager[indexPath.row]
        if !messaggio.fromDriver! {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellPassenger", for: indexPath) as! ChatMessagerCell
            cell.lblMessaggio.text = messaggio.content
            cell.lblData.text = messaggio.createDate?.substring(from: 11)
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellDriver", for: indexPath) as! ChatMessagerCell
            cell.lblMessaggio.text = messaggio.content
            cell.lblData.text = messaggio.createDate?.substring(from: 11)
            return cell
        }
        
    }
    //----
    func convertJson(content:String) -> String {
        var json="";
        let id = DataNavigo.booking.driver?.driverID
        json += "\"toID\":"+"\""+id!+"\","
        json += "\"content\":"+"\""+content+"\""

        json="{"+json+"}"
        return json
    }
    
    func jsonConvertChatMessage(json:JSON) {
        let driverID = json[keyJson.F_DRIVER_ID].stringValue
        let driverName = json[keyJson.F_DRIVER_NAME].stringValue
        let passengerID = json[keyJson.F_PASSENGER_ID].stringValue
        let passengerName = json[keyJson.F_PASSENGER_NAME].stringValue
        let fromDriver = json[keyJson.F_FROM_DRIVER].boolValue
        let createDate = convertDateTime(datetime: json[keyJson.F_CREATE_DATE].stringValue)
        let contentSocket = json[keyJson.F_CONTENT].stringValue
        if fromDriver {
            let messaggio:ChatMessage = ChatMessage(driverID: driverID, driverName: driverName, passengerID: passengerID, passengerName: passengerName, fromDriver: fromDriver, createDate: createDate, content: contentSocket)
            DataNavigo.arrMessager.append(messaggio)
            tableView.reloadData()
            let indice = IndexPath.init(row: DataNavigo.arrMessager.count - 1, section: 0)
            tableView.scrollToRow(at: indice, at: .top, animated: false)
        }
        if DataNavigo.foreground {
            pushNotification(content: contentSocket)
        }
    }
    
    func pushNotification(content:String){
        let notificationSettings = UIUserNotificationSettings(
            types: [.badge, .sound, .alert], categories: nil)
        UIApplication.shared.registerUserNotificationSettings(notificationSettings)
        
        let notification = UILocalNotification()
        //notification.userInfo = ["gcm.notification.notifyType": "NotificationChat"]
        notification.alertTitle = DataNavigo.booking.driver?.driverName
        notification.alertBody = content
        notification.alertAction = "open"
        notification.fireDate = Date(timeIntervalSinceNow: 1)
        notification.soundName = UILocalNotificationDefaultSoundName
        UIApplication.shared.scheduleLocalNotification(notification)
        
    }
    
    func convertDateTime(datetime: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss+SSSS"//this your string date format
        dateFormatter.timeZone = NSTimeZone(name: "UTC")! as TimeZone
        let datek = dateFormatter.date(from: datetime)
        
        dateFormatter.dateFormat = "dd/MM/yyyy HH:mm"///this is what you want to convert format
        dateFormatter.timeZone = NSTimeZone(name: "UTC")! as TimeZone
        if(datek != nil){
            let timeStamp = dateFormatter.string(from: datek!)
            return timeStamp
        }else{
            let date = datetime.substring(to: 10)
            let timefull = datetime.substring(from: 11)
            let time = timefull.substring(to: 5)
            let dateFormat = DateFormatter()
            dateFormat.dateFormat = "yyyy-MM-dd"//this your string date format
            let date12 = dateFormat.date(from: date)
            dateFormat.dateFormat = "dd/MM/yyyy"
            var timeStamp = dateFormat.string(from: date12!)
            timeStamp = timeStamp + " " + time
            return timeStamp
        }
    }
    
    func showToast(message : String) {
        let toastLabel = UILabel(frame: CGRect(x: 10, y: self.view.frame.size.height-100, width: self.view.frame.size.width - 20 , height: 35))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "Montserrat-Light", size: 12.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 15;
        toastLabel.clipsToBounds  =  true
        
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 2.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    
    //Check book
    func checkBooking()  {
        let CompletionHandler: ((Book,NavigoError?) -> Void) = {
            booking,navigoError in
            DispatchQueue.main.async {
                if(navigoError == nil){
                    self.endRequest()
                    if(DataNavigo.update == true){
                        DataNavigo.booking = booking
                    }
                }else{
                    if(navigoError?.errorCode == Errorcode.ERROR_NO_INTERNET){
                        self.errorNoInternet()
                    }
                    else{
                        self.errorServerSend(errorCode: (navigoError?.errorCode)!, errorToast: (navigoError?.errorMessage)!)
                    }
                }
            }
        }
        
        let connector = APIBook();
        connector.checkBooking(book: DataNavigo.booking, CompletionHandler: CompletionHandler)
    }
    
    func socketConnect() -> String {
        var json="";
        let id = "0"
        json += "\"toID\":"+"\""+id+"\","
        json += "\"content\":"+"\"Hello\""
        
        json="{"+json+"}"
        return json
    }
    
}

extension ChatController: WebSocketDelegate{
    func websocketDidConnect(socket: WebSocketClient) {
        print("ChatController is connected")
        checkSocket = true
    }
    
    func websocketDidDisconnect(socket: WebSocketClient, error: Error?) {
        checkSocket = false
        if let e = error{
            print("ChatController is disconnected: \(e.localizedDescription)")
        } else {
            print("ChatController disconnected")
        }
    }
    
    func websocketDidReceiveMessage(socket: WebSocketClient, text: String) {
        print("ChatController: \(text)")
        let jsons = JSON(parseJSON: text)
        jsonConvertChatMessage(json: jsons)
        
    }
    
    func websocketDidReceiveData(socket: WebSocketClient, data: Data) {
        print("Received data: \(data.count)")
    }
}

extension ChatController: HandingScreen {
    func errorConnect() {
        self.checkBooking()
        if DataNavigo.booking.state == BookingState.STATE_ACCEPT_BY_DRIVER {
            do {
                if !(DataNavigo.socket?.isConnected)! || !checkSocket {
                    DataNavigo.socket?.connect()
                }else{
                    DataNavigo.socket?.write(string: socketConnect())
                }
            } catch {
                print(error)
            }
        }
    }
    
    func errorNoInternet() {
   
    }
    
    func errorServerSend(errorCode: String, errorToast: String) {
        if errorCode.range(of: Errorcode.NAVIGO_SESSION_EXPIRED) != nil{
            login()
        }
    }
    
    func startRequest() {

    }
    
    func endRequest() {
        if DataNavigo.booking.state != BookingState.STATE_ACCEPT_BY_DRIVER {
            let viewControllers:[UIViewController] = (self.navigationController?.viewControllers)!;
            for controller in viewControllers {
                if controller is MapController {
                    self.navigationController!.popToViewController(controller, animated: true)
                }
            }
        }
    }
}
extension ChatController: IAPDelegate{
    func purchaseSuccessful() {
        
    }
    
    func pushNotiSuccessful() {
        
    }
    
    func pushAlterView() {
        
    }
    
    func purchaseForeground() {
        tastieraSu = false
        NotificationCenter.default.addObserver(self, selector: #selector(ChatController.tastieraFuori(notifica:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
    }
}
extension ChatController: PopupDelegate {
    func PopupAccept(code: String, value: String) {
        chatByCode(code: code, value: value)
    }
    
    func PopupCancel(code: String, value: String) {
        
    }
    
    func Timeout() {
        
    }
    

}



