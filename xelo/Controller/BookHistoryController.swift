//
//  BookHistoryController.swift
//  NaviGo
//
//  Created by HoangManh on 10/4/17.
//  Copyright © 2017 DUMV. All rights reserved.
//

import UIKit

class BookHistoryController: UIViewController,HandingScreen {
    func errorNoInternet() {
        
    }
    
    func errorConnect() {
        
    }
    
    func errorServerSend(errorCode: String, errorToast: String) {
        
    }
    
    func startRequest() {
        
    }
    
    func endRequest() {
        
    }
    
    
    @IBOutlet weak var tbtHistory: UITableView!
    
    var alert:UIAlertController? = nil
    var checkLoadData:Bool = true
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //indicatorLoadData()
        DataNavigo.arrBookHistory.removeAll()
        loadHistory(time: "")
        //self.navigationController?.navigationBar.topItem?.backBarButtonItem?.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        navigationController?.navigationBar.tintColor = .white
        navigationItem.title = "Lịch sử hành trình"

    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        if(DataNavigo.idUIController == 8){
            DataNavigo.bookingInfo = Book()
        }
        DataNavigo.idUIController = 2
        self.navigationController?.isNavigationBarHidden = false
        let backButton = UIBarButtonItem()
        backButton.title = " "
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadHistory(time:String)  {
        let CompletionHandler: ((Book,NavigoError?) -> Void) = {
            booking,navigoError in
            DispatchQueue.main.async {
                if(navigoError == nil){
                    self.checkLoadData = true
                    self.tbtHistory.reloadData()
                }else{
                    if(navigoError?.errorCode == Errorcode.ERROR_NO_INTERNET){
                        if navigoError?.errorCode?.range(of: Errorcode.NAVIGO_SESSION_EXPIRED) != nil{
                            self.login()
                        }
                    }
                }
            }
        }        
        let booking = APIBook();
        booking.listBook(lastDate: time, CompletionHandler: CompletionHandler)
    }
    
    func indicatorLoadData() {
        
        alert = UIAlertController(title: nil, message: "Đang xử lý...", preferredStyle: .alert)
        
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        loadingIndicator.startAnimating();
        
        alert?.view.addSubview(loadingIndicator)
        
    }
    
    func showBookingDetail(book:Book)  {
        //present(alert!, animated: false, completion: nil)
        let CompletionHandler: ((DriverInfo,NavigoError?) -> Void) = {
            driverinfo,navigoError in
            //self.alert?.dismiss(animated: true, completion: nil)
            DispatchQueue.main.async {
                if(navigoError == nil){
                    DataNavigo.driverInfo = driverinfo
                    DataNavigo.bookingInfo = book
                    
                    let sb = UIStoryboard(name: "Main", bundle: nil)
                    let feedBack = sb.instantiateViewController(withIdentifier: "FeedBackController") as! FeedBackController
                    self.navigationController?.pushViewController(feedBack, animated: true)
                }else{
                    if(navigoError?.errorCode == Errorcode.ERROR_NO_INTERNET){
                        
                    }
                    else{
                        if navigoError?.errorCode?.range(of: Errorcode.NAVIGO_SESSION_EXPIRED) != nil{
                            self.login()
                        }
                    }
                }
            }
        }
        let connector = APIBook();
        connector.getDriverInfo(book: book, CompletionHandler: CompletionHandler)
    }
    
    /// login lai
    func login() {
        let CompletionHandler: ((LoginResult,NavigoError?) -> Void) = {
            loginResult,navigoError in
            DispatchQueue.main.async {
                
                if(navigoError == nil){
                }else{
                    if(navigoError?.errorCode == Errorcode.ERROR_NO_INTERNET){
                        if navigoError?.errorCode?.range(of: Errorcode.NAVIGO_SESSION_EXPIRED) != nil{
                            self.login()}
                    }
                    else{
                    }
                }
            }
        }
        let connector = PassengerConnector();
        connector.reconnect(CompletionHandler: CompletionHandler)
    }
    
    func convertDate(date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy HH:mm:ss"//this your string date format
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
        let datelate = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "yyyyMMddHHmmss"///this is what you want to convert format
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
        let timeStamp = dateFormatter.string(from: datelate!)
        return timeStamp
    }
    
    func convertTime12h(value:String) -> String {
        let datetime = value.substring(to: 10)
        var timefull = value.substring(from: 11)
        timefull = timefull.replacingOccurrences(of: ":", with: "")
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "dd/MM/yyyy"//this your string date format
        let date = dateFormat.date(from: datetime)
        dateFormat.dateFormat = "yyyyMMdd"
        var timeStamp = dateFormat.string(from: date!)
        timeStamp = timeStamp + timefull
        return timeStamp
    }
    
    
    
}
extension BookHistoryController: UITableViewDelegate, UITableViewDataSource {
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return  DataNavigo.arrBookHistory.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryCell", for: indexPath) as! HistoryCell
        let b:Book = DataNavigo.arrBookHistory[indexPath.row]
        
        cell.lblTimeBook.text = b.bookDate
        cell.lblFrom.text = b.fromAddress
        cell.lblTo.text = b.toAddress
        cell.imgFrom.image = #imageLiteral(resourceName: "cur")
        cell.imgTo.image = #imageLiteral(resourceName: "des")
        let state = b.state
        if state == 4 {
            cell.lblStatus.text = "Hoàn thành"
            cell.lblStatus.textColor = NaviColor.naviLabel
        }else{
            cell.lblStatus.text = "Đã huỷ"
            cell.lblStatus.textColor = UIColor.black
        }
        return (cell)
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       // print("chon cai nay ",indexPath.row)
        let book:Book = DataNavigo.arrBookHistory[indexPath.row]
        DataNavigo.bookingInfo = book
        showBookingDetail(book: book)
        
        
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastElement = DataNavigo.arrBookHistory.count - 1
        let book:Book = DataNavigo.arrBookHistory[indexPath.row]
        if  indexPath.row == lastElement && checkLoadData {
            checkLoadData = !checkLoadData
            loadHistory(time: self.convertTime12h(value: book.bookDate!))
        }
        
    }
}

