//
//  WalletInfoController.swift
//  xelo
//
//  Created by Nguyen Thieu on 11/16/18.
//  Copyright © 2018 DUMV. All rights reserved.
//

import UIKit

class WalletInfoController: UIViewController {
    @IBOutlet weak var lblPromoWallet: UILabel!
    @IBOutlet weak var lblPromoExpireDate: UILabel!
    
    @IBOutlet weak var tblTransWallet: UITableView!
    @IBOutlet weak var viewInfo: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tblTransWallet.delegate = self
        tblTransWallet.dataSource = self
        viewInfo.backgroundColor = NaviColor.naviBtnOrange
        tblTransWallet.separatorStyle = UITableViewCellSeparatorStyle.none
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.tintColor = .white
        navigationItem.title = "Ví khuyến mãi"
        self.navigationController?.isNavigationBarHidden = false
        let backButton = UIBarButtonItem()
        backButton.title = " "
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        listPromoTrans()
    }

    func listPromoTrans()  {
        let CompletionHandler: ((Array<Transactions>, WalletInfo, NavigoError?) -> Void) = {
            arrTrans,walletInfo,navigoError in
            DispatchQueue.main.async {
                
                if(navigoError == nil){
                    self.lblPromoWallet.text =  self.convertMoney(value: walletInfo.promoWallet!) + " VNĐ"
                    self.lblPromoExpireDate.text = "Hạn sử dụng: " + walletInfo.promoExpireDate!
                    DataNavigo.arrTransWallet = arrTrans
                    self.tblTransWallet.reloadData()
                }else{
                    if(navigoError?.errorCode == Errorcode.ERROR_NO_INTERNET){
                        if navigoError?.errorCode?.range(of: Errorcode.NAVIGO_SESSION_EXPIRED) != nil{
                        }
                    }
                    else{
                    }
                }
            }
        }
        
        let wallet = APIWallet();
        wallet.listPromoTrans(CompletionHandler: CompletionHandler)
    }
    
    func convertMoney(value: Int) -> String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.groupingSeparator = "."
        let formattedString = formatter.string(for: value)
        
        return formattedString!
    }

}

extension WalletInfoController: UITableViewDelegate, UITableViewDataSource{
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return  DataNavigo.arrTransWallet.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WalletCell", for: indexPath) as! WalletCell
        let trans:Transactions = DataNavigo.arrTransWallet[indexPath.row]
        cell.img.image = #imageLiteral(resourceName: "ic_promotion_wallet")
        cell.lblXeloPrice.text = self.convertMoney(value: Int(trans.amount!)!) + " VNĐ"
        cell.lblNote.text = trans.note
        cell.lblDate.text = trans.transDate
        //let bonus = Int(book.xeloBonus!) + book.promotionValue!
        //let fees = Int(book.xeloFee!)
        //cell.lblXeloPrice.text = "-" + formatString(price:fees) + " đ "
        //            cell.lblWallet.text = "+" + formatString(price: bonus) + " đ"
        //cell.lblDate.text = book.bookDate
        //            cell.lblTime.text = book.bookDate?.substring(from: 9)
        return (cell)
        
    }
}
