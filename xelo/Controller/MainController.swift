//
//  MainController.swift
//  NaviGo
//
//  Created by HoangManh on 9/20/17.
//  Copyright © 2017 DUMV. All rights reserved.
//

import UIKit

class MainController: UIViewController,HandingScreen,UINavigationControllerDelegate  {
    var ko:Int = 1
    func navigationControllerSupportedInterfaceOrientations(_ navigationController: UINavigationController) -> UIInterfaceOrientationMask {
            return UIInterfaceOrientationMask.portrait
        }
    @IBOutlet weak var btnLogin: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let curren_language = UserDefaults.standard.string(forKey: "curren_language")
        if curren_language == "" || curren_language == nil {
            UserDefaults.standard.set("VN", forKey: "curren_language")
        }
        //UIApplication.shared.statusBarStyle = .lightContent
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                DataNavigo.deviceModel = Constant.deviceModelSmall
                print("===iPhone 5 or 5S or 5C")
            case 1334:
                DataNavigo.deviceModel = Constant.deviceModelNormal
                print("===iPhone 6/6S/7/8")
            case 2208:
                DataNavigo.deviceModel = Constant.deviceModelPlus
                print("===iPhone 6+/6S+/7+/8+")
            case 2436:
                DataNavigo.deviceModel = Constant.deviceModelX
                print("===iPhone X")
            case 2688:
                DataNavigo.deviceModel = Constant.deviceModelXSM
                print("===iPhone XS Max")                
            case 1792:
                DataNavigo.deviceModel = Constant.deviceModelXR
                print("===iPhone XR")
            default:
                DataNavigo.deviceModel = Constant.deviceModelNormal
                print("===unknown")
            }
        }
        btnLogin.layer.cornerRadius = 5
        btnLogin.alpha = 0
        btnLogin.addTarget(self, action: #selector(loginApp), for: .touchUpInside)
        //lblMessage.alpha = 0
        DataNavigo.serviceBackground.beginBackgroundDownLoad()
        DataNavigo.serviceBackground.doBackgroundTask()
        startRequest()

        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.red]
        navigationController?.navigationBar.tintColor = .red
        self.navigationController?.navigationBar.barTintColor = .white
        self.navigationController?.isNavigationBarHidden = true
        self.navigationController?.navigationBar.isTranslucent = false
       // print("==================")
        //DataNavigo.passenger = Passenger()
        DataNavigo.passenger.deviceSerial=UIDevice.current.identifierForVendor!.uuidString
        loginApp()
        
    }
    
    @objc func loginApp() {
        if (DataNavigo.passenger.getInformationUser() == true){
           // print("Đã có tài khoản")
            getAuthenKey()
            ko=1
            startRequest()
            login()
            
        }else {
            perform(#selector(loginController), with: nil, afterDelay: 0.01)
        }
    }
    
    var NOTIFICATION_TITLE:String = "Thông báo"
    var UPDATE_TITLE:String = "Cập nhật"
    var CONFIRM:String = "Xác nhận"
    var CANCEL:String = "Huỷ"
    var NOTIFICATION_UPDATE_PART:String = "Xelo vừa cập nhật phiên bản mới!\n"
    
    @IBOutlet weak var lblMessage: UILabel!
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getAuthenKey() {
        let key:String = "ABQjsaA138aA139091ABkshfoqhfldpqng27191nfd"
        let time:String = Int64(Date().timeIntervalSince1970*1000).description
        let anthenkey = DataNavigo.passenger.acountID + "-"+time+"-"+DataNavigo.passenger.deviceSerial+"-"+DataNavigo.passenger.securekey + "-" + key
        print("anthenkey",anthenkey)
        let ss = MD5(anthenkey).lowercased()
        DataNavigo.passenger.authenkey = ss
        DataNavigo.passenger.time = time
    }
    ///login
    func login() {
        //lblMessage.alpha = 1
        let CompletionHandler: ((LoginResult,NavigoError?) -> Void) = {
            loginResult,navigoError in
            
            DispatchQueue.main.async {
                DataNavigo.arrService = (loginResult.arrService)
                DataNavigo.booking = (loginResult.booking)
                DataNavigo.loginResult = loginResult
                let h = HistoriesAsynctacsk()
                h.doInBackGround()
                if(navigoError == nil){
                    if(DataNavigo.booking.state != BookingState.STATE_NONE){
                        var i = 0;
                        for s:Service in DataNavigo.arrService{
                            if s.serviceCode?.range(of:  DataNavigo.booking.serviceCode!) != nil{
                                DataNavigo.serviceCar = i
                                break
                            }
                            i += 1
                        }
                    }
                    if(loginResult.hasNewVersion == false){
                        self.endRequest()
                    }
                    else{
                        var messageText = "XELO vừa cập nhật phiên bản mới"
                        if(loginResult.releaseNote != "" && loginResult.releaseNote != nil){
                            messageText += "\n " + loginResult.releaseNote
                        }
                        messageText += "\n" + "Bạn có muốn cập nhật không?"
                        let alert:UIAlertController = UIAlertController(title: "Thông báo", message: messageText, preferredStyle: UIAlertControllerStyle.alert)
                        let btnOk:UIAlertAction = UIAlertAction(title: "Cập nhật", style: .destructive) { (btnOk) in
                            //"itms-apps://itunes.apple.com/app/id1314786940"
                            if let url = URL(string: DataNavigo.loginResult.appUrl),
                                UIApplication.shared.canOpenURL(url)
                            {
                                if #available(iOS 10.0, *) {
                                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                                } else {
                                    UIApplication.shared.openURL(url)
                                }
                            }
                        }
                        let btnCancel:UIAlertAction = UIAlertAction(title: "Huỷ", style: .destructive) { (btnCancel) in
                           self.endRequest()
                        }
                        alert.addAction(btnOk)
                        alert.addAction(btnCancel)
                        self.present(alert, animated: true, completion: nil)
                    }
                }else{
                    self.btnLogin.alpha = 1

                    if(navigoError?.errorCode == Errorcode.ERROR_NO_INTERNET){
                        self.errorNoInternet()
                        self.showToast(message: "Thiết bị đang mất kết nối mạng")
                    }
                    else if(navigoError?.errorCode == Errorcode.ERROR_KXD){
                        self.errorNoInternet()
                        print((navigoError?.errorMessage)!)
                        self.showToast(message: (navigoError?.errorMessage)!)
                    }
                    else{
                        self.errorServerSend(errorCode: (navigoError?.errorCode)!, errorToast: (navigoError?.errorMessage)!)
                    }
                }
            }
        }
        
        let connector = PassengerConnector();
        connector.loginAccount(passenger: DataNavigo.passenger, reconnect: false, CompletionHandler: CompletionHandler)
        
    }
    //reactive
    func reactive() {
        let CompletionHandler: ((Passenger,NavigoError?) -> Void) = {
            passenger,navigoError in
            
            DispatchQueue.main.async {
                DataNavigo.passenger.acountID = passenger.acountID
                DataNavigo.passenger.securekey = passenger.securekey
                 if(navigoError == nil){
                    self.endRequest()
                }else{
                    if(navigoError?.errorCode == Errorcode.ERROR_NO_INTERNET){
                        self.errorNoInternet()
                    }
                    else{
                        self.errorServerSend(errorCode: (navigoError?.errorCode)!, errorToast: (navigoError?.errorMessage)!)
                    }
                }
            }        }
        
        let connector = PassengerConnector();
        connector.reActiveAccount(passenger: DataNavigo.passenger, CompletionHandler: CompletionHandler)
        
    }
    ///
    @objc func loginController() {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let signin = sb.instantiateViewController(withIdentifier: "SignInController") as! SignInController
        self.navigationController?.pushViewController(signin, animated: true)
//        let sb = UIStoryboard(name: "Main", bundle: nil)
//        let view = sb.instantiateViewController(withIdentifier: "ViewController") as! ViewController
//        self.navigationController?.pushViewController(view, animated: true)
    }
    
    func errorConnect() {
    }
    func errorNoInternet() {
    }
    func errorServerSend(errorCode: String, errorToast: String) {
        if errorCode.range(of: Errorcode.NAVIGO_ACCOUNT_DUPLICATE) != nil{
            let alert:UIAlertController = UIAlertController(title: "Tài khoản đã được đăng ký", message: "Bạn có muốn kích hoạt lại tài khoản này không?", preferredStyle: UIAlertControllerStyle.alert)
            
            let btnOk:UIAlertAction = UIAlertAction(title: "Kích hoạt", style: .destructive) { (btnOk) in
                self.ko = 2
                if DataNavigo.passenger.authenByFirebase{
                    self.endRequest()
                }else{
                    self.reactive()
                }
            }
            let btnCancel:UIAlertAction = UIAlertAction(title: "Huỷ", style: .destructive) { (btnCancel) in
                self.loginController()
            }
            alert.addAction(btnOk)
            alert.addAction(btnCancel)
            present(alert, animated: true, completion: nil)
        }else if errorToast.range(of: Errorcode.NAVIGO_ACCOUNT_NOT_FOUND) != nil{
//            let alert:UIAlertController = UIAlertController(title: "Tài khoản đã được đăng ký", message: "Bạn có muốn kích hoạt lại tài khoản này không?", preferredStyle: UIAlertControllerStyle.alert)
//
//            let btnOk:UIAlertAction = UIAlertAction(title: "Kích hoạt", style: .destructive) { (btnOk) in
//                self.ko = 2
//                if DataNavigo.passenger.authenByFirebase{
//                    self.endRequest()
//                }else{
//                    self.reactive()
//                }
//            }
//            let btnCancel:UIAlertAction = UIAlertAction(title: "Huỷ", style: .destructive) { (btnCancel) in
//
//            }
//            alert.addAction(btnOk)
//            alert.addAction(btnCancel)
//            present(alert, animated: true, completion: nil)
            perform(#selector(loginController), with: nil, afterDelay: 0.01)
        }else if errorCode.range(of: Errorcode.NAVIGO_ACCOUNT_ACTIVATED) != nil{
            
        }else if errorCode.range(of: Errorcode.NAVIGO_ACCOUNT_BLOCKED) != nil{
            
        }else if errorCode.range(of: Errorcode.NAVIGO_ACCOUNT_INVALID) != nil{
            let alert:UIAlertController = UIAlertController(title: "Tài khoản đã được đăng ký", message: "Bạn có muốn kích hoạt lại tài khoản này không?", preferredStyle: UIAlertControllerStyle.alert)
            
            let btnOk:UIAlertAction = UIAlertAction(title: "Kích hoạt", style: .destructive) { (btnOk) in
                self.ko = 2
                if DataNavigo.passenger.authenByFirebase{
                    self.endRequest()
                }else{
                    self.reactive()
                }
            }
            let btnCancel:UIAlertAction = UIAlertAction(title: "Huỷ", style: .destructive) { (btnCancel) in
                self.loginController()
            }
            alert.addAction(btnOk)
            alert.addAction(btnCancel)
            present(alert, animated: true, completion: nil)
        }else if errorCode.range(of: Errorcode.NAVIGO_INVALID_EMAIL) != nil{
            
        }else if errorCode.range(of: Errorcode.NAVIGO_SKYMAP_ID_EXISTS) != nil{
            
        }else if errorCode.range(of: Errorcode.NAVIGO_INPUT_REQUIRED) != nil{
            
        }else{
            if errorToast != "" {
                let alert:UIAlertController = UIAlertController(title: "Thông báo", message: errorToast, preferredStyle: UIAlertControllerStyle.alert)
                let btnOk:UIAlertAction = UIAlertAction(title: "Xác nhận", style: .destructive) { (btnOk) in
                }
                alert.addAction(btnOk)
                present(alert, animated: true, completion: nil)
            }else{
                let alertController = UIAlertController(title: "Thông báo", message: "Đăng nhập không thành công", preferredStyle: .alert)
                let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                }
                alertController.addAction(OKAction)
                self.present(alertController, animated: true, completion:nil)
            }
        }
    }
    func startRequest() {
        DataNavigo.handingScreen = self
        
        //lblMessage.alpha = 0
    }
    func endRequest() {
        if(ko==1){
//            let sb = UIStoryboard(name: "BusCar", bundle: nil)
//            let skymap = sb.instantiateViewController(withIdentifier: "BusBookingController") as! BusBookingController
//            self.navigationController?.pushViewController(skymap, animated: true)
            let sb = UIStoryboard(name: "Main", bundle: nil)
            let skymap = sb.instantiateViewController(withIdentifier: "HomeController") as! HomeController
            self.navigationController?.pushViewController(skymap, animated: true)
        }else if(ko == 2){
            let sb = UIStoryboard(name: "Main", bundle: nil)
            let activecode = sb.instantiateViewController(withIdentifier: "SimpleActiveCodeController") as! SimpleActiveCodeController
            self.navigationController?.pushViewController(activecode, animated: true)
        }
    }
    func showToast(message : String) {
        
        let toastLabel = UILabel(frame: CGRect(x: 10, y: self.view.frame.size.height-100, width: self.view.frame.size.width - 20 , height: 35))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "Montserrat-Light", size: 12.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 15;
        toastLabel.clipsToBounds  =  true
        
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 2.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
}
extension String {
    func index(from: Int) -> Index {
        return self.index(startIndex, offsetBy: from)
    }
    
    func substring(from: Int) -> String {
        let fromIndex = index(from: from)
        return substring(from: fromIndex)
    }
    
    func substring(to: Int) -> String {
        let toIndex = index(from: to)
        return substring(to: toIndex)
    }
    
    func substring(with r: Range<Int>) -> String {
        let startIndex = index(from: r.lowerBound)
        let endIndex = index(from: r.upperBound)
        return substring(with: startIndex..<endIndex)
    }
}

