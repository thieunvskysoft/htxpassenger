//
//  ProfileViewController.swift
//  xelo
//
//  Created by Nguyen Thieu on 12/19/18.
//  Copyright © 2018 DUMV. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController,HandingScreen, UITextFieldDelegate  {
    
    @IBOutlet weak var imgAvatar: UIImageView!
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var contentview: UIView!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    func signAccount() {
        var phone = self.txtPhone.text!
        if phone.first == "0" {
            phone = phone.substring(from: 1)
        }
        updatePassenger(name: self.txtName.text!, email: self.txtEmail.text!)
    }
    
    func getPassenger() {
        let CompletionHandler: ((Passenger,NavigoError?) -> Void) = {
            passenger,navigoError in
            DispatchQueue.main.async {
                if(navigoError == nil){
                    self.txtName.text = passenger.nameUser
                    self.txtEmail.text = passenger.emailUser
                    self.txtPhone.text = passenger.numberphoneUser.replacingOccurrences(of: "+84", with: "0")
                    
                }else{
                    if(navigoError?.errorCode == Errorcode.ERROR_NO_INTERNET){
                        self.errorNoInternet()
                    }
                    else{
                        self.errorServerSend(errorCode: (navigoError?.errorCode)!, errorToast: (navigoError?.errorMessage)!)
                    }
                }
            }
        }
        let connector = PassengerConnector();
        connector.getPassenger(CompletionHandler: CompletionHandler)
    }
    
    func updatePassenger(name:String, email:String) {
        let CompletionHandler: ((String,NavigoError?) -> Void) = {
            passenger,navigoError in
            DispatchQueue.main.async {
                if(navigoError == nil){
                    //self.endRequest()
//                    DataNavigo.passenger.acountID = passenger.acountID
                    DataNavigo.passenger.nameUser = self.txtName.text ?? ""
                    DataNavigo.passenger.emailUser = self.txtEmail.text ?? ""
                    self.showMessage()
                }else{
                    if(navigoError?.errorCode == Errorcode.ERROR_NO_INTERNET){
                        self.errorNoInternet()
                    }
                    else{
                        self.errorServerSend(errorCode: (navigoError?.errorCode)!, errorToast: (navigoError?.errorMessage)!)
                    }
                }
            }
        }
        let connector = PassengerConnector();
        connector.updatePassenger(fullName: name, email: email, CompletionHandler: CompletionHandler)
    }
    @IBAction func btnViewAvata(_ sender: Any) {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let activecode = sb.instantiateViewController(withIdentifier: "ImageViewController") as! ImageViewController
        self.navigationController?.pushViewController(activecode, animated: true)
    }
    
    func showMessage() {
        let alert:UIAlertController = UIAlertController(title: "Thông báo", message: "Cập nhật thông tin thành công", preferredStyle: UIAlertControllerStyle.alert)
        let btnOk:UIAlertAction = UIAlertAction(title: "Xác nhận", style: .destructive) { (btnOk) in
            self.navigationController?.popViewController(animated: true)
        }
        alert.addAction(btnOk)
        present(alert, animated: true, completion: nil)
    }

    
    override func viewWillAppear(_ animated: Bool) {
        //navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor :UIColor.white]
        //self.navigationController?.navigationBar.barTintColor = NaviColor.navigation
        //navigationController?.navigationBar.alpha = 1
        self.navigationController?.isNavigationBarHidden = false
        let backButton = UIBarButtonItem()
        backButton.title = " "
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        navigationItem.title = "Thông tin người dùng"
        navigationController?.navigationBar.tintColor = .white
    }
    

    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var btnSignUp: DesignButton!

    
    //DataLanguage---
    var noti_Title:String = "Thông báo"
    var CONFIRM:String = "Xác nhận"
    var CANCEL:String = "Huỷ"
    var EDIT:String = "Sửa"
    var ASK_PHONE_NUMBER :String = "Đây là số điện thoại của bạn?"
    var MISSING_INFORMATION = "Thiếu thông tin"
    var CONFIRM_SMS_NUMBER = "Bạn sẽ sớm nhận được SMS kích hoạt vào số điện thoại này. Bạn có xác nhận số điện thoại này không?"
    var LANGUAGE_VIEW = "Ngôn ngữ"
    //----
    var name:String!
    var email:String!
    var yFloat:CGFloat = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadViewLanguage()
        getPassenger()
        imgAvatar.layer.cornerRadius = 45
        imgAvatar.clipsToBounds = true
        imgAvatar.image = DataNavigo.loginResult.avatarFace ?? UIImage(named: "Menu_Avt")
        txtName.text = DataNavigo.passenger.nameUser
        txtEmail.text = DataNavigo.passenger.emailUser
        txtPhone.text = DataNavigo.passenger.numberphoneUser //.replacingOccurrences(of: "+84", with: "0")
        txtPhone.isEnabled = false
        txtName.keyboardType = .default
        txtName.returnKeyType = UIReturnKeyType.done
//        txtPhone.keyboardType = .numberPad
//        txtPhone.returnKeyType = UIReturnKeyType.next
        txtEmail.keyboardType = .emailAddress
        txtEmail.returnKeyType = UIReturnKeyType.done
        txtName.delegate = self
        //txtPhone.delegate = self
        txtEmail.delegate = self
        btnSignUp.buttonRadius = 5
        btnSignUp.rounded = true
        btnSignUp.backgroundColor = NaviColor.naviBtnOrange
        btnSignUp.addTarget(self, action: #selector(displayMessageNoti), for: .touchUpInside)
        // Do any additional setup after loading the view.

        self.addDoneButtonOnKeyboard()
        NotificationCenter.default.addObserver(self, selector: #selector(SignInController.keyboardDidShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(SignInController.keyboardWillBeHidden(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 45))
        doneToolbar.barStyle = UIBarStyle.default
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(SignInController.doneButtonAction))
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        
        doneToolbar.items = items as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        
        self.txtPhone.inputAccessoryView = doneToolbar
    }
    
    func doneButtonAction()
    {
        self.txtPhone.resignFirstResponder()
    }
    
    func keyboardDidShow(_ notification: NSNotification) {
        let keyBoard = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as! NSValue
        let sizeKey = keyBoard.cgRectValue
        let heightKey = sizeKey.height
        let kc = view.frame.height - (yFloat + heightKey)
        if kc < 0 {
            let a = kc * -1 + 50
            bottomConstraint.constant = a
            let point:CGPoint = CGPoint(x: 0, y: a)
            scrollview.setContentOffset(point, animated: true)
        }
        
    }
    
    @IBAction func textfieldEdit(_ sender: Any) {
        let txt:UITextField = sender as! UITextField
        txt.delegate = self
        self.yFloat = txt.frame.origin.y + 30
        
    }
    
    func keyboardWillBeHidden(_ notification: NSNotification) {
        self.bottomConstraint.constant = 0
        UIView.animate(withDuration: 1) {
            self.scrollview.layoutSubviews()
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
//        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
//            nextField.becomeFirstResponder()
//            NotificationCenter.default.addObserver(self, selector: #selector(SignInController.keyboardDidShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
//        } else {
            textField.resignFirstResponder()
//            return true;
//        }
        return true
    }
    
    func loadViewLanguage() {
        let curren_language = UserDefaults.standard.string(forKey: "curren_language")
        let language = LanguageData()
        //language.localizedString(forKey:curren_language!,forKey: "TITLE_NOTI_REGISTER")
        txtPhone.placeholder = language.localizedString(forKey:curren_language!,forKey: "PHONE_INPUT")
        txtName.placeholder = language.localizedString(forKey:curren_language!,forKey: "NAME_INPUT")
        txtEmail.placeholder = language.localizedString(forKey:curren_language!,forKey: "EMAIL_INPUT")
        
        //btnSignUp.setTitle(language.localizedString(forKey:curren_language!,forKey: "REGISTER_PASSENGER"), for: .normal)
        noti_Title = language.localizedString(forKey:curren_language!,forKey: "NOTIFICATION_TITLE")
        CONFIRM = language.localizedString(forKey:curren_language!,forKey: "CONFIRM")
        EDIT = language.localizedString(forKey:curren_language!,forKey: "EDIT")
        CANCEL = language.localizedString(forKey:curren_language!,forKey: "CANCEL")
        //ASK_PHONE_NUMBER
        ASK_PHONE_NUMBER = language.localizedString(forKey:curren_language!,forKey: "ASK_PHONE_NUMBER")
        CONFIRM_SMS_NUMBER = language.localizedString(forKey:curren_language!,forKey: "CONFIRM_SMS_NUMBER")
        MISSING_INFORMATION = language.localizedString(forKey:curren_language!,forKey: "MISSING_INFORMATION")
        LANGUAGE_VIEW = language.localizedString(forKey:curren_language!,forKey: "LANGUAGE_VIEW")
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    fileprivate func isCheckValidateform() -> Bool {
        var ischeck = true
        let phone = txtPhone.text
        let providedEmailAddress = txtEmail.text
        _ = isValidEmailAddress(emailAddressString: providedEmailAddress!)
        if phone?.trimmingCharacters(in: .whitespaces).count != 0 {
            
        }else {
            displayAlertMessage(messageToDisplay: MISSING_INFORMATION)
            ischeck = false
        }
        //        if !isEmailAddressValid{
        //            displayAlertMessage(messageToDisplay: "Địa chỉ email không hợp lệ")
        //            ischeck = false
        //        }
        return ischeck
        //UserDefaults.standard.isLoggedIn()
    }
    func isValidEmailAddress(emailAddressString: String) -> Bool {
        
        var returnValue = true
        let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
        
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = emailAddressString as NSString
            let results = regex.matches(in: emailAddressString, range: NSRange(location: 0, length: nsString.length))
            
            if results.count == 0
            {
                returnValue = false
            }
            
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        
        return  returnValue
    }
    
    func displayAlertMessage(messageToDisplay: String)
    {
        if messageToDisplay != ""  {
            let alertController = UIAlertController(title: noti_Title, message: messageToDisplay, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: CONFIRM , style: .default) { (action:UIAlertAction!) in
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion:nil)
        }
    }
    
    @objc func displayMessageNoti()
    {
        if isCheckValidateform() {
            self.signAccount()
        }
    }
    func errorConnect() {
    }
    func errorNoInternet() {

    }
    func errorServerSend(errorCode: String, errorToast: String) {

    }
    func startRequest() {
        DataNavigo.handingScreen = self
    }
    func endRequest() {
 
    }
}

