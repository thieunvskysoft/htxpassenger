//
//  ImageViewController.swift
//  xelo
//
//  Created by Nguyen Thieu on 7/3/19.
//  Copyright © 2019 DUMV. All rights reserved.
//

import UIKit

class ImageViewController: UIViewController {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var btnAction: UIBarButtonItem!
    @IBOutlet weak var viewBottom: UIView!
    //static var imgResult = ImageListResult()
    
    @IBOutlet weak var viewHidden: UIView!
    static var imgCode:String = ""
    static var imgIndex:Int = 0
    var imageValue:UIImage? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.tintColor = .white
        viewBottom.backgroundColor = NaviColor.navigationOrange
        viewHidden.backgroundColor = NaviColor.navigationOrange
//        navigationItem.title = ImageViewController.imgResult.description
//        getImage(value: ImageViewController.imgResult.photoType!)
    }
    override func viewWillAppear(_ animated: Bool) {
        //DataNavigo.idUIController = 2
        self.navigationController?.isNavigationBarHidden = false
        let backButton = UIBarButtonItem()
        backButton.title = " "
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        
        //imageView.layer.borderWidth = 0.5
        
    }
    
    @IBAction func btnUpdateImg(_ sender: Any) {
        if imageValue != nil {
            updateImage(imgAvatar: imageValue!)
        }else{
            let alert:UIAlertController = UIAlertController(title: "Thông báo", message: "Vui lòng cập nhật ảnh mới trước khi gửi", preferredStyle: UIAlertControllerStyle.alert)
            let btnOk:UIAlertAction = UIAlertAction(title: "Xác nhận", style: .destructive) { (btnOk) in
            }
            alert.addAction(btnOk)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    @IBAction func btnCamera(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
        
    }
    @IBAction func btnPhotos(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            let imagePickerController = UIImagePickerController()
            imagePickerController.delegate = self
            imagePickerController.sourceType = .photoLibrary
            self.present(imagePickerController, animated: true, completion: nil)
        }
    }
    @IBAction func btnRotateLeft(_ sender: Any) {
        guard let image = self.imageView.image?.imageRotatedByDegrees(-90, flip: false) else {
            return
        }
        imageValue = image
        self.imageView.image = imageValue
    }
    @IBAction func btnRotateRight(_ sender: Any) {
        guard let image = self.imageView.image?.imageRotatedByDegrees(90, flip: false) else {
            return
        }
        imageValue = image
        self.imageView.image = imageValue
    }
    
    func updateImage(imgAvatar:UIImage) {
        let CompletionHandler: ((String,NavigoError?) -> Void) = {
            value,navigoError in
            DispatchQueue.main.async {
                if(navigoError == nil){
                    let alert:UIAlertController = UIAlertController(title: "Thông báo", message: "Cập nhật thành công", preferredStyle: UIAlertControllerStyle.alert)
                    let btnOk:UIAlertAction = UIAlertAction(title: "Xác nhận", style: .destructive) { (btnOk) in
                        DataNavigo.loginResult.avatarFace = imgAvatar
                    }
                    alert.addAction(btnOk)
                    self.present(alert, animated: true, completion: nil)
                }else{
                    
                }
            }
        }
        let connector = PassengerConnector();
        connector.storeAvatar(imgAvata: imgAvatar, CompletionHandler: CompletionHandler)
    }
    
    
    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        var widthRatio: CGFloat = 0.0
        var heightRatio: CGFloat = 0.0
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(size.height > size.width) {
            heightRatio = 680.0
            widthRatio = (size.width * 680.0) / size.height
            newSize = CGSize(width: widthRatio, height: heightRatio)
        } else {
            widthRatio = 680.0
            heightRatio = (size.height * 680.0) / size.width
            newSize = CGSize(width: widthRatio, height: heightRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
}

extension ImageViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        guard let image = info[UIImagePickerControllerOriginalImage] as? UIImage else{
            return
        }
        imageValue = resizeImage(image: image, targetSize: CGSize(width: 680.0, height: 680.0))
        self.imageView.image = imageValue
        self.dismiss(animated: true, completion: nil)
    }
}

