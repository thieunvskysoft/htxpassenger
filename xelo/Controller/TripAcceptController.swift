//
//  TripAcceptController.swift
//  xelo
//
//  Created by Nguyen Thieu on 3/9/20.
//  Copyright © 2020 DUMV. All rights reserved.
//

import UIKit

class TripAcceptController: UIViewController {
    
    @IBOutlet weak var tripId: UILabel!
    @IBOutlet weak var tripDate: UILabel!
    @IBOutlet weak var tripDistance: UILabel!
    @IBOutlet weak var tripPrice: UILabel!
    @IBOutlet weak var tripNote: UILabel!
    @IBOutlet weak var tripStart: UILabel!
    @IBOutlet weak var tripEnd: UILabel!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var viewStatus: UIView!
    @IBOutlet weak var tripPromotion: UILabel!
    @IBOutlet weak var tripService: UILabel!
    @IBOutlet weak var viewNote: UIView!
    @IBOutlet weak var imgDriver: UIImageView!
    @IBOutlet weak var nameDriver: UILabel!
    @IBOutlet weak var plateNumber: UILabel!
    
    var tripInfo = Book()
    
    
    override func viewDidLoad() {
            super.viewDidLoad()
            let backButton = UIBarButtonItem()
            backButton.title = " "
            self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
            navigationController?.navigationBar.tintColor = .red
            self.navigationController?.navigationBar.barTintColor = .white
            navigationItem.title = "Thông tin cuốc"
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.red]
            tripId.text = tripInfo.bookingId
            tripDate.text = tripInfo.departureDate
            tripDistance.text = String(format: "%.1f", Double(tripInfo.estDistance!)/1000) + " Km"
            //tripPersions.text = tripInfo.persons.description
            tripPrice.text = "đ " + (tripInfo.fromCharge?.formatnumberPrice())!
            tripNote.text = tripInfo.note
            tripStart.text = tripInfo.fromAddress
            tripEnd.text = tripInfo.toAddress
            btnCancel.layer.cornerRadius = 10
            viewStatus.layer.cornerRadius = 3
            viewNote.layer.cornerRadius = 5
            //viewNote.layer.borderColor = CGColor.
            viewNote.layer.borderWidth = 0.5
            getdriverInfor()
            imgDriver.layer.cornerRadius = 30
        }
        
        override func viewWillAppear(_ animated: Bool) {
            print("tripInfo.state!",tripInfo.state!)
            if tripInfo.state! == 0 || tripInfo.state! == 1 {
                btnCancel.alpha = 1
            } else {
                btnCancel.alpha = 0
            }
            if tripInfo.state == 0 {
                status.text = "Đang tìm"
            }
            if tripInfo.state == 1 {
                status.text = "Đã nhận"
            }
            if tripInfo.state == 2 {
                status.text = "Đã đón"
            }
        }
        
        
        
//        @IBAction func btnCancel(_ sender: Any) {
//            cancelBooking()
//        }
        
        func getdriverInfor()  {
            let CompletionHandler: ((DriverInfo,NavigoError?) -> Void) = {
                driverinfo,navigoError in
                DispatchQueue.main.async {
                    if(navigoError == nil){
                        self.imgDriver.image = driverinfo.avatarFace
                        self.nameDriver.text = driverinfo.fullName
                        self.plateNumber.text = driverinfo.plateNumber
                    }else{
                        if(navigoError?.errorCode == Errorcode.ERROR_NO_INTERNET){
                            //self.errorNoInternet()
                        }
                        else{
                            //self.errorServerSend(errorCode: (navigoError?.errorCode)!, errorToast: (navigoError?.errorMessage)!)
                        }
                    }
                }
            }
            let connector = APIBook();
            connector.getDriverInfo(book: tripInfo, CompletionHandler: CompletionHandler)
        }
        
        func cancelBooking()  {
            let CompletionHandler: ((Book,NavigoError?) -> Void) = {
                booking,navigoError in
                DispatchQueue.main.async {
                    if(navigoError == nil){
                        DataNavigo.booking.state = nil
                        DataNavigo.booking.bookingId = nil
                        self.navigationController?.popViewController(animated: true)

                    }else{
                        if(navigoError?.errorCode == Errorcode.ERROR_NO_INTERNET){
    //                        self.errorNoInternet()
                        }
                        else{
    //                        self.errorServerSend(errorCode: (navigoError?.errorCode)!, errorToast: Errorcode.ERROR_MESSAGE)
                        }
                    }
                }
            }
            let connector = APIBook();
            connector.cancelBooking(book: tripInfo, cancelCode: "", CompletionHandler: CompletionHandler)
        }
        
    }

