//
//  ListTripController.swift
//  xelo
//
//  Created by Nguyen Thieu on 1/30/20.
//  Copyright © 2020 DUMV. All rights reserved.
//

import UIKit

class ListTripController: UIViewController {

    @IBOutlet weak var triptable: UITableView!
    var arrBook = [Book]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        let backButton = UIBarButtonItem()
        backButton.title = " "
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        navigationController?.navigationBar.tintColor = .red
        self.navigationController?.navigationBar.barTintColor = .white
        navigationItem.title = "Cuốc xe đã đặt"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.red]
        self.triptable.rowHeight = 150
        triptable.dataSource = self
        triptable.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getLiveBooking()
    }
    
    func getLiveBooking()  {
        let CompletionHandler: ((Array<Book>,NavigoError?) -> Void) = {
            booking,navigoError in
            DispatchQueue.main.async {
                if(navigoError == nil){
                    self.arrBook = booking
                    self.triptable.reloadData()
                }else{
                    
                }
            }
        }
        let booking = APIBook();
        booking.getLiveBookings(CompletionHandler: CompletionHandler)
    }
    
    @IBAction func btnBack(_ sender: Any) {
        let viewControllers:[UIViewController] = (self.navigationController?.viewControllers)!;
        for controller in viewControllers {
            if controller is HomeController {
                self.navigationController!.popToViewController(controller, animated: true)
            }
        }
    }
    
    func formatnumber(number:Double) -> String {
        let formater = NumberFormatter()
        formater.groupingSeparator = "."
        formater.numberStyle = .decimal
        let formattedNumber = formater.string(from: NSNumber(value: number))
        return formattedNumber!
    }
}
extension ListTripController: UITableViewDelegate, UITableViewDataSource {
    
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrBook.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TripCell", for: indexPath) as! TripCell
        let info:Book = arrBook[indexPath.row]
        cell.lblStart.text = info.fromAddress
        cell.lblEnd.text = info.toAddress
        //cell.lblSeat.text = "Số người: " + info.persons.description
        cell.lblTime.text = info.departureDate
        cell.lblPrice.text = formatnumber(number: info.fromCharge!)
        cell.tripNote.text = info.note
        cell.tripId.text = info.bookingId
        if info.state == 0 {
            cell.lblType.text = "Đang tìm"
        }
        if info.state == 1 {
            cell.lblType.text = "Đã nhận"
        }
        if info.state == 2 {
            cell.lblType.text = "Đã đón"
        }
        return (cell)
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let tripInfo = sb.instantiateViewController(withIdentifier: "TripInfoController") as! TripInfoController
        tripInfo.tripInfo = arrBook[indexPath.row]
        self.navigationController?.pushViewController(tripInfo, animated: true)
    }
}
