//
//  ServiceViewCell.swift
//  NaviGo
//
//  Created by HoangManh on 9/28/17.
//  Copyright © 2017 DUMV. All rights reserved.
//

import UIKit

class ServiceViewCell: UITableViewCell {

    @IBOutlet weak var imageService: UIImageView!
    @IBOutlet weak var nameService: UILabel!
    @IBOutlet weak var priceService: UILabel!
    @IBOutlet weak var serviceViewDetail: UIView!
    @IBOutlet weak var normalPrice: UILabel!
    @IBOutlet weak var peakImg: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        serviceViewDetail.layer.cornerRadius = 5
        imageService.center.y = serviceViewDetail.center.y
        priceService.sizeToFit()
        priceService.center.y = imageService.center.y
        nameService.sizeToFit()
        normalPrice.textColor = UIColor.darkGray
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
