//
//  PartnerController.swift
//  NaviGo
//
//  Created by HoangManh on 11/2/17.
//  Copyright © 2017 DUMV. All rights reserved.
//

import UIKit
import WebKit

class PartnerController: UIViewController, UIWebViewDelegate {

    var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        let backButton = UIBarButtonItem()
        backButton.title = " "
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        navigationController?.navigationBar.tintColor = .white
        navigationItem.title = "Trở thành tài xế"
        _ = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        webView = UIWebView(frame: UIScreen.main.bounds)
        webView.delegate = self
        view.addSubview(webView)
        if let url = URL(string: Constant.URL_PARTNER + "?accountID="+DataNavigo.passenger.acountID) {
            let request = URLRequest(url: url)
            webView.loadRequest(request)
        }
    }

}
