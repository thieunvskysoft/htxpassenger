//
//  ViewVihicleController.swift
//  xelo
//
//  Created by Nguyen Thieu on 4/5/19.
//  Copyright © 2019 DUMV. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import SwiftyJSON
import Alamofire

class ViewVehiclesController: UIViewController, CLLocationManagerDelegate, GMSMapViewDelegate {

    var googleMap:GMSMapView?
    var arrGMSMarker: Array<GMSMarker> = Array()
    var arrplateNumber: Array<GMSMarker> = Array()
    var locationManager = CLLocationManager()
    var currentZoom:CGFloat = 15.0
    var locationPoint = Point()
    var locationDragged:Bool = false
    var addressLocation:String = ""
    var point:Int?
    var lastMoveMap:Int64 = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        DataNavigo.idUIController = 3
        setupGoogleMap()
        DataNavigo.handingScreen = self
    }
    override func viewWillAppear(_ animated: Bool) {
        let backButton = UIBarButtonItem()
        backButton.title = " "
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        navigationController?.navigationBar.tintColor = .white
        navigationItem.title = "Tìm xe 24h"
        //navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor :UIColor.white]
        if locationPoint.x != nil && locationPoint.y != nil {
            listPublicVehicles()
        }
    }
    
    //setup map
    func setupGoogleMap() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        locationManager.startMonitoringSignificantLocationChanges()
        locationManager.distanceFilter = 2
        locationManager.requestLocation()
        locationManager.requestAlwaysAuthorization()
        let camera = GMSCameraPosition.camera(withLatitude: 21.05, longitude: 105.777711, zoom: 15)
        googleMap = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        googleMap?.camera = camera
        googleMap?.delegate = self
        googleMap?.isMyLocationEnabled = true
        googleMap?.settings.myLocationButton = true
        //button location
        self.googleMap?.padding = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
        let navBarHeight = (self.navigationController?.navigationBar.intrinsicContentSize.height)! + UIApplication.shared.statusBarFrame.height
        do {
            // Set the map style by passing the URL of the local file.
            if let styleURL = Bundle.main.url(forResource: "style", withExtension: "json") {
                googleMap?.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
            } else {
                NSLog("Unable to find style.json")
            }
        } catch {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
        view.addSubview(googleMap!)
        if(DataNavigo.deviceModel == Constant.deviceModelX){
            googleMap?.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height - navBarHeight - 33)
            
        }else{
            googleMap?.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height - navBarHeight)
            
        }
    }
    
    func listPublicVehicles()  {
        let CompletionHandler: ((Array<Vehicle>,NavigoError?) -> Void) = {
            arrVehicle, navigoError in
            DispatchQueue.main.async {
                if(navigoError == nil){
                    for marker:GMSMarker in self.arrGMSMarker{
                        marker.map =  nil
                    }
                    for marker:GMSMarker in self.arrplateNumber{
                        marker.map =  nil
                    }
                    self.arrGMSMarker.removeAll()
                    self.arrplateNumber.removeAll()
                    for post:Vehicle in arrVehicle{
                        if post.speed != 0 {
                            self.createMarkerVehicle(titleMarker: post.travelLine,snippet: post.plateNumber!,iconMarker: UIImage(named: "vehicle_green")!, latitude: post.y!, longitude: post.x!, direction: post.direction!)
                            
                        }else{
                            self.createMarkerVehicle(titleMarker: post.travelLine,snippet: post.plateNumber!, iconMarker: UIImage(named: "vehicle_red")!, latitude: post.y!, longitude: post.x!, direction: post.direction!)
                        }

                    }
                }else{
                    if(navigoError?.errorCode == Errorcode.ERROR_NO_INTERNET){
                        
                    }else if (navigoError?.errorCode == Errorcode.NAVIGO_SESSION_EXPIRED){
                        self.reconect()
                    }
                }
            }
        }
        let apiVehicle = APIgetVehicles();
        apiVehicle.listPublicVehicles(location: locationPoint, CompletionHandler: CompletionHandler)
    }
    
    ///login
    func reconect() {
        let CompletionHandler: ((LoginResult,NavigoError?) -> Void) = {
            loginResult,navigoError in
            DispatchQueue.main.async {
                
            }
        }
        let connector = PassengerConnector();
        connector.loginAccount(passenger: DataNavigo.passenger, reconnect: true, CompletionHandler: CompletionHandler)
    }
    
    func createMarkerVehicle(titleMarker: String,snippet:String, iconMarker: UIImage, latitude: CLLocationDegrees, longitude: CLLocationDegrees,direction:Int) {

        let infoWindow = Bundle.main.loadNibNamed("MarkerView", owner: self, options: nil)?[0] as? MarkerView
        let imageRotate  = iconMarker.rotate(radians: CGFloat(direction))   
        infoWindow!.imgMarker.image = imageRotate

        let strokeTextAttributesPlateNumber = [
            NSAttributedStringKey.strokeColor.rawValue : UIColor.white,
            NSAttributedStringKey.foregroundColor : UIColor.blue,
            NSAttributedStringKey.strokeWidth : -1.0,
            NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 12)
            ] as! [NSAttributedStringKey : Any]
        
        let customizedText = NSMutableAttributedString(string: snippet, attributes: strokeTextAttributesPlateNumber)
        let strokeTextAttributesTravelLine = [
            NSAttributedStringKey.strokeColor.rawValue : UIColor.white,
            NSAttributedStringKey.foregroundColor : UIColor.blue,
            NSAttributedStringKey.strokeWidth : -1.0,
            NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 12)
            ] as! [NSAttributedStringKey : Any]
        
        let customizedTextTravelLine = NSMutableAttributedString(string: titleMarker, attributes: strokeTextAttributesTravelLine)
        
        infoWindow!.lblPlateNumber.attributedText = customizedText
        infoWindow!.lblTravelLine.attributedText = customizedTextTravelLine
        
        UIGraphicsBeginImageContext(infoWindow!.bounds.size);
        infoWindow!.layer.render(in: UIGraphicsGetCurrentContext()!)
        let screenShot = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2DMake(latitude, longitude)
        marker.icon = screenShot
        marker.groundAnchor = CGPoint(x: 0.5, y: 0.8)
        marker.isFlat = true
        marker.map = googleMap
        marker.tracksInfoWindowChanges = true
        arrGMSMarker.append(marker)
    }
    
    func imageWithImage(image:UIImage, scaledToSize newSize:CGSize) -> UIImage{
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0);
        //image.draw(in: CGRectMake(0, 0, newSize.width, newSize.height))
        image.draw(in: CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: newSize.width, height: newSize.height))  )
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    
}

//Map
extension ViewVehiclesController {
    // MARK: CLLocation Manager Delegate
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        //  print("Error while get location \(error)")
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        //    print("didupdateLocation")
        let location = locations.last
        locationPoint.x = location?.coordinate.longitude
        locationPoint.y = location?.coordinate.latitude
        self.locationManager.stopUpdatingLocation()
    }
    
    // MARK: GMSMapview Delegate
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        self.googleMap?.isMyLocationEnabled = true
        locationPoint.x = mapView.camera.target.longitude
        locationPoint.y = mapView.camera.target.latitude
        listPublicVehicles()
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        addressLocation = ""
        locationPoint.x = mapView.camera.target.longitude
        locationPoint.y = mapView.camera.target.latitude
    }
    
}

extension ViewVehiclesController: HandingScreen{
    func errorNoInternet() {
        
    }
    
    func errorConnect() {
        if locationPoint.x != nil && locationPoint.y != nil {
            listPublicVehicles()
        }
    }
    
    func errorServerSend(errorCode: String, errorToast: String) {
        
    }
    
    func startRequest() {
        
    }
    
    func endRequest() {
        
    }
    

}

extension UIImage{
    convenience init(view: UIView) {
        
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.isOpaque, 0.0)
        view.drawHierarchy(in: view.bounds, afterScreenUpdates: false)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.init(cgImage: (image?.cgImage)!)
        
    }
    func rotate(radians: CGFloat) -> UIImage {
        let rotatedSize = CGRect(origin: .zero, size: size)
            .applying(CGAffineTransform(rotationAngle: CGFloat(radians)))
            .integral.size
        UIGraphicsBeginImageContext(rotatedSize)
        if let context = UIGraphicsGetCurrentContext() {
            let origin = CGPoint(x: rotatedSize.width / 2.0,
                                 y: rotatedSize.height / 2.0)
            context.translateBy(x: origin.x, y: origin.y)
            context.rotate(by: radians)
            draw(in: CGRect(x: -origin.x, y: -origin.y,
                            width: size.width, height: size.height))
            let rotatedImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            return rotatedImage ?? self
        }
        
        return self
    }
}
