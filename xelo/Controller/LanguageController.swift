//
//  LanguageController.swift
//  xelo
//
//  Created by Nguyen Thieu on 8/8/18.
//  Copyright © 2018 DUMV. All rights reserved.
//

import UIKit

class LanguageController: UIViewController {

    @IBOutlet weak var tblViewLang: UITableView!
    var arrayLanguageOptions = [Dictionary<String,String>]()
    var LANGUAGE_VIEW = "Ngôn ngữ"
    var VIETNAM = "Việt Nam"
    var ENGLISH = "English"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblViewLang.delegate = self
        tblViewLang.dataSource = self
        tblViewLang.isScrollEnabled = false
        tblViewLang.separatorStyle = UITableViewCellSeparatorStyle.none
        arrayLanguageOptions.append(["title":"Việt Nam", "icon":"vnflag"])
        arrayLanguageOptions.append(["title":"English", "icon":"enflag"])
        tblViewLang.reloadData()
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        loadViewLanguage()
        self.navigationController?.isNavigationBarHidden = false
        let backButton = UIBarButtonItem()
        backButton.title = " "
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        
    }
    func loadViewLanguage() {
        let curren_language = UserDefaults.standard.string(forKey: "curren_language")
        let language = LanguageData()
        LANGUAGE_VIEW = language.localizedString(forKey:curren_language!,forKey: "LANGUAGE_VIEW")
        navigationItem.title = LANGUAGE_VIEW
    }
    
    func login() {
        let CompletionHandler: ((LoginResult,NavigoError?) -> Void) = {
            loginResult,navigoError in
            DispatchQueue.main.async {

            }
        }
        let connector = PassengerConnector();
        connector.loginAccount(passenger: DataNavigo.passenger, reconnect: false, CompletionHandler: CompletionHandler)
    }


}
extension LanguageController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayLanguageOptions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LanguageCell") as! LanguageCell
        cell.lblCountry?.text = arrayLanguageOptions[indexPath.row]["title"]!
        cell.imgFlag.image = UIImage(named: arrayLanguageOptions[indexPath.row]["icon"]!)
        return cell
    }
}

extension LanguageController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let item = arrayLanguageOptions[indexPath.row]["title"]
        let langCode = Language.getLanguageCode(item!)
        Language.setLanguage(langCode)
        self.loadViewLanguage()
        navigationController?.popViewController(animated: true)
    }
}
