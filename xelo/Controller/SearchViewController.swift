//
//  SearchViewController.swift
//  NaviGo
//
//  Created by Admin on 8/21/17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import SwiftyJSON
import Alamofire

enum Location {
    case startLocation
    case destinationLocation
}

class SearchViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchResultsUpdating, UISearchBarDelegate, CustomSearchControllerDelegate {
    
    @IBOutlet weak var tblSearchResults: UITableView!
    
    var dataArray = [String]()
    var filteredArray = [String]()
    var dataid = [String]()
    var runCheck:Bool=true
    let run:Run = Run()
    var addressLocation:String = ""
    var shouldShowSearchResults = false
    var searchController: UISearchController!
    var customSearchController: CustomSearchController!
    var listObject = [String : JSON]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //searchController.becomeFirstResponder()
        tblSearchResults.delegate = self
        tblSearchResults.dataSource = self
        loadListOfCountries()
        configureCustomSearchController()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if getLocation.choice == 1 {
            customSearchController.customSearchBar.text = DataNavigo.booking.fromAddress != nil ? DataNavigo.booking.fromAddress! :""
        }else{
            customSearchController.customSearchBar.text = DataNavigo.booking.toAddress != nil ? DataNavigo.booking.toAddress! :""
        }
        tblSearchResults.reloadData()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
 
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if shouldShowSearchResults {
            return filteredArray.count
        }
        else {
            return DataNavigo.arrHistories.count
            //return dataArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let indexPat = indexPath.row
        //let cell = tableView.dequeueReusableCell(withIdentifier: "idCell", for: indexPath) as! SearchViewCell
        let cell = tableView.cellForRow(at: indexPath) as! SearchViewCell
        if shouldShowSearchResults {
            addressLocation = (cell.lblLocation?.text)!
            run.getLocation(ss: dataid[indexPat])
        }   else{
            let his:Histories = DataNavigo.arrHistories[indexPath.row]
            addressLocation = (cell.lblLocation?.text)!
            updateLocation(lat:his.y!, lng:his.x!)
        }

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "idCell", for: indexPath) as! SearchViewCell
        if shouldShowSearchResults {
            cell.lblLocation.text = filteredArray[indexPath.row]
            cell.imgLocation.image = #imageLiteral(resourceName: "map-start")
            cell.setID(id: dataid[indexPath.row])
            cell.histori = nil
        }
        else {
            let his:Histories = DataNavigo.arrHistories[indexPath.row]
            cell.imgLocation.image = #imageLiteral(resourceName: "map-start")
            cell.lblLocation.text = his.fullAddress
            cell.histori = DataNavigo.arrHistories[indexPath.row]
            cell.setID(id: "histories")
        }
        cell.delegate = self
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40.0
    }

    // MARK: Custom functions
    
    func loadListOfCountries() {
        let pathToFile = Bundle.main.path(forResource: "countries", ofType: "txt")
        if let path = pathToFile {
            _ = try! String(contentsOfFile: path, encoding: String.Encoding.utf8)
            tblSearchResults.reloadData()
        }
    }
    
    func configureSearchController() {
        // Initialize and perform a minimum configuration to the search controller.
        searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Tìm kiếm..."
        searchController.searchBar.delegate = self
        searchController.searchBar.sizeToFit()
        
        // Place the search bar view to the tableview headerview.
        tblSearchResults.tableHeaderView = searchController.searchBar
    }
 
    func configureCustomSearchController() {
        customSearchController = CustomSearchController(searchResultsController: self, searchBarFrame: CGRect(x: 0.0, y: 0.0, width: tblSearchResults.frame.size.width, height: 40.0), searchBarFont: UIFont.systemFont(ofSize: 14), searchBarTextColor: UIColor.black, searchBarTintColor: UIColor.white)
        customSearchController.customSearchBar.placeholder = "Tìm điểm"
        tblSearchResults.tableHeaderView = customSearchController.customSearchBar
        customSearchController.customDelegate = self
    }

    // MARK: UISearchBarDelegate functions
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        print("11111111")
        shouldShowSearchResults = true
        tblSearchResults.reloadData()
    }
 
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        print("222222222")
        shouldShowSearchResults = false
        tblSearchResults.reloadData()
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        print("333333")
        if !shouldShowSearchResults {
            shouldShowSearchResults = true
            tblSearchResults.reloadData()
        }
        searchController.searchBar.resignFirstResponder()
    }

    // MARK: UISearchResultsUpdating delegate function
    func updateSearchResults(for searchController: UISearchController) {
        print("4444444444")
        guard let searchString = searchController.searchBar.text else {
            return
        }
        // Filter the data array and get only those countries that match the search text.
        filteredArray = dataArray.filter({ (country) -> Bool in
            let countryText:NSString = country as NSString
            return (countryText.range(of: searchString, options: NSString.CompareOptions.caseInsensitive).location) != NSNotFound
        })
        
        // Reload the tableview.
        tblSearchResults.reloadData()
    }
  
    // MARK: CustomSearchControllerDelegate functions
    func didStartSearching() {
        print("5555555555")
        shouldShowSearchResults = true
        tblSearchResults.reloadData()
    }
  
    func didTapOnSearchButton() {
        print("66666666666")
        if !shouldShowSearchResults {
            shouldShowSearchResults = true
            tblSearchResults.reloadData()
        }
    }

    func createaddress(address:Address)  {
        let CompletionHandler: ((NavigoError?) -> Void) = {
            navigoError in
            DispatchQueue.main.async {
                if(navigoError == nil){
                }else{
                    if(navigoError?.errorCode == Errorcode.ERROR_NO_INTERNET){
                    }
                    else{
                    }
                }
            }
        }
        let apiAddress = APIAddress();
        apiAddress.createAddress(address: address,CompletionHandler: CompletionHandler)
    }
    
    func didTapOnCancelButton() {
        shouldShowSearchResults = false
        tblSearchResults.reloadData()
    }
    
    func didChangeSearchText(_ searchText: String) {
        if(searchText.characters.count>0){
            let checkData = listObject.contains {
                key, value in
                    return key.elementsEqual(searchText)
            }
            if checkData {
                fillDataTable(data: listObject[searchText]!)
                print("111111111")
            }else{
                print("222222222")
                if(runCheck){
                    run.lou=self
                    //let cString = searchText.addingPercentEncoding(withAllowedCharacters: .alphanumerics)
                    run.getStringformUrl(text: searchText)
                    runCheck=false
                }
            }
        }
    }
    
    func updateSearch(dataValue:JSON , dataId:String)  {
        runCheck=true
        listObject[dataId] = dataValue
        fillDataTable(data: dataValue)
    }
    
    func fillDataTable(data:JSON)  {
        var dataArrayName = [String]()
        var dataArrayId = [String]()
        
        let array=data["predictions"].array
        
        for json1 in array!{
            dataArrayName.append(json1["description"].stringValue)
            dataArrayId.append(json1["place_id"].stringValue)
        }
        filteredArray=dataArrayName
        dataid=dataArrayId
        runCheck=true
        tblSearchResults.reloadData()
    }
    func updateLocation(lat:Double , lng:Double) {
        if getLocation.choice == 1 {
            DataNavigo.booking.fromPosition?.y = lat
            DataNavigo.booking.fromPosition?.x = lng
            DataNavigo.booking.fromAddress = addressLocation
        }
        else{
            DataNavigo.booking.toPosition?.y = lat
            DataNavigo.booking.toPosition?.x = lng
            DataNavigo.booking.toAddress = addressLocation
        }
        DataNavigo.checkDrawPath = true
        let viewControllers:[UIViewController] = (self.navigationController?.viewControllers)!;
        for controller in viewControllers {
            if controller is MapController {
                self.navigationController!.popToViewController(controller, animated: true)
            }
        }
       // print(lat)
       // print(lng)
    }
    
    func getLocationCreateAddress(id:String,name :String){
        
        DispatchQueue.global(qos: .userInitiated).async {
            let url1="https://maps.googleapis.com/maps/api/place/details/json?placeid="+id+"&key=AIzaSyBXZKFWRkjJRcf7PQO9OM9mj9SCUkft24s"
            let myUrl = URL(string: url1);
            var request = URLRequest(url:myUrl!)
            request.httpMethod="POST"
            
            let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in

                if let data = data,
                    let   html = String(data: data, encoding: String.Encoding.utf8) {
                    do {
                        let json1 = JSON(parseJSON: html)
                        DispatchQueue.main.async {
                            let address = Address()
                            address.y=json1["result"]["geometry"]["location"]["lat"].double
                            address.x=json1["result"]["geometry"]["location"]["lng"].double
                            address.fullAddress = json1["result"]["formatted_address"].stringValue
                            address.fullName = name
                            self.createaddress(address: address)
                        }
                        
                    }
                }   }
            
            task.resume()
        }
    }    
}


extension SearchViewController : PointDelegate {
    func didAddPoint(dataID: String, history: Histories) {
        let alert:UIAlertController = UIAlertController(title: "Tạo điểm yêu thích", message: "", preferredStyle: UIAlertControllerStyle.alert)
        alert.addTextField { (txtPoint) in
            txtPoint.placeholder = "Tên điểm "
            txtPoint.keyboardType = .default
        }
        let btnOk:UIAlertAction = UIAlertAction(title: "Xác nhận", style: .destructive) { (btnOk) in
            let namePoint = (alert.textFields![0].text)!
            if(dataID == "histories"){
                let address = Address()
                address.y=history.y
                address.x=history.x
                address.fullAddress = history.fullAddress
                address.fullName = namePoint
                self.createaddress(address: address)
            }else{
                self.getLocationCreateAddress(id: dataID, name: namePoint)
            }
            
        }
        let btnCancel:UIAlertAction = UIAlertAction(title: "Huỷ", style: .destructive) { (btnCancel) in
            
        }
        alert.addAction(btnOk)
        alert.addAction(btnCancel)
        present(alert, animated: true, completion: nil)
    }
    
}
