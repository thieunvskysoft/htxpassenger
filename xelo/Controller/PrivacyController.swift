//
//  PrivacyController.swift
//  xelo
//
//  Created by Nguyen Thieu on 3/1/18.
//  Copyright © 2018 DUMV. All rights reserved.
//

import UIKit

class PrivacyController: UIViewController, UIWebViewDelegate {
    
    var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.tintColor = .white
        navigationItem.title = "Chính sách bảo mật"
        webView = UIWebView(frame: UIScreen.main.bounds)
        webView.delegate = self
        view.addSubview(webView)
        if let url = URL(string: Constant.URL_PRIVACY) {
            let request = URLRequest(url: url)
            webView.loadRequest(request)
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        let backButton = UIBarButtonItem()
        backButton.title = " "
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        
    }
}
