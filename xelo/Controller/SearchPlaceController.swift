//
//  SearchPlaceController.swift
//  xelo
//
//  Created by Nguyen Thieu on 8/10/18.
//  Copyright © 2018 DUMV. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class SearchPlaceController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {

    

    @IBOutlet weak var vSearchBar: UISearchBar!
    @IBOutlet weak var tblPlace: UITableView!
    var inSearchMode = false
    var dataArray = [GooglePlace]()
    static var dataArrayStart = [GooglePlace]()
    static var dataArrayEnd = [GooglePlace]()
    var filteredData = [String]()
    var addressLocation = ""
    static var searchStart:String = ""
    static var searchEnd:String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        //getGooglePlace(googlePlace: "75")
        vSearchBar.delegate = self
        vSearchBar.returnKeyType = UIReturnKeyType.done
        tblPlace.delegate = self
        tblPlace.dataSource = self

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        view.endEditing(true)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if SearchPlaceController.searchStart != "" && getLocation.choice == 1 {
            vSearchBar.text = SearchPlaceController.searchStart
            inSearchMode = true
            self.dataArray = SearchPlaceController.dataArrayStart
            self.tblPlace.reloadData()
            //getGooglePlace(googlePlace: SearchPlaceController.searchStart)
            //view.becomeFirstResponder()
        }else if SearchPlaceController.searchEnd != "" && getLocation.choice == 2 {
            vSearchBar.text = SearchPlaceController.searchEnd
            inSearchMode = true
            self.dataArray = SearchPlaceController.dataArrayEnd
            self.tblPlace.reloadData()
            //getGooglePlace(googlePlace: SearchPlaceController.searchEnd)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getGooglePlace(googlePlace:String)  {
        let CompletionHandler: ((Array<GooglePlace>,NavigoError?) -> Void) = {
            listPlace,navigoError in
            
            DispatchQueue.main.async {
                if(navigoError == nil){
                    self.dataArray = listPlace
                    self.tblPlace.reloadData()
                    if getLocation.choice == 1 {
                        SearchPlaceController.dataArrayStart = listPlace
                    }else {
                        SearchPlaceController.dataArrayEnd = listPlace
                    }
                }else{
                    if(navigoError?.errorCode == Errorcode.ERROR_NO_INTERNET){
                        
                    }else if (navigoError?.errorCode == Errorcode.NAVIGO_SESSION_EXPIRED){
                        self.reconect()
                    }else{
                        
                    }
                }
            }
        }
        let apiAddress = APIAddress();
        apiAddress.getListPlace(place: googlePlace, CompletionHandler: CompletionHandler)
    }

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text == nil || searchBar.text == "" {          
            inSearchMode = false
            //view.endEditing(true)
            tblPlace.reloadData()
            
        } else {
            inSearchMode = true
            getGooglePlace(googlePlace: searchBar.text!)
            //tblPlace.reloadData()
        }
        if getLocation.choice == 1 {
            SearchPlaceController.searchStart = searchBar.text!
        }else {
            SearchPlaceController.searchEnd = searchBar.text!
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if inSearchMode {
            return dataArray.count
        }
        return DataNavigo.arrHistories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PlaceCell", for: indexPath) as! PlaceCell
        if inSearchMode {
            let data = dataArray[indexPath.row]
            cell.imgPlace.image = #imageLiteral(resourceName: "ic_location")
            cell.lblPlace.text = data.mainText
            cell.lbldescription.text = data.description
            cell.setID(id: data.placeID!)
        }else{
            let his:Histories = DataNavigo.arrHistories[indexPath.row]
            cell.imgPlace.image = #imageLiteral(resourceName: "ic_location")
            cell.lblPlace.text = his.fullAddress
            cell.lbldescription.text = his.fullAddress
            cell.histori = DataNavigo.arrHistories[indexPath.row]
            cell.setID(id: "histories")
        }
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! PlaceCell
        if inSearchMode {
            let data = dataArray[indexPath.row]
            getPlaceDetail(placeID: data.placeID!)
            addressLocation = data.description!
        }   else{
            let his:Histories = DataNavigo.arrHistories[indexPath.row]
            addressLocation = (cell.lblPlace.text)!
            updateLocation(lat:his.y!, lng:his.x!)
        }
        
    }
    
    func getPlaceDetail(placeID:String){
        let CompletionHandler: ((Double, Double,NavigoError?) -> Void) = {
           lat, lon, navigoError in
            DispatchQueue.main.async {
                if(navigoError == nil){
                    self.updateLocation(lat: lat, lng: lon)
                }else{
                    if(navigoError?.errorCode == Errorcode.ERROR_NO_INTERNET){
                        
                    }
                }
            }
        }
        let apiAddress = APIAddress();
        apiAddress.getPlaceInfo(place: placeID, CompletionHandler: CompletionHandler)
    }
    
    func updateLocation(lat:Double , lng:Double) {
        if getLocation.choice == 1 {
            DataNavigo.booking.fromPosition?.y = lat
            DataNavigo.booking.fromPosition?.x = lng
            DataNavigo.booking.fromAddress = addressLocation
        }
        else if getLocation.choice == 2{
            DataNavigo.booking.toPosition?.y = lat
            DataNavigo.booking.toPosition?.x = lng
            DataNavigo.booking.toAddress = addressLocation
        }
        else if getLocation.choice == 3{
            BusBookingController.fromLocation.y = lat
            BusBookingController.fromLocation.x = lng
            BusBookingController.fromAddress = addressLocation
        }
        else if getLocation.choice == 4{
            BusBookingController.toLocation.y = lat
            BusBookingController.toLocation.x = lng
            BusBookingController.toAddress = addressLocation
        }
        DataNavigo.checkDrawPath = true
        DataNavigo.booking.estDistance = 0
        navigationController?.popViewController(animated: true)
//        let viewControllers:[UIViewController] = (self.navigationController?.viewControllers)!;
//        for controller in viewControllers {
//            if controller is MapController {
//                self.navigationController!.popToViewController(controller, animated: true)
//            }
//        }
        // print(lat)
        // print(lng)
        
    }
    
    func createaddress(address:Address)  {
        let CompletionHandler: ((NavigoError?) -> Void) = {
            navigoError in
            
            DispatchQueue.main.async {
                if(navigoError == nil){
                    
                }else{
                    if(navigoError?.errorCode == Errorcode.ERROR_NO_INTERNET){
                        
                    }
                    else{
                        
                    }
                }
            }
        }
        let apiAddress = APIAddress();
        apiAddress.createAddress(address: address,CompletionHandler: CompletionHandler)
    }
    
    func getLocationCreateAddress(id:String,name :String){
        
        DispatchQueue.global(qos: .userInitiated).async {
            let url1="https://maps.googleapis.com/maps/api/place/details/json?placeid="+id+"&key=AIzaSyBXZKFWRkjJRcf7PQO9OM9mj9SCUkft24s"
            let myUrl = URL(string: url1);
            var request = URLRequest(url:myUrl!)
            request.httpMethod="POST"
            
            let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
                
                if let data = data,
                    let   html = String(data: data, encoding: String.Encoding.utf8) {
                    do {
                        let json1 = JSON(parseJSON: html)
                        DispatchQueue.main.async {
                            let address = Address()
                            address.y=json1["result"]["geometry"]["location"]["lat"].double
                            address.x=json1["result"]["geometry"]["location"]["lng"].double
                            address.fullAddress = json1["result"]["formatted_address"].stringValue
                            address.fullName = name
                            self.createaddress(address: address)
                        }
                        
                    }
                }
            }
            task.resume()
        }
    }
    
    ///login
    func reconect() {
        let CompletionHandler: ((LoginResult,NavigoError?) -> Void) = {
            loginResult,navigoError in
            DispatchQueue.main.async {
                
            }
        }
        let connector = PassengerConnector();
        connector.loginAccount(passenger: DataNavigo.passenger, reconnect: true, CompletionHandler: CompletionHandler)
    }

}
extension SearchPlaceController : PointDelegate {
    func didAddPoint(dataID: String, history: Histories) {
        let alert:UIAlertController = UIAlertController(title: "Tạo điểm yêu thích", message: "", preferredStyle: UIAlertControllerStyle.alert)
        alert.addTextField { (txtPoint) in
            txtPoint.placeholder = "Tên điểm "
            txtPoint.keyboardType = .default
        }
        let btnOk:UIAlertAction = UIAlertAction(title: "Xác nhận", style: .destructive) { (btnOk) in
            let namePoint = (alert.textFields![0].text)!
            if(dataID == "histories"){
                let address = Address()
                address.y=history.y
                address.x=history.x
                address.fullAddress = history.fullAddress
                address.fullName = namePoint
                self.createaddress(address: address)
            }else{
                self.getLocationCreateAddress(id: dataID, name: namePoint)
            }
            
        }
        let btnCancel:UIAlertAction = UIAlertAction(title: "Huỷ", style: .destructive) { (btnCancel) in
            
        }
        alert.addAction(btnOk)
        alert.addAction(btnCancel)
        present(alert, animated: true, completion: nil)
    }
    
}
