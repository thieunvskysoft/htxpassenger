//
//  SearchDriverController.swift
//  NaviGo
//
//  Created by HoangManh on 9/25/17.
//  Copyright © 2017 DUMV. All rights reserved.
//

import UIKit

class SearchDriverController: UIViewController {
    
    static let instance = SearchDriverController()
    
    @IBOutlet weak var vDetailBook: UIView!
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblTwoWay: UILabel!
    @IBOutlet weak var lblPromotionValue: UILabel!
    @IBOutlet weak var lblKhuyenmai: UILabel!
    @IBOutlet weak var DistanceLabel: UILabel!
    @IBOutlet weak var WayLabel: UILabel!
    @IBOutlet weak var PriceLabel: UILabel!
    @IBOutlet weak var lblpeakHour: UILabel!
    @IBOutlet weak var excludingTolls: UILabel!
    
    
    @IBOutlet weak var lblBlockRoad: UILabel!
    var TOTAL_CHARGE:String = "Cước: "
    var EXPECTED_CHARGE:String = "Cước dự kiến: "
    var MINUTE:String = " phút"
    var SLIDE_TO_CANCEL:String = "Trượt để huỷ chuyến"
    var DISTANCE_:String = "Quãng đường: "
    var PROMOTION:String = "Khuyến mại: "
    var ONE_WAY_TITLE:String = "Cuốc 1 chiều"
    var TWO_WAY_TITLE:String = "Cuốc 2 chiều"
    var NOTIFICATION_TITLE:String = "Thông báo"
    var CONFIRM:String = "Xác nhận"
    var CANCEL:String = "Huỷ"
    var FINDING_DRIVER:String = "Đang tìm lái xe..."
    var EXCLUDING_TOLLS:String = "Chưa bao gồm phí cầu đường"
    var PEAK_HOUR:String = ""
    var timer:Timer? = nil
    var check:Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        DataNavigo.idUIController = 4
        check = 0
        DataNavigo.driverInfo = DriverInfo()
        self.btnSlideCancel.delegate = self
        DataNavigo.animationDelegate = self
        //view.backgroundColor = #colorLiteral(red: 0.4745098054, green: 0.8392156959, blue: 0.9764705896, alpha: 1)
        self.navigationController?.isNavigationBarHidden = true
        let navBarHeight = (self.navigationController?.navigationBar.intrinsicContentSize.height)! + UIApplication.shared.statusBarFrame.height
        self.view.addSubview(distance)
        distance.frame = CGRect(x: 10, y: navBarHeight + 10, width: view.frame.width - 20 , height: 80)
        view.addSubview(lblMessage)
        lblMessage.frame = CGRect(x: 10, y: view.frame.height / 2 + 5, width: 100, height: 25)
        lblMessage.sizeToFit()
        lblMessage.center.x = self.view.center.x
        view.addSubview(indicatorView)
        indicatorView.frame = CGRect(x: 100, y: view.frame.height / 2 + 40, width: 80, height: 80)
        indicatorView.center.x = self.view.center.x
        
        view.addSubview(lblPlateNumber)
        lblPlateNumber.frame = CGRect(x: 10, y: view.frame.height / 2 + 140, width: 100, height: 20)
        //lblPlateNumber.sizeToFit()
        lblPlateNumber.center.x = self.view.center.x
        lblBlockRoad.text = ""

        indicatorView.startAnimating()
        view.addSubview(btnSlideCancel)
        btnSlideCancel.frame = CGRect(x: 20, y: view.frame.size.height - 90, width: view.frame.size.width - 40, height: 50)
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(countDownTimer), userInfo: nil, repeats: true)
    }
    
    func loadViewLanguage() {
        let curren_language = UserDefaults.standard.string(forKey: "curren_language")
        let language = LanguageData()
        TOTAL_CHARGE = language.localizedString(forKey:curren_language!,forKey: "TOTAL_CHARGE")
        EXPECTED_CHARGE = language.localizedString(forKey:curren_language!,forKey: "EXPECTED_CHARGE")
        MINUTE = language.localizedString(forKey:curren_language!,forKey: "MINUTE")
        SLIDE_TO_CANCEL = language.localizedString(forKey:curren_language!,forKey: "SLIDE_TO_CANCEL")
        DISTANCE_ = language.localizedString(forKey:curren_language!,forKey: "DISTANCE_")
        ONE_WAY_TITLE = language.localizedString(forKey:curren_language!,forKey: "ONE_WAY_TITLE")
        TWO_WAY_TITLE = language.localizedString(forKey:curren_language!,forKey: "TWO_WAY_TITLE")
        PROMOTION = language.localizedString(forKey:curren_language!,forKey: "PROMOTION")
        NOTIFICATION_TITLE = language.localizedString(forKey:curren_language!,forKey: "NOTIFICATION_TITLE")
        CONFIRM = language.localizedString(forKey:curren_language!,forKey: "CONFIRM")
        CANCEL = language.localizedString(forKey:curren_language!,forKey: "CANCEL")
        FINDING_DRIVER = language.localizedString(forKey:curren_language!,forKey: "FINDING_DRIVER")
        EXCLUDING_TOLLS = language.localizedString(forKey:curren_language!,forKey: "EXCLUDING_TOLLS")
        PEAK_HOUR = language.localizedString(forKey:curren_language!,forKey: "PEAK_HOUR")
    }

    override func viewWillAppear(_ animated: Bool) {
        print("viewWillAppear")
        //DataNavigo.handingScreen = self
        indicatorView.startAnimating()
        loadViewLanguage()
        if(DataNavigo.booking.twoWays)
        {
            lblTwoWay.text = "Cuốc hai chiều(chờ " + (DataNavigo.booking.waitDuration?.description)! + " phút)"
            
        }else{
            lblTwoWay.text = ONE_WAY_TITLE
        }
        DistanceLabel.text = DISTANCE_
        PriceLabel.text = EXPECTED_CHARGE
        lblpeakHour.text = PEAK_HOUR
        
        lblDistance.text = String(format: "%.1f", ((DataNavigo.booking.estDistance)!/1000)) + " Km"
        let promotionValue = DataNavigo.booking.promotionValue
        if (promotionValue != 0 && promotionValue != nil){
            lblKhuyenmai.text = PROMOTION
            lblPromotionValue.text = (promotionValue?.description)! + " Đ"
        }
        if (DataNavigo.booking.plateNumber != "") {
            print("DataNavigo.booking.plateNumber", DataNavigo.booking.plateNumber?.count ?? "1")
            //lblPlateNumber.text = (DataNavigo.booking.plateNumber?.substring(to: (DataNavigo.booking.plateNumber?.characters.count)! - 2))! + "xx"
        }
//        if !DataNavigo.booking.peakHour! {
//            lblpeakHour.alpha = 0
//        }
        lblMessage.text = FINDING_DRIVER
        btnSlideCancel.buttonText = SLIDE_TO_CANCEL
        excludingTolls.text = EXCLUDING_TOLLS
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        //Về map-----
        if timer != nil {
            timer?.invalidate()
            timer = nil
        }
    }

    func reStartAnimating() {
        indicatorView.startAnimating()
    }
    
    let btnSlideCancel: MMSlidingButton = {
        let button = MMSlidingButton()
        button.buttonText = "Trượt để huỷ chuyến"
        button.dragPointWidth = 50
        button.buttonDragPoint = true
        button.dragPointColor = NaviColor.naviBtnOrange
        button.imageName = #imageLiteral(resourceName: "Arrorw")
        button.buttonColor = UIColor.gray
        return button
    }()
    
    let indicatorView: InstagramActivityIndicator = {
        let indicator = InstagramActivityIndicator()
        indicator.strokeColor = NaviColor.naviBtnOrange
        indicator.numSegments = 8
        indicator.lineWidth = 5
        
        return indicator
    }()
 
    let distance: DistanceView = {
        let v = DistanceView()
        v.startPoint = #imageLiteral(resourceName: "cur")
        
        v.endPoint   = #imageLiteral(resourceName: "des")
        v.locationStart = DataNavigo.booking.fromAddress != nil ? DataNavigo.booking.fromAddress! :"Unknow"
        v.locationEnd = DataNavigo.booking.toAddress != nil ? DataNavigo.booking.toAddress! :"Unknow"
      //  v.distance = String(Int(ceilf((DataNavigo.booking.estDistance)!/1000))) + " km"
        v.valueTextColor = NaviColor.naviText
       // v.lblTextColor = #colorLiteral(red: 0.9568627477, green: 0.6588235497, blue: 0.5450980663, alpha: 1)
        return v
    }()
    
    let lblMessage:UILabel = {
        let lbl = UILabel()
        lbl.text = "Đang tìm lái xe..."
        lbl.textColor = #colorLiteral(red: 0.9411764741, green: 0.4980392158, blue: 0.3529411852, alpha: 1)
        lbl.font = UIFont.systemFont(ofSize: 20)
        return lbl
    }()
    let lblPlateNumber:UILabel = {
        let lbl = UILabel()
        lbl.textColor = UIColor.gray
        lbl.font = UIFont.systemFont(ofSize: 16)
        return lbl
    }()
    
    
    func getPrice(service:Int) -> String {
        let point = DataNavigo.arrService[service]
        var price:String = ""
        if MapController.locationSucsess {
            if (DataNavigo.servicePrice.count != 0){
                let priceMin = formatterPrice(price: Int(ceilf(Float(DataNavigo.servicePrice[service].fromPrice!)/1000))*1000)
                let priceMax = formatterPrice(price: Int(ceilf(Float(DataNavigo.servicePrice[service].toPrice!)/1000))*1000)
                let priceBook = Int(ceilf(Float(DataNavigo.servicePrice[service].price!)/1000))*1000
                price = priceMin + " - " + priceMax + " VNĐ"
                if(priceBook > 0){
                    price = formatterPrice(price: priceBook) + " VNĐ"
                }
            }else{
                let priceMin = formatterPrice(price:Int(ceilf(Float(DataNavigo.booking.fromCharge!)/1000))*1000)
                let priceMax = formatterPrice(price:Int(ceilf(Float(DataNavigo.booking.toCharge!)/1000))*1000)
                //let priceBook = Int(ceilf(Float(DataNavigo.servicePrice[service].price!/1000)))
                price = priceMin + " - " + priceMax + " VNĐ"
//                if(priceBook > 0){
//                    price = priceBook.description + "K"
//                }
            }

        }else{
            let priceMin = String(Int(ceilf(Float(point.fromPrice!))/1000))
            let priceMax = String(Int(ceilf(Float(point.toPrice!))/1000))
            price = priceMin + " - " + priceMax + "K/Km"
            if(priceMin.elementsEqual(priceMax)){
                price = priceMin + "K/Km"
            }
        }
        return price
    }
    
    func priceTotal(price:Int, service:Int) -> Int {
        var actCharge = (Int(price * Int(DataNavigo.booking.estDistance!))/1000)
        if(DataNavigo.booking.twoWays) {
            let ser:Service = DataNavigo.arrService[service]
            
            if(ser != nil) {
                if(ser.returnRate != 0) {
                    actCharge += ((actCharge*ser.returnRate!)/100);
                }
                //wait charge
                if(ser.waitFee != 0 && DataNavigo.booking.waitDuration != 0) {
                    let waitCharge = (DataNavigo.booking.waitDuration! * ser.waitFee!)/60;
                    actCharge += waitCharge
                }
            }
        }
        return actCharge
    }
    
    @objc func countDownTimer() {
        print("DataNavigo.booking.peakHour",DataNavigo.booking.peakHour ?? "")
        if(DataNavigo.booking.state == BookingState.STATE_ACCEPT_BY_DRIVER){
            if (DataNavigo.driverInfo.fullName == nil && check == 0){
                print("Lấy thông tin lái xe...")
                showAlertPath()
                getdriverInfor()
            }else{
                print("Lấy xong thông tin lái xe")
                popToMap()
            }

        }else if(DataNavigo.booking.state == BookingState.STATE_NEW || DataNavigo.booking.state == BookingState.STATE_NONE){
            
        }else if(DataNavigo.booking.state == nil ){
            popToMap()
        }else{
            let viewControllers:[UIViewController] = (self.navigationController?.viewControllers)!;
            for controller in viewControllers {
                if controller is MapController {
                    self.navigationController!.popToViewController(controller, animated: true)
                }
            }
        }
        if DataNavigo.booking.fixCharge {
            let price = Int(ceilf(Float(DataNavigo.booking.estCharge!/1000)))*1000
            lblPrice.text = formatterPrice(price: price) + " VNĐ"
            PriceLabel.text = TOTAL_CHARGE
        }else{
            lblPrice.text = getPrice(service: DataNavigo.serviceCar)
        }
        
        if DataNavigo.booking.peakHour! {
            lblpeakHour.alpha = 1
        }else{
            lblpeakHour.alpha = 0
        }
        let promotionValue = DataNavigo.booking.promotionValue
        if (promotionValue != 0 && promotionValue != nil){
            lblKhuyenmai.text = PROMOTION
            lblPromotionValue.text = (promotionValue?.description)! + " Đ"
        }
        if (DataNavigo.booking.plateNumber != "" && DataNavigo.booking.plateNumber != nil) {
            let plateCount = DataNavigo.booking.plateNumber?.count.hashValue
            lblPlateNumber.text = (DataNavigo.booking.plateNumber?.substring(to: plateCount! - 2))! + "xx"
        }
        if DataNavigo.booking.blockRoad
        {
            lblBlockRoad.text = "Điểm đầu/Điểm cuối của bạn đang nằm trong khung giờ cấm xe, việc đặt xe có thể khó khăn hơn."
        }else if DataNavigo.booking.peakHour! {
            
            lblBlockRoad.text = "Khung giờ cao điểm, đêm muộn, việc đặt xe có thể khó khăn hơn."
        }else {
            lblBlockRoad.text = ""
        }
    }
    
    //getDriverInfo------------------
    func getdriverInfor()  {
        print("get---driver")
        let CompletionHandler: ((DriverInfo,NavigoError?) -> Void) = {
            driverinfo,navigoError in
            DispatchQueue.main.async {
                if(navigoError == nil){
                    DataNavigo.driverInfo = driverinfo
                    MapController.showAlert = true
                    DataNavigo.checkDrawPath = true
                    self.alertPath.dismiss(animated: true, completion: nil)
                }else{
                    if(navigoError?.errorCode == Errorcode.ERROR_NO_INTERNET){
                        self.errorNoInternet()
                    }
                    else{
                        self.errorServerSend(errorCode: (navigoError?.errorCode)!, errorToast: (navigoError?.errorMessage)!)
                    }
                    DataNavigo.driverInfo = DriverInfo()
                }
            }
        }
        let connector = APIBook();
        connector.getDriverInfo(book: DataNavigo.booking, CompletionHandler: CompletionHandler)
    }
    
    func cancelBooking()  {
        DataNavigo.runCheck = false
        DataNavigo.update = false
        let CompletionHandler: ((Book,NavigoError?) -> Void) = {
            booking,navigoError in
            DispatchQueue.main.async {
                if(navigoError == nil){
                    DataNavigo.booking.state = nil
                    DataNavigo.booking.bookingId = nil
                    self.popToMap()
                }else{
                    if(navigoError?.errorCode == Errorcode.ERROR_NO_INTERNET){
                        self.errorNoInternet()
                    }
                    else{
                        self.errorServerSend(errorCode: (navigoError?.errorCode)!, errorToast: Errorcode.ERROR_MESSAGE)
                    }
                }
            }
        }
        let connector = APIBook();
        connector.cancelBooking(book: DataNavigo.booking, cancelCode: "", CompletionHandler: CompletionHandler)
    }
    
    
    func popToMap() {
        //DataNavigo.checkDrawPath = true
        print("self.navigationController?.viewControllers",self.navigationController?.viewControllers ?? "")
        //alertPath.dismiss(animated: true, completion: nil)
        let viewControllers:[UIViewController] = (self.navigationController?.viewControllers)!
        for controller in viewControllers {
            print("controller",controller)
            if controller is MapController {
                print("MapController---")
                sleep(1)
                self.navigationController!.popToViewController(controller as! MapController, animated: true)
                print("MapController222")
            }
        }
    }
    
    var alertPath = UIAlertController()
    
    func showAlertPath() {
        alertPath = UIAlertController(title: nil, message: "Đang lấy thông tin tài xế...\n\n", preferredStyle: .alert)
        let spinnerIndicator = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        spinnerIndicator.center = CGPoint(x: 135.0, y: 65.5)
        spinnerIndicator.color = UIColor.black
        spinnerIndicator.startAnimating()
        check = 1
        alertPath.view.addSubview(spinnerIndicator)
        self.present(alertPath, animated: false, completion: nil)
    }
    
    func formatterPrice(price: Int) -> String {
        var str:String = ""
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.groupingSeparator = "."
        str = formatter.string(for: price)!
        return str
    }
}

extension SearchDriverController: HandingScreen {
    func errorConnect() {
        // dang docve thi bi loi
    }
    func errorNoInternet() {
        // khong co internet
        
    }
    func errorServerSend(errorCode: String, errorToast: String) {
//        print("errorCode",errorCode)
//        let alert:UIAlertController = UIAlertController(title: "Đã có lỗi trong khi xử lý", message: "Bạn có muốn thử lại?", preferredStyle: UIAlertControllerStyle.alert)
//
//        let btnOk:UIAlertAction = UIAlertAction(title: "Xác nhận", style: .destructive) { (btnOk) in
//
//        }
//        let btnCancel:UIAlertAction = UIAlertAction(title: "Huỷ", style: .destructive) { (btnCancel) in
//
//        }
//        alert.addAction(btnOk)
//        alert.addAction(btnCancel)
//        present(alert, animated: true, completion: nil)
        
        self.popToMap()
    }
    func startRequest() {
    
    }
    
    func endRequest() {
    }

    
    // huy booking

    /// login lai
    func login() {
        let CompletionHandler: ((LoginResult,NavigoError?) -> Void) = {
            loginResult,navigoError in
            DispatchQueue.main.async {
                if(navigoError == nil){
                    //self.endRequest()
                }else{
                    if(navigoError?.errorCode == Errorcode.ERROR_NO_INTERNET){
                        self.errorNoInternet()
                    }
                    else{
                        self.errorServerSend(errorCode: (navigoError?.errorCode)!, errorToast: Errorcode.ERROR_MESSAGE)
                    }
                    
                }
            }
        }
        let connector = PassengerConnector();
        connector.reconnect(CompletionHandler: CompletionHandler)
    }
    
}

extension SearchDriverController: SlideButtonDelegate {

    func buttonStatus(status: String, sender: MMSlidingButton) {
        if (status == "Success") {
            cancelBooking()
          
            
        }
    }
}

extension SearchDriverController: AnimationDelegate{
    func Timeout() {
        self.cancelBooking()
    }
    
    func AnimationSuccess() {
        self.reStartAnimating()
    }
    
    func AnimationFailed() {
        
    }
    
    

}

