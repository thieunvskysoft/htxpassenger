//
//  ListServiceController.swift
//  NaviGo
//
//  Created by HoangManh on 9/28/17.
//  Copyright © 2017 DUMV. All rights reserved.
//

import UIKit

class ListServiceController: UIViewController {

    @IBOutlet weak var btnDropdown: UIButton!
    @IBOutlet weak var tbService: UITableView!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var viewTbl: UIView!
    @IBOutlet weak var heightView: NSLayoutConstraint!
    @IBOutlet weak var heightTbl: NSLayoutConstraint!
    @IBOutlet weak var bottomConst: NSLayoutConstraint!
    
    @IBOutlet weak var btnClose: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        //tbService.separatorStyle = UITableViewCellSeparatorStyle.none
        if(DataNavigo.deviceModel == Constant.deviceModelX){
            bottomConst.constant = 100
            btnDropdown.frame = CGRect(x: 100, y: view.frame.height - 95, width: view.frame.width - 20, height: 50)
        }else{
            bottomConst.constant = 70
            btnDropdown.frame = CGRect(x: 100, y: view.frame.height - 65, width: view.frame.width - 20, height: 50)
        }
        btnDropdown.center.x = view.center.x
        viewTbl.layer.cornerRadius = 5
       // viewTbl.widthAnchor.constraint(equalTo: btnDropdown.widthAnchor, multiplier: 1.0).isActive = true
        btnDropdown.layer.cornerRadius = 5
    }
    override func viewWillAppear(_ animated: Bool) {
        var height = DataNavigo.arrService.count * 54 + 5
        if(DataNavigo.deviceModel == Constant.deviceModelXSM || DataNavigo.deviceModel == Constant.deviceModelXR){

                heightTbl.constant = CGFloat(height)
                tbService.layoutIfNeeded()
                heightView.constant = CGFloat(height)
                viewTbl.layoutIfNeeded()
                tbService.isScrollEnabled = false
        }else{
            if height > 560 //ip678
            {
                height = 560
                heightTbl.constant = CGFloat(height)
                tbService.layoutIfNeeded()
                heightView.constant = CGFloat(height)
                viewTbl.layoutIfNeeded()
            }else{
                heightTbl.constant = CGFloat(height)
                tbService.layoutIfNeeded()
                heightView.constant = CGFloat(height)
                viewTbl.layoutIfNeeded()
                tbService.isScrollEnabled = false
            }
        }
        //tbService.isScrollEnabled = false
    }
    @IBAction func dismissPopup(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }
    
    func getPrice(service:Int) -> String {
        let point = DataNavigo.arrService[service]
        var price:String = ""
        if MapController.locationSucsess {
            let priceMin = (Double(DataNavigo.servicePrice[service].fromPrice!/1000)).formatToDouble("")
            let priceMax = (Double(DataNavigo.servicePrice[service].toPrice!/1000)).formatToDouble("")
            let priceBook = Int(ceilf(Float(DataNavigo.servicePrice[service].price!/1000)))
            price = priceMin + " - " + priceMax + "K"
            if(priceBook > 0){
                price = (Double(DataNavigo.servicePrice[service].price!/1000)).formatToDouble("") + "K"
            }
        }else{
//            let priceMin = (Double(point.fromPrice!)/1000).formatToDouble("")
//            let priceMax = (Double(point.toPrice!)/1000).formatToDouble("")
//            print(priceMin,priceMax)
//            price = priceMin + " - " + priceMax + "K/Km"
//            if(priceMin.elementsEqual(priceMax)){
//                price = priceMin + "K/Km"
//            }
        }
        return price
    }
    
    func getPriceNormal(service:Int) -> String {
        let point = DataNavigo.arrService[service]
        var price:String = ""
        if MapController.locationSucsess {
            let priceMin = (Double(DataNavigo.servicePrice[service].fromNormalPrice!/1000)).formatToDouble("")
            let priceMax = (Double(DataNavigo.servicePrice[service].toNormalPrice!/1000)).formatToDouble("")
            price = priceMin + " - " + priceMax + "K"
            if(priceMin.elementsEqual(priceMax)){
                price = priceMin + "K"
            }
        }else{
//            let priceMin = (Double(point.fromNormalPrice!)/1000).formatToDouble("")
//            let priceMax = (Double(point.toNormalPrice!)/1000).formatToDouble("")
//            price = priceMin + " - " + priceMax + "K/Km"
//            if(priceMin.elementsEqual(priceMax)){
//                price = priceMin + "K/Km"
//            }
        }
        return price
    }
    
    func priceTotal(price:Int, service:Int) -> Int {
        var actCharge = (Int(price * Int(DataNavigo.booking.estDistance!))/1000)
        if(DataNavigo.booking.twoWays) {
            let ser:Service = DataNavigo.arrService[service]
            
            if(ser != nil) {
                
                if(ser.returnRate != 0) {
                    actCharge += ((actCharge*ser.returnRate!)/100);
                }
                //wait charge
                if(ser.waitFee != 0 && DataNavigo.booking.waitDuration != 0) {
                    let waitCharge = (DataNavigo.booking.waitDuration! * ser.waitFee!)/60;
                    actCharge += waitCharge
                    
                }
            }
        }
        return actCharge
    }
}

extension ListServiceController :UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      //  print("chon cai nay ",indexPath.row)
        DataNavigo.serviceCar = indexPath.row
        DataNavigo.handingScreen?.endRequest()
        DataNavigo.handingScreen?.errorConnect()
        dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataNavigo.arrService.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //let curren_language = UserDefaults.standard.string(forKey: "curren_language")
        //let language = LanguageData()
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyCell", for: indexPath) as! ServiceViewCell
        let point = DataNavigo.arrService[indexPath.row]
        if indexPath.row == DataNavigo.serviceCar {
            cell.serviceViewDetail.backgroundColor = UIColor.groupTableViewBackground
        }
        cell.peakImg.alpha = 0
        cell.imageService.image = point.serviceIcon
        //cell.priceService.text = getPrice(service: indexPath.row)
        cell.nameService.text = point.serviceName //language.localizedString(forKey:curren_language!,forKey: point.serviceCode!)
//        if MapController.locationSucsess {
//            let p = DataNavigo.servicePrice[indexPath.row]
//            let price:Int = p.fromPrice!
//            if p.fromNormalPrice! > price && p.fromNormalPrice! > 0 && p.toNormalPrice! > p.price! {
//                let textRange = NSMakeRange(1, getPriceNormal(service: indexPath.row).count)
//                let attributedText = NSMutableAttributedString(string: "(" + getPriceNormal(service: indexPath.row) + ")")
//                attributedText.addAttributes([ NSAttributedStringKey.strikethroughStyle: NSUnderlineStyle.styleSingle.rawValue,
//                                               NSAttributedStringKey.strikethroughColor:
//                                                NaviColor.navigationOrange], range: textRange)
//                cell.normalPrice.attributedText = attributedText
//            }
//            if p.peakHour {
//                cell.peakImg.alpha = 1
//            }else{
//                cell.peakImg.alpha = 0
//            }
//        }else{
//            let price:Int = point.fromPrice!
//            if point.fromNormalPrice! > price && point.fromNormalPrice! > 0 && point.toNormalPrice! > point.price! {
//                let textRange = NSMakeRange(1, getPriceNormal(service: indexPath.row).count)
//                let attributedText = NSMutableAttributedString(string: "(" + getPriceNormal(service: indexPath.row) + ")")
//                attributedText.addAttributes([ NSAttributedStringKey.strikethroughStyle: NSUnderlineStyle.styleSingle.rawValue,
//                                               NSAttributedStringKey.strikethroughColor:
//                                                NaviColor.navigationOrange], range: textRange)
//                cell.normalPrice.attributedText = attributedText
//            }
//            if point.peakHour {
//                cell.peakImg.alpha = 1
//            }else{
//                cell.peakImg.alpha = 0
//            }
        //}

        return cell
    }
}
extension Double {
    func formatToDouble(_ pattern: String) -> String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        return formatter.string(from: NSNumber(value: self))!
    }
}
