//
//  ShareController.swift
//  xelo
//
//  Created by Nguyen Thieu on 5/24/18.
//  Copyright © 2018 DUMV. All rights reserved.
//

import UIKit

class ShareController: UIViewController,UIWebViewDelegate {

    @IBOutlet weak var txtPhoneShare: FloatLabelTextField!
    @IBOutlet weak var btnSendPhoneShare: UIButton!
    @IBOutlet weak var viewShare: UIView!
    
    @IBOutlet weak var webViewShare: UIWebView!
    var webView: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        let backButton = UIBarButtonItem()
        backButton.title = " "
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        navigationController?.navigationBar.tintColor = .white
        self.navigationController?.navigationBar.barTintColor = NaviColor.navigationOrange
        navigationItem.title = "Chia sẻ & nhận thưởng"
        addDoneButtonOnKeyboard()
        txtPhoneShare.keyboardType = .numberPad
        //webView = UIWebView(frame: viewShare.frame)
        //webView.delegate = self
        //viewShare.addSubview(webView)
        
        if let url = URL(string: Constant.URL_SHARE_AND_BONUS + "?" + keyJson.F_TOKEN_ID + "="+DataNavigo.loginResult.tokenID)//"?accountID="+DataNavigo.passenger.acountID)
        {
            print("url shrae", url)
            let request = URLRequest(url: url)
            webViewShare.loadRequest(request)
        }
        btnSendPhoneShare.addTarget(self, action: #selector(inputForBonus), for: .touchUpInside)
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.barTintColor = NaviColor.naviBtnOrange
    }
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 45))
        doneToolbar.barStyle = UIBarStyle.default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(ShareController.doneButtonAction))
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        
        doneToolbar.items = items as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        
        self.txtPhoneShare.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction()
    {
        self.txtPhoneShare.resignFirstResponder()
    }
    
    
    @objc func inputForBonus() {
        print("Xử lý dữ liệu")
        sendMobileNo()
    }
    
    func sendMobileNo() {
        let CompletionHandler: ((String,NavigoError?) -> Void) = {status,navigoError in
            DispatchQueue.main.async {
                if(navigoError == nil){
                    print("sttuc",status)
                    self.webViewShare.reload()
                    self.showMessage(message: "Hệ thống đã ghi nhận thành công")
                }else{
                    print("error",navigoError?.errorMessage ?? "")
                    self.showMessage(message: (navigoError?.errorMessage)!)
                }
            }
        }
        let sendBonus = APIForBonus();
        sendBonus.inputForBonus(mobileNo: txtPhoneShare.text!, CompletionHandler: CompletionHandler)
    }
    
    
    func showMessage(message:String) {
        let alert:UIAlertController = UIAlertController(title: "Thông báo", message: message, preferredStyle: UIAlertControllerStyle.alert)
        let btnOk:UIAlertAction = UIAlertAction(title: "Xác nhận", style: .destructive) { (btnOk) in
            self.txtPhoneShare.text = ""
        }
        alert.addAction(btnOk)
        present(alert, animated: true, completion: nil)
    }

}
