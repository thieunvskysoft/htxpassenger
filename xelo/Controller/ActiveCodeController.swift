//
//  ActiveCodeController.swift
//  NaviGo
//
//  Created by HoangManh on 9/20/17.
//  Copyright © 2017 DUMV. All rights reserved.
//

import UIKit

class ActiveCodeController: UIViewController,HandingScreen {
    
    @IBOutlet weak var scrollActive: UIScrollView!
    @IBOutlet weak var contentActive: UIView!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    var status:Int? = 1
    var codeActive = ""
    var codeInputView:CodeInputView!
    override func viewDidLoad() {
        super.viewDidLoad()
        startRequest()
        contentActive.addSubview(lblTitle)
        lblTitle.frame = CGRect(x: 10, y: 50, width: 250, height: 40)
        lblTitle.sizeToFit()
        lblTitle.center.x = self.view.center.x
        contentActive.addSubview(lblMessage)
        lblMessage.frame = CGRect(x: 10, y: 90, width: 300, height: 60)
        lblMessage.sizeToFit()
        lblMessage.center.x = self.view.center.x
        addDoneButtonOnKeyboard()
        let frame = CGRect(x: (view.frame.width-215)/2, y: 150, width: 215, height: 50)
        codeInputView = CodeInputView(frame: frame)
        codeInputView.delegate = self
        contentActive.addSubview(codeInputView)
        codeInputView.keyboardType = .numberPad
        
        codeInputView.becomeFirstResponder()
        codeInputView.center.x = self.view.center.x
        contentActive.addSubview(buttonActive)
        buttonActive.frame = CGRect(x: 16, y: 200, width: view.frame.width - 32, height: 50)
        contentActive.addSubview(buttonReActive)
        buttonReActive.frame = CGRect(x: 16, y: 270, width: view.frame.width - 32, height: 50)
        buttonActive.addTarget(self, action: #selector(activecode), for: .touchUpInside)
        buttonReActive.addTarget(self, action: #selector(reActivecode), for: .touchUpInside)
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(ActiveCodeController.keyboardDidShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ActiveCodeController.keyboardWillBeHidden(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        startTimer()
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.tapDetected(sender:)))
        self.view.addGestureRecognizer(tapGestureRecognizer)
    }
    @objc func tapDetected(sender: UITapGestureRecognizer) {
        codeInputView.becomeFirstResponder()

    }
    @objc func keyboardDidShow(_ notification: NSNotification) {
        let keyBoard = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as! NSValue
        let sizeKey = keyBoard.cgRectValue
        let heightKey = sizeKey.height
        let kc = view.frame.height - (codeInputView.frame.origin.y + heightKey + 150)
        if kc < 0 {
            let a = kc * -1 + 30
            bottomConstraint.constant = a
            let point:CGPoint = CGPoint(x: 0, y: a)
            scrollActive.setContentOffset(point, animated: true)
        }
        
    }
    
    
    @objc func keyboardWillBeHidden(_ notification: NSNotification) {
        self.bottomConstraint.constant = 0
        UIView.animate(withDuration: 1) {
            self.scrollActive.layoutSubviews()
        }
    }
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 45))
        doneToolbar.barStyle = UIBarStyle.default
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(ActiveCodeController.doneButtonAction))
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        doneToolbar.items = items as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        
        //self.codeInputView.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction()
    {
        self.codeInputView.resignFirstResponder()
    }
    
    @objc func activecode() {
        DataNavigo.handingScreen = self
        DataNavigo.activeKey = codeActive
        if codeActive.count < 4 {
            let alert:UIAlertController = UIAlertController(title: "Thông báo", message: "Bạn vui lòng nhập đầy đủ mã kích hoạt", preferredStyle: UIAlertControllerStyle.alert)
            let btnOk:UIAlertAction = UIAlertAction(title: "Xác nhận", style: .destructive) { (btnOk) in
            }
            alert.addAction(btnOk)
            present(alert, animated: true, completion: nil)
        }else{
            buttonActive.backgroundColor = UIColor.gray
            buttonActive.isEnabled = false
            status = 1
            active()
            //show//()
        }
    }
    
    @objc func reActivecode() {
        startTimer()
        status = 2
        reactive()
        buttonActive.backgroundColor = NaviColor.naviBtnOrange
        buttonActive.isEnabled = true
        buttonReActive.backgroundColor = UIColor.gray
        buttonReActive.isEnabled = false
    }
    
    let buttonActive: DesignButton = {
        let btn = DesignButton()
        btn.backgroundColor = NaviColor.naviBtnOrange
        btn.setTitle("Kích hoạt",for: .normal)
        btn.rounded = true
        btn.borderWidth = 0.6
        return btn
    }()
    let buttonReActive: DesignButton = {
        let btn = DesignButton()
        btn.backgroundColor = UIColor.gray
        btn.isEnabled = false
        btn.setTitle("Gửi lại mã",for: .normal)
        btn.rounded = true
        btn.borderWidth = 0.6
        return btn
    }()
    
    let lblTitle: UILabel = {
        let label = UILabel()
        label.text = "KÍCH HOẠT TÀI KHOẢN"
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.textColor = NaviColor.naviLabel
        return label
        
    }()
    let lblMessage: UILabel = {
        let label = UILabel()
        label.text = "Mã kích hoạt đã được gửi vào tin nhắn SMS,\n xin vui lòng kiểm tra tin nhắn"
        label.textAlignment = .center
        label.numberOfLines = 2
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = NaviColor.naviLabel
        return label
        
    }()
    var countdownTimer: Timer!
    var totalTime = 30
    func startTimer() {
        totalTime = 30
        countdownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
        buttonReActive.backgroundColor = UIColor.gray
        buttonReActive.isEnabled = false
    }
    
    @objc func updateTime() {
        if totalTime != 0 {
            totalTime -= 1
        } else {
            endTimer()
        }
    }
    
    func endTimer() {
        countdownTimer.invalidate()
        buttonReActive.backgroundColor = NaviColor.naviBtnOrange
        buttonReActive.isEnabled = true
    }
    
    
    //reactive
    func reactive() {
        let CompletionHandler: ((Passenger,NavigoError?) -> Void) = {
            passenger,navigoError in
            DispatchQueue.main.async {
                DataNavigo.passenger.acountID = passenger.acountID
                DataNavigo.passenger.securekey = passenger.securekey
                if(navigoError == nil){
                    //self.endRequest()
                }else{
                    if(navigoError?.errorCode == Errorcode.ERROR_NO_INTERNET){
                        self.errorNoInternet()
                    }
                    else{
                        self.errorServerSend(errorCode: (navigoError?.errorCode)!, errorToast: (navigoError?.errorMessage)!)
                    }
                    self.buttonActive.backgroundColor = NaviColor.naviBtnOrange
                    self.buttonActive.isEnabled = true
                }
            }
        }
        let connector = PassengerConnector();
        connector.reActiveAccount(passenger: DataNavigo.passenger, CompletionHandler: CompletionHandler)
    }
    
    //active
    func active() {
        let CompletionHandler: ((Passenger,NavigoError?) -> Void) = {
            passenger,navigoError in
            DispatchQueue.main.async {
                DataNavigo.passenger.acountID = passenger.acountID
                DataNavigo.passenger.securekey = passenger.securekey
                if(navigoError == nil){
                    self.endRequest()
                }else{
                    if(navigoError?.errorCode == Errorcode.ERROR_NO_INTERNET){
                        self.errorNoInternet()
                    } else{
                        self.errorServerSend(errorCode: (navigoError?.errorCode)!, errorToast: (navigoError?.errorMessage)!)
                    }
                    self.buttonActive.backgroundColor = NaviColor.naviBtnOrange
                    self.buttonActive.isEnabled = true
                }
            }
        }
        let connector = PassengerConnector();
        connector.activeAccount(passenger: DataNavigo.passenger, CompletionHandler: CompletionHandler, activeKey: codeActive)
    }
    
    func getAuthenKey() {
        let key:String = "ABQjsaA138aA139091ABkshfoqhfldpqng27191nfd"
        let time:String = Int64(Date().timeIntervalSince1970*1000).description
        let anthenkey = DataNavigo.passenger.acountID + "-"+time+"-"+DataNavigo.passenger.deviceSerial+"-"+DataNavigo.passenger.securekey + "-" + key
        let ss = MD5(anthenkey).lowercased()
        DataNavigo.passenger.authenkey = ss
        DataNavigo.passenger.time = time
    }
    
    ///login
    func login() {
        let CompletionHandler: ((LoginResult,NavigoError?) -> Void) = {
            loginResult,navigoError in
            DispatchQueue.main.async {
                DataNavigo.arrService = (loginResult.arrService)
                DataNavigo.booking = (loginResult.booking)
                DataNavigo.loginResult = loginResult
                if(navigoError == nil){
                    self.endRequest()
                }else{
                    if(navigoError?.errorCode == Errorcode.ERROR_NO_INTERNET){
                        self.errorNoInternet()
                    }
                    else{
                        self.errorServerSend(errorCode: (navigoError?.errorCode)!, errorToast: Errorcode.ERROR_MESSAGE)
                    }
                }
            }
        }
        let connector = PassengerConnector();
        connector.loginAccount(passenger: DataNavigo.passenger, reconnect: false, CompletionHandler: CompletionHandler)
    }
    
    func errorConnect() {
        // dang docve thi bi loi
    }
    
    func errorNoInternet() {
        // khong co internet
    }
    
    func errorServerSend(errorCode: String, errorToast: String) {
        if errorCode.range(of: Errorcode.NAVIGO_ACCOUNT_DUPLICATE) != nil{
            let alert:UIAlertController = UIAlertController(title: "Tài khoản đã được đăng ký", message: "Bạn có muốn kích hoạt lại tài khoản này không?", preferredStyle: UIAlertControllerStyle.alert)
            let btnOk:UIAlertAction = UIAlertAction(title: "Kích hoạt", style: .destructive) { (btnOk) in
                self.status = 2
                self.reactive()
            }
            let btnCancel:UIAlertAction = UIAlertAction(title: "Huỷ", style: .destructive) { (btnCancel) in
                
            }
            alert.addAction(btnOk)
            alert.addAction(btnCancel)
            present(alert, animated: true, completion: nil)
        }else if errorCode.range(of: Errorcode.NAVIGO_ACCOUNT_NOT_FOUND) != nil{
            let alert:UIAlertController = UIAlertController(title: "Thông báo", message: errorToast, preferredStyle: UIAlertControllerStyle.alert)
            let btnOk:UIAlertAction = UIAlertAction(title: "Xác nhận", style: .destructive) { (btnOk) in
            }
            alert.addAction(btnOk)
            present(alert, animated: true, completion: nil)
        }else if errorCode.range(of: Errorcode.NAVIGO_ACCOUNT_ACTIVATED) != nil{
            let alert:UIAlertController = UIAlertController(title: "Thông báo", message: errorToast, preferredStyle: UIAlertControllerStyle.alert)
            let btnOk:UIAlertAction = UIAlertAction(title: "Xác nhận", style: .destructive) { (btnOk) in
            }
            alert.addAction(btnOk)
            present(alert, animated: true, completion: nil)
        }else if errorCode.range(of: Errorcode.NAVIGO_ACCOUNT_BLOCKED) != nil{
            let alert:UIAlertController = UIAlertController(title: "Thông báo", message: errorToast, preferredStyle: UIAlertControllerStyle.alert)
            let btnOk:UIAlertAction = UIAlertAction(title: "Xác nhận", style: .destructive) { (btnOk) in
            }
            alert.addAction(btnOk)
            present(alert, animated: true, completion: nil)
        }else if errorCode.range(of: Errorcode.NAVIGO_ACCOUNT_INVALID) != nil{
            let alert:UIAlertController = UIAlertController(title: "Tài khoản đã được đăng ký", message: "Bạn có muốn kích hoạt lại tài khoản này không?", preferredStyle: UIAlertControllerStyle.alert)
            let btnOk:UIAlertAction = UIAlertAction(title: "Kích hoạt", style: .destructive) { (btnOk) in
                self.status = 2
                self.reactive()
            }
            let btnCancel:UIAlertAction = UIAlertAction(title: "Huỷ", style: .destructive) { (btnCancel) in
                
            }
            alert.addAction(btnOk)
            alert.addAction(btnCancel)
            present(alert, animated: true, completion: nil)
        }else if errorCode.range(of: Errorcode.NAVIGO_INVALID_EMAIL) != nil{
            let alert:UIAlertController = UIAlertController(title: "Thông báo", message: errorToast, preferredStyle: UIAlertControllerStyle.alert)
            let btnOk:UIAlertAction = UIAlertAction(title: "Xác nhận", style: .destructive) { (btnOk) in
            }
            alert.addAction(btnOk)
            present(alert, animated: true, completion: nil)
        }else if errorCode.range(of: Errorcode.NAVIGO_SKYMAP_ID_EXISTS) != nil{
            let alert:UIAlertController = UIAlertController(title: "Thông báo", message: errorToast, preferredStyle: UIAlertControllerStyle.alert)
            let btnOk:UIAlertAction = UIAlertAction(title: "Xác nhận", style: .destructive) { (btnOk) in
            }
            alert.addAction(btnOk)
            present(alert, animated: true, completion: nil)
        }else if errorCode.range(of: Errorcode.NAVIGO_INVALID_ACTIVE_KEY) != nil{
            let alert:UIAlertController = UIAlertController(title: "Thông báo", message: errorToast, preferredStyle: UIAlertControllerStyle.alert)
            let btnOk:UIAlertAction = UIAlertAction(title: "Xác nhận", style: .destructive) { (btnOk) in
            }
            alert.addAction(btnOk)
            present(alert, animated: true, completion: nil)
        }else{
            let alert:UIAlertController = UIAlertController(title: "Thông báo", message: errorToast, preferredStyle: UIAlertControllerStyle.alert)
            let btnOk:UIAlertAction = UIAlertAction(title: "Xác nhận", style: .destructive) { (btnOk) in
            }
            alert.addAction(btnOk)
            present(alert, animated: true, completion: nil)
        }
    }
    
    func startRequest() {
        DataNavigo.handingScreen = self
    }
    
    func endRequest() {
        if(status == 1){
            DataNavigo.passenger.saveInformationUser()
            status = 3
            login()
        }else if(status == 2){
            DataNavigo.passenger.saveInformationUser()
            status = 3
            login()
        }else if(status == 3){
            let sb = UIStoryboard(name: "Main", bundle: nil)
            let skymap = sb.instantiateViewController(withIdentifier: "MapController") as! MapController
            self.navigationController?.pushViewController(skymap, animated: true)
        }
    }
    
    var alertPath = UIAlertController()
    
    func showAlertPath() {
        alertPath = UIAlertController(title: nil, message: "Đang xử lý...\n\n", preferredStyle: .alert)
        let spinnerIndicator = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        spinnerIndicator.center = CGPoint(x: 135.0, y: 65.5)
        spinnerIndicator.color = UIColor.black
        spinnerIndicator.startAnimating()
        alertPath.view.addSubview(spinnerIndicator)
        self.present(alertPath, animated: false, completion: nil)
    }
    //alertPath.dismiss(animated: true, completion: nil)
    
}

extension ActiveCodeController: CodeInputViewDelegate{
    func codeInputView(_ codeInputView: CodeInputView, didFinishWithCode code: String)
    {
        codeActive = code
    }
}
