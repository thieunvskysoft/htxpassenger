//
//  PromotionController.swift
//  NaviGo
//
//  Created by HoangManh on 11/3/17.
//  Copyright © 2017 DUMV. All rights reserved.
//

import UIKit
import WebKit

class PromotionController: UIViewController,UIWebViewDelegate {

    var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        let backButton = UIBarButtonItem()
        backButton.title = " "
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        navigationController?.navigationBar.tintColor = .white
        navigationItem.title = "Thông tin khuyến mại"
        
        webView = UIWebView(frame: UIScreen.main.bounds)
        webView.delegate = self
        view.addSubview(webView)
        if let url = URL(string: Constant.URL_PROMOTION + "?accountID="+DataNavigo.passenger.acountID) {
          //  print(url)
            let request = URLRequest(url: url)
            webView.loadRequest(request)
        }
    }

}
