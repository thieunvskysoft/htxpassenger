//
//  HomeController.swift
//  xelo
//
//  Created by Nguyen Thieu on 2/27/19.
//  Copyright © 2019 DUMV. All rights reserved.
//


import UIKit
import UserNotifications

protocol MenuActionDelegate {
    func openSegue(_ segueName: String, sender: AnyObject?)
    func reopenMenu()
}

class HomeController: UIViewController{
    let interactor = Interactor()
    @IBOutlet weak var collectionService: UICollectionView!
    fileprivate let sectionInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var btnMenu: UIBarButtonItem!

    
    @IBOutlet weak var topConst: NSLayoutConstraint!
    @IBOutlet weak var viewTitle: UIView!
    @IBOutlet weak var tblPromotion: UITableView!
    @IBOutlet weak var colServiceHeightConst: NSLayoutConstraint!
    
    @IBOutlet weak var lblPromotion: UILabel!
    var PROMOTION_INFORMATION:String = ""
    var XELO_MOTO:String = ""
    var XELO_CAR:String = ""
    var TICKET:String = ""
    var TIMXE24:String = ""
    var NOTIFICATION_TITLE:String = "Thông báo"
    var FEATURE_NOT_YET:String = "Tính năng hiện tại chưa khả dụng"
    
    override func viewDidLoad() {
        print("map- viewdidload")
        UIApplication.shared.statusBarStyle = .lightContent
        UIApplication.shared.isIdleTimerDisabled = true
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
       // isScrollEnabled
        loadViewLanguage()
        collectionService.isScrollEnabled = false
        tblPromotion.isScrollEnabled = false
        
        self.collectionService.register(UINib(nibName: "ItemCell", bundle: nil), forCellWithReuseIdentifier: "ItemCell")
        
        self.tblPromotion.rowHeight = view.frame.width / 1.7
        tblPromotion.separatorStyle = UITableViewCellSeparatorStyle.none
        collectionService.dataSource = self
        collectionService.delegate = self
        tblPromotion.dataSource = self
        tblPromotion.delegate = self
        //viewTitle.layer.cornerRadius = 5
        //viewTitle.backgroundColor = UIColor.lightGray //NaviColor.naviBtnOrange
        if let layout = collectionService.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.minimumLineSpacing = 5
            layout.minimumInteritemSpacing = 5
            //layout.sectionInset = sectionInsets
            //layout.scrollDirection = .horizontal

        }
        //collectionService.contentInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        collectionService.isPagingEnabled = false
        view.layoutIfNeeded()
        view.layoutIfNeeded()
        collectionService.reloadData()
        tblPromotion.reloadData()
        navigationController?.navigationBar.tintColor = .white
        //navigationItem.title = "XELO"
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) { (success, error) in
                if error != nil {
                    print("Authorization Unsuccessfull")
                }else {
                    print("Authorization Successfull")
                }
            }
        } else {
            // Fallback on earlier versions
        }
        messageBackground()
        //print("view height", view.frame.height, self.navigationController?.navigationBar.intrinsicContentSize.height, UIApplication.shared.statusBarFrame.height)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        DataNavigo.handingScreen = nil
        DataNavigo.ipaDelegate = self
        //let height = ticketFares.count * 43
        let width = ((self.view.frame.size.width - CGFloat(cellMarginSize) * CGFloat(3)) / CGFloat(3)) + 5
        colServiceHeightConst.constant = width
        collectionService.layoutIfNeeded()
        collectionService.reloadData()
        //lblPromotion.text = PROMOTION_INFORMATION
    }
    override func viewWillDisappear(_ animated: Bool) {

    }

    func loadViewLanguage() {
        let curren_language = UserDefaults.standard.string(forKey: "curren_language")
        let language = LanguageData()
        PROMOTION_INFORMATION = language.localizedString(forKey:curren_language!,forKey: "PROMOTION_INFORMATION")
        XELO_MOTO = language.localizedString(forKey:curren_language!,forKey: "XELO_MOTO")
        XELO_CAR = language.localizedString(forKey:curren_language!,forKey: "XELO_CAR")
        TICKET = language.localizedString(forKey:curren_language!,forKey: "TICKET")
        TIMXE24 = language.localizedString(forKey:curren_language!,forKey: "TIMXE24")
        FEATURE_NOT_YET = language.localizedString(forKey:curren_language!,forKey: "FEATURE_NOT_YET")
        NOTIFICATION_TITLE = language.localizedString(forKey:curren_language!,forKey: "NOTIFICATION_TITLE")
        
    }
    
    
    var cellMarginSize = 28.0
    
    func navigationControllerSupportedInterfaceOrientations(navigationController: UINavigationController) -> UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.portrait
    }
    
    let btnMenuView: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "menu"), for: .normal)
        button.frame = CGRect(x: 10, y: UIApplication.shared.statusBarFrame.height + 10, width: 32, height: 32)
        return button
    }()
    
    func openMenu() {
        performSegue(withIdentifier: "openMenu", sender: nil)
    }
    
    @IBAction func edgePanGesture(_ sender: UIScreenEdgePanGestureRecognizer) {
        
        let translation = sender.translation(in: view)
        let progress = MenuHelper.calculateProgress(translation, viewBounds: view.bounds, direction: .right)
        MenuHelper.mapGestureStateToInteractor(
            sender.state,
            progress: progress,
            interactor: interactor){
                self.performSegue(withIdentifier: "openMenu", sender: nil)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationViewController = segue.destination as? MenuViewController {
            destinationViewController.transitioningDelegate = self
            destinationViewController.interactor = interactor
            destinationViewController.menuActionDelegate = self
        }
    }
    
    @IBAction func viewMenu(_ sender: Any) {
        performSegue(withIdentifier: "openMenu", sender: nil)
    }
    
    
    func messageBackground() {
        if DataNavigo.notiFB != nil {
            NotiDetailController.noti = true
            let sb = UIStoryboard(name: "Main", bundle: nil)
            let activecode = sb.instantiateViewController(withIdentifier: "NotiDetailController") as! NotiDetailController
            self.navigationController?.pushViewController(activecode, animated: true)
        }else if DataNavigo.loginResult.warningMessage != "" {
            //print("warningMessage",DataNavigo.loginResult.warningMessage)
            let alert:UIAlertController = UIAlertController(title: "Thông báo", message: DataNavigo.loginResult.warningMessage, preferredStyle: UIAlertControllerStyle.alert)
            let btnOk:UIAlertAction = UIAlertAction(title: "Xác nhận", style: .destructive) { (btnOk) in
            }
            alert.addAction(btnOk)
            present(alert, animated: true, completion: nil)
        }
    }
    
    
    func openWebsite(linkUrl:String) {
        if let url = URL(string: linkUrl),
            UIApplication.shared.canOpenURL(url)
        {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    func getListService(type:String) {
        let CompletionHandler: ((Array<Service>,NavigoError?) -> Void) = {
            arrServices,navigoError in
            DispatchQueue.main.async {
                if(navigoError == nil){
                    if arrServices.count > 0 {
                        DataNavigo.arrService = arrServices
                        self.loadtoMap()
                    }
                }else{
                }
            }
        }
        let connector = PassengerConnector();
        connector.getListServices(type: type, CompletionHandler: CompletionHandler)
    }
    
    func loadtoMap() {
        DataNavigo.booking = Book()
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let skymap = sb.instantiateViewController(withIdentifier: "MapController") as! MapController
        self.navigationController?.pushViewController(skymap, animated: true)
    }
    
}

extension HomeController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ItemCell", for: indexPath) as! ItemCell
        let item = DataNavigo.loginResult.arrAppItem[indexPath.row]
        if indexPath.row == 0 {
            cell.imgService.image = UIImage(named: "ic_lt")
            cell.textLabel.text = "Xe liên tỉnh"
            DataNavigo.serviceAccept = 0
        }else if indexPath.row == 1 {
            cell.imgService.image = UIImage(named: "ic_sb")
            cell.textLabel.text = "Xe sân bay"
        }
        else if indexPath.row == 2 {
            cell.imgService.image = UIImage(named: "ic_dn")
            cell.textLabel.text = "Xe đi ngay"
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let item = DataNavigo.loginResult.arrAppItem[indexPath.row]
        if indexPath.row == 0 {
            DataNavigo.serviceAccept = 0
            let alert:UIAlertController = UIAlertController(title: "Xe liên tỉnh", message: "Tìm các chuyến xe chạy rỗng của tài xế để tối ưu chi phí, chỉ từ 5.500 đồng/km", preferredStyle: UIAlertControllerStyle.alert)
            let btnOk:UIAlertAction = UIAlertAction(title: "Xác nhận", style: .destructive) { (btnOk) in
                self.getListService(type: "xtc")
            }
            let btnCancel:UIAlertAction = UIAlertAction(title: "Huỷ", style: .default) { (btnCancel) in
            }
            
            alert.addAction(btnCancel)
            alert.addAction(btnOk)
            present(alert, animated: true, completion: nil)
        }else if indexPath.row == 1 {
            DataNavigo.serviceAccept = 1
            self.getListService(type: "xtc")
        }
        else if indexPath.row == 2 {
            DataNavigo.serviceAccept = 2
            let alert:UIAlertController = UIAlertController(title: "Xe đi ngay", message: "Giá cước cao hơn để đảm bảo có xe phục vụ bạn nhanh nhất", preferredStyle: UIAlertControllerStyle.alert)
            let btnOk:UIAlertAction = UIAlertAction(title: "Xác nhận", style: .destructive) { (btnOk) in
                self.getListService(type: "xtc")
            }
            let btnCancel:UIAlertAction = UIAlertAction(title: "Huỷ", style: .default) { (btnCancel) in
            }
            
            alert.addAction(btnCancel)
            alert.addAction(btnOk)
            present(alert, animated: true, completion: nil)
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = self.calculateWith()
        return CGSize(width: width, height: width)
    }
    
    func calculateWith() -> CGFloat {
        let width = (self.view.frame.size.width - CGFloat(cellMarginSize) * CGFloat(3)) / CGFloat(3)
        return width
    }
        
//        if collectionView == self.collectionService {
//
//            var collectionViewSize = collectionService.frame.size
//            collectionViewSize.width = collectionViewSize.width/3.0
//            collectionViewSize.height = collectionViewSize.width
//            return collectionViewSize
//        } else {
//            return CGSize(width: 60, height: 60)
//        }
//    }
}
extension HomeController: UITableViewDelegate, UITableViewDataSource {
    
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return DataNavigo.loginResult.arrHomeAds.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PromotionCell", for: indexPath) as! PromotionCell
        let ads:HomeAds = DataNavigo.loginResult.arrHomeAds[indexPath.row]
        let url:URL = URL(string: ads.imageUrl!)!
        let data:Data = try! Data(contentsOf: url as URL)
        cell.imgPromotion.image = UIImage(data: data as Data)
        cell.imgPromotion.layer.cornerRadius = 5
        return (cell)
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // print("chon cai nay ",indexPath.row)
        //let ads:HomeAds = DataNavigo.loginResult.arrHomeAds[indexPath.row]
        //openWebsite(linkUrl: ads.linkUrl!)
    }
}
extension HomeController: UIViewControllerTransitioningDelegate {
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return PresentMenuAnimator()
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return DismissMenuAnimator()
    }
    
    func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return interactor.hasStarted ? interactor : nil
    }
    
    func interactionControllerForPresentation(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return interactor.hasStarted ? interactor : nil
    }
}

extension HomeController : MenuActionDelegate {
    func openSegue(_ segueName: String, sender: AnyObject?) {
        dismiss(animated: true){
            self.performSegue(withIdentifier: segueName, sender: sender)
        }
    }
    func reopenMenu(){
        // print("--------------")
        // performSegue(withIdentifier: "openMenu", sender: nil)
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let view = sb.instantiateViewController(withIdentifier: "BookHistoryController") as! BookHistoryController
        self.navigationController?.pushViewController(view, animated: true)
    }
}
extension HomeController: UNUserNotificationCenterDelegate {
    
    @available(iOS 10.0, *)
    public func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Swift.Void) {
        completionHandler( [.alert, .badge, .sound])
    }
    
    @available(iOS 10.0, *)
    public func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Swift.Void) {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let activecode = sb.instantiateViewController(withIdentifier: "NotiDetailController") as! NotiDetailController
        self.navigationController?.pushViewController(activecode, animated: true)
        //print("Tapped in notification")
    }
}
extension HomeController: IAPDelegate{
    func purchaseSuccessful() {
        
    }
    
    func pushNotiSuccessful() {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let activecode = sb.instantiateViewController(withIdentifier: "NotiDetailController") as! NotiDetailController
        self.navigationController?.pushViewController(activecode, animated: true)
    }
    
    func pushAlterView() {
        if #available(iOS 10.0, *) {
            let str = StringConvert()
            let value = str.emojiConvert(str: (DataNavigo.notiFB?.content!)!)
            let content = NotificationContent(title: DataNavigo.notiFB!.title ?? "Thông báo", body:value)
            
            content.categoryIdentifier = "requestIdentifier"
            let request = UNNotificationRequest(identifier: "requestIdentifier", content: content, trigger: nil)
            UNUserNotificationCenter.current().add(request) { error in
                UNUserNotificationCenter.current().delegate = self
                if (error != nil){
                    
                }
            }
        } 
    }
    
    func purchaseForeground() {
        
    }
    
}
