//
//  ViewController.swift
//  NaviGo
//
//  Created by HoangManh on 9/20/17.
//  Copyright © 2017 DUMV. All rights reserved.
//

import UIKit


class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(lblTitle)
        lblTitle.frame = CGRect(x: 10, y: 90, width: 280, height: 60)
        lblTitle.sizeToFit()
        lblTitle.center.x = self.view.center.x
        
        view.addSubview(imgLogo)
        imgLogo.frame = CGRect(x: 10, y: 150, width: 150, height: 50)
        //imgLogo.sizeToFit()
        imgLogo.center.x = self.view.center.x
        
        view.addSubview(buttonLoginDevice)
        //view.addSubview(buttonLoginFB)
        buttonLoginDevice.frame = CGRect(x: 16, y: view.frame.height - 60, width: view.frame.width - 32, height: 45)
        buttonLoginDevice.addTarget(self, action: #selector(loginByDevice), for: .touchUpInside)
        
        //buttonLoginFB.frame = CGRect(x: 16, y: view.frame.height - 120, width: view.frame.width - 32, height: 45)
        
        //buttonLoginFB.addTarget(self, action: #selector(viewFBLogin), for: .touchUpInside)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

//    let buttonLoginFB: VSSocialButton = {
//        let btn = VSSocialButton()
//        btn.buttonBackgroundColor = UIColor(colorLiteralRed: 38/255, green: 68/255, blue: 131/255, alpha: 1)
//        btn.buttonTitle = "Đăng ký bằng facebook"
//        btn.buttonCornerRadius = 5
//        btn.buttonTitleColor = UIColor.white
//        btn.socialNetworkImage = #imageLiteral(resourceName: "FBLogo")
//        btn.buttonTitleFontSize = 16
//        btn.buttonTitleFontName = "System"
//        return btn
//    }()
    
    let buttonLoginDevice: VSSocialButton = {
        let btn = VSSocialButton()
        btn.buttonBackgroundColor = UIColor(red: 0, green: 150/255, blue: 255/255, alpha: 0.8)
        btn.buttonTitle = "Đăng ký với thiết bị"
        btn.buttonCornerRadius = 5
        btn.buttonTitleColor = UIColor.white
        btn.buttonTitleFontSize = 16
        btn.buttonTitleFontName = "System"
        return btn
    }()
    

    
    let lblAppName: UILabel = {
        let label = UILabel()
        label.text = "NaviGo"
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 22)
        label.textColor = NaviColor.naviLabel
        return label
    
    }()
    let lblTitle: UILabel = {
        let label = UILabel()
        label.text = "Chào mừng bạn đến với dịch vụ \n đặt xe của Skysoft"
        label.textAlignment = .center
        label.numberOfLines = 2
        label.font = UIFont.systemFont(ofSize: 16)
        label.textColor = NaviColor.naviLabel
        return label
        
    }()
    
    let imgLogo:UIImageView = {
        let img = UIImageView()
        img.image = #imageLiteral(resourceName: "logo_flash")
        return img
    }()
    
    @objc func loginByDevice() {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let signin = sb.instantiateViewController(withIdentifier: "SignInController") as! SignInController
        self.navigationController?.pushViewController(signin, animated: true)
    }
}


