//
//  TripdefaultController.swift
//  xelo
//
//  Created by Nguyen Thieu on 3/1/20.
//  Copyright © 2020 DUMV. All rights reserved.
//

import UIKit

class TripdefaultController: UIViewController {

    var service = Service()
        
        override func viewDidLoad() {
            super.viewDidLoad()
            DataNavigo.handingScreen = self
            navigationItem.title = "CHI TIẾT CUỐC ĐẶT"
            navigationController?.navigationBar.tintColor = .red
            self.navigationController?.isNavigationBarHidden = false
            let backButton = UIBarButtonItem()
            backButton.title = " "
            self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
            viewInfo.layer.borderWidth = 1
            viewInfo.layer.borderColor = UIColor.lightGray.cgColor
    //        viewInfo.layer.cornerRadius = 5
    //        viewNote.layer.cornerRadius = 5
            viewNote.layer.borderWidth = 1
            viewNote.layer.borderColor = UIColor.lightGray.cgColor
            checkPriceHTX()
        }
        
        override func viewWillAppear(_ animated: Bool) {
            let conv = Convert()
            lblStart.text = DataNavigo.booking.fromAddress
            lblEnd.text = DataNavigo.booking.toAddress
            lblDatetime.text = DataNavigo.booking.departureDate != "" ? conv.convertDatetimeByDateformat(datetime: DataNavigo.booking.departureDate, dateFormat: "dd/MM/yyyy HH:mm", value: 2) : ""
            lblNote.text = DataNavigo.booking.note
        }
        @IBOutlet weak var viewInfo: UIView!
        
        @IBOutlet weak var lblStart: UILabel!
        @IBOutlet weak var lblEnd: UILabel!
        @IBOutlet weak var lblDatetime: UILabel!
        @IBOutlet weak var lblService: UILabel!
        @IBOutlet weak var lblDistance: UILabel!
        @IBOutlet weak var lblPromo: UILabel!
        @IBOutlet weak var lblPrice: UILabel!
        @IBOutlet weak var lblNote: UILabel!
        @IBOutlet weak var viewNote: UIView!
        
        
        @IBAction func btnBooking(_ sender: Any) {
            bookingNow()
        }
        
        
        func checkPriceHTX() {

            let CompletionHandler: ((Array<Service>, Array<Point>, Int, NavigoError?) -> Void) = {
                bookPrice, arrPoints, distance, navigoError in
                DispatchQueue.main.async {
                    if(navigoError == nil){
                        self.lblPrice.text = bookPrice[0].price!.formatnumberPrice()
                        self.lblService.text = bookPrice[0].serviceName
                        
                        self.lblDistance.text = String(format: "%.1f", Double(distance)/1000) + " Km"
                        print(bookPrice[0],Double(distance), distance/1000)
                    }else{
                        if(navigoError?.errorCode == Errorcode.ERROR_NO_INTERNET){
                            self.errorNoInternet()
                        }
                        else{
                            self.errorServerSend(errorCode: (navigoError?.errorCode)!, errorToast: (navigoError?.errorMessage)!)
                        }
                    }
                }
            }
            let connector = APIBook();
            connector.checkPriceHTX(book: DataNavigo.booking, CompletionHandler: CompletionHandler)
        }
        
        func bookingNow()  {
            //DataNavigo.booking.serviceCode = DataNavigo.arrService[DataNavigo.serviceCar].serviceCode
            //DataNavigo.booking.promotionCode = tripCode.currentTitle
            let CompletionHandler: ((Book,NavigoError?) -> Void) = {
                booking,navigoError in
                DispatchQueue.main.async {
                    if(navigoError == nil){
                        if (booking.bookingId != "0") {
                            let sb = UIStoryboard(name: "Main", bundle: nil)
                            let tripInfo = sb.instantiateViewController(withIdentifier: "ListTripController") as! ListTripController
                            self.navigationController?.pushViewController(tripInfo, animated: true)
                        }

                    }else{
                        if(navigoError?.errorCode == Errorcode.ERROR_NO_INTERNET){
                            self.errorNoInternet()
                        }
                        else{
                            self.errorServerSend(errorCode: (navigoError?.errorCode)!, errorToast: (navigoError?.errorMessage)!)
                        }
                    }
                }
            }
            let connector = APIBook();
            connector.bookingNowHTX(book: DataNavigo.booking, CompletionHandler: CompletionHandler)
        }
    
    func showToast(message : String) {
        let toastLabel = UILabel(frame: CGRect(x: 10, y: self.view.frame.size.height-100, width: self.view.frame.size.width - 20 , height: 55))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "Montserrat-Light", size: 12.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 15;
        toastLabel.clipsToBounds  =  true
        toastLabel.numberOfLines = 2
        
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 2.0, delay: 0.3, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    }
    extension TripdefaultController: HandingScreen {
        func errorConnect() {
            let date = Date()
            let calendar = Calendar.current
            let hour = calendar.component(.hour, from: date)
            let minutes = calendar.component(.minute, from: date)
            let seconds = calendar.component(.second, from: date)
            print("hours = \(hour):\(minutes):\(seconds)")
        }
        
        func errorNoInternet() {
            print("errorSeverSend")
        }
        
        func errorServerSend(errorCode: String, errorToast: String) {
            print("errorSeverSend",errorCode, errorToast )
            if errorCode.range(of: Errorcode.NAVIGO_SESSION_EXPIRED) != nil{
                
            }
            self.showToast(message: errorToast)
        }
        
        func startRequest() {
            print("startRequest")
        }
        
        func endRequest() {
            print("endRequest")
        }
    }
