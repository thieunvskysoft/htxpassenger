//
//  DetailPointController.swift
//  NaviGo
//
//  Created by HoangManh on 11/8/17.
//  Copyright © 2017 DUMV. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import SwiftyJSON
import Alamofire

class DetailPointController: UIViewController, CLLocationManagerDelegate, GMSMapViewDelegate, UITextFieldDelegate{

    
    @IBOutlet weak var topConst: NSLayoutConstraint!
    var googleMap:GMSMapView?
    var locationManager = CLLocationManager()
    var locationPoint = CLLocation()
    let marker = GMSMarker()
    static var detailAddress = Address()
    static var updatePoint:Bool = false
    var lastMoveMap:Int64 = 0
    var addressLocation:String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        
        setupGoogleMap()
        if DetailPointController.updatePoint == true {
            createMarker(titleMarker: DetailPointController.detailAddress.fullName!, iconMarker: #imageLiteral(resourceName: "marker-end"), latitude: DetailPointController.detailAddress.y!, longitude: DetailPointController.detailAddress.x!)
        }else{
            txtFullName.text = ""
        }

        lastMoveMap = getCurrentMillis()
        startTimer()
        
    }
    @IBOutlet weak var viewInfo: UIView!
    @IBOutlet weak var txtFullName: UITextField!
    @IBOutlet weak var lblFullAdress: UILabel!
    @IBOutlet weak var btnAccept: UIButton!
    
    override func viewWillAppear(_ animated: Bool) {
        UIApplication.shared.statusBarStyle = .lightContent
        self.navigationController?.isNavigationBarHidden = false
        let backButton = UIBarButtonItem()
        backButton.title = " "
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        navigationController?.navigationBar.tintColor = .white
        navigationItem.title = "Thêm điểm yêu thích"
        //navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor :UIColor.white]
    }
    
    let viewPointInfo:UIView = {
        let v = UIView()
        return v
    }()
    let txtFullNamePoint:UITextField = {
        let txt = UITextField()
        txt.placeholder = "Tên điểm"
        
        return txt
    }()
    let lblFullAddressPoint:UILabel = {
        let lbl = UILabel()
        return lbl
    }()
    ///
    func updateAddress(address:Address){
        let CompletionHandler: ((NavigoError?) -> Void) = {
            navigoError in
            DispatchQueue.main.async {
                if(navigoError == nil){
                    //sua xong diem
                    self.navigationController?.popViewController(animated: true)
                }else{
                    if(navigoError?.errorCode == Errorcode.ERROR_NO_INTERNET){
                       
                    }
                    else{
                        if navigoError?.errorCode?.range(of: Errorcode.NAVIGO_SESSION_EXPIRED) != nil{
                            self.login()
                        }
                    }
                }
            }
        }
        let a = APIAddress()
        a.modifyAddress(address: address, CompletionHandler: CompletionHandler)
    }
    func login() {
        let CompletionHandler: ((LoginResult,NavigoError?) -> Void) = {
            loginResult,navigoError in
            DispatchQueue.main.async {
                if(navigoError == nil){
                   
                }else{
                  
                }
            }
        }
        let connector = PassengerConnector();
        connector.reconnect(CompletionHandler: CompletionHandler)
    }
    
    func createaddress(address:Address)  {
        let CompletionHandler: ((NavigoError?) -> Void) = {
            navigoError in
            
            DispatchQueue.main.async {
                if(navigoError == nil){
                    self.navigationController?.popViewController(animated: true)
                }else{
                    if(navigoError?.errorCode == Errorcode.ERROR_NO_INTERNET){
                    }
                    else{
                        if navigoError?.errorCode?.range(of: Errorcode.NAVIGO_SESSION_EXPIRED) != nil{
                            self.login()
                        }

                    }
                }
            }
        }
        let apiAddress = APIAddress();
        apiAddress.createAddress(address: address,CompletionHandler: CompletionHandler)
    }
    //setup map
    func setupGoogleMap() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        locationManager.startMonitoringSignificantLocationChanges()
        if DetailPointController.updatePoint == true {
            let camera = GMSCameraPosition.camera(withLatitude: DetailPointController.detailAddress.y!, longitude: DetailPointController.detailAddress.x!, zoom: 17)
            googleMap = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
            googleMap?.camera = camera
        }else{
            let camera = GMSCameraPosition.camera(withLatitude: 21.05, longitude: 105.777711, zoom: 15)
            googleMap = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
            googleMap?.camera = camera
        }
        googleMap?.delegate = self
        googleMap?.isMyLocationEnabled = true
        googleMap?.settings.myLocationButton = true
        //button location
        _ = (self.navigationController?.navigationBar.intrinsicContentSize.height)! + UIApplication.shared.statusBarFrame.height
        do {
            // Set the map style by passing the URL of the local file.
            if let styleURL = Bundle.main.url(forResource: "style", withExtension: "json") {
                googleMap?.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
            } else {
                NSLog("Unable to find style.json")
            }
        } catch {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
        view.addSubview(googleMap!)
        if(DataNavigo.deviceModel == Constant.deviceModelX){
            googleMap?.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height - 35)
        }else{
            googleMap?.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        }
        googleMap?.addSubview(viewInfo)
        self.googleMap?.layer.zPosition = 1
        self.viewInfo.layer.zPosition = 90
        self.viewInfo.layer.cornerRadius = 5
        self.txtFullName.layer.zPosition = 99
        txtFullName.text = DetailPointController.detailAddress.fullName
        lblFullAdress.text = DetailPointController.detailAddress.fullAddress
        btnAccept.addTarget(self, action: #selector(customPoint), for: .touchUpInside)
        btnAccept.backgroundColor = NaviColor.naviBtnOrange
        btnAccept.layer.cornerRadius = 5
        self.googleMap?.bringSubview(toFront: self.txtFullName)
        txtFullName.keyboardType = UIKeyboardType.default
        txtFullName.returnKeyType = UIReturnKeyType.done
        txtFullName.delegate = self
        for gesture in (googleMap?.gestureRecognizers!)! {
            googleMap?.removeGestureRecognizer(gesture)
        }
        //topConst.constant = navBarHeight + 10
        
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField!) -> Bool {
        print("textFieldShouldReturn")
        self.view.endEditing(true)
        return true
    }
    
    
    func createMarker(titleMarker: String, iconMarker: UIImage, latitude: CLLocationDegrees, longitude: CLLocationDegrees) {
        
        marker.position = CLLocationCoordinate2DMake(latitude, longitude)
        marker.title = titleMarker
        marker.icon = self.imageWithImage(image: iconMarker, scaledToSize: CGSize(width: 32.0, height: 32.0))
        marker.map = googleMap
    }
    
    @objc func customPoint()  {
        if DetailPointController.updatePoint == true {
            DetailPointController.detailAddress.fullAddress = lblFullAdress.text
            DetailPointController.detailAddress.fullName = txtFullName.text
            DetailPointController.detailAddress.x = locationPoint.coordinate.longitude
            DetailPointController.detailAddress.y = locationPoint.coordinate.latitude
            updateAddress(address: DetailPointController.detailAddress)
        }else{
            let point = Address()
            point.fullAddress = lblFullAdress.text
            point.fullName = txtFullName.text
            point.x = locationPoint.coordinate.longitude
            point.y = locationPoint.coordinate.latitude
            createaddress(address: point)
        }
    }
    
    func imageWithImage(image:UIImage, scaledToSize newSize:CGSize) -> UIImage{
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0);
        //image.draw(in: CGRectMake(0, 0, newSize.width, newSize.height))
        image.draw(in: CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: newSize.width, height: newSize.height))  )
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    func getCurrentMillis()->Int64 {
        return Int64(Date().timeIntervalSince1970 * 1000)
    }
    var timer: Timer?
    
    func startTimer() {
        if timer == nil {
            timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(getLocationAddress), userInfo: nil, repeats: true)
        //    print("Start time")
        }
    }
    
    func stopTimer() {
        if timer != nil {
       //     print("stop time")
            timer?.invalidate()
            timer = nil
        }
    }
    
    let btnSetPoint:UIButton = {
        let btn = UIButton()
        btn.setTitle("Xác nhận", for: .normal)
        btn.backgroundColor = NaviColor.naviButton
        return btn
    }()
    
    @objc func getLocationAddress() {
     //   print("getLocationAddress")
        getAddressByGeocode(coordinate: locationPoint.coordinate)
    }
    
    func getAddressByGeocode(coordinate:CLLocationCoordinate2D) {
        if (getCurrentMillis() - lastMoveMap >= (1*1000) && addressLocation == ""){
           // print("getAddressByGeocode")
            let geocoder = GMSGeocoder()
            geocoder.reverseGeocodeCoordinate(coordinate) { response , error in
                if let address = response?.firstResult() {
                    let lines = address.lines! as [String]
                    self.addressLocation = lines.joined(separator: "\n")
                    self.lblFullAdress.text = lines.joined(separator: "\n")
                  //  print(lines)
                    self.stopTimer()
                }
            }
        }
    }
}
//Map
extension DetailPointController {
    // MARK: CLLocation Manager Delegate
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
       // print("Error while get location \(error)")
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last
        if(DetailPointController.updatePoint != true){
            let camera = GMSCameraPosition.camera(withLatitude: (location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!, zoom: 17.0)
            self.googleMap?.animate(to: camera)
            createMarker(titleMarker: "", iconMarker: #imageLiteral(resourceName: "marker-end"), latitude: (location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!)
        }
        self.locationManager.stopUpdatingLocation()
    }
    
    // MARK: GMSMapview Delegate
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        self.googleMap?.isMyLocationEnabled = true
    }
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        self.googleMap?.isMyLocationEnabled = true
        startTimer()
        if (gesture) {
            lastMoveMap = getCurrentMillis()
            addressLocation = ""
            mapView.selectedMarker = nil
        }
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        lastMoveMap = getCurrentMillis()
        addressLocation = ""
        locationPoint = CLLocation(latitude: mapView.camera.target.latitude, longitude: mapView.camera.target.longitude)
        marker.position = CLLocationCoordinate2DMake(mapView.camera.target.latitude, mapView.camera.target.longitude)
    }
    
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
      //  print("Click")
        startTimer()
        lastMoveMap = getCurrentMillis()
        addressLocation = ""
        googleMap?.isMyLocationEnabled = true
        googleMap?.selectedMarker = nil
        return false
    }
}
