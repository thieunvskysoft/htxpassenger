//
//  ListPointController.swift
//  NaviGo
//
//  Created by HoangManh on 11/3/17.
//  Copyright © 2017 DUMV. All rights reserved.
//

import UIKit

class ListPointController: UIViewController {

    @IBOutlet weak var tblListPoint: UITableView!
    @IBOutlet weak var bottomView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblListPoint.delegate = self
        tblListPoint.dataSource = self
        //tblListPoint.tableHeaderView =

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        getAll()
        bottomView.backgroundColor = NaviColor.naviBtnOrange
    }
    @IBAction func btnAddPoint(_ sender: UIButton) {
        DetailPointController.updatePoint = false
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let detailPoint = sb.instantiateViewController(withIdentifier: "DetailPointController") as! DetailPointController
        self.navigationController?.pushViewController(detailPoint, animated: true)
    }
    func createaddress(address:Address)  {
        let CompletionHandler: ((NavigoError?) -> Void) = {
            navigoError in
            
            DispatchQueue.main.async {
                if(navigoError == nil){
                    
                }else{
                    if(navigoError?.errorCode == Errorcode.ERROR_NO_INTERNET){
                    }
                    else{
                    }
                }
            }
        }
        let apiAddress = APIAddress();
        apiAddress.createAddress(address: address,CompletionHandler: CompletionHandler)
    }
    
    func editAddress(address:Address)  {
        let CompletionHandler: ((NavigoError?) -> Void) = {
            navigoError in
            
            DispatchQueue.main.async {
                if(navigoError == nil){
                    
                }else{
                    if(navigoError?.errorCode == Errorcode.ERROR_NO_INTERNET){
                        
                    }
                    else{
                        
                    }
                }
            }
        }
        let apiAddress = APIAddress();
        apiAddress.modifyAddress(address: address,CompletionHandler: CompletionHandler)
    }
    
    func removeAddress(address:Address)  {
        let CompletionHandler: ((NavigoError?) -> Void) = {
            navigoError in            
            DispatchQueue.main.async {
                if(navigoError == nil){
                    self.getAll()
                }else{
                    if(navigoError?.errorCode == Errorcode.ERROR_NO_INTERNET){
                        
                    }
                    else{
                        
                    }
                }
            }
        }
        let apiAddress = APIAddress();
        apiAddress.deleteAddress(address: address,CompletionHandler: CompletionHandler)
    }
    
    func getAll()  {
        let CompletionHandler: ((Array<Address>,NavigoError?) -> Void) = {
            lisAddress,navigoError in
            
            DispatchQueue.main.async {
                if(navigoError == nil){
                    DataNavigo.arrAddress.removeAll()
                    DataNavigo.arrAddress = lisAddress
                    
                    self.tblListPoint.reloadData()
                }else{
                    if(navigoError?.errorCode == Errorcode.ERROR_NO_INTERNET){
                        
                    }
                    else{
                        
                    }
                }
            }
        }
        let apiAddress = APIAddress();
        apiAddress.getListAddress(CompletionHandler: CompletionHandler)
        
    }
}
extension ListPointController: UITableViewDelegate, UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Điểm yêu thích"
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
       // print("diemyeuthich",DataNavigo.arrAddress.count)
        return  DataNavigo.arrAddress.count
        
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let point = DataNavigo.arrAddress[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "LikePointCell", for: indexPath) as! LikePointCell
        cell.setPoint(address: point)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     //   print("chon cai nay ",indexPath.row)
        let point = DataNavigo.arrAddress[indexPath.row]
//        if getLocation.choice == 1 {
//            DataNavigo.booking.fromPosition?.y = point.y
//            DataNavigo.booking.fromPosition?.x = point.x
//            DataNavigo.booking.fromAddress = point.fullAddress
//        }
//        else{
//            DataNavigo.booking.toPosition?.y = point.y
//            DataNavigo.booking.toPosition?.x = point.x
//            DataNavigo.booking.toAddress = point.fullAddress
//        }
//        DataNavigo.checkDrawPath = true
//        DataNavigo.booking.estDistance = 0
//        let viewControllers:[UIViewController] = (self.navigationController?.viewControllers)!;
//        for controller in viewControllers {
//            if controller is MapController {
//                self.navigationController!.popToViewController(controller, animated: true)
//            }
//        }
        if getLocation.choice == 1 {
            DataNavigo.booking.fromPosition?.y = point.y
            DataNavigo.booking.fromPosition?.x = point.x
            DataNavigo.booking.fromAddress = point.fullAddress
        }
        else if getLocation.choice == 2{
            DataNavigo.booking.toPosition?.y = point.y
            DataNavigo.booking.toPosition?.x = point.x
            DataNavigo.booking.toAddress = point.fullAddress
        }
        else if getLocation.choice == 3{
            BusBookingController.fromLocation.y = point.y
            BusBookingController.fromLocation.x = point.x
            BusBookingController.fromAddress = point.fullAddress!
        }
        else if getLocation.choice == 4{
            BusBookingController.toLocation.y = point.y
            BusBookingController.toLocation.x = point.x
            BusBookingController.toAddress = point.fullAddress!
        }
        DataNavigo.checkDrawPath = true
        DataNavigo.booking.estDistance = 0
        navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {

         let point = DataNavigo.arrAddress[indexPath.row]
        // action one
        let editAction = UITableViewRowAction(style: .default, title: "\u{270e}\nSửa", handler: { (action, indexPath) in
            print("Edit tapped")
            self.didTapEdit(address: point)
        })
        editAction.backgroundColor = NaviColor.naviBtnBlue


        // action two
        let deleteAction = UITableViewRowAction(style: .default, title: "\u{2716}\nXoá", handler: { (action, indexPath) in
          //  print("Delete tapped")
            self.didTapDel(address: point)
        })
        //deleteAction.title = attributedStringRemove.string
        deleteAction.backgroundColor = NaviColor.naviBtnOrange

        return [editAction, deleteAction]
//        let delete = UITableViewRowAction(style: .default, title: "\u{1f5d1}\n Xoá") { action, index in
//            print("more button tapped")
//           // self.tableView(tableView, commit: UITableViewCellEditingStyle.delete, forRowAt: indexPath)
//        }
//        delete.backgroundColor = UIColor.red
//
//        let apply = UITableViewRowAction(style: .default, title: "\u{2606}\n Like") { action, index in
//            print("favorite button tapped")
//            //self.tableView(tableView, commit: UITableViewCellEditingStyle.insert, forRowAt: indexPath)
//        }
//        apply.backgroundColor = UIColor.orange
//
//        let take = UITableViewRowAction(style: .normal, title: "\u{2605}\n Rate") { action, index in
//            print("share button tapped")
//           // self.tableView(tableView, commit: UITableViewCellEditingStyle.none, forRowAt: indexPath)
//        }
//        take.backgroundColor = UIColor.green
//
//        return [take, apply, delete]
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
    }
    
    func didTapEdit(address: Address) {
//        let viewControllers:[UIViewController] = (self.navigationController?.viewControllers)!;
//        for controller in viewControllers {
//            if controller is DetailPointController {
//                DetailPointController.detailAddress = address
//                self.navigationController!.popToViewController(controller, animated: true)
//            }
//        }
        DetailPointController.detailAddress = address
        DetailPointController.updatePoint = true
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let detailPoint = sb.instantiateViewController(withIdentifier: "DetailPointController") as! DetailPointController
        self.navigationController?.pushViewController(detailPoint, animated: true)
    }
    
    func didTapDel(address: Address) {
        let alertTitle = "Xoá điểm"
        let message = "Bạn có muốn xoá điểm yêu thích này không?"
        let alert = UIAlertController(title: alertTitle, message: message, preferredStyle: .alert)
        let btnOk:UIAlertAction = UIAlertAction(title: "Xác nhận", style: .destructive) { (btnOk) in
            //del điểm
            self.removeAddress(address: address)
            //self.tblListPoint.reloadData()
        }
        let btnCancel:UIAlertAction = UIAlertAction(title: "Huỷ", style: .destructive) { (btnCancel) in
            
        }
        alert.addAction(btnOk)
        alert.addAction(btnCancel)
        present(alert, animated: true, completion: nil)
    }
}
