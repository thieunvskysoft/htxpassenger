//
//  SetupController.swift
//  xelo
//
//  Created by Nguyen Thieu on 8/8/18.
//  Copyright © 2018 DUMV. All rights reserved.
//

import UIKit

class SetupController: UIViewController {
    
    var arraySettingOptions = [Dictionary<String,String>]()
    var LANGUAGE_VIEW = "Ngôn ngữ"
    var SETTINGS_TITLE = "Cài đặt"
    
    
    @IBOutlet weak var tblSetting: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tblSetting.delegate = self
        tblSetting.dataSource = self
        tblSetting.isScrollEnabled = false
        tblSetting.separatorStyle = UITableViewCellSeparatorStyle.none
        navigationController?.navigationBar.tintColor = .white

    }
    
    override func viewWillAppear(_ animated: Bool) {
        arraySettingOptions.removeAll()
        loadViewLanguage()
        self.navigationController?.isNavigationBarHidden = false
        let backButton = UIBarButtonItem()
        backButton.title = " "
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        navigationItem.title = SETTINGS_TITLE
        arraySettingOptions.append(["title":LANGUAGE_VIEW, "icon":"settings_language"])
        //arraySettingOptions.append(["title":"English", "icon":"enflag"])
        tblSetting.reloadData()
    }
 
    func loadViewLanguage() {
        let curren_language = UserDefaults.standard.string(forKey: "curren_language")
        let language = LanguageData()
        LANGUAGE_VIEW = language.localizedString(forKey:curren_language!,forKey: "LANGUAGE_VIEW")
        SETTINGS_TITLE = language.localizedString(forKey:curren_language!,forKey: "SETTINGS_TITLE")
    }
    
}
extension SetupController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arraySettingOptions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsCell") as! SettingsCell
        cell.lblMenu?.text = arraySettingOptions[indexPath.row]["title"]!
        cell.imgIcon.image = UIImage(named: arraySettingOptions[indexPath.row]["icon"]!)
        return cell
    }
}

extension SetupController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        //let item = arraySettingOptions[indexPath.row]
        switch indexPath.row {
            case 0:
                let sb = UIStoryboard(name: "Main", bundle: nil)
                let feedBack = sb.instantiateViewController(withIdentifier: "LanguageController") as! LanguageController
                self.navigationController?.pushViewController(feedBack, animated: true)
                break
            default:
                break
            }
    }
}

