//
//  NotificationController.swift
//  xelo
//
//  Created by Nguyen Thieu on 2/1/18.
//  Copyright © 2018 DUMV. All rights reserved.
//

import UIKit

class NotificationController: UIViewController {
    
    @IBOutlet weak var tblNotify: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.tintColor = .white
        navigationItem.title = "Thông báo"
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        let backButton = UIBarButtonItem()
        backButton.title = " "
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        DataNavigo.arrNotify.removeAll()
        getListNotification()
        tblNotify.delegate = self
        tblNotify.dataSource = self
        tblNotify.separatorStyle = UITableViewCellSeparatorStyle.none
    }
    
    func getListNotification() {
        
        let CompletionHandler: ((Notifications,NavigoError?) -> Void) = {noti,navigoError in
            
            DispatchQueue.main.async {
                if(navigoError == nil){
                    print("Chayvaoday")
                    self.tblNotify.reloadData()
                }else{
                    if(navigoError?.errorCode == Errorcode.ERROR_NO_INTERNET){
                    }
                    else{
                        print("navigoError", navigoError?.errorCode ?? "")
                        print("navigoError", Errorcode.ERROR_MESSAGE)
                    }
                    
                }
            }
        }
        
        let connector = APINotification();
        connector.listNotication(CompletionHandler: CompletionHandler)
    }
    
}

extension NotificationController: UITableViewDelegate, UITableViewDataSource {
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        print("arrNotify",DataNavigo.arrNotify.count)
        return  DataNavigo.arrNotify.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell", for: indexPath) as! NotificationCell
        let notify:Notifications = DataNavigo.arrNotify[indexPath.row]
        let str = StringConvert()
        cell.lblNotiType.text = notify.title
        cell.lblContent.text = str.emojiConvert(str: notify.content!)
        cell.lblDate.text = notify.createDate
        let state = notify.read
        if state == true {
            cell.imgNoti.image = #imageLiteral(resourceName: "notiopen")
        }else if state == false{
            cell.imgNoti.image = #imageLiteral(resourceName: "notify")
        }
        return (cell)
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         print("chon cai nay ",indexPath.row)
        let notify:Notifications = DataNavigo.arrNotify[indexPath.row]
        NotiDetailController.notifyID = notify.notifyID!
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let viewnotify = sb.instantiateViewController(withIdentifier: "NotiDetailController") as! NotiDetailController
        //viewnotify.notify = notify
        self.navigationController?.pushViewController(viewnotify, animated: true)
        
    }
    
}

