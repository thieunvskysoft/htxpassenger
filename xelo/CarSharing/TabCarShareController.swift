//
//  TabCarShareController.swift
//  xelo
//
//  Created by Nguyen Thieu on 6/21/19.
//  Copyright © 2019 DUMV. All rights reserved.
//

import UIKit

class TabCarShareController: UIViewController {

    enum TabIndex : Int {
        case firstChildTab = 0
        case secondChildTab = 1
    }
    
    @IBOutlet weak var segmentedControl: TwicketSegmentedControl!
    
    
    @IBOutlet weak var contentView: UIView!
    
    var currentViewController: UIViewController?
    lazy var firstChildTabVC: UIViewController? = {
        let firstChildTabVC = self.storyboard?.instantiateViewController(withIdentifier: "ShareBookingController")
        return firstChildTabVC
    }()
    lazy var secondChildTabVC : UIViewController? = {
        let secondChildTabVC = self.storyboard?.instantiateViewController(withIdentifier: "CarOwnerController")
        return secondChildTabVC
    }()
    
    
    var TICKET:String = ""
    var HISTORY_TITLE:String = ""
    var BOOK_TICKET:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.statusBarStyle = .lightContent
        self.navigationController?.isNavigationBarHidden = false
        //    print("-------------------------")
        //loadViewLanguage()
        let titles = ["Đặt xe", "Thông tin xe"]
        segmentedControl.setSegmentItems(titles)
        segmentedControl.delegate = self
        displayCurrentTab(TabIndex.firstChildTab.rawValue)
        //Constant.URL_CHAT
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //   print("==========================")
        UIApplication.shared.statusBarStyle = .lightContent
        self.navigationController?.isNavigationBarHidden = false
        let backButton = UIBarButtonItem()
        backButton.title = " "
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        //self.navigationController?.navigationBar.topItem?.backBarButtonItem?.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        navigationController?.navigationBar.tintColor = .white
        navigationItem.title = TICKET
        //navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor :UIColor.white]
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if let currentViewController = currentViewController {
            currentViewController.viewWillDisappear(animated)
        }
    }
    
    func loadViewLanguage() {
        let curren_language = UserDefaults.standard.string(forKey: "curren_language")
        let language = LanguageData()
        BOOK_TICKET = language.localizedString(forKey:curren_language!,forKey: "BOOK_TICKET")
        HISTORY_TITLE = language.localizedString(forKey:curren_language!,forKey: "HISTORY_TITLE")
        TICKET = language.localizedString(forKey:curren_language!,forKey: "TICKET")
        
    }
    
    func displayCurrentTab(_ tabIndex: Int){
        if let vc = viewControllerForSelectedSegmentIndex(tabIndex) {
            
            self.addChildViewController(vc)
            vc.didMove(toParentViewController: self)
            vc.view.frame = self.contentView.bounds
            self.contentView.addSubview(vc.view)
            self.currentViewController = vc
            view.endEditing(true)
        }
    }
    
    func viewControllerForSelectedSegmentIndex(_ index: Int) -> UIViewController? {
        var vc: UIViewController?
        switch index {
        case TabIndex.firstChildTab.rawValue :
            vc = firstChildTabVC
        case TabIndex.secondChildTab.rawValue :
            vc = secondChildTabVC
        default:
            return nil
        }
        
        return vc
    }
    
}
extension TabCarShareController: TwicketSegmentedControlDelegate{
    func didSelect(_ segmentIndex: Int) {
        displayCurrentTab(segmentIndex)
    }
}
