//
//  MenuViewController.swift
//  InteractiveSlideoutMenu
//
//  Created by Robert Chen on 2/7/16.
//
//  Copyright (c) 2016 Thorn Technologies LLC
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.

import UIKit

class MenuViewController : UIViewController {
    
    @IBOutlet weak var viewMenu: UIView!
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var btnFacebook: UIButton!
    @IBOutlet weak var btnYoutube: UIButton!
    @IBOutlet weak var btnAppStore: UIButton!
    @IBOutlet weak var lblVersion: UILabel!
    var LISTTRIP:String = "Cuốc xe đã đặt"
    var PAYMENT:String = "Ví của tôi"
    var PROMOTION:String = "Khuyến mãi"
    var HISTORY_TITLE:String = "Lịch sử"
    var SHARE_AND_BONUS:String = "Chia sẻ và nhận thưởng"
    var NOTIFICATION_TITLE:String = "Thông báo"
    var SETTINGS_TITLE:String = "Cài đặt"
    var CONTACT_TITLE:String = "Liên hệ"
    var BE_DRIVER_TITLE:String = "Trở thành tài xế"
    var FEATURE_NOT_YET:String = "Tính năng hiện tại chưa khả dụng"
    var CONFIRM:String = "Xác nhận"
    var CANCEL:String = "Huỷ"
    
    var interactor:Interactor? = nil
    
    var menuActionDelegate:MenuActionDelegate? = nil
    
    var arrayMenuOptions = [Dictionary<String,String>]()
    var openSegueArray = [String]()
    
    func updateArrayMenuOptions(){
        arrayMenuOptions.append(["title":LISTTRIP, "icon":"ic_listtrip"])
        openSegueArray.append("listtrip")
        arrayMenuOptions.append(["title":NOTIFICATION_TITLE, "icon":"MenuNotification"])
        openSegueArray.append("notification")
        arrayMenuOptions.append(["title":PAYMENT, "icon":"MenuWallet"])
        openSegueArray.append("wallet")
        arrayMenuOptions.append(["title":HISTORY_TITLE, "icon":"MenuHistory"])
        openSegueArray.append("history")
        arrayMenuOptions.append(["title":CONTACT_TITLE, "icon":"MenuContact"])
        openSegueArray.append("contact")

        tableView.reloadData()
    }
    
    
    @IBAction func handleGesture(_ sender: UIPanGestureRecognizer) {
        let translation = sender.translation(in: view)

        let progress = MenuHelper.calculateProgress(translation, viewBounds: view.bounds, direction: .left)
        
        MenuHelper.mapGestureStateToInteractor(
            sender.state,
            progress: progress,
            interactor: interactor){
                self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func closeMenu(_ sender: AnyObject) {
        dismiss(animated: true, completion: nil)
    }
    
    func delay(seconds: Double, completion:@escaping ()->()) {
        let popTime = DispatchTime.now() + Double(Int64( Double(NSEC_PER_SEC) * seconds )) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: popTime) {
            completion()
        }
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        dismiss(animated: true){
            self.delay(seconds: 0.2){
                self.menuActionDelegate?.reopenMenu()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblUsername.text = DataNavigo.passenger.nameUser.uppercased()
        self.tableView.rowHeight = 48.0
        imgUser.layer.cornerRadius = 45
        imgUser.clipsToBounds = true
        //imgUser.image = #imageLiteral(resourceName: "Xelo_icon")
        imgUser.image = DataNavigo.loginResult.avatarFace ?? UIImage(named: "Menu_Avt")
        //imgUser.contentMode = .center
        loadViewLanguage()
        viewMenu.backgroundColor = UIColor.red
        viewBottom.backgroundColor = UIColor.red
        tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        btnFacebook.layer.cornerRadius = btnFacebook.frame.height/2
        btnYoutube.layer.cornerRadius = btnYoutube.frame.height/2
        btnAppStore.layer.cornerRadius = btnAppStore.frame.height/2
        let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        lblVersion.text = "Phiên bản " + version!
    }
    
    func loadViewLanguage() {
        let curren_language = UserDefaults.standard.string(forKey: "curren_language")
        let language = LanguageData()
        PAYMENT = language.localizedString(forKey:curren_language!,forKey: "PAYMENT")
        PROMOTION = language.localizedString(forKey:curren_language!,forKey: "PROMOTION")
        HISTORY_TITLE = language.localizedString(forKey:curren_language!,forKey: "HISTORY_TITLE")
        SHARE_AND_BONUS = language.localizedString(forKey:curren_language!,forKey: "SHARE_AND_BONUS")
        NOTIFICATION_TITLE = language.localizedString(forKey:curren_language!,forKey: "NOTIFICATION_TITLE")
        SETTINGS_TITLE = language.localizedString(forKey:curren_language!,forKey: "SETTINGS_TITLE")
        CONTACT_TITLE = language.localizedString(forKey:curren_language!,forKey: "CONTACT_TITLE")
        BE_DRIVER_TITLE = language.localizedString(forKey:curren_language!,forKey: "BE_DRIVER_TITLE")
        FEATURE_NOT_YET = language.localizedString(forKey:curren_language!,forKey: "FEATURE_NOT_YET")
        CONFIRM = language.localizedString(forKey:curren_language!,forKey: "CONFIRM")
        updateArrayMenuOptions()
    }
    
    @IBAction func btnProfile(_ sender: Any) {
        menuActionDelegate?.openSegue("profile", sender: nil)
    }
    
    @IBAction func btnfbView(_ sender: Any) {
        self.openFBApp(value: "1859176990779050")
    }
    @IBAction func btnytbView(_ sender: Any) {
        openWebsite()
    }
    @IBAction func btnstoreView(_ sender: Any) {
        openAppstore()
    }
    
    func openFBApp(value:String) {
        //2004891583090201--- 1859176990779050
        if let url = URL(string: "fb://profile/" + value ) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:],completionHandler: { (success) in
                    //print("Open fb://profile/100004415829784: \(success)")
                })
            } else {
                _ = UIApplication.shared.openURL(url)
                //print("Open fb://profile/100004415829784: \(success)")
            }
        }
        
    }
    
    func openAppstore() {
        if let url = URL(string: "itms-apps://itunes.apple.com/app/id1314786940"),
            UIApplication.shared.canOpenURL(url)
        {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    func openWebsite() {
        if let url = URL(string: "http://xelo.vn/"),
            UIApplication.shared.canOpenURL(url)
        {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
}

extension MenuViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayMenuOptions.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuCell") as! MenuCell
        cell.lblMenu?.text = arrayMenuOptions[indexPath.row]["title"]!
        cell.imgIcon.image = UIImage(named: arrayMenuOptions[indexPath.row]["icon"]!)
        return cell
    }
}

extension MenuViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let segue = openSegueArray[indexPath.row]
        menuActionDelegate?.openSegue(segue, sender: nil)
//        if segue == "payCard" {
//            let alertController = UIAlertController(title: NOTIFICATION_TITLE, message: FEATURE_NOT_YET, preferredStyle: .alert)
//            let OKAction = UIAlertAction(title: CONFIRM, style: .default) { (action:UIAlertAction!) in
//            }
//
//            alertController.addAction(OKAction)
//
//            self.present(alertController, animated: true, completion:nil)
//        }else{
//            menuActionDelegate?.openSegue(segue, sender: nil)
//        }
        
//        switch indexPath.row {
//        case 0:
//            let alertController = UIAlertController(title: "Thông báo", message: "Ứng dụng chưa hỗ trợ thanh toán thẻ", preferredStyle: .alert)
//
//            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
//            }
//
//            alertController.addAction(OKAction)
//
//            self.present(alertController, animated: true, completion:nil)
//            break
//        case 1:
//            menuActionDelegate?.openSegue("history", sender: nil)
//            break
//        case 2:
//            menuActionDelegate?.openSegue("promotion", sender: nil)
//            break
//        case 3:
//            menuActionDelegate?.openSegue("notification", sender: nil)
//            break
//        case 4:
//            menuActionDelegate?.openSegue("contact", sender: nil)
//            break
//        case 5:
//            menuActionDelegate?.openSegue("partner", sender: nil)
//            break
//        default:
//            break
//        }
    }
}
