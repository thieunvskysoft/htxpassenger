//
//  MenuCell.swift
//  NaviGo
//
//  Created by HoangManh on 10/13/17.
//  Copyright © 2017 DUMV. All rights reserved.
//

import UIKit

class MenuCell: UITableViewCell {

    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lblMenu: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        imgIcon.alpha = 0.7
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
}
