import Foundation
import UIKit

@IBDesignable class LocationViewButton: UIView{
    
    
    @IBInspectable var dragPointWidth: CGFloat = 45 {
        didSet{
            setStyle()
        }
    }
    @IBInspectable var viewborderColor: UIColor = UIColor.white {
        didSet{
            setStyle()
        }
    }
    
    @IBInspectable var dragPointColor: UIColor = UIColor.white {
        didSet{
            setStyle()
        }
    }
    
    @IBInspectable var buttonColor: UIColor = UIColor.init(red: 1, green: 1, blue: 1, alpha: 0.85) {
        didSet{
            setStyle()
        }
    }
    
    @IBInspectable var buttonText: String = "Start" {
        didSet{
            setStyle()
        }
    }
    
    @IBInspectable var locationText: String = "Unknow" {
        didSet{
            setStyle()
        }
    }
    
    @IBInspectable var imageName: UIImage = UIImage() {
        didSet{
            setStyle()
        }
    }
    
    @IBInspectable var buttonTextColor: UIColor = UIColor.black {
        didSet{
            setStyle()
        }
    }
    
    @IBInspectable var dragPointTextColor: UIColor = UIColor.black {
        didSet{
            setStyle()
        }
    }
    
    @IBInspectable var buttonUnlockedTextColor: UIColor = UIColor.white {
        didSet{
            setStyle()
        }
    }
    
    @IBInspectable var buttonCornerRadius: CGFloat = 1 {
        didSet{
            setStyle()
        }
    }
    
    @IBInspectable var buttonUnlockedText: String   = ""
    @IBInspectable var buttonUnlockedColor: UIColor = UIColor.black
    var buttonFont                                  = UIFont.systemFont(ofSize: 12)
    var locationFont                                = UIFont.boldSystemFont(ofSize: 12)
    
    
    var dragPoint            = UIView()
    var buttonLabel          = UILabel()
    var locationLabel        = UILabel()
    var imageView            = UIImageView()
    var unlocked             = false
    var layoutSet            = false
    var borderView           = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    
    override init (frame : CGRect) {
        super.init(frame : frame)
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        
    }
    
    override func layoutSubviews() {
        if !layoutSet{
            self.setUpButton()
            self.layoutSet = true
        }
    }
    
    func setStyle(){
        self.buttonLabel.text               = self.buttonText
        self.locationLabel.text             = self.locationText

        self.dragPoint.frame.size.width     = self.dragPointWidth
        self.dragPoint.backgroundColor      = self.dragPointColor
        self.backgroundColor                = self.buttonColor
        self.imageView.image                = imageName
        self.buttonLabel.textColor          = self.buttonTextColor
        
        self.dragPoint.layer.cornerRadius   = buttonCornerRadius
        self.layer.cornerRadius             = buttonCornerRadius
    }
    
    func setUpButton(){
        
        self.backgroundColor              = self.buttonColor
        
        self.dragPoint                    = UIView(frame: CGRect(x: dragPointWidth - self.frame.size.width, y: 0, width: self.frame.size.width, height: self.frame.size.height))
        self.dragPoint.backgroundColor    = dragPointColor
        self.dragPoint.layer.cornerRadius = buttonCornerRadius
        self.addSubview(self.dragPoint)
        
        if !self.buttonText.isEmpty{
            
            self.buttonLabel               = UILabel(frame: CGRect(x: self.frame.size.height + 5, y: 0, width: self.frame.size.width, height: self.frame.size.height / 2))
            self.buttonLabel.textAlignment = .left
            self.buttonLabel.text          = buttonText
            self.buttonLabel.textColor     = UIColor.white
            self.buttonLabel.font          = self.buttonFont
            self.buttonLabel.textColor     = self.buttonTextColor
            self.addSubview(self.buttonLabel)
            
            self.locationLabel               = UILabel(frame: CGRect(x: self.frame.size.height + 5, y: self.frame.size.height / 2, width: self.frame.size.width - 75, height: self.frame.size.height / 2))
            self.locationLabel.textAlignment = .left
            self.locationLabel.text          = locationText
            self.locationLabel.textColor     = UIColor.white
            self.locationLabel.font          = self.locationFont
            self.locationLabel.textColor     = self.buttonTextColor
            self.addSubview(self.locationLabel)
        }
        self.bringSubview(toFront: self.dragPoint)
        
        if self.imageName != UIImage(){
            self.imageView = UIImageView(frame: CGRect(x: self.frame.size.width - dragPointWidth, y: 0, width: self.dragPointWidth, height: self.frame.size.height))
            self.imageView.contentMode = .center
            self.imageView.image = self.imageName
            self.dragPoint.addSubview(self.imageView)
        }
        
        self.layer.masksToBounds = true
        
    }
    

    
}
