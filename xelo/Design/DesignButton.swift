//
//  DesignButton.swift
//  NaviGo
//
//  Created by HoangManh on 9/20/17.
//  Copyright © 2017 DUMV. All rights reserved.
//

import UIKit

@IBDesignable class DesignButton:UIButton{
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setView()
    }
    @IBInspectable var rounded: Bool = false {
        didSet{
            setView()
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet{
            setView()
        }
    }
    
    @IBInspectable var buttonRadius: CGFloat = 0 {
        didSet{
            setView()
        }
    }
    
    @IBInspectable var borderColor: UIColor = UIColor.clear {
        didSet{
            setView()
        }
    }
    
    func setView(){
        layer.cornerRadius = rounded ? buttonRadius : 0
        self.layer.borderWidth = borderWidth
        self.layer.borderColor = borderColor.cgColor
    }
}
