//
//  AlertView.swift
//  NaviGo
//
//  Created by HoangManh on 11/6/17.
//  Copyright © 2017 DUMV. All rights reserved.
//

import UIKit

class AlertView: UIView, alertProtocol {
    
    // Declare the protocol controls
    var backgroundView = UIView()
    var dialogView = UIView()
    var appearFrom = String()
    var clearBackground = Bool()
    
    fileprivate var imageView = UIImageView()
    fileprivate var titleLabel = UILabel()
    fileprivate var nameLabel = UILabel()
    fileprivate var starDriver = FloatRatingView()
    fileprivate var infoCar = UILabel()
    fileprivate var lblMessage = UILabel()
    fileprivate var btnDone = UIButton()
    
    convenience init(title:String, image:UIImage, name: String, star: Float, info:String, description:String) {
        self.init(frame:UIScreen.main.bounds)
        self.initialise(title:title, image:image, name:name, star:star, info:info, description:description)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func initialise(title:String, image:UIImage, name: String, star: Float, info:String, description:String) {
        
        // get dynamic width for alertView and all controlls
        let dialogViewWidth = 310
        
        backgroundView.frame = frame
        backgroundView.backgroundColor = UIColor.black
        backgroundView.alpha = 0.6
        addSubview(backgroundView)
        
        titleLabel.frame = CGRect(x: 10, y: 15, width: CGFloat(dialogViewWidth) + 8, height: 30)
        titleLabel.text = title
        titleLabel.font = UIFont.boldSystemFont(ofSize: 20)
        titleLabel.textAlignment = .center
        titleLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        titleLabel.numberOfLines = 0
        titleLabel.sizeToFit()
        dialogView.addSubview(titleLabel)
        
        imageView.frame.origin = CGPoint(x: 20, y: titleLabel.frame.origin.y + titleLabel.frame.size.height + 8)
        imageView.frame.size = CGSize(width: 70 , height: 70)
        imageView.image = image
        imageView.layer.cornerRadius = imageView.frame.size.height/2
        imageView.clipsToBounds = true
        imageView.alpha = 0.9
        imageView.contentMode = .scaleAspectFit
        dialogView.addSubview(imageView)
        
        nameLabel.frame = CGRect(x: 10, y: Int(imageView.frame.origin.y + imageView.frame.size.height + 8 + 10), width: Int(CGFloat(dialogViewWidth) + 8), height: 30)
        nameLabel.text = name
        nameLabel.font = UIFont.systemFont(ofSize: 20)
        nameLabel.textAlignment = .center
        nameLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        nameLabel.numberOfLines = 0
        nameLabel.sizeToFit()
        dialogView.addSubview(nameLabel)
        
        starDriver.frame = CGRect(x: 10, y: Int(nameLabel.frame.origin.y + nameLabel.frame.size.height + 8 + 10), width: 100, height: 20)
        starDriver.fullImage = #imageLiteral(resourceName: "StarFull")
        starDriver.emptyImage = #imageLiteral(resourceName: "StarEmpty")
        starDriver.rating = Double(star)
        dialogView.addSubview(starDriver)
        
        infoCar.frame = CGRect(x: 17, y: Int(starDriver.frame.origin.y + starDriver.frame.size.height + 8 + 10), width: dialogViewWidth - 30, height: 30)
        infoCar.numberOfLines = 0
        infoCar.lineBreakMode = NSLineBreakMode.byWordWrapping
        infoCar.font = UIFont.systemFont(ofSize: 18)
        infoCar.textAlignment = NSTextAlignment.center
        infoCar.textColor = UIColor.darkGray
        infoCar.text = info
        infoCar.sizeToFit()
        dialogView.addSubview(infoCar)
        
        lblMessage.frame = CGRect(x: 17, y: Int(infoCar.frame.origin.y + infoCar.frame.size.height + 8 + 10), width: dialogViewWidth - 30, height: 50)
        lblMessage.numberOfLines = 0
        lblMessage.lineBreakMode = NSLineBreakMode.byWordWrapping
        lblMessage.font = UIFont.systemFont(ofSize: 18)
        lblMessage.textAlignment = NSTextAlignment.center
        lblMessage.textColor = UIColor.darkGray
        lblMessage.text = description
        lblMessage.sizeToFit()
        dialogView.addSubview(lblMessage)
        
        // set dynamic height for alert view and their controls
        let dialogViewHeight = titleLabel.frame.height  + imageView.frame.height  + nameLabel.frame.height  + starDriver.frame.height + infoCar.frame.height + lblMessage.frame.height + 110
        dialogView.frame.origin = CGPoint(x: 32, y: frame.height)
        dialogView.frame.size = CGSize(width: CGFloat(dialogViewWidth), height: dialogViewHeight)
        dialogView.backgroundColor = UIColor.clear
        self.createGradientLayer(view: dialogView, colorOne: UIColor.white, colorTwo: UIColor.white)
        titleLabel.center.x = CGFloat(dialogView.frame.size.width / 2)
        imageView.center.x = CGFloat(dialogView.frame.size.width / 2)
        nameLabel.center.x = CGFloat(dialogView.frame.size.width / 2)
        starDriver.center.x = CGFloat(dialogView.frame.size.width / 2)
        infoCar.center.x = CGFloat(dialogView.frame.size.width / 2)
        lblMessage.center.x = CGFloat(dialogView.frame.size.width / 2)
        dialogView.layer.cornerRadius = 10
        dialogView.clipsToBounds = true
        addSubview(dialogView)
        btnDone = UIButton(frame: frame)
        btnDone.addTarget(self, action: #selector(didButtonTapped), for: UIControlEvents.touchUpInside)
        addSubview(btnDone)
        
    }
    
    // dismiss the alert view
    @objc func didButtonTapped() {
        dismiss(animated: true)
    }
    
    // set gradient colors
    func createGradientLayer(view:UIView, colorOne:UIColor, colorTwo:UIColor) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = view.bounds
        gradientLayer.colors = [colorOne.cgColor, colorTwo.cgColor]
        view.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    // set shadow of controls
    func createShadow(view:UIView, color:UIColor, Offset:CGSize, opacity:Float, radious:Float){
        view.layer.shadowColor = color.cgColor
        view.layer.shadowOffset = Offset
        view.layer.shadowOpacity = opacity
        view.layer.shadowRadius = CGFloat(radious)
    }
}

