//
//  LoadingButton.swift
//  NaviGo
//
//  Created by HoangManh on 10/21/17.
//  Copyright © 2017 DUMV. All rights reserved.
//

import UIKit

//
// MARK: Initialize and default setup
//
@IBDesignable
public class LoadingButton: UIButton {
    private var indicatorView: UIActivityIndicatorView = UIActivityIndicatorView()
    
    @IBInspectable public var disabledTextColor: UIColor! = UIColor.white
    @IBInspectable public var enabledTextColor: UIColor! = UIColor.white {
        didSet {
            self.setTitleColor(self.enabledTextColor, for: .normal)
        }
    }
    
    @IBInspectable public var disabledBackgroundColor: UIColor! = UIColor(red: 0, green: 131.0 / 255.0, blue: 191.0 / 255.0, alpha: 0)
    @IBInspectable public var enabledBackgroundColor: UIColor! = UIColor(red: 0, green: 152.0 / 255.0, blue: 215.0 / 255.0, alpha: 0) {
        didSet {
            self.backgroundColor = self.enabledBackgroundColor
        }
    }
    
    
    @IBInspectable public var indicatorColor: UIColor! = UIColor.lightGray {
        didSet {
            self.indicatorView.color = self.indicatorColor
        }
    }
    
    @IBInspectable public var normalText: String! = "" {
        didSet {
            self.setTitle(self.normalText, for: .normal)
        }
    }
    @IBInspectable public var loadingText: String! = "Đang xử lý..."
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.commonInit()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.commonInit()
    }
    
    private func commonInit() {
        self.initIndicatorView()
        self.initView()
    }
    
    private func initIndicatorView() {

        self.indicatorView.color = self.indicatorColor
        self.indicatorView.backgroundColor = self.enabledBackgroundColor
        self.indicatorView.color = UIColor.white
        self.indicatorView.isHidden = true
    }
    
    private func initView() {
        self.backgroundColor = self.enabledBackgroundColor
        self.setTitleColor(self.enabledTextColor, for: .normal)
        self.setTitle(self.normalText, for: .normal)
    }
    
    private func setupIndicatorView() {
        self.indicatorView.removeFromSuperview()
        self.indicatorView.frame = self.calculateIndicatorViewFrame()
        self.indicatorView.backgroundColor = self.disabledBackgroundColor
        
        self.addSubview(indicatorView)
        
        if !self.indicatorView.isHidden {
            self.indicatorView.startAnimating()
        }
    }
    
    private func calculateIndicatorViewFrame() -> CGRect {
        let padding: CGFloat = 5
        var y = padding
        var height = self.frame.height - (padding * 2)
        var width = height
        var x = (self.frame.width / 2) - (width / 2)
        
        if let label = self.titleLabel {
            height = label.frame.height - 5
            width = height
            x = label.frame.origin.x - (width + padding)
            y = label.frame.origin.y +  ((label.frame.height - height) / 2.0)
            return CGRect(x: x, y: y, width: width, height: height)

        }
        
        return CGRect(x: x, y: y, width: width, height: height)
    }
    
    func startAnimatingIndicatorView() {
        self.indicatorView.isHidden = false
        self.indicatorView.startAnimating()
    }
    
    func stopAnimatingIndicatorView() {
        self.indicatorView.isHidden = true
        self.indicatorView.stopAnimating()
    }
    
    override public func draw(_ rect: CGRect) {
        self.setupIndicatorView()
        
        super.draw(rect)
    }
}

//
// MARK: Public methods and properties
//
extension LoadingButton {
    public func disable() {
        self.setTitle(self.loadingText, for: .normal)
        self.backgroundColor = self.disabledBackgroundColor
        self.setTitleColor(self.disabledTextColor, for: .normal)
        self.isUserInteractionEnabled = false
    }
    
    public func enable() {
        self.setTitle(self.normalText, for: .normal)
        self.backgroundColor = self.enabledBackgroundColor
        self.setTitleColor(self.enabledTextColor, for: .normal)
        self.isUserInteractionEnabled = true
    }
    
    public func startAnimating() {
        self.disable()
        self.startAnimatingIndicatorView()
    }
    
    public func stopAnimating() {
        self.enable()
        self.stopAnimatingIndicatorView()
    }
}
