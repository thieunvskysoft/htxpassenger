import Foundation
import UIKit

@IBDesignable class DistanceView: UIView{
    
    
    @IBInspectable var viewHeight: CGFloat = 180 {
        didSet{
            setStyle()
        }
    }
    @IBInspectable var viewborderColor: UIColor = UIColor.white {
        didSet{
            setStyle()
        }
    }
    
    @IBInspectable var bgPointColor: UIColor = UIColor.white {
        didSet{
            setStyle()
        }
    }
    
    
    @IBInspectable var locationStart: String = "Unknow" {
        didSet{
            setStyle()
        }
    }
    @IBInspectable var locationEnd: String = "Unknow" {
        didSet{
            setStyle()
        }
    }
    @IBInspectable var startPoint: UIImage = UIImage() {
        didSet{
            setStyle()
        }
    }
    
    @IBInspectable var endPoint: UIImage = UIImage() {
        didSet{
            setStyle()
        }
    }
    @IBInspectable var lblTextColor: UIColor = UIColor.black {
        didSet{
            setStyle()
        }
    }
    
    @IBInspectable var valueTextColor: UIColor = UIColor.black {
        didSet{
            setStyle()
        }
    }
    
    @IBInspectable var viewCornerRadius: CGFloat = 1 {
        didSet{
            setStyle()
        }
    }
    
    @IBInspectable var bAmuont: Bool = false {
        didSet{
            setStyle()
        }
    }
    var priceFont                                  = UIFont.boldSystemFont(ofSize: 14)
    var locationFont                                = UIFont.systemFont(ofSize: 14)
    
    
    var viewLocaPoint        = UIView()
    var startLabel           = UILabel()
    var endLabel             = UILabel()
    var imageStart           = UIImageView()
    var imageEnd             = UIImageView()
    var unlocked             = false
    var layoutSet            = false
    var borderView           = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    
    override init (frame : CGRect) {
        super.init(frame : frame)
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        
    }
    
    override func layoutSubviews() {
        if !layoutSet{
            self.setUpview()
            self.layoutSet = true
        }
    }
    
    func setStyle(){
        
        //self.viewLocaPoint.frame.size.width     = self.viewWidth
        self.viewLocaPoint.backgroundColor  = self.bgPointColor
        self.backgroundColor                = self.bgPointColor
        self.imageStart.image               = self.startPoint
        self.imageEnd.image                 = self.endPoint
        self.startLabel.textColor           = self.valueTextColor
        self.endLabel.textColor             = self.valueTextColor
    }
    
    func setUpview(){
        
        self.backgroundColor                  = self.bgPointColor
        self.viewLocaPoint                    = UIView(frame: CGRect(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height))
        self.viewLocaPoint.backgroundColor    = bgPointColor
        self.addSubview(self.viewLocaPoint)
        
        if self.imageStart != UIImage(){
            self.imageStart = UIImageView(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
            self.imageStart.contentMode = .center
            self.imageStart.image = self.startPoint
            self.viewLocaPoint.addSubview(self.imageStart)
        }
        if self.imageEnd != UIImage(){
            self.imageEnd = UIImageView(frame: CGRect(x: 0, y: 35, width: 30, height: 30))
            self.imageEnd.contentMode = .center
            self.imageEnd.image = self.endPoint
            self.viewLocaPoint.addSubview(self.imageEnd)
        }
        self.startLabel               = UILabel(frame: CGRect(x: imageStart.frame.width + 5, y: 0, width: self.frame.size.width - 40, height: 20))
        self.startLabel.textAlignment = .left
        self.startLabel.text          = self.locationStart
        self.startLabel.font          = self.locationFont
        self.startLabel.textColor     = self.valueTextColor
        //self.startLabel.sizeToFit()
        self.startLabel.center.y = self.imageStart.center.y
        self.viewLocaPoint.addSubview(self.startLabel)
        
        self.endLabel               = UILabel(frame: CGRect(x: imageStart.frame.width + 5, y: 35, width: self.frame.size.width - 40, height: 20))
        self.endLabel.textAlignment = .left
        self.endLabel.text          = self.locationEnd
        self.endLabel.font          = self.locationFont
        self.endLabel.textColor     = self.valueTextColor
        //self.endLabel.sizeToFit()
        self.endLabel.center.y = self.imageEnd.center.y
        self.viewLocaPoint.addSubview(self.endLabel)
        self.bringSubview(toFront: self.viewLocaPoint)
        
        self.layer.masksToBounds = true
        
    }
    
    
    
}


