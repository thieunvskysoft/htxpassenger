//
//  DesignTextField.swift
//  NaviGo
//
//  Created by HoangManh on 9/20/17.
//  Copyright © 2017 DUMV. All rights reserved.
//

import UIKit

@IBDesignable class DesignTextField:UITextField{
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setView()
    }
    @IBInspectable var cornerRadius: CGFloat = 25 {
        didSet{
            setView()
        }
    }
    @IBInspectable var leftImage:UIImage? {
        didSet{
            setView()
        }
    }
    
    @IBInspectable var leftPadding: CGFloat = 0 {
        didSet{
            setView()
        }
    }
    
    @IBInspectable var borderText: UIColor = UIColor.clear {
        didSet{
            setView()
        }
    }
    
    @IBInspectable var placeholderText: UIColor = UIColor.clear {
        didSet{
            setView()
        }
    }
    func setView(){
        if let image = leftImage {
            leftViewMode = .always
            let imageView = UIImageView(frame: CGRect(x: 10, y: 0, width: 20, height: 20))
            imageView.image = image
            imageView.tintColor = tintColor
            var width = leftPadding + 20
            if borderStyle == UITextBorderStyle.none || borderStyle == UITextBorderStyle.line {
                width = width + 5
            }
            let view = UIView(frame: CGRect(x: 0, y: 0, width: width, height: 20))
            view.addSubview(imageView)
            leftView = view
            
        }
        else{
            leftViewMode = .never
        }
        attributedPlaceholder = NSAttributedString(string: placeholder != nil ? placeholder! :"" , attributes: [NSAttributedStringKey.foregroundColor: placeholderText])
        self.layer.borderWidth = 0.6
        self.layer.borderColor = borderText.cgColor
    }
}
