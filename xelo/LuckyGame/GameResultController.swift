//
//  GameResultController.swift
//  xelo
//
//  Created by Nguyen Thieu on 7/17/19.
//  Copyright © 2019 DUMV. All rights reserved.
//

import UIKit

class GameResultController: UIViewController,UIWebViewDelegate {
    var gameInfo = LuckyGame()
    var webview: UIWebView!
    @IBOutlet weak var viewContent: UIView!
    
    @IBOutlet weak var scrollViewContent: UIScrollView!
    @IBOutlet weak var viewResult: UIView!
    @IBOutlet weak var heightResult: NSLayoutConstraint!
    @IBOutlet weak var viewAnswer: UIView!
    @IBOutlet weak var heightTblAnswer: NSLayoutConstraint!
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var heightTblTop: NSLayoutConstraint!
    @IBOutlet weak var viewImage: UIView!
    @IBOutlet weak var imageGame: UIImageView!
    @IBOutlet weak var webViewGame: UIWebView!
    @IBOutlet weak var imgPassenger: UIImageView!
    @IBOutlet weak var namePassenger: UILabel!
    @IBOutlet weak var numberNoPassenger: UILabel!
    @IBOutlet weak var lblLuckyNo: UILabel!
    @IBOutlet weak var tblAnswer: UITableView!
    @IBOutlet weak var tblTop: UITableView!
    var arrAnswer = [Answers]()
    
    @IBOutlet weak var heightViewImg: NSLayoutConstraint!
    @IBOutlet weak var lblGame: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        imgPassenger.layer.cornerRadius = 35
        imgPassenger.clipsToBounds = true
        imgPassenger.image = DataNavigo.loginResult.avatarFace ?? UIImage(named: "Menu_Avt")
        namePassenger.text = DataNavigo.passenger.nameUser
        numberNoPassenger.text = DataNavigo.passenger.numberphoneUser.replacingOccurrences(of: "+84", with: "0")
        let url:URL = URL(string: gameInfo.imageUrl!)!
        let data:Data = try! Data(contentsOf: url as URL)
        imageGame.image = UIImage(data: data as Data)
        tblTop.isScrollEnabled = false
        tblAnswer.isScrollEnabled = false
        
        tblTop.separatorStyle = UITableViewCellSeparatorStyle.none
        tblAnswer.separatorStyle = UITableViewCellSeparatorStyle.none
        tblAnswer.delegate = self
        tblAnswer.dataSource = self
        tblTop.delegate = self
        tblTop.dataSource = self

    }
    override func viewWillAppear(_ animated: Bool) {
        let backButton = UIBarButtonItem()
        backButton.title = " "
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        navigationController?.navigationBar.tintColor = .white
        if gameInfo.arrAnswers.count > 0 {
            navigationItem.title = "Kết quả game"
        }else{
            navigationItem.title = "Dự đoán của bạn"
        }
        
        //navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor :UIColor.white]
        
        loadLuckyNo()
        loadAnswer()
        loadTop()
        loadWebview()
        lblGame.text = ""
        heightViewImg.constant = CGFloat(250)
        viewImage.layoutIfNeeded()
    }

    func loadWebview(){
        //webViewPreface = UIWebView(frame: UIScreen.main.bounds)
        webViewGame.delegate = self
        //webView.addSubview(btnJoinGame)
        //view.addSubview(webView)
        if let url = URL(string: Constant.URL_GAME_XELO + "?tokenID="+DataNavigo.loginResult.tokenID) {
            let request = URLRequest(url: url)
            webViewGame.loadRequest(request)
        }
        webViewGame.scrollView.isScrollEnabled = false
    }
    func loadLuckyNo() {
        if gameInfo.luckyNo != "-1" {
            lblLuckyNo.text = gameInfo.answer! + " - " + gameInfo.luckyNo!
        }else{
            heightResult.constant = CGFloat(0)
            viewResult.layoutIfNeeded()
        }
    }
    func loadAnswer() {
        if arrAnswer.count > 0 {
            let height = arrAnswer.count * 40 + 50
            heightTblAnswer.constant = CGFloat(height)
            viewAnswer.layoutIfNeeded()
        }else{
            heightTblAnswer.constant = CGFloat(0)
            viewAnswer.layoutIfNeeded()
        }
    }
    func loadTop() {
        if gameInfo.arrAnswers.count > 0 {
            let height = gameInfo.arrAnswers.count * 40 + 50
            heightTblTop.constant = CGFloat(height)
            viewTop.layoutIfNeeded()
        }else{
            heightTblTop.constant = CGFloat(0)
            viewTop.layoutIfNeeded()
        }
    }
    
//    let height = gameAsw.count * 40 + 1
//    
//    self.heightTblAnswer.constant = CGFloat(height)
//    self.tblAnswer.layoutIfNeeded()
//    
//    self.arrAnswer = gameAsw
//    self.tblAnswer.reloadData()
}
extension GameResultController: UITableViewDelegate, UITableViewDataSource {
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if tableView == tblAnswer {
            return  arrAnswer.count
        }else {
            return  gameInfo.arrAnswers.count
        }
        
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if tableView == tblAnswer {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AnswerCell", for: indexPath) as! AnswerCell
            let game:Answers = arrAnswer[indexPath.row]
            cell.lblDatetime.text = game.updateDate
            cell.lblAnswer.text = game.answer! + " - " + game.luckyNo!
            
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AnswerCell", for: indexPath) as! AnswerCell
            let game:Answers = gameInfo.arrAnswers[indexPath.row]
            cell.lblDatetime.text = game.updateDate
            cell.lblAnswer.text = game.fullName
            return cell
        }

    }
}

