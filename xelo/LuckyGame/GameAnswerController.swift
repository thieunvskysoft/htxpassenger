//
//  GameAnswerController.swift
//  xelo
//
//  Created by Nguyen Thieu on 7/16/19.
//  Copyright © 2019 DUMV. All rights reserved.
//

import UIKit

class GameAnswerController: UIViewController {

    @IBOutlet weak var passengerImage: UIImageView!
    @IBOutlet weak var passengerName: UILabel!
    @IBOutlet weak var passengerMobileNo: UILabel!
    @IBOutlet weak var passengerGameCode: UILabel!
    @IBOutlet weak var viewInfo: UIView!
    @IBOutlet weak var viewPassenger: UIView!
    var gameID:Int = 0
    var arrAnswer = [Answers]()
    
    @IBOutlet weak var tblAnswer: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        passengerImage.layer.cornerRadius = 35
        passengerImage.clipsToBounds = true
        passengerImage.image = DataNavigo.loginResult.avatarFace ?? UIImage(named: "Menu_Avt")
        passengerName.text = DataNavigo.passenger.nameUser
        passengerMobileNo.text = DataNavigo.passenger.numberphoneUser.replacingOccurrences(of: "+84", with: "0")
        tblAnswer.delegate = self
        tblAnswer.dataSource = self
        tblAnswer.separatorStyle = UITableViewCellSeparatorStyle.none
        tblAnswer.isScrollEnabled = false
        getLuckyGame()
        passengerGameCode.text = gameID.description
        viewPassenger.layer.cornerRadius = 5
        viewInfo.layer.cornerRadius = 5
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let backButton = UIBarButtonItem()
        backButton.title = " "
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        navigationController?.navigationBar.tintColor = .white
        navigationItem.title = "Dự đoán của bạn"
        //navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor :UIColor.white]
        //luckyGameAnswer()
    }
    
    func getLuckyGame() {
        let CompletionHandler: ((Array<Answers>,NavigoError?) -> Void) = {
            gameAsw,navigoError in
            DispatchQueue.main.async {
                if(navigoError == nil){
                    self.arrAnswer = gameAsw
                    self.tblAnswer.reloadData()
                }else{
                    if(navigoError?.errorCode == Errorcode.ERROR_NO_INTERNET){
                        //self.errorNoInternet()
                        print("Lỗi ")
                    }else if (navigoError?.errorCode == Errorcode.NAVIGO_SESSION_EXPIRED){
                    }
                }
            }
        }
        let connector = APIXeloGame();
        connector.luckyGameAnswer(gameID: gameID, CompletionHandler: CompletionHandler)
    }
}

extension GameAnswerController: UITableViewDelegate, UITableViewDataSource {
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return  arrAnswer.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AnswerCell", for: indexPath) as! AnswerCell
        let aws:Answers = arrAnswer[indexPath.row]
        cell.lblDatetime.text = aws.updateDate
        cell.lblAnswer.text = aws.answer! + " - " + aws.luckyNo!
        return cell
}
}
