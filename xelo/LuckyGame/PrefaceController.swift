//
//  PrefaceController.swift
//  xelo
//
//  Created by Nguyen Thieu on 7/5/19.
//  Copyright © 2019 DUMV. All rights reserved.
//

import UIKit

class PrefaceController: UIViewController, UIWebViewDelegate {
    
    @IBOutlet weak var webViewPreface: UIWebView!
    @IBOutlet weak var btnJoinGame: UIButton!
    var game = LuckyGame()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        var image = UIImage(named: "more-button")
        image = image?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        //self.navigationItem.rightBarButtonItem = UIBarButtonItem(image:image , style: UIBarButtonItemStyle.plain, target: self, action: #selector(loadviewHistory))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(loadviewHistory))
        self.btnJoinGame.alpha = 0
        btnJoinGame.layer.cornerRadius = 5
        btnJoinGame.backgroundColor = NaviColor.naviBtnOrange
        self.navigationController?.isNavigationBarHidden = false
        let backButton = UIBarButtonItem()
        backButton.title = " "
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        navigationController?.navigationBar.tintColor = .white
        navigationItem.title = "XELO GAME"
        _ = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        loadWebview()
        getLuckyGame()
    }
    
    func loadWebview(){
        //webViewPreface = UIWebView(frame: UIScreen.main.bounds)
        webViewPreface.delegate = self
        //webView.addSubview(btnJoinGame)
        //view.addSubview(webView)
        if let url = URL(string: Constant.URL_GAME_XELO + "?tokenID="+DataNavigo.loginResult.tokenID) {
            let request = URLRequest(url: url)
            webViewPreface.loadRequest(request)
        }
        //webViewPreface.scalesPageToFit = true
        
    }
    
    @objc func loadviewHistory() {
        let sb = UIStoryboard(name: "GameXelo", bundle: nil)
        let xelogame = sb.instantiateViewController(withIdentifier: "GameHistoryController") as! GameHistoryController
        self.navigationController?.pushViewController(xelogame, animated: true)
    }
    
    @IBAction func btnJoingame(_ sender: Any) {
        let sb = UIStoryboard(name: "GameXelo", bundle: nil)
        let xelogame = sb.instantiateViewController(withIdentifier: "GameInfoController") as! GameInfoController
        xelogame.gameInfo = game
        self.navigationController?.pushViewController(xelogame, animated: true)
    }
    
    func getLuckyGame() {
        let CompletionHandler: ((LuckyGame,NavigoError?) -> Void) = {
            gameInfo,navigoError in
            DispatchQueue.main.async {
                if(navigoError == nil){
                    if(gameInfo.gameID != 0){
                        self.btnJoinGame.alpha = 1
                        self.game = gameInfo
                    }else{
                        self.btnJoinGame.alpha = 0
                    }
                    
                }else{
                    if(navigoError?.errorCode == Errorcode.ERROR_NO_INTERNET){
                        //self.errorNoInternet()
                        print("Lỗi ")
                    }else if (navigoError?.errorCode == Errorcode.NAVIGO_SESSION_EXPIRED){
                    }
                }
            }
        }
        let connector = APIXeloGame();
        connector.getLuckyGame(CompletionHandler: CompletionHandler)
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        let scrollableSize = CGSize(width: view.frame.size.width, height: webView.scrollView.contentSize.height)
        self.webViewPreface?.scrollView.contentSize = scrollableSize
    }
    
}
