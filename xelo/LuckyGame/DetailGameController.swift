//
//  DetailGameController.swift
//  xelo
//
//  Created by Nguyen Thieu on 7/5/19.
//  Copyright © 2019 DUMV. All rights reserved.
//

import UIKit

class DetailGameController: UIViewController {
    @IBOutlet weak var passengerImage: UIImageView!
    @IBOutlet weak var passengerName: UILabel!
    @IBOutlet weak var passengerMobileNo: UILabel!
    @IBOutlet weak var pasengerAnswer: UILabel!
    @IBOutlet weak var passengerLuckyNo: UILabel!
    @IBOutlet weak var passengerGameCode: UILabel!
    @IBOutlet weak var viewInfo: UIView!
    @IBOutlet weak var viewPassenger: UIView!
    @IBOutlet weak var viewAnswer: UIView!
    @IBOutlet weak var imgGame: UIImageView!
    var gameInfo = LuckyGame()
    var answer : String = ""
    var luckyNo : String = ""
    @IBOutlet weak var btnAccept: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        passengerImage.layer.cornerRadius = 35
        passengerImage.clipsToBounds = true
        passengerImage.image = DataNavigo.loginResult.avatarFace  ?? UIImage(named: "Menu_Avt")
        passengerName.text = DataNavigo.passenger.nameUser
        passengerMobileNo.text = DataNavigo.passenger.numberphoneUser.replacingOccurrences(of: "+84", with: "0")

        let url:URL = URL(string: gameInfo.imageUrl!)!
        let data:Data = try! Data(contentsOf: url as URL)
        imgGame.image = UIImage(data: data as Data)
        viewInfo.layer.cornerRadius = 5
        viewPassenger.layer.cornerRadius = 5
        imgGame.layer.cornerRadius = 5
        btnAccept.layer.cornerRadius = 5
        pasengerAnswer.text = answer
        passengerLuckyNo.text = luckyNo
        passengerGameCode.text = gameInfo.gameID?.description
        btnAccept.backgroundColor = NaviColor.naviBtnOrange
    }
    override func viewWillAppear(_ animated: Bool) {
        let backButton = UIBarButtonItem()
        backButton.title = " "
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        navigationController?.navigationBar.tintColor = .white
        navigationItem.title = "Thông tin kết quả"
        //navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor :UIColor.white]
    }

    func inputGameAnswer(answer:String, luckyNo:String) {
        let CompletionHandler: ((String,NavigoError?) -> Void) = {
            actionResult,navigoError in
            DispatchQueue.main.async {
                if(navigoError == nil){
                    let viewControllers:[UIViewController] = (self.navigationController?.viewControllers)!;
                    for controller in viewControllers {
                        if controller is HomeController {
                            self.navigationController!.popToViewController(controller, animated: true)
                        }
                    }
                }else{
                    if(navigoError?.errorCode == Errorcode.ERROR_NO_INTERNET){
                        //self.errorNoInternet()
                        print("Lỗi ")
                    }else if (navigoError?.errorCode == Errorcode.NAVIGO_SESSION_EXPIRED){
                    }
                }
            }
        }
        let connector = APIXeloGame();
        connector.inputGameAnswer(gameID: gameInfo.gameID!, answerValue: answer, luckyNo: luckyNo, CompletionHandler: CompletionHandler)
    }
    
    
    @IBAction func acceptAnswer(_ sender: Any) {
        inputGameAnswer(answer: answer, luckyNo: luckyNo)
    }
    
}
