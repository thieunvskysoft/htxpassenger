//
//  GameInfoController.swift
//  xelo
//
//  Created by Nguyen Thieu on 7/5/19.
//  Copyright © 2019 DUMV. All rights reserved.
//

import UIKit

class GameInfoController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var imgPassenger: UIImageView!
    @IBOutlet weak var passengerName: UILabel!
    @IBOutlet weak var passengerMobileNumber: UILabel!
    @IBOutlet weak var imgGame: UIImageView!
    @IBOutlet weak var txtAnswer: UITextField!
    @IBOutlet weak var txtLuckyNo: UITextField!
    @IBOutlet weak var btnSend: UIButton!
    var gameInfo = LuckyGame()
    var const = 0
    @IBOutlet weak var viewPassenger: UIView!
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var viewTitle: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imgPassenger.layer.cornerRadius = 35
        imgPassenger.clipsToBounds = true
        imgPassenger.image = DataNavigo.loginResult.avatarFace ?? UIImage(named: "Menu_Avt")
        passengerName.text = DataNavigo.passenger.nameUser
        passengerMobileNumber.text = DataNavigo.passenger.numberphoneUser.replacingOccurrences(of: "+84", with: "0")
        const = checkConst()
        let url:URL = URL(string: gameInfo.imageUrl!)!
        let data:Data = try! Data(contentsOf: url as URL)
        imgGame.image = UIImage(data: data as Data)
        viewPassenger.layer.cornerRadius = 5
        imgGame.layer.cornerRadius = 5
        btnSend.layer.cornerRadius = 5
        btnSend.backgroundColor = NaviColor.naviBtnOrange
        viewTitle.layer.cornerRadius = 5
        var image = UIImage(named: "more-button")
        image = image?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        //self.navigationItem.rightBarButtonItem = UIBarButtonItem(image:image , style: UIBarButtonItemStyle.plain, target: self, action: #selector(loadDetailGame))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(loadDetailGame))
        self.addDoneButtonOnKeyboard()
        NotificationCenter.default.addObserver(self, selector: #selector(GameInfoController.keyboardDidShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(GameInfoController.keyboardWillBeHidden(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        let backButton = UIBarButtonItem()
        backButton.title = " "
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        navigationController?.navigationBar.tintColor = .white
        navigationItem.title = "Số may mắn"
        //navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor :UIColor.white]
        //luckyGameAnswer()
    }
    
    func checkConst() -> Int {
        var const = 0
        switch UIScreen.main.nativeBounds.height {
        case 1136:
            print("===iPhone 5 or 5S or 5C")
        case 1334:
            print("===iPhone 6/6S/7/8--667")
        case 2208:
            print("===iPhone 6+/6S+/7+/8+---736")
        case 2436:
            print("===iPhone X---812")
            const = 35
        case 2688:
            print("===iPhone XS Max---896")
            const = 35
        case 1792:
            print("===iPhone XR ---896")
            const = 35
        default:
            print("===unknown")
        }
        return const
    }
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 45))
        doneToolbar.barStyle = UIBarStyle.default
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(GameInfoController.doneButtonAction))
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        
        doneToolbar.items = items as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        
        self.txtAnswer.inputAccessoryView = doneToolbar
        self.txtLuckyNo.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction()
    {
        self.txtAnswer.resignFirstResponder()
        self.txtLuckyNo.resignFirstResponder()
    }
    
    @objc func keyboardDidShow(_ notification: NSNotification) {
        let keyBoard = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as! NSValue
        let sizeKey = keyBoard.cgRectValue
        let heightKey = sizeKey.height - CGFloat(const)
        bottomConstraint.constant = heightKey
        UIView.animate(withDuration: 2) {
            self.viewBottom.layoutSubviews()
        }
    }
    
    @IBAction func textfieldEdit(_ sender: Any) {
        let txt:UITextField = sender as! UITextField
        txt.delegate = self
        
    }
    
    @objc func keyboardWillBeHidden(_ notification: NSNotification) {
        self.bottomConstraint.constant = 0
        UIView.animate(withDuration: 2) {
            self.viewBottom.layoutSubviews()
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
            NotificationCenter.default.addObserver(self, selector: #selector(SignInController.keyboardDidShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        } else {
            textField.resignFirstResponder()
            return true;
        }
        return false
    }
    
    
    @objc func loadDetailGame() {
        let sb = UIStoryboard(name: "GameXelo", bundle: nil)
        let xelogame = sb.instantiateViewController(withIdentifier: "GameAnswerController") as! GameAnswerController
        xelogame.gameID = gameInfo.gameID!
        self.navigationController?.pushViewController(xelogame, animated: true)
    }
    
    @IBAction func btnSend(_ sender: Any) {
        if txtAnswer.text == "" {
            showMessage(message: "Bạn chưa trả lời câu hỏi.")
        }else if txtLuckyNo.text == "" {
            showMessage(message: "Bạn chưa chọn số may mắn.")
        }else{
            inputGameAnswer(answer: txtAnswer.text!, luckyNo: txtLuckyNo.text!)
        }
    }
    
    func showMessage(message:String) {
        let alert:UIAlertController = UIAlertController(title: "Thông báo", message: message, preferredStyle: UIAlertControllerStyle.alert)
        let btnOk:UIAlertAction = UIAlertAction(title: "Xác nhận", style: .destructive) { (btnOk) in
        }
        alert.addAction(btnOk)
        present(alert, animated: true, completion: nil)
    }
    
    func inputGameAnswer(answer:String, luckyNo:String) {
        let CompletionHandler: ((String,NavigoError?) -> Void) = {
            actionResult,navigoError in
            DispatchQueue.main.async {
                if(navigoError == nil){
                    let sb = UIStoryboard(name: "GameXelo", bundle: nil)
                    let xelogame = sb.instantiateViewController(withIdentifier: "DetailGameController") as! DetailGameController
                    xelogame.gameInfo = self.gameInfo
                    xelogame.answer = self.txtAnswer.text!
                    xelogame.luckyNo = self.txtLuckyNo.text!
                    self.navigationController?.pushViewController(xelogame, animated: true)
                }else{
                    if(navigoError?.errorCode == Errorcode.ERROR_NO_INTERNET){
                        //self.errorNoInternet()
                        print("Lỗi ")
                    }else if (navigoError?.errorCode == Errorcode.NAVIGO_SESSION_EXPIRED){
                    }
                }
            }
        }
        let connector = APIXeloGame();
        connector.inputGameAnswer(gameID: gameInfo.gameID!, answerValue: answer, luckyNo: luckyNo, CompletionHandler: CompletionHandler)
    }
}
