//
//  GameHistoryController.swift
//  xelo
//
//  Created by Nguyen Thieu on 7/4/19.
//  Copyright © 2019 DUMV. All rights reserved.
//

import UIKit

class GameHistoryController: UIViewController {

    var arrGame = [LuckyGame]()
    
    @IBOutlet weak var tblGameList: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tblGameList.delegate = self
        tblGameList.dataSource = self
        tblGameList.separatorStyle = UITableViewCellSeparatorStyle.none
        getHistoryLuckyGame()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let backButton = UIBarButtonItem()
        backButton.title = " "
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        navigationController?.navigationBar.tintColor = .white
        navigationItem.title = "Lịch sử game"
        //navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor :UIColor.white]
        tblGameList.reloadData()
        //        let search = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(searchDetail))
        //        navigationItem.rightBarButtonItems = [search]
    }
    
    func getHistoryLuckyGame() {
        let CompletionHandler: ((Array<LuckyGame>,NavigoError?) -> Void) = {
            gameInfo,navigoError in
            DispatchQueue.main.async {
                if(navigoError == nil){
                    self.arrGame = gameInfo
                    self.tblGameList.reloadData()
                }else{
                    if(navigoError?.errorCode == Errorcode.ERROR_NO_INTERNET){
                        //self.errorNoInternet()
                        print("Lỗi ")
                    }else if (navigoError?.errorCode == Errorcode.NAVIGO_SESSION_EXPIRED){
                    }
                }
            }
        }
        let connector = APIXeloGame();
        connector.listHistoryGame(CompletionHandler: CompletionHandler)
    }
    
    func getLuckyGame(game:LuckyGame) {
        let CompletionHandler: ((Array<Answers>,NavigoError?) -> Void) = {
            gameAsw,navigoError in
            DispatchQueue.main.async {
                if(navigoError == nil){
                    let sb = UIStoryboard(name: "GameXelo", bundle: nil)
                    let xelogame = sb.instantiateViewController(withIdentifier: "GameResultController") as! GameResultController
                    xelogame.gameInfo = game
                    xelogame.arrAnswer = gameAsw
                    self.navigationController?.pushViewController(xelogame, animated: true)
                }else{
                    if(navigoError?.errorCode == Errorcode.ERROR_NO_INTERNET){
                        //self.errorNoInternet()
                        print("Lỗi ")
                    }else if (navigoError?.errorCode == Errorcode.NAVIGO_SESSION_EXPIRED){
                    }
                }
            }
        }
        let connector = APIXeloGame();
        connector.luckyGameAnswer(gameID: game.gameID!, CompletionHandler: CompletionHandler)
    }
    
    
}

extension GameHistoryController: UITableViewDelegate, UITableViewDataSource {
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return  arrGame.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ImageCell", for: indexPath) as! ImageCell
        let game:LuckyGame = arrGame[indexPath.row]
        cell.lblTitle.text = game.gameTitle
        cell.lblNote.text = game.fromDate! + " - " + game.toDate!
        if game.isAvailable! {
            cell.imgIcon.image = UIImage(named: "cur")
        }else{
            cell.imgIcon.image = UIImage(named: "des")
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // print("chon cai nay ",indexPath.row)
        let game:LuckyGame = arrGame[indexPath.row]
        getLuckyGame(game: game)
        
    }
}

