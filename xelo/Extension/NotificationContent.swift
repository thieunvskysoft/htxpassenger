//
//  NotificationContent.swift
//  xelo
//
//  Created by Nguyen Thieu on 8/7/19.
//  Copyright © 2019 DUMV. All rights reserved.
//

import Foundation
import UserNotifications

@available(iOS 10.0, *)
final class NotificationContent: UNMutableNotificationContent {
    init(title: String, body: String) {
        super.init()
        self.title = title
        self.body = body
        self.sound = UNNotificationSound.default()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
