//
//  String+Emoji.swift
//  xelo
//
//  Created by Nguyen Thieu on 5/5/19.
//  Copyright © 2019 DUMV. All rights reserved.
//

import Foundation

extension String {
    
    public func addingPercentEncodingForQueryParameter() -> String? {
        let generalDelimitersToEncode = ":#[]@" // does not include "?" or "/" due to RFC 3986 - Section 3.4
        let subDelimitersToEncode = "!$&'()*+,;="
        
        var allowedCharacterSet = CharacterSet.urlQueryAllowed
        allowedCharacterSet.remove(charactersIn: "\(generalDelimitersToEncode)\(subDelimitersToEncode)")
        
        return addingPercentEncoding(withAllowedCharacters: allowedCharacterSet)
    }
    
}

public class StringConvert{
    
    func emojiConvert(str:String) -> String {
        var myString = str
        for item in emoji {
            if (myString).contains(item.key){
                myString = myString.replacingOccurrences(of: item.key, with: encodeEmj(nCode: item.value))
            }
        }
        return myString
    }
    
    func encodeEmj(nCode:Int)->String{
        var myString = ""
        let emoji = UnicodeScalar(nCode)
        myString.append(Character(emoji!))
        return myString
    }
}
extension Double {
    func formatnumberPrice() -> String {
        //        let formater = NumberFormatter()
        //        formater.groupingSeparator = "."
        //        formater.numberStyle = .decimal
        //        return formater.string(from: NSNumber(value: self))!
        let formater = NumberFormatter()
        formater.groupingSeparator = "."
        formater.numberStyle = .decimal
        let formattedNumber = formater.string(from: NSNumber(value: self))
        return formattedNumber!
    }
}

extension Int {
    func formatnumberPrice() -> String {
        //        let formater = NumberFormatter()
        //        formater.groupingSeparator = "."
        //        formater.numberStyle = .decimal
        //        return formater.string(from: NSNumber(value: self))!
        let formater = NumberFormatter()
        formater.groupingSeparator = "."
        formater.numberStyle = .decimal
        let formattedNumber = formater.string(from: NSNumber(value: self))
        return formattedNumber!
    }
}
