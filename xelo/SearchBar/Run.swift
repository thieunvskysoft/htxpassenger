//
//  Run.swift
//  NaviGo
//
//  Created by Admin on 9/8/17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation
import SwiftyJSON

public class Run{
    var json:JSON?
    var json1:JSON?
    var lou:SearchViewController?
    var outputView:SearchViewController?
    func getStringformUrl(text:String){
        let cString = text.addingPercentEncoding(withAllowedCharacters: .alphanumerics)
        var keyGoogle:String = ""
        if DataNavigo.loginResult.googlePlaceApiKey == "" {
            keyGoogle = "AIzaSyBXZKFWRkjJRcf7PQO9OM9mj9SCUkft24s"
        }else{
            keyGoogle = DataNavigo.loginResult.googlePlaceApiKey
        }
        
        DispatchQueue.global(qos: .userInitiated).async {
            let url1="https://maps.googleapis.com/maps/api/place/autocomplete/json?input="+cString!+"&components=country:VN&language=vi&key=" + keyGoogle
            let myUrl = URL(string: url1);
            print("url1",url1)
            var request = URLRequest(url:myUrl!)
            
            request.httpMethod="POST"
            let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
                if let data = data,
                    let   html = String(data: data, encoding: String.Encoding.utf8) {
                    do {
                        self.json = JSON(parseJSON: html)
                        //print("json 1 \(self.json)")
                        
                        DispatchQueue.main.async {
                            self.handlingStringApiAccount(input:text)
                        }
                    }
                }
            }
            task.resume()
        }     
    }
    func handlingStringApiAccount(input:String){
        lou?.updateSearch(dataValue: json!, dataId: input)
    }
    ///////get locaticon
    func getLocation(ss:String){
        var keyGoogle:String = ""
        if DataNavigo.loginResult.googlePlaceApiKey == "" {
            keyGoogle = "AIzaSyBXZKFWRkjJRcf7PQO9OM9mj9SCUkft24s"
        }else{
            keyGoogle = DataNavigo.loginResult.googlePlaceApiKey
        }
        
        DispatchQueue.global(qos: .userInitiated).async {
            let url1="https://maps.googleapis.com/maps/api/place/details/json?placeid="+ss+"&key="+keyGoogle
            let myUrl = URL(string: url1);
            var request = URLRequest(url:myUrl!)
            request.httpMethod="POST"
            let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
                if let data = data,
                    let   html = String(data: data, encoding: String.Encoding.utf8) {
                    do {
                        self.json1 = JSON(parseJSON: html)
                        //print("json 1 \(self.json1)")
                        DispatchQueue.main.async {
                            self.handlingLocation()
                        }
                    }
                }
            }
            task.resume()
        }
    }
    func handlingLocation(){
        var lat:Double?
        var lng:Double?
        lat=json1?["result"]["geometry"]["location"]["lat"].double
        lng=json1?["result"]["geometry"]["location"]["lng"].double
        lou?.updateLocation(lat: lat!, lng: lng!)
    }
}
