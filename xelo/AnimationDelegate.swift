//
//  AnimationDelegate.swift
//  xelo
//
//  Created by Nguyen Thieu on 3/7/18.
//  Copyright © 2018 DUMV. All rights reserved.
//

import Foundation

protocol AnimationDelegate {
    func AnimationSuccess()
    func AnimationFailed()
    func Timeout()
}
