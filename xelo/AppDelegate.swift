//
//  AppDelegate.swift
//  xelo
//
//  Created by HoangManh on 11/13/17.
//  Copyright © 2017 DUMV. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import Firebase
import FirebaseMessaging
import FirebaseInstanceID
import UserNotifications
import FirebaseAuth


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate , UNUserNotificationCenterDelegate, MessagingDelegate{
 
    var window: UIWindow?
    
    func configApplePush(_ application: UIApplication)
    {
        if #available(iOS 10.0, *)
        {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            // For iOS 10 data message (sent via FCM
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
    }
    
    // This method will be called when app received push notifications in foreground
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
    {
        completionHandler([])
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void)
    {
        let state = UIApplication.shared.applicationState
        if state == .active
        {
        }
        else
        {
            
        }
        completionHandler()
    }
    
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        configApplePush(application)
        GMSServices.provideAPIKey("AIzaSyBXZKFWRkjJRcf7PQO9OM9mj9SCUkft24s")
        GMSPlacesClient.provideAPIKey("AIzaSyBXZKFWRkjJRcf7PQO9OM9mj9SCUkft24s")
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        DataNavigo.foreground = true
        
    }
    func applicationDidEnterBackground(_ application: UIApplication) {
        DataNavigo.foreground = true
        window?.endEditing(true)
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        if DataNavigo.idUIController == 4  {
            DataNavigo.animationDelegate?.AnimationSuccess()
        }
        DataNavigo.handingScreen?.startRequest()
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        DataNavigo.foreground = false
        DataNavigo.ipaDelegate?.purchaseForeground()
    }
    
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let firebaseAuth = Auth.auth()
        #if DEBUG
            firebaseAuth.setAPNSToken(deviceToken, type: AuthAPNSTokenType.sandbox)
            print("AuthAPNSTokenType .sandbox-----------------")
        #else
            firebaseAuth.setAPNSToken(deviceToken, type: AuthAPNSTokenType.prod)
            print("AuthAPNSTokenType .prod-------------------")
        #endif
        Messaging.messaging().apnsToken = deviceToken
        
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String)
    {
        print("InstanceID token3: \(Messaging.messaging().fcmToken!)")
        DataNavigo.passenger.tokenID = Messaging.messaging().fcmToken!
        
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any])
    {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print message ID.
        if let messageID = userInfo["gcmMessageIDKey"]
        {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void)
    {
        print(userInfo)
        let noti = Notifications()
        let notifyType = userInfo["gcm.notification.notifyType"] as? String
        let notifyID = userInfo["gcm.notification.notifyID"] as? String
        let notifyUrl = userInfo["url" as NSString] as? String
        let aps = userInfo["aps" as NSString] as? [String:AnyObject]
        let alert = aps?["alert"] as? [String:AnyObject]
        let title = alert?["title"] as? String
        let body = alert?["body"] as? String
        noti.notifyID = notifyID
        noti.notifyType = notifyType
        noti.title = title
        noti.content = body
        noti.url = notifyUrl
        DataNavigo.notiFB = noti
        
        if DataNavigo.foreground {
            //pushNotification(noti: DataNavigo.notiFB)
        }else {
            NotiDetailController.noti = true
            NotiDetailController.notifyID = notifyID!
            DataNavigo.ipaDelegate?.pushAlterView()
        }
        completionHandler(UIBackgroundFetchResult.newData)
    }

}



