//
//  Address.swift
//  NaviGo
//
//  Created by HoangManh on 11/2/17.
//  Copyright © 2017 DUMV. All rights reserved.
//

import Foundation
import SwiftyJSON
public class Address{
    var addressID:String?
    var fullName:String?
    var fullAddress:String?
    var y:Double?
    var x:Double?
    
    init()
    {}
    
    init(addressID: String, fullName: String, fullAddress: String, y: Double, x: Double) {
        self.addressID = addressID
        self.fullName = fullName
        self.fullAddress = fullAddress
        self.y = y
        self.x = x
    }

    
}

public class DistanceRoute {
    var Distance:Int? = nil
    var Duration:Int? = nil
    var Routes:Array<JSON>?
    var Polyline:[String:JSON]?
}

