//
//  Vehicle.swift
//  xelo
//
//  Created by Nguyen Thieu on 4/5/19.
//  Copyright © 2019 DUMV. All rights reserved.
//

import UIKit

public class Vehicle {
    var x:Double? = nil
    var y:Double? = nil
    var direction:Int? = 0
    var speed:Int? = nil
    var plateNumber:String? = nil
    var gpsTime:String = ""
    var createDate:String = ""
    
    var driverID:String = ""
    var updateDate:String = ""
    var travelLine:String = ""
    
}
