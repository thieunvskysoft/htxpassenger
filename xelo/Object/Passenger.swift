//
//  InformationUser.swift
//  testNavigo
//
//  Created by Linh  on 9/26/17.
//  Copyright © 2017 DUMV. All rights reserved.
//

import Foundation
class Passenger{
    
    public var nameUser:String=""
    public var emailUser:String=""
    public var numberphoneUser:String=""
    public var referMobileNo:String=""
    public var acountID:String=""
    public var deviceSerial:String=""
    public var securekey:String="="
    public var authenkey:String = ""
    public var time:String = ""
    public var tokenID:String = ""
    public var accountExisting:Bool = false
    public var authenByFirebase:Bool = false
    func toStringSave()-> String{
        var json:String!
        json="";
        json=json+"\"nameUser\":"+"\""+nameUser+"\","
        json=json+"\"emailUser\":"+"\""+emailUser+"\","
        json=json+"\"numberphoneUser\":"+"\""+numberphoneUser+"\","
        json=json+"\"acountID\":"+"\""+acountID+"\","
        json=json+"\"deviceSerial\":"+"\""+deviceSerial+"\","
        json=json+"\"securekey\":"+"\""+securekey+"\""
        json="{"+json+"}"
        print("toStringSave",json)
        return json
    }
    func exChange(jsonString:String){
        print("exChange",jsonString)
        let data=jsonString.data(using: String.Encoding.utf8,allowLossyConversion:false)
        let json = try? JSONSerialization.jsonObject(with: data!, options: .mutableContainers)as?NSDictionary
        self.nameUser=(json??["nameUser"]as?String)!
        self.emailUser=(json??["emailUser"]as?String)!
        self.numberphoneUser=(json??["numberphoneUser"]as?String)!
        self.acountID=(json??["acountID"]as?String)!
        self.deviceSerial=(json??["deviceSerial"]as?String)!
        self.securekey=(json??["securekey"]as?String)!
    }
    
    func saveInformationUser(){
        let fileName = Constant.FINE_NAME
        let DocumentDirURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        let fileURL = DocumentDirURL.appendingPathComponent(fileName).appendingPathExtension("txt")
        let writeString = toStringSave()
        do {
            try writeString.write(to: fileURL, atomically: true, encoding: String.Encoding.utf8)
        } catch _ as NSError {
            
        }
    }
    
    init() {
        
    }
    
    func getInformationUser()->Bool{
        let fileName = Constant.FINE_NAME
        let DocumentDirURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        let fileURL = DocumentDirURL.appendingPathComponent(fileName).appendingPathExtension("txt")        
        var readString = ""
        do {
            readString = try String(contentsOf: fileURL)
            exChange(jsonString: readString)
            return true
        } catch _ as NSError {
            return false
        }
        
    }
}
