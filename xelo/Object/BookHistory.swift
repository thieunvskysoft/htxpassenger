//
//  BookHistory.swift
//  NaviGo
//
//  Created by HoangManh on 10/16/17.
//  Copyright © 2017 DUMV. All rights reserved.
//

import Foundation
import SwiftyJSON

public class BookinhHistoryAsynctacsk : AsynctacskSwift {
    var uiview:MenuViewController?
    var body:String? = ""
    
    func set(uiview1:MenuViewController)  {
        self.uiview=uiview1
    }
    
    func createBody(time:String)  {
        if(time.characters.count==0){
            body="action=listTrips&startDate="
        }else{
            body="action=listTrips&startDate="+time
        }
    }
    
     override func doInBackGround(){
        createBody(time: "")
        let myUrl = URL(string: Constant.URL_ACCOUNT)
        var request = URLRequest(url:myUrl!)
        request.httpMethod="POST"
        request.httpBody=body?.data(using: String.Encoding.utf8, allowLossyConversion: false)
        runAllAsynctacsk(request: request, ansyn: self)
    }
    
    override func postExecute(sJson: JSON) {
        print("jsonhistory",sJson)
        if let arrServices = sJson["bookings"].array{
            for service in arrServices  {
                if  service is JSON{
                    let serviceN :Book = DataNavigo.convert.jsonConvertBookH(json: service  )
                    DataNavigo.arrBookHistory.append(serviceN)
                }
            }
        }
        //uiview?.loadHistory()
    }
    
    func runAllAsynctacsk(request:URLRequest, ansyn:AsynctacskSwift)  {
        DispatchQueue.global(qos: .userInitiated).async {
            let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
                do {
                    if let data = data,
                        let   html = String(data: data, encoding: String.Encoding.utf8) {
                        let json = JSON(parseJSON: html)
                    DispatchQueue.main.async {
                        ansyn.postExecute(sJson: json)
                        }
                    }
                } catch {
                }
            }
            task.resume()
        }
    }
}

public class HistoriesAsynctacsk : AsynctacskSwift {
    var body:String? = ""
    
    func createBody()  {
            body="action=getHistories"
    }
    
    override func doInBackGround(){
        createBody()
        let myUrl = URL(string: Constant.URL_ACCOUNT)
        var request = URLRequest(url:myUrl!)
        request.httpMethod="POST"
        request.httpBody=body?.data(using: String.Encoding.utf8, allowLossyConversion: false)
        runAllAsynctacsk(request: request, ansyn: self)
    }
    
    override func postExecute(sJson: JSON) {
        DataNavigo.arrHistories.removeAll()
        if let arrServices = sJson["addresses"].array{
            for service in arrServices  {
                if  service is JSON{
                    let serviceN :Histories = DataNavigo.convert.jsonConvertHistories(json: service  )
                    DataNavigo.arrHistories.append(serviceN)
                }
            }
        }
        //uiview?.loadHistory()
    }
    
    func runAllAsynctacsk(request:URLRequest, ansyn:AsynctacskSwift)  {
        DispatchQueue.global(qos: .userInitiated).async {
            let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
                do {
                    if let data = data,
                        let   html = String(data: data, encoding: String.Encoding.utf8) {
                        let json = JSON(parseJSON: html)
                        DispatchQueue.main.async {
                            ansyn.postExecute(sJson: json)
                        }
                    }
                } catch {
                }
            }
            task.resume()
        }
    }
}

public class AsynctacskSwift{
    func doInBackGround(){
    }
    
    func postExecute(sJson:JSON){
    }
    
    func run()  {
        self.doInBackGround()
    }
    
}

