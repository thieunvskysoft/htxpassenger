//
//  Book.swift
//  testNavigo
//
//  Created by Linh  on 9/26/17.
//  Copyright © 2017 DUMV. All rights reserved.
//

import Foundation

import UIKit

public class Book {
    var type:String = "htx"
    var state:Int? = 99
    var serviceName:String?
    var serviceCode:String?
    var bookingId:String?=nil
    var fromPosition:Point? = Point()
    var toPosition:Point? = Point()
    // var actDistanCe:Double? = nil
    var tariffPice:Double? = nil
    var extraCharge:Double? = 0
    var estCharge:Double? = nil
    var fromCharge:Double? = nil
    var toCharge:Double? = nil
    var toAddress:String? = nil
    var note:String? = ""
    var fromAddress:String? = nil
    var estDistance:Double? = 0.0
    var passengerID:String? = nil
    var passengerName:String? = nil
    var promotionCode:String? = ""
    var bookDate:String? = nil
    var plateNumber:String? = nil
    var fixCharge:Bool = false
    var priceFirst:Bool = true
    var fromCenter:Bool = true
    var starPoint:Int = 0
    var pickupDistance:Int? = nil
    var duration:Int? = 0
    var waitDuration:Int? = 0
    var actDuration:Int? = nil
    var waitCharge:Int? = nil
    var promotionValue:Int? = nil
    var customerID:Int? = nil
    var tariffPrice:Double? = 0
    var bookType:Int? = 0
    var peakRate:Double? = 0
    var peakHour:Bool? = false
    var currentPosition:Point? = Point()
    //var driverPosition:Position?
    var passengerContactNo:String? = nil
    var driver:Driver? = Driver()
    var position:Vehicle = Vehicle()
    var twoWays:Bool = false
    var returnRate:Int = 0
    var waitFee:Int = 0
    var cancelNote:String = ""
    var blockRoad:Bool = false
    var persons:Int = 0
    var ecoChoice:Bool = true
    var departureDate:String = ""
    var routingID:String = ""
    
    var xeloBonus:Int? = 0
    var extraFee:Int? = 0
    var xeloFee:Int? = 0
    var CancelCode:String? = nil
    var feeRate:Int? = 0
    func clean(){
        bookingId = nil
        state = BookingState.STATE_NONE
        position.x = nil
        /* fromPosition?.x = nil
         fromPosition?.y = nil
         toPosition?.x = nil
         toPosition?.y = nil
         */
    }
    func cancelBook(){
        bookingId = nil
        state = BookingState.STATE_NONE
        DataNavigo.booking.driver = nil
        DataNavigo.booking.plateNumber = ""
        //DataNavigo.booking.
    }
}

public class Point {
    var x:Double? = nil
    var y:Double? = nil
}

public class CancelReasons {
    var CancelNote:String? = nil
    var CancelCode:String? = nil
}

//public class Position {
//    var x:Double? = nil
//    var y:Double? = nil
//    var direction:Int? = 0
//    var speed:Int? = nil
//    var plateNumber:String? = nil
//}
public class Driver{
    var driverContactNo:String? = ""
    var driverID:String? = nil
    var driverName:String? = nil
}

public class DriverInfo{
    var fullName:String? = nil
    var dateOfBirth: String? = nil
    var mobileNo: String? = nil
    var plateNumber: String? = nil
    var licenseType:String? = nil
    var starPoint: Double? = nil
    var email: String? = nil
    var vehicleType:String? = nil
    var licenseNo: String? = nil
    var address: String? = nil
    var color:String? = ""
    var serviceCode:String? = nil
    var idCardNo:String? = nil
    var avatarFace:UIImage? = nil
}
public class Histories{
    var addressID:String?
    var fullName:String?
    var fullAddress:String?
    var y:Double?
    var x:Double?
    var accountID:String?
    var status:String?
    var createDate:String?
}

public class GooglePlace{
    var placeID:String?
    var description:String?
    var secondaryText:String?
    var mainText:String?
}

public class Notifications {
    var notifyID:String? = nil
    var accountID:String? = nil
    var notifyType:String? = nil
    var content:String? = nil
    var title:String? = nil
    var read:Bool? = false
    var createDate:String? = nil
    var url:String? = nil
}

public class ChatTemplates{
    var description:String?
    var chatCode:String?
}


public class ChatMessage {
    var driverID:String? = nil
    var driverName:String? = nil
    var passengerID:String? = nil
    var passengerName:String? = nil
    var fromDriver:Bool? = false
    var createDate:String? = nil
    var content:String? = nil
    
    init()
    {}
    
    init(driverID: String, driverName: String, passengerID: String, passengerName: String, fromDriver: Bool, createDate: String, content: String) {
        self.driverID = driverID
        self.driverName = driverName
        self.passengerID = passengerID
        self.passengerName = passengerName
        self.fromDriver = fromDriver
        self.createDate = createDate
        self.content = content
    }
    
}
