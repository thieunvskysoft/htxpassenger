//
//  File.swift
//  NaviGo
//
//  Created by HoangManh on 9/22/17.
//  Copyright © 2017 DUMV. All rights reserved.
//

import Foundation
import UIKit

public class Service{
    var serviceIcon:UIImage? = nil//
    var serviceCode:String? = nil
    var price:Int? = nil
    var serviceName:String? = nil
    var mapIcon:UIImage? = nil
    var fromPrice:Int? = nil
    var toPrice:Int? = nil
    var waitFee:Int? = nil
    var returnRate:Int? = nil
    var referServiceCode:String? = nil
    var serviceCode2:String? = nil
    var fromNormalPrice:Int? = 0
    var toNormalPrice:Int? = 0
    var peakHour:Bool = false
    var peakRate:Double = 1
    var ecoPrice:Int = 0
}

