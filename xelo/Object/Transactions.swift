//
//  Transactions.swift
//  xelo
//
//  Created by Nguyen Thieu on 11/17/18.
//  Copyright © 2018 DUMV. All rights reserved.
//

import Foundation
import UIKit

public class Transactions {
    var driverID:String? = nil
    var note:String? = nil
    var _id:String? = nil
    var amount:String? = nil
    var transType:String? = nil
    var bookingID:String? = nil
    var accountType:String? = nil
    var createDate:String? = nil
    var transDate:String? = nil
}

public class WalletInfo {
    var promoWallet:Int? = nil
    var promoExpireDate:String? = nil
    var depositWallet:Int? = nil
}
public class WalletList {
    var walletValue:Int? = nil
    var walletImage:String? = nil
    var walletName:String? = nil
    
    init()
    {}
    
    init(walletValue: Int, walletImage: String, walletName: String) {
        self.walletValue = walletValue
        self.walletImage = walletImage
        self.walletName = walletName
    }
    
}
