//
//  BusTrip.swift
//  xelo
//
//  Created by Nguyen Thieu on 3/25/19.
//  Copyright © 2019 DUMV. All rights reserved.
//

import Foundation

public class BusTrip {
    var pickupX:Double?
    var pickupY:Double?
    var dropOffX:Double?
    var dropOffY :Double?
    var pickupAddress:String = ""
    var dropOffAddress:String = ""
    var fromAddress:String = ""
    var toAddress:String = ""
    var busOperator:String = ""
    var tripID:String = ""
    var plateNumber:String = ""
    var ticketFare:Int? = nil
    var capacity:Int = 0
    var numOfBooked:Int = 0
    var modelName:String = ""
    var createDate:String = ""
    var departureDate:String = ""
    var paid:Bool = false
    var stateNote:String = ""
    
    var ticketFare1:Int = 0
    var ticketFare2:Int = 0
    var ticketFare3:Int = 0
    var quantity1:Int = 0
    var quantity2:Int = 0
    var quantity3:Int = 0
    var vehicleID:Int = 0
    var hotLine:String = ""
    var contactName:String = ""
    var geom:String = ""
    
    
    init()
    {}
    
    init(pickupAddress: String, dropOffAddress: String, fromAddress: String, toAddress: String, pickupX: Double, pickupY:Double, dropOffX: Double, dropOffY:Double, busOperator: String, tripID: String, plateNumber: String, ticketFare: Int, capacity:Int, numOfBooked:Int, modelName:String,
         createDate:String, departureDate:String, paid: Bool, stateNote:String, ticketFare1: Int, ticketFare2:Int, ticketFare3:Int,quantity1: Int, quantity2:Int, quantity3:Int,vehicleID: Int, hotLine:String, contactName:String, geom:String) {
        self.pickupAddress = pickupAddress
        self.dropOffAddress = dropOffAddress
        self.fromAddress = fromAddress
        self.toAddress = toAddress
        self.pickupX = pickupX
        self.pickupY = pickupY
        self.dropOffX = dropOffX
        self.dropOffY = dropOffY
        self.busOperator = busOperator
        self.tripID = tripID
        self.plateNumber = plateNumber
        self.ticketFare = ticketFare
        self.capacity = capacity
        self.numOfBooked = numOfBooked
        self.modelName = modelName
        self.createDate = createDate
        self.departureDate = departureDate
        self.paid = paid
        self.stateNote = stateNote
        
        self.ticketFare1 = ticketFare1
        self.ticketFare2 = ticketFare2
        self.ticketFare3 = ticketFare3
        self.quantity1 = quantity1
        self.quantity2 = quantity2
        self.quantity3 = quantity3
        self.vehicleID = vehicleID
        self.hotLine = hotLine
        self.contactName = contactName
        
        self.geom = geom
    }
    
}

class TicketFares {
    var description:String = ""
    var note:String = ""
    var ticketFare:Int = 0
    var ticketID:Int = 0
    var capacity:Int = 0
    var choice:Int = 0
    
    init()
    {}
    
    init(description:String, note:String, ticketFare:Int, ticketID:Int, capacity:Int)
    {
        self.description = description
        self.note = note
        self.ticketFare = ticketFare
        self.ticketID = ticketID
        self.capacity = capacity
    }
    
}

class Place {
    var x:Double? = nil
    var y:Double? = nil
    var placeID:Int = 0
    var time:String = ""
    var description:String = ""
}

class BusTripResult {
    var busTrip:BusTrip = BusTrip()    
    var places:Array<Place> = Array()  
    var ticketFares: Array<TicketFares> = Array()
}

