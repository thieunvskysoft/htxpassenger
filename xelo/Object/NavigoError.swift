//
//  NavigoError.swift
//  NaviGo
//
//  Created by HoangManh on 10/16/17.
//  Copyright © 2017 DUMV. All rights reserved.
//
import UIKit
import Foundation

class NavigoError {
    var errorCode:String? = ""//passenger
    var errorMessage:String? = ""
}

class LoginResult {
    public var serviceCode:String? = nil
    public var arrService: Array<Service> = Array()
    public var arrReasons: Array<CancelReasons> = Array()
    public var arrChatTemplate: Array<ChatTemplates> = Array()
    public var arrHomeAds: Array<HomeAds> = Array()
    public var arrAppItem: Array<AppItem> = Array()
    public var fullName:String? = nil
    public var booking:Book = Book()
    public var hasNewVersion:Bool = false
    public var releaseNote:String = ""
    public var sessionID:String = ""
    public var warningMessage:String = ""
    public var googlePlaceApiKey:String = ""
    public var appUrl:String = ""
    public var showShareAndBonus:Bool? = nil
    public var peakHour:Bool = false
    public var peakRate:Double = 1
    public var tokenID:String = ""
    var avatarFace:UIImage? = nil
}
class PathResult {

}

public class BlockRoads {
    var description:String? = nil
    var wayPoints:String? = nil
}
