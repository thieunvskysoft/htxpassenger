//
//  DriverBook.swift
//  NaviGo
//
//  Created by HoangManh on 9/22/17.
//  Copyright © 2017 DUMV. All rights reserved.
//

import Foundation

public class DriverBook {
    var fullName:String?
    var mobileNo:String?
    var lat:Double?
    var log:Double?
    var fromLat:Double?
    var fromLog:Double?
}
