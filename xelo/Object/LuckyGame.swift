//
//  LuckyGame.swift
//  xelo
//
//  Created by Nguyen Thieu on 7/6/19.
//  Copyright © 2019 DUMV. All rights reserved.
//

import Foundation

public class LuckyGame{
    var gameID:Int? = nil//
    var gameTitle:String? = nil//
    var gameDescription:String? = nil//
    var fromDate:String? = nil//
    var toDate:String? = nil//
    var imageUrl:String? = nil//
    var createDate:String? = nil//
    var lotteryDate:String? = nil//
    var isAvailable:Bool? = nil//
    var luckyNo:String? = nil//
    var answer:String? = nil
    var arrAnswers: Array<Answers> = Array()
    var win:Bool? = nil//
}

public class Answers{
    var gameID:Int? = nil//
    var luckyNo:String? = nil//
    var answer:String? = nil
    var updateDate:String? = nil//
    var fullName:String? = nil
    var mobileNo:String? = nil//
}
