//
//  ItemCell.swift
//  GridViewExampleApp
//
//  Created by Chandimal, Sameera on 12/22/17.
//  Copyright © 2017 Pearson. All rights reserved.
//

import UIKit

class MarkerView: UIView {
    
    var view: UIView!
    @IBOutlet weak var imgMarker: UIImageView!
    @IBOutlet weak var lblPlateNumber: UILabel!
    @IBOutlet weak var lblTravelLine: UILabel!
    
    var rotate = 0
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        //xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //xibSetup()
    }
    func xibSetup()
    {
        view = viewFromNibForClass()
        view.frame = self.bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
        setupView()
    }
    
    func setupView() {
        
        lblPlateNumber.textColor = UIColor.blue
        lblTravelLine.textColor = UIColor.blue
        imgMarker.image?.rotate(radians: CGFloat(rotate))
    }
    
    fileprivate func viewFromNibForClass() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
}
