//
//  HistoryCell.swift
//  NaviGo
//
//  Created by HoangManh on 10/16/17.
//  Copyright © 2017 DUMV. All rights reserved.
//

import UIKit

class HistoryCell: UITableViewCell {

    @IBOutlet weak var lblFrom: UILabel!
    @IBOutlet weak var lblTo: UILabel!
    @IBOutlet weak var lblTimeBook: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var imgFrom: UIImageView!
    @IBOutlet weak var imgTo: UIImageView!

    
    @IBOutlet weak var viewSpace: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
