//
//  ViewDetailDriver.swift
//  NaviGo
//
//  Created by HoangManh on 10/10/17.
//  Copyright © 2017 DUMV. All rights reserved.
//

import UIKit

extension MapController {

    
    func createviewIFDriver() -> UIView {
        let viewOne:UIView = UIView()
        let viewDriver:UIView = UIView()
        
        viewDriver.frame = CGRect(x: 0, y: screenHeight - 110, width: screenWidth, height: 110)
        viewDriver.backgroundColor = UIColor.white
        let imgView:UIImageView = UIImageView()
        imgView.frame = CGRect(x: 100, y: 5, width: 30, height: 30)
        imgView.image = #imageLiteral(resourceName: "top-arrow")
        imgView.center.x = view.center.x
        viewDriver.addSubview(imgView)
        
        viewOne.frame = CGRect(x: 0, y: 50, width: screenWidth, height: 60)
        //viewOne.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        viewDriver.addSubview(viewOne)
        
        let imageDiver:UIImageView = UIImageView()
        imageDiver.frame = CGRect(x: 5, y: 5, width: 50, height: 50)
        imageDiver.image = DataNavigo.driverInfo.avatarFace
        viewOne.addSubview(imageDiver)
        imageDiver.layer.cornerRadius = 25
        imageDiver.clipsToBounds = true
        
        lblNameDriver.font = lblNameDriver.font.withSize(18)
        lblNameDriver.textColor = #colorLiteral(red: 0.9411764741, green: 0.4980392158, blue: 0.3529411852, alpha: 1)
        lblNameDriver.frame = CGRect(x: 60, y: 0, width: screenWidth-139, height: 30)
        lblNameDriver.text = DataNavigo.booking.driver?.driverName
        viewOne.addSubview(lblNameDriver)
        
        let viewRateDriver:FloatRatingView = {
            let rate = FloatRatingView()
            rate.contentMode = UIViewContentMode.scaleAspectFit
            rate.type = .floatRatings
            rate.rating = 3.6
            rate.minRating = 0
            rate.maxRating = 5
            rate.editable = false
            rate.emptyImage = #imageLiteral(resourceName: "StarEmpty")
            rate.fullImage = #imageLiteral(resourceName: "StarFull")
            return rate
        }()
        
        viewRateDriver.frame = CGRect(x: 60, y: 30, width: 100, height: 15)
        viewOne.addSubview(viewRateDriver)
        
        return viewDriver
    }

}
