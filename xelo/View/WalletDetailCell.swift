//
//  WalletDetailCell.swift
//  xelo
//
//  Created by Nguyen Thieu on 12/12/18.
//  Copyright © 2018 DUMV. All rights reserved.
//

import UIKit

class WalletDetailCell: UITableViewCell {
    
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lblMenu: UILabel!
    @IBOutlet weak var walletValue: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
