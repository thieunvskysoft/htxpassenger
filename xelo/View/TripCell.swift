//
//  TripCell.swift
//  Passenger
//
//  Created by Nguyen Thieu on 12/28/19.
//  Copyright © 2019 Nguyen Thieu. All rights reserved.
//

import UIKit

class TripCell: UITableViewCell {
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var lblStart: UILabel!
    @IBOutlet weak var lblEnd: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblNoteLabel: UILabel!
    @IBOutlet weak var tripId: UILabel!
    @IBOutlet weak var tripNote: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
