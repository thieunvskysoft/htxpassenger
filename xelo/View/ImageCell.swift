//
//  ImageCell.swift
//  xelo
//
//  Created by Nguyen Thieu on 7/5/19.
//  Copyright © 2019 DUMV. All rights reserved.
//

import UIKit

class ImageCell: UITableViewCell {
    
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblNote: UILabel!
    @IBOutlet weak var imgNext: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        lblNote.textColor = NaviColor.naviBtnOrange
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
}
