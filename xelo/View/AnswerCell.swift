//
//  AnswerCell.swift
//  xelo
//
//  Created by Nguyen Thieu on 7/16/19.
//  Copyright © 2019 DUMV. All rights reserved.
//

import UIKit

class AnswerCell: UITableViewCell {
    
    @IBOutlet weak var lblDatetime: UILabel!
    @IBOutlet weak var lblAnswer: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
}
