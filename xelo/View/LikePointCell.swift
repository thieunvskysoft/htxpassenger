//
//  LikePointCell.swift
//  NaviGo
//
//  Created by HoangManh on 10/30/17.
//  Copyright © 2017 DUMV. All rights reserved.
//

import UIKit

class LikePointCell: UITableViewCell {

    @IBOutlet weak var customAddress: UILabel!
    @IBOutlet weak var locationAddress: UILabel!
    var pointItem:Address!
    
    func setPoint(address: Address) {
        pointItem = address
        customAddress.text = address.fullName
        locationAddress.text = address.fullAddress

    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
