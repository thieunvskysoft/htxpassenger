//
//  ChatMessagerCell.swift
//  xelodriver
//
//  Created by Nguyen Thieu on 2/12/18.
//  Copyright © 2018 DUMV. All rights reserved.
//

import UIKit

class ChatMessagerCell: UITableViewCell {
    
    @IBOutlet weak var lblMessaggio: UILabel!
    @IBOutlet weak var lblData: UILabel!
    @IBOutlet weak var viewContent: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewContent.layer.cornerRadius = 10
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
