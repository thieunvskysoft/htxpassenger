//
//  CollectionServiceCell.swift
//  xelo
//
//  Created by Nguyen Thieu on 3/1/19.
//  Copyright © 2019 DUMV. All rights reserved.
//

import UIKit

class CollectionServiceItem: NSObject {
    var imageName = ""
    var serviceName = ""
    
}

class CollectionServiceCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setUp(_ collectionService: CollectionServiceItem) {
        print("image: \(collectionService.imageName)")
        imageView.image = UIImage(named: collectionService.imageName)
    }
}
