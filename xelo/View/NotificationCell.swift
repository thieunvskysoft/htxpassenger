//
//  NotificationCell.swift
//  xelodriver
//
//  Created by Nguyen Thieu on 1/17/18.
//  Copyright © 2018 DUMV. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell {
    
    @IBOutlet weak var contentNotiView: UIView!
    @IBOutlet weak var imgNoti: UIImageView!
    @IBOutlet weak var lblNotiType: UILabel!
    @IBOutlet weak var lblContent: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        contentNotiView.layer.cornerRadius = 5
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}
