//
//  ViewTable.swift
//  xelo
//
//  Created by Nguyen Thieu on 7/19/19.
//  Copyright © 2019 DUMV. All rights reserved.
//

import UIKit

class ViewTable: UIView, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tblData: UITableView!
    var arrAnswer = [Answers]()
    var iMode = false
    
    var view: UIView!
    
    override init(frame: CGRect)
    {
        // 1. setup any properties here
        // 2. call super.init(frame:)
        
        super.init(frame: frame)
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        // 1. setup any properties here
        
        // 2. call super.init(coder:)
        
        super.init(coder: aDecoder)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    func xibSetup()
    {
        view = loadViewFromNib()
        view.frame = self.bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView
    {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "ViewTable", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AnswerCell", for: indexPath) as! AnswerCell
        let aws:Answers = arrAnswer[indexPath.row]
        cell.lblDatetime.text = aws.updateDate
        if iMode {
            cell.lblAnswer.text = aws.fullName
        }else{
            cell.lblAnswer.text = aws.answer! + " - " + aws.luckyNo!
        }
        return cell
    }
}

