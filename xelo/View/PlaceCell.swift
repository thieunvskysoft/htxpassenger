//
//  PlaceCell.swift
//  xelo
//
//  Created by Nguyen Thieu on 8/10/18.
//  Copyright © 2018 DUMV. All rights reserved.
//

import UIKit

class PlaceCell: UITableViewCell {
    
    @IBOutlet weak var lblPlace: UILabel!
    @IBOutlet weak var imgPlace: UIImageView!
    
    @IBOutlet weak var lbldescription: UILabel!
    
    var histori:Histories? = nil
    var pointID:String!
    var delegate: PointDelegate?
    
    func setID(id:String) {
        pointID = id
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @IBAction func btnAddPoint(_ sender: Any) {
        if(pointID != "histories"){
            histori = DataNavigo.arrHistories[0]
        }
        delegate?.didAddPoint(dataID: pointID, history:histori! )
    }
    
}



