//
//  PromotionCell.swift
//  xelo
//
//  Created by Nguyen Thieu on 3/7/19.
//  Copyright © 2019 DUMV. All rights reserved.
//

import UIKit

class PromotionCell: UITableViewCell {
    
    @IBOutlet weak var imgPromotion: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
