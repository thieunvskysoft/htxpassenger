//
//  WalletCell.swift
//  xelo
//
//  Created by Nguyen Thieu on 11/19/18.
//  Copyright © 2018 DUMV. All rights reserved.
//

import UIKit

class WalletCell: UITableViewCell {
    
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblXeloPrice: UILabel!
    
    @IBOutlet weak var lblNote: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
}
