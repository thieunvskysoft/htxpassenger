  //
  //  Convert.swift
  //  testNavigo
  //
  //  Created by Linh  on 9/26/17.
  //  Copyright © 2017 DUMV. All rights reserved.
  //
  
  import Foundation
  import SwiftyJSON
  public class Convert{
    
    func jsonCovertDriverInfo(json:JSON) -> DriverInfo {
        let d = DriverInfo()
        d.fullName = json["driver"]["fullName"].stringValue
        d.dateOfBirth = json["driver"]["dateOfBirth"].stringValue
        d.mobileNo = json["driver"]["mobileNo"].stringValue
        d.plateNumber = json["driver"]["plateNumber"].stringValue
        d.licenseType = json["driver"]["licenseType"].stringValue
        d.starPoint = json["driver"]["starPoint"].doubleValue
        d.email = json["driver"]["email"].stringValue
        d.vehicleType = json["driver"]["vehicleType"].stringValue
        d.licenseNo = json["driver"]["licenseNo"].stringValue
        d.address = json["driver"]["address"].stringValue
        d.color = json["driver"]["color"].stringValue
        d.serviceCode = json["driver"]["serviceCode"].stringValue
        d.idCardNo = json["driver"]["idCardNo"].stringValue
        let serviceIconString:String = json["driver"]["avatarFace"].stringValue
        let data: NSData = NSData(base64Encoded: serviceIconString , options: .ignoreUnknownCharacters)!
        d.avatarFace = UIImage(data: data as Data)
        
        return d
    }
    
    func convertPrice(price:String) -> String
    {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.groupingSeparator = ","
        let formattedString = formatter.string(for: price)
        //print(String(describing: formattedString))
        return formattedString!
    }
    
    func jsonConvertBookH(json:JSON) -> Book {
        let book:Book = Book()
        let d = Driver()
        //print(json)
        book.state = json[keyJson.F_STATE].intValue
        book.bookingId = json[keyJson.F_BOOKING_ID].stringValue
        book.serviceCode = json[keyJson.F_SERVICE_CODE].stringValue
        book.fromPosition?.y = json[keyJson.F_FROM_Y].double
        book.fromPosition?.x = json[keyJson.F_FROM_X].double
        book.toPosition?.y = json[keyJson.F_TO_Y].double
        book.toPosition?.x = json[keyJson.F_TO_X].double
        book.currentPosition?.x = json[keyJson.F_X].double
        book.currentPosition?.y = json[keyJson.F_Y].double
        //  book.actDistanCe = json[keyJson.F_ACT_DISTANCE].double
        book.promotionValue = json[keyJson.F_PROMOTION_VALUE].intValue
        book.promotionCode = json[keyJson.F_PROMOTION_CODE].stringValue
        book.tariffPice = json[keyJson.F_TARIFF_PRICE].double
        book.extraCharge = json[keyJson.F_EXTRA_CHARGE].double
        book.estCharge = json[keyJson.F_CHARGE].double
        book.toAddress = json[keyJson.F_TO_ADDRESS].stringValue
        book.fromAddress = json[keyJson.F_FROM_ADDRESS].stringValue
        book.note = json[keyJson.F_NOTE].stringValue
        book.passengerName = json[keyJson.F_PASSENGER_NAME].stringValue
        var date:String? = nil
        date = json[keyJson.F_BOOK_DATE].stringValue
        if(date != ""){
            //book?.bookDate = convertDateShort(date: date!)
            book.bookDate = convertDatetimeByDateformat(datetime: date!, dateFormat: "dd/MM/yyyy HH:mm:ss", value: 1)
        }
        //book.bookDate = convertDateFormatter1(date: json[keyJson.F_BOOK_DATE].stringValue)
        book.plateNumber = json[keyJson.F_PLATE_NUMBER].stringValue
        book.blockRoad = json[keyJson.F_BLOCK_ROAD].boolValue
        
        book.bookType = json[keyJson.F_BOOK_TYPE].intValue
        book.cancelNote = json[keyJson.F_CANCEL_NOTE].stringValue
        book.fixCharge = json["fixCharge"].bool!
        //book.priceFirst = json[keyJson.F_PRICE_FIRST].bool!
        book.starPoint = json[keyJson.F_STAR_MARK].intValue
        book.pickupDistance = json["pickupDistance"].intValue
        book.waitDuration = json["waitDuration"].intValue
        book.estDistance = json[keyJson.F_DISTANCE].doubleValue
        book.actDuration = json[keyJson.F_ACT_DURATION].intValue
        book.waitCharge = json["waitCharge"].intValue
        //   book.actCharge = json[keyJson.F_ACT_CHARGE].doubleValue
        book.customerID = json[keyJson.F_CUSTOMER_ID].intValue
        book.passengerContactNo = json[keyJson.F_PASSENGER_CONTACT_NO].stringValue
        
        book.peakRate = json[keyJson.F_PEAK_RATE].doubleValue
        book.peakHour = json[keyJson.F_PEAK_HOUR].boolValue
        
        d.driverContactNo = json["driverContactNo"].stringValue
        d.driverID = json["driverID"].stringValue
        DataNavigo.diverID = d.driverID
        d.driverName = json["driverName"].stringValue
        
        book.driver = d
        return book
    }
    
    func jsonConvertBook1(json:JSON) -> Book {
        let book:Book = Book()
        let d = Driver()
        book.state = json["booking"][keyJson.F_STATE].intValue
        book.bookingId = json["booking"][keyJson.F_BOOKING_ID].stringValue
        book.serviceCode = json["booking"][keyJson.F_SERVICE_CODE].stringValue
        book.fromPosition?.y = json["booking"][keyJson.F_FROM_Y].double
        book.fromPosition?.x = json["booking"][keyJson.F_FROM_X].double
        book.toPosition?.y = json["booking"][keyJson.F_TO_Y].double
        book.toPosition?.x = json["booking"][keyJson.F_TO_X].double
        if(DataNavigo.booking.state != 2){
            // book.currentPosition?.x = json["booking"][keyJson.F_X].double
            // //?.y = json["booking"][keyJson.F_Y].double
        }
        //     book.actDistanCe = json["booking"][keyJson.F_ACT_DISTANCE].double
        book.promotionValue = json["booking"][keyJson.F_PROMOTION_VALUE].intValue
        book.promotionCode = json[keyJson.F_PROMOTION_CODE].stringValue
        book.tariffPice = json["booking"][keyJson.F_TARIFF_PRICE].double
        book.extraCharge = json["booking"][keyJson.F_EXTRA_CHARGE].double
        book.estCharge = json["booking"][keyJson.F_CHARGE].double
        book.fromCharge = json["booking"][keyJson.F_FROM_CHARGE].double
        book.toCharge = json["booking"][keyJson.F_TO_CHARGE].double
        book.toAddress = json["booking"][keyJson.F_TO_ADDRESS].stringValue
        book.fromAddress = json["booking"][keyJson.F_FROM_ADDRESS].stringValue
        book.note = json["booking"]["note"].stringValue
        book.passengerName = json["booking"]["passengerName"].stringValue
        //book.bookDate = convertDateFormatter1(date: json["booking"]["bookDate"].stringValue) 0962852391
        var date:String? = nil
        date = json["booking"][keyJson.F_BOOK_DATE].stringValue
        if(date != ""){
            //book?.bookDate = convertDateShort(date: date!)
            book.bookDate = convertDatetimeByDateformat(datetime: date!, dateFormat: "dd/MM/yyyy HH:mm:ss", value: 1)
        }
        book.plateNumber = json["booking"]["plateNumber"].stringValue
        book.bookType = json["booking"][keyJson.F_BOOK_TYPE].intValue
        book.cancelNote = json["booking"][keyJson.F_CANCEL_NOTE].stringValue
        book.fixCharge = json["booking"]["fixCharge"].bool ?? false
        book.starPoint = json["booking"][keyJson.F_STAR_MARK].intValue
        book.pickupDistance = json["booking"]["pickupDistance"].intValue
        book.waitDuration = json["booking"]["waitDuration"].intValue
        book.estDistance = json["booking"]["distance"].doubleValue
        book.actDuration = json["booking"]["actDuration"].intValue
        book.waitCharge = json["booking"]["waitCharge"].intValue
        book.twoWays = json["booking"]["twoWays"].boolValue
        book.customerID = json["booking"]["customerID"].intValue
        book.passengerContactNo = json["booking"]["passengerContactNo"].stringValue
        book.tariffPrice = json["booking"]["tariffPrice"].doubleValue
        book.peakRate = json["booking"][keyJson.F_PEAK_RATE].doubleValue
        book.peakHour = json["booking"][keyJson.F_PEAK_HOUR].boolValue
        
        book.blockRoad = json["booking"][keyJson.F_BLOCK_ROAD].boolValue
        
        d.driverContactNo = json["booking"]["driverContactNo"].stringValue
        d.driverID = json["booking"]["driverID"].stringValue
        DataNavigo.diverID = d.driverID
        d.driverName = json["booking"]["driverName"].stringValue
        
        let position = Vehicle()
        position.x = json["booking"]["position"][keyJson.F_X].doubleValue
        position.y = json["booking"]["position"][keyJson.F_Y].doubleValue
        position.speed = json["booking"]["position"][keyJson.F_SPEED].intValue
        position.direction = json["booking"]["position"][keyJson.F_DIRECTION].intValue
        book.position = position
        // print("Gialol ",book.estDistance)
        //print("Gialol ",book.estCharge)
        book.driver = d
        return book
    }
    
    func jsonConvertBooking(json:JSON) -> Book {
        let book:Book = Book()
        let d = Driver()
        book.state = json[keyJson.F_STATE].intValue
        book.bookingId = json[keyJson.F_BOOKING_ID].stringValue
        book.serviceCode = json[keyJson.F_SERVICE_CODE].stringValue
        book.fromPosition?.y = json[keyJson.F_FROM_Y].double
        book.fromPosition?.x = json[keyJson.F_FROM_X].double
        book.toPosition?.y = json[keyJson.F_TO_Y].double
        book.toPosition?.x = json[keyJson.F_TO_X].double
        book.currentPosition?.x = json[keyJson.F_X].double
        book.currentPosition?.y = json[keyJson.F_Y].double
        book.promotionValue = json[keyJson.F_PROMOTION_VALUE].intValue
        book.promotionCode = json[keyJson.F_PROMOTION_CODE].stringValue
        book.tariffPice = json[keyJson.F_TARIFF_PRICE].double
        book.extraCharge = json[keyJson.F_EXTRA_CHARGE].double
        book.estCharge = json[keyJson.F_CHARGE].double
        book.toAddress = json[keyJson.F_TO_ADDRESS].stringValue
        book.fromAddress = json[keyJson.F_FROM_ADDRESS].stringValue
        book.note = json[keyJson.F_NOTE].stringValue
        book.passengerName = json[keyJson.F_PASSENGER_NAME].stringValue
        var date:String? = nil
        date = json[keyJson.F_BOOK_DATE].stringValue
        if(date != ""){
            book.bookDate = convertDatetimeByDateformat(datetime: date!, dateFormat: "dd/MM/yyyy HH:mm:ss", value: 1)
        }
        book.plateNumber = json[keyJson.F_PLATE_NUMBER].stringValue
        book.blockRoad = json[keyJson.F_BLOCK_ROAD].boolValue
        book.bookType = json[keyJson.F_BOOK_TYPE].intValue
        book.cancelNote = json[keyJson.F_CANCEL_NOTE].stringValue
        book.fixCharge = json[keyJson.F_FIX_CHARGE].boolValue
        book.priceFirst = json[keyJson.F_PRICE_FIRST].boolValue
        book.starPoint = json[keyJson.F_STAR_MARK].intValue
        book.pickupDistance = json[keyJson.F_PICKUP_DISTANCE].intValue
        book.waitDuration = json[keyJson.F_WAIT_DURATION].intValue
        book.estDistance = json[keyJson.F_DISTANCE].doubleValue
        book.duration = json[keyJson.F_ACT_DURATION].intValue
        book.waitCharge = json[keyJson.F_WAIT_CHARGE].intValue
        book.customerID = json[keyJson.F_CUSTOMER_ID].intValue
        book.passengerID = json[keyJson.F_PASSENGER_ID].stringValue
        book.passengerContactNo = json[keyJson.F_PASSENGER_CONTACT_NO].stringValue
        book.serviceName = json[keyJson.F_SERVICE_NAME].stringValue
        book.peakRate = json[keyJson.F_PEAK_RATE].doubleValue
        book.peakHour = json[keyJson.F_PEAK_HOUR].boolValue
        book.twoWays = json[keyJson.F_TWO_WAY].boolValue
        book.returnRate = json[keyJson.F_RETURN_RATE].intValue
        book.waitFee = json[keyJson.F_WAIT_FEE].intValue
        book.fromCharge = json[keyJson.F_FROM_CHARGE].double
        book.toCharge = json[keyJson.F_TO_CHARGE].double
        book.xeloBonus = json[keyJson.F_TO_CHARGE].intValue
        book.routingID = json[keyJson.F_ROUTING_ID].stringValue
        book.extraFee = json[keyJson.F_EXTRA_FEE].intValue
        book.xeloFee = json[keyJson.F_XELO_FEE].intValue
        book.CancelCode = json[keyJson.F_CANCEL_CODE].stringValue
        book.ecoChoice = json[keyJson.F_ECO_CHOICE].boolValue
        book.feeRate = json[keyJson.F_FEE_RATE].intValue
        book.persons = json[keyJson.F_PERSONS].intValue
        book.departureDate = json[keyJson.F_RETURN_RATE].stringValue
        
        var departureDate:String? = nil
        departureDate = json[keyJson.F_DEPARTURE_DATE].stringValue
        if(departureDate != ""){
            book.departureDate = convertDatetimeByDateformat(datetime: departureDate!, dateFormat: "dd/MM/yyyy HH:mm", value: 2)
        }
        d.driverContactNo = json[keyJson.F_DRIVER_CONTACT_NO].stringValue
        d.driverID = json[keyJson.F_DRIVER_ID].stringValue
        d.driverName = json[keyJson.F_DRIVER_NAME].stringValue
        book.driver = d
        return book
    }
    
    func jsonConvertVehicle(json:JSON) -> Vehicle {
        let veh = Vehicle()
        veh.x = json[keyJson.F_X].doubleValue
        veh.y = json[keyJson.F_Y].doubleValue
        veh.speed = json[keyJson.F_SPEED].intValue
        veh.direction = json[keyJson.F_DIRECTION].intValue
        veh.plateNumber = json[keyJson.F_PLATE_NUMBER].stringValue
        veh.gpsTime = json[keyJson.F_GPS_TIME].stringValue
        veh.createDate = json[keyJson.F_CREATE_DATE].stringValue
        
        veh.updateDate = json[keyJson.F_UPDATE_DATE].stringValue
        veh.driverID = json[keyJson.F_DRIVER_ID].stringValue
        veh.travelLine = json[keyJson.F_TRAVEL_LINE].stringValue
        return veh
    }
    
    func jsonConvertService(json:JSON) -> Service {
        let ser:Service=Service()
        let serviceIconString = json[keyJson.F_SERVICE_ICON].stringValue
        let data: NSData = NSData(base64Encoded: serviceIconString , options: .ignoreUnknownCharacters)!
        ser.serviceIcon = UIImage(data: data as Data)
        ser.serviceCode = json[keyJson.F_SERVICE_CODE].stringValue
        ser.price = json[keyJson.F_PRICE].intValue
        ser.serviceName = json[keyJson.F_SERVICE_NAME].stringValue
        let maiIconString = json[keyJson.F_MAP_ICON].stringValue
        let data1: NSData = NSData(base64Encoded: maiIconString , options: .ignoreUnknownCharacters)!
        ser.mapIcon = UIImage(data: data1 as Data)
        ser.toPrice = json[keyJson.F_TO_PRICE].intValue
        ser.fromPrice = json[keyJson.F_FROM_PRICE].intValue
        ser.waitFee = json[keyJson.F_WAIT_FEE].intValue
        ser.returnRate = json[keyJson.F_RETURN_RATE].intValue
        ser.toNormalPrice = json[keyJson.F_TO_NORMAL_PRICE].intValue
        ser.fromNormalPrice = json[keyJson.F_FROM_NORMAL_PRICE].intValue
        ser.peakRate = json[keyJson.F_PEAK_RATE].doubleValue
        ser.peakHour = json[keyJson.F_PEAK_HOUR].boolValue
        return ser
    }
    
    func jsonConvertServicePrice(json:JSON) -> Service {
        let ser:Service=Service()
        ser.serviceCode = json[keyJson.F_SERVICE_CODE].stringValue
        ser.price = json[keyJson.F_PRICE].intValue
        ser.serviceName = json[keyJson.F_SERVICE_NAME].stringValue
        ser.toPrice = json[keyJson.F_TO_PRICE].intValue
        ser.fromPrice = json[keyJson.F_FROM_PRICE].intValue
        ser.waitFee = json[keyJson.F_WAIT_FEE].intValue
        ser.returnRate = json[keyJson.F_RETURN_RATE].intValue
        ser.toNormalPrice = json[keyJson.F_TO_NORMAL_PRICE].intValue
        ser.fromNormalPrice = json[keyJson.F_FROM_NORMAL_PRICE].intValue
        ser.referServiceCode = json[keyJson.F_REFER_SERVICE_CODE].stringValue
        ser.peakRate = json[keyJson.F_PEAK_RATE].doubleValue
        ser.peakHour = json[keyJson.F_PEAK_HOUR].boolValue
        ser.ecoPrice = json[keyJson.F_ECO_PRICE].intValue
        return ser
    }
    
    func jsonConvertPoint(json:JSON) -> Point {
        let p:Point=Point()
        p.x = json[keyJson.F_X].doubleValue
        p.y = json[keyJson.F_Y].doubleValue
        return p
    }
    
    func jsonConvertReasons(json:JSON) -> CancelReasons {
        let r:CancelReasons=CancelReasons()
        r.CancelCode = json[keyJson.F_CANCEL_CODE].stringValue
        r.CancelNote = json[keyJson.F_CANCEL_NOTE].stringValue
        return r
    }
    
    func jsonConvertChatTemplates(json:JSON) -> ChatTemplates {
        let c:ChatTemplates=ChatTemplates()
        c.description = json[keyJson.F_CHAT_DESCRIPTION].stringValue
        c.chatCode = json[keyJson.F_CHAT_CODE].stringValue
        return c
    }
    
    func jsonCovertAddress(json:JSON) -> Address {
        let adds:Address = Address()
        adds.addressID = json[keyJson.F_ADDRESSID].stringValue
        adds.fullName = json[keyJson.F_NAME_ADDRESS].stringValue
        adds.fullAddress = json[keyJson.F_ADDRESS_ADDRESS].stringValue
        adds.x = json[keyJson.F_X].doubleValue
        adds.y = json[keyJson.F_Y].doubleValue
        
        return adds
    }
    
    func jsonConvertBlockRoads(json:JSON) -> BlockRoads {
        let item:BlockRoads=BlockRoads()
        item.description = json[keyJson.F_DESCRIPTION].stringValue
        item.wayPoints = json[keyJson.F_WAY_POINT].stringValue
        return item
    }
    
    func jsonConvertGooglePlace(json:JSON) -> GooglePlace {
        let place:GooglePlace = GooglePlace()
        place.placeID = json[keyJson.F_PLACE_ID].stringValue
        place.mainText = json[keyJson.F_MAIN_TEXT].stringValue
        place.description = json[keyJson.F_DESCRIPTION].stringValue
        place.secondaryText = json[keyJson.F_SECONDARY_TEXT].stringValue
        return place
    }
    
    func jsonConvertHistories(json:JSON) -> Histories {
        let history = Histories()
        history.addressID = json[keyJson.F_ADDRESSID].stringValue
        history.fullName = json[keyJson.F_NAME_ADDRESS].stringValue
        history.fullAddress = json[keyJson.F_ADDRESS_ADDRESS].stringValue
        history.x = json[keyJson.F_X].doubleValue
        history.y = json[keyJson.F_Y].doubleValue
        return history
    }
    
    func jsonConvertNotifications(json :JSON) -> Notifications {
        let notify = Notifications()
        notify.content = json[keyJson.F_CONTENT].stringValue
        notify.title = json[keyJson.F_TITLE].stringValue
        notify.accountID = json[keyJson.F_ACCOUNT_ID].stringValue
        notify.read = json[keyJson.F_READ].boolValue
        notify.notifyType = json[keyJson.F_NOTI_TYPE].stringValue
        notify.notifyID = json[keyJson.F_NOTI_ID].stringValue
        var date:String? = nil
        date = json[keyJson.F_CREATE_DATE].stringValue
        if(date != ""){
            notify.createDate = convertDatetimeByDateformat(datetime: date!, dateFormat: "dd/MM/yyyy", value: 3)
        }
        notify.url = json[keyJson.F_URL].stringValue
        return notify
    }
    
    func jsonConvertChatMessage(json :JSON) -> ChatMessage {
        let message = ChatMessage()
        message.content = json[keyJson.F_CONTENT].stringValue
        message.driverID = json[keyJson.F_DRIVER_ID].stringValue
        message.driverName = json[keyJson.F_DRIVER_NAME].stringValue
        message.passengerID = json[keyJson.F_PASSENGER_ID].stringValue
        message.passengerName = json[keyJson.F_PASSENGER_NAME].stringValue
        message.fromDriver = json[keyJson.F_FROM_DRIVER].boolValue
        var date:String? = nil
        date = json[keyJson.F_CREATE_DATE].stringValue
        if(date != ""){
            message.createDate = convertDatetimeByDateformat(datetime: date!, dateFormat: "dd/MM/yyyy HH:mm", value: 2)
        }
        return message
    }
    
    func jsonConvertRoutes(json:JSON) -> DistanceRoute {
        
        let rou = DistanceRoute()
        let ds = json["legs"].array
        for d in ds!{
            rou.Distance = d["distance"]["value"].intValue
            rou.Duration = d["duration"]["value"].intValue
        }
        rou.Routes = json["legs"].array
        rou.Polyline = json["overview_polyline"].dictionary
        return rou
    }
    
    
    func convertDatetimeByDateformat(datetime: String, dateFormat: String, value:Int ) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss+SSSS"
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        let datek = dateFormatter.date(from: datetime)
        
        dateFormatter.dateFormat = dateFormat
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        if(datek != nil){
            var timeStamp = dateFormatter.string(from: datek!)
            if (timeStamp.range(of: "AM") != nil && value == 2)
            {
                timeStamp = timeStamp.replacingOccurrences(of: "AM", with: "")
            }
            if (timeStamp.range(of: "AM") != nil && value == 3)
            {
                timeStamp = timeStamp.replacingOccurrences(of: "AM", with: "")
            }
            return timeStamp
        }else{
            let date = datetime.substring(to: 10)
            if value == 1 {
                let timefull = datetime.substring(from: 11)
                let time = timefull.substring(to: 8)
                let dateFormat = DateFormatter()
                dateFormat.dateFormat = "yyyy-MM-dd"//this your string date format
                let date12 = dateFormat.date(from: date)
                dateFormat.dateFormat = "dd/MM/yyyy"
                var timeStamp = dateFormat.string(from: date12!)
                timeStamp = timeStamp + " " + time
                return timeStamp
            }else if value == 2 {
                let timefull = datetime.substring(from: 11)
                let time = timefull.substring(to: 5)
                let dateFormat = DateFormatter()
                dateFormat.dateFormat = "yyyy-MM-dd"//this your string date format
                let date12 = dateFormat.date(from: date)
                dateFormat.dateFormat = "dd/MM/yyyy"
                var timeStamp = dateFormat.string(from: date12!)
                timeStamp = timeStamp + " " + time
                return timeStamp
            }else if value == 3 {
                let timefull = datetime.substring(from: 11)
                let time = timefull.substring(to: 5)
                let dateFormat = DateFormatter()
                dateFormat.dateFormat = "yyyy-MM-dd"//this your string date format
                let date12 = dateFormat.date(from: date)
                dateFormat.dateFormat = "dd/MM/yyyy"
                var timeStamp = dateFormat.string(from: date12!)
                timeStamp = time + " " + timeStamp
                return timeStamp
            }else{
                let dateFormat = DateFormatter()
                dateFormat.dateFormat = "yyyy-MM-dd"//this your string date format
                let date12 = dateFormat.date(from: date)
                dateFormat.dateFormat = "dd/MM/yyyy"
                let timeStamp = dateFormat.string(from: date12!)
                return timeStamp
            }
        }
    }
    
    // update 14/11/2018 transactions
    func jsonConvertTransactions(jsons:JSON) -> Transactions {
        let transactions = Transactions()
        transactions.driverID = jsons[keyJson.F_DRIVER_ID].stringValue
        transactions._id = jsons[keyJson.F_ID].stringValue
        transactions.note = jsons[keyJson.F_NOTE].stringValue
        transactions.amount = jsons[keyJson.F_AMOUNT].stringValue
        transactions.transType = jsons[keyJson.F_TRANS_TYPE].stringValue
        transactions.bookingID = jsons[keyJson.F_BOOKING_ID].stringValue
        transactions.createDate = jsons[keyJson.F_CREATE_DATE].stringValue
        transactions.accountType = jsons[keyJson.F_ACCOUNT_TYPE].stringValue
        transactions.transDate = jsons[keyJson.F_TRANS_DATE].stringValue
        var date:String? = nil
        date = jsons[keyJson.F_TRANS_DATE].stringValue
        if(date != ""){
            //book?.bookDate = convertDateFormatter(date: date!)
            transactions.transDate = convertDatetimeByDateformat(datetime: date!, dateFormat: "dd/MM/yyyy HH:mm:ss", value: 1)
        }
        return transactions
    }
    
    // update 22/03/2019 BusTrip
    func jsonConvertBusTrips(jsons:JSON) -> BusTrip {
        let busTrip = BusTrip()
        busTrip.busOperator = jsons[keyJson.F_BUS_OPERATOR].stringValue
        busTrip.numOfBooked = jsons[keyJson.F_NUMBER_OF_BOOKED].intValue
        busTrip.plateNumber = jsons[keyJson.F_PLATE_NUMBER].stringValue
        busTrip.modelName = jsons[keyJson.F_MODEL_NAME].stringValue
        busTrip.ticketFare = jsons[keyJson.F_TICKET_FARE].intValue
        busTrip.tripID = jsons[keyJson.F_TRIP_ID].stringValue
        busTrip.capacity = jsons[keyJson.F_CAPACITY].intValue
        
        
        busTrip.fromAddress = jsons[keyJson.F_FROM_ADDRESS].stringValue
        busTrip.toAddress = jsons[keyJson.F_TO_ADDRESS].stringValue
        busTrip.pickupAddress = jsons[keyJson.F_PICKUP_ADDRESS].stringValue
        busTrip.dropOffAddress = jsons[keyJson.F_DROP_OFF_ADDRESS].stringValue
        
        busTrip.pickupX = jsons[keyJson.F_PICKUP_X].doubleValue
        busTrip.pickupY = jsons[keyJson.F_PICKUP_Y].doubleValue
        busTrip.dropOffX = jsons[keyJson.F_DROP_OFF_X].doubleValue
        busTrip.dropOffY = jsons[keyJson.F_DROP_OFF_Y].doubleValue
        busTrip.paid = jsons[keyJson.F_PAID].boolValue
        busTrip.stateNote = jsons[keyJson.F_STATE_NOTE].stringValue
        
        busTrip.quantity1 = jsons[keyJson.F_QUANTITY_1].intValue
        busTrip.quantity2 = jsons[keyJson.F_QUANTITY_2].intValue
        busTrip.quantity3 = jsons[keyJson.F_QUANTITY_3].intValue
        busTrip.ticketFare1 = jsons[keyJson.F_TICKET_FARE_1].intValue
        busTrip.ticketFare2 = jsons[keyJson.F_TICKET_FARE_2].intValue
        busTrip.ticketFare3 = jsons[keyJson.F_TICKET_FARE_3].intValue
        busTrip.vehicleID = jsons[keyJson.F_VEHICLE_ID].intValue
        busTrip.hotLine = jsons[keyJson.F_HOTLINE].stringValue
        busTrip.contactName = jsons[keyJson.F_CONTACT_NAME].stringValue
        
        busTrip.geom = jsons[keyJson.F_GEOM].stringValue
        var date:String? = nil
        date = jsons[keyJson.F_DEPARTURE_DATE].stringValue
        if(date != ""){
            //book?.bookDate = convertDateFormatter(date: date!)
            busTrip.departureDate = convertDatetimeByDateformat(datetime: date!, dateFormat: "HH:mm dd/MM/yyyy", value: 3)
        }
        var createdate:String? = nil
        createdate = jsons[keyJson.F_CREATE_DATE].stringValue
        if(createdate != ""){
            //book?.bookDate = convertDateFormatter(date: date!)
            busTrip.createDate = convertDatetimeByDateformat(datetime: date!, dateFormat: "HH:mm dd/MM/yyyy", value: 3)
        }
        return busTrip
    }
    
    func jsonConvertTicketFares(jsons:JSON) -> TicketFares {
        let ticket = TicketFares()
        ticket.description = jsons[keyJson.F_DESCRIPTION].stringValue
        ticket.note = jsons[keyJson.F_NOTE].stringValue
        ticket.ticketID = jsons[keyJson.F_TICKET_ID].intValue
        ticket.capacity = jsons[keyJson.F_CAPACITY].intValue
        ticket.ticketFare = jsons[keyJson.F_TICKET_FARE].intValue
        
        return ticket
    }
    
    func jsonConvertBusPlace(json:JSON) -> Place {
        let place:Place = Place()
        place.placeID = json[keyJson.F_PLACE_ID].intValue
        place.x = json[keyJson.F_X].doubleValue
        place.description = json[keyJson.F_DESCRIPTION].stringValue
        place.y = json[keyJson.F_Y].doubleValue
        var date:String? = nil
        date = json[keyJson.F_TIME].stringValue
        if(date != ""){
            //book?.bookDate = convertDateFormatter(date: date!)
            place.time = convertDatetimeByDateformat(datetime: date!, dateFormat: "dd/MM/yyyy HH:mm", value: 2)
        }
        return place
    }
    
    //update 13/06/2019
    func jsonConvertHomeAds(json:JSON) -> HomeAds {
        let ads:HomeAds=HomeAds()
        ads.imageUrl = json[keyJson.F_IMAGE_URL].stringValue
        ads.linkUrl = json[keyJson.F_LINK_URL].stringValue
        return ads
    }
    //update 24/06/2019
    func jsonConvertAppItem(json:JSON) -> AppItem {
        let item:AppItem=AppItem()
        item.appCode = json[keyJson.F_APP_CODE].stringValue
        return item
    }
    
    //update 05/07/2019
    func jsonConvertGame(json:JSON) -> LuckyGame {
        let game:LuckyGame=LuckyGame()
        var imgLink:String? = nil
        imgLink = json[keyJson.F_IMAGE_URL].stringValue
        if (imgLink!.range(of: "http") != nil){
            game.imageUrl = imgLink
        }else{
            game.imageUrl = Constant.URL_ROOT + imgLink!
        }
        game.gameID = json[keyJson.F_GAME_ID].intValue
        var fromDate:String? = nil
        fromDate = json[keyJson.F_FROM_DATE].stringValue
        if(fromDate != ""){
            game.fromDate = convertDatetimeByDateformat(datetime: fromDate!, dateFormat: "dd/MM/yyyy", value: 0)
        }
        var toDate:String? = nil
        toDate = json[keyJson.F_TO_DATE].stringValue
        if(toDate != ""){
            game.toDate = convertDatetimeByDateformat(datetime: toDate!, dateFormat: "dd/MM/yyyy", value: 0)
        }
        game.gameTitle = json[keyJson.F_TITLE].stringValue
        game.gameDescription = json[keyJson.F_DESCRIPTION].stringValue
        game.lotteryDate = json[keyJson.F_LOTTERY_DATE].stringValue
        game.createDate = json[keyJson.F_CREATE_DATE].stringValue
        game.isAvailable = json[keyJson.F_AVAILABLE].boolValue
        game.luckyNo = json[keyJson.F_LUCKY_NO].stringValue
        game.answer = json[keyJson.F_ANSWER].stringValue
        game.win = json[keyJson.F_WIN].boolValue
        if let arrAsw = json[keyJson.F_ANSWERS].array {
            for item in arrAsw  {
                if  item is JSON{
                    let asw :Answers = jsonConvertAnswer(json: item)
                    game.arrAnswers.append(asw)
                }
            }
        }
        return game
    }
    
    func jsonConvertAnswer(json:JSON) -> Answers {
        let answer:Answers=Answers()
        answer.answer = json[keyJson.F_ANSWER].stringValue
        answer.gameID = json[keyJson.F_GAME_ID].intValue
        var fromDate:String? = nil
        fromDate = json[keyJson.F_UPDATE_DATE].stringValue
        if(fromDate != ""){
            answer.updateDate = convertDatetimeByDateformat(datetime: fromDate!, dateFormat: "dd/MM/yyyy HH:mm", value: 2)
        }
        answer.luckyNo = json[keyJson.F_LUCKY_NO].stringValue
        answer.fullName = json[keyJson.F_FULL_NAME].stringValue
        answer.mobileNo = json[keyJson.F_MOBILE_NO].stringValue
        return answer
    }
  }
  
