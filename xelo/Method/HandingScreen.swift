//
//  protocoll.swift
//  NaviGo
//
//  Created by HoangManh on 9/28/17.
//  Copyright © 2017 DUMV. All rights reserved.
//

import Foundation
protocol HandingScreen {
    func errorNoInternet()
    func errorConnect()
    func errorServerSend(errorCode :String ,errorToast:String)
    func startRequest()
    func endRequest()
}
